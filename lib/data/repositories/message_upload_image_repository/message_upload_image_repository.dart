import 'dart:io';

import 'package:home_food/data/repositories/upload_image_repository/upload_image_repository.dart';
import 'package:home_food/data/services/message_upload_photos/message_upload_photos.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:injectable/injectable.dart';

abstract class MessageUploadPhotosRepository {
  Future<List<UploadedImage>> messageUploadPhotos(List<File> photos);
}

@Injectable(as: MessageUploadPhotosRepository)
class MessageUploadPhotosRepositoryImpl extends MessageUploadPhotosRepository {
  final MessageUploadPhotosService _messageUploadPhotosService;

  MessageUploadPhotosRepositoryImpl(this._messageUploadPhotosService);

  @override
  Future<List<UploadedImage>> messageUploadPhotos(List<File> photos) =>
      _messageUploadPhotosService.messageUploadPhotos(photos);


}