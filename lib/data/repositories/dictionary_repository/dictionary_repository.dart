import 'package:home_food/data/services/dictionary_service/dictionary_service.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/ingredient.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/district/district.dart';
import 'package:injectable/injectable.dart';

abstract class DictionaryRepository {
  Future<List<City>> getCities();

  Future<List<DishType>> getDishTypes();

  Future<List<Preference>> getPreferences();

  Future<List<Ingredient>> getIngredientList();

  Future<List<District>> getDistrictList();
}

@Injectable(as: DictionaryRepository)
class DictionaryRepositoryImpl extends DictionaryRepository {
  final DictionaryService _dictionaryService;

  DictionaryRepositoryImpl(this._dictionaryService);

  @override
  Future<List<City>> getCities() => _dictionaryService.getCities();

  @override
  Future<List<DishType>> getDishTypes() => _dictionaryService.getDishTypes();

  @override
  Future<List<Preference>> getPreferences() =>
      _dictionaryService.getPreferences();

  @override
  Future<List<Ingredient>> getIngredientList() =>
      _dictionaryService.getIngredientList();

  @override
  Future<List<District>> getDistrictList() =>
      _dictionaryService.getDistrictList();
}
