import 'package:home_food/data/services/favorite_service/favourite_service.dart';
import 'package:home_food/models/favourite_dish/favourite_dish.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:injectable/injectable.dart';

abstract class FavoriteRepository {
  Future<Pagination<FavouriteDish>> getFavouriteDishes({
    int? page = 0,
    int? size = 10,
  });

  Future<String> deleteFavouriteDishById({
    required String favDishId,
  });

  Future<List<String>> deleteAllFavouriteDishes();
}
@Injectable(as: FavoriteRepository)
class FavoriteRepositoryImpl extends FavoriteRepository {
  final FavoriteService _favoriteService;

  FavoriteRepositoryImpl(this._favoriteService);

  @override
  Future<Pagination<FavouriteDish>> getFavouriteDishes({
    int? page = 0,
    int? size = 10,
  }) =>
      _favoriteService.getFavouriteDishes(
        page: page,
        size: size,
      );

  @override
  Future<String> deleteFavouriteDishById({
    required String favDishId,
  }) =>
      _favoriteService.deleteFavouriteDishById(
        favDishId: favDishId,
      );

  @override
  Future<List<String>> deleteAllFavouriteDishes() =>
      _favoriteService.deleteAllFavouriteDishes(
      );
}
