import 'package:home_food/data/services/visitor_service/visitor_service.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/grade/grade.dart';
import 'package:injectable/injectable.dart';

abstract class VisitorRepository {
  /// Получение списка предпочтений пользователя
  Future<List<Preference>> getUserPreferences();

  /// Изменить список предпочтений пользователя
  Future<List<String>> updateUserPreferences({
    required List<Preference> list,
  });

  /// Изменить токен файрбэйз
  Future<String> uploadFirebaseToken({
    required String firebaseToken,
  });

  Future<List<Grade>> getMerchantGrades({
    required String merchantId,
    int page = 0,
    int limit = 10,
  });
}

@Injectable(as: VisitorRepository)
class VisitorRepositoryImpl extends VisitorRepository {
  final VisitorService visitorService;

  VisitorRepositoryImpl(this.visitorService);

  /// Получение списка предпочтений пользователя
  @override
  Future<List<Preference>> getUserPreferences() =>
      visitorService.getUserPreferences();

  /// Изменить список предпочтений пользователя
  @override
  Future<List<String>> updateUserPreferences({
    required List<Preference> list,
  }) =>
      visitorService.updateUserPreferences(
        list: list,
      );

  /// Изменить токен файрбэйз
  @override
  Future<String> uploadFirebaseToken({
    required String firebaseToken,
  }) =>
      visitorService.uploadFirebaseToken(
        firebaseToken: firebaseToken,
      );

  @override
  Future<List<Grade>> getMerchantGrades({
    required String merchantId,
    int page = 0,
    int limit = 10,
  }) =>
      visitorService.getMerchantGrades(
        merchantId: merchantId,
      );
}
