import 'package:home_food/data/services/order_service/order_service.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:home_food/models/statistics/statistics.dart';
import 'package:home_food/models/statistics/top_dish_statistics.dart';
import 'package:injectable/injectable.dart';

abstract class OrderRepository {
  /// Получить список заказов покупателя [UserType.CUSTOMER]
  Future<Pagination<OrderModel>> getCustomerOrders({
    int? page = 0,
    int? size = 10,
    String? status,
    String? searchText,
  });

  /// Получить список заказов мерчанта [UserType.MANUFACTURER]
  Future<Pagination<OrderModel>> getMerchantOrders({
    int? page = 0,
    int? size = 10,
    String? status,
    String? searchText,
  });

  /// Изменение данных в корзине
  /// [OrderDTO] моделька данных на изменение
  Future<OrderModel> updatePreOrder({
    required OrderModel body,
  });

  /// Получить данные о корзине
  Future<OrderModel> getPreOrder();

  /// Подтвердить корзину в заказ
  Future<void> createOrder();

  /// Изменение количества блюда в корзине
  /// [positionId] айди позиции блюда в корзине
  /// [amount] количество
  Future<String> updateDishCardOrder({
    required String positionId,
    required int amount,
  });

  /// Удалить одно блюдо из корзины
  /// [positionId] позиция блюда в корзине
  Future<String> deleteDishFromBasket({
    required String positionId,
  });

  /// Добавление блюда в корзину
  /// [dishCardId] айди блюда
  /// [amount] количество
  Future<String> putDishToPreorder({
    required String dishCardId,
    required int amount,
  });

  /// Удалить все блюдо из корзины
  Future<String> removeAllDishCardOrder();

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDERED]
  /// [orderId] айди заказа
  Future<String> changeOrderStatusToAccepted({
    required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDER_ACCEPTED]
  /// [orderId] айди заказа
  Future<String> changeOrderStatusToPreparation({
    required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.PREPARATION]
  /// [orderId] айди заказа
  Future<String> changeOrderStatusToDelivery({
    required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.DELIVERY]
  /// [orderId] айди заказа
  Future<String> changeOrderStatusToDelivered({
    required String orderId,
  });

  /// Получить заказ по айди
  /// [orderId] айди заказа
  Future<OrderModel> getOrderById({
    required String orderId,
  });

  ///Возврат/Жалоба к заказу
  /// [orderId] айди заказа
  Future<String> complaintOrder({
    required String orderId,
    required String comment,
    required String base64Image,
  });

  /// Получить статистику по продажам
  Future<Statistics> getSellStatistics({
    required String startDate,
    required String endDate,
    required String period,
    int? month,
    int? year,
  });

  /// Получить статистику по топ блюдам
  Future<List<TopDishStatistics>> getTopDishStatistics({
    required String startDate,
    required String endDate,
  });
}

@Injectable(as: OrderRepository)
class OrderRepositoryImpl extends OrderRepository {
  final OrderService _orderService;

  OrderRepositoryImpl(this._orderService);

  /// Изменение данных в корзине
  /// [OrderDTO] моделька данных на изменение
  @override
  Future<OrderModel> updatePreOrder({required OrderModel body}) =>
      _orderService.updatePreOrder(body: body);

  /// Получить данные о корзине
  @override
  Future<OrderModel> getPreOrder() => _orderService.getPreOrder();

  /// Изменение количества блюда в корзине
  /// [positionId] айди позиции блюда в корзине
  /// [amount] количество
  @override
  Future<String> updateDishCardOrder({
    required String positionId,
    required int amount,
  }) =>
      _orderService.updateDishCardOrder(
        positionId: positionId,
        amount: amount,
      );

  /// Подтвердить корзину в заказ
  @override
  Future<void> createOrder() => _orderService.createOrder();

  /// Получить список заказов покупателя [UserType.CUSTOMER]
  @override
  Future<Pagination<OrderModel>> getCustomerOrders({
    int? page = 0,
    int? size = 10,
    String? status,
    String? searchText,
  }) =>
      _orderService.getCustomerOrders(
        page: page,
        size: size,
        status: status,
        searchText: searchText,
      );

  /// Получить список заказов мерчанта [UserType.MANUFACTURER]
  @override
  Future<Pagination<OrderModel>> getMerchantOrders({
    int? page = 0,
    int? size = 10,
    String? status,
    String? searchText,
  }) =>
      _orderService.getMerchantOrders(
        page: page,
        size: size,
        status: status,
        searchText: searchText,
      );

  /// Удалить одно блюдо из корзины
  /// [positionId] позиция блюда в корзине
  @override
  Future<String> deleteDishFromBasket({
    required String positionId,
  }) =>
      _orderService.deleteDishFromBasket(
        positionId: positionId,
      );

  /// Добавление блюда в корзину
  /// [dishCardId] айди блюда
  /// [amount] количество
  @override
  Future<String> putDishToPreorder({
    required String dishCardId,
    required int amount,
  }) =>
      _orderService.putDishToPreorder(
        dishCardId: dishCardId,
        amount: amount,
      );

  /// Удалить все блюдо из корзины
  @override
  Future<String> removeAllDishCardOrder() =>
      _orderService.removeAllDishCardOrder();

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDERED]
  /// [orderId] айди заказа
  @override
  Future<String> changeOrderStatusToAccepted({
    required String orderId,
  }) =>
      _orderService.changeOrderStatusToAccepted(
        orderId: orderId,
      );

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDER_ACCEPTED]
  /// [orderId] айди заказа
  @override
  Future<String> changeOrderStatusToPreparation({
    required String orderId,
  }) =>
      _orderService.changeOrderStatusToPreparation(
        orderId: orderId,
      );

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.PREPARATION]
  /// [orderId] айди заказа
  @override
  Future<String> changeOrderStatusToDelivery({
    required String orderId,
  }) =>
      _orderService.changeOrderStatusToDelivery(
        orderId: orderId,
      );

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.DELIVERY]
  /// [orderId] айди заказа
  @override
  Future<String> changeOrderStatusToDelivered({
    required String orderId,
  }) =>
      _orderService.changeOrderStatusToDelivered(
        orderId: orderId,
      );

  /// Получить заказ по айди
  /// [orderId] айди заказа
  @override
  Future<OrderModel> getOrderById({
    required String orderId,
  }) =>
      _orderService.getOrderById(
        orderId: orderId,
      );

  /// Получить статистику по продажам
  @override
  Future<Statistics> getSellStatistics({
    required String startDate,
    required String endDate,
    required String period,
    int? month,
    int? year,
  }) =>
      _orderService.getSellStatistics(
        startDate: startDate,
        endDate: endDate,
        period: period,
        month: month,
        year: year,
      );

  /// Получить статистику по топ блюдам
  @override
  Future<List<TopDishStatistics>> getTopDishStatistics({
    required String startDate,
    required String endDate,
  }) =>
      _orderService.getTopDishStatistics(
        startDate: startDate,
        endDate: endDate,
      );

  @override
  Future<String> complaintOrder({
    required String orderId,
    required String comment,
    required String base64Image,
  }) =>
      _orderService.complaintOrder(
        orderId: orderId,
        comment: comment,
        base64Image: base64Image,
      );
}
