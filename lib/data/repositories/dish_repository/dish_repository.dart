import 'package:home_food/data/dto/dish_filter_request_dto/dish_filter_request_dto.dart';
import 'package:home_food/data/dto/edit_food_request/edit_dish_request.dart';
import 'package:home_food/data/services/dish_service/dish_service.dart';
import 'package:home_food/models/dish/create_dish_request.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/dish_list_filter_request/dish_list_filter_request.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:injectable/injectable.dart';

abstract class DishRepository {
  Future<dynamic> addNewDish(CreateFoodRequest body);

  Future<dynamic> editDish(EditFoodRequest body);

  Future<Pagination<Dish>> getPopularDishList({
    int page = 0,
    int size = 10,
  });

  Future<Pagination<Dish>> getDishes({
    int page = 0,
    int size = 10,
    required DishFilterRequestDTO filter,
  });

  Future<int> getDishesCount(
    DishFilterRequest filter,
  );

  Future<Pagination<Dish>> getMyDishes({
    int page = 0,
    int size = 10,
    required DishFilterRequestDTO filter,
  });

  Future<void> changeStatusActivate({
    required String dishCardId,
  });

  Future<void> changeStatusModerate({
    required String dishCardId,
  });

  Future<void> changeStatusDeactive({
    required String dishCardId,
  });

  Future<Dish> getDishById({
    required String dishId,
  });
}

@Injectable(as: DishRepository)
class DishRepositoryImpl extends DishRepository {
  final DishService _dishService;

  DishRepositoryImpl(this._dishService);

  @override
  Future addNewDish(body) => _dishService.addNewDish(body);

  @override
  Future editDish(EditFoodRequest body) => _dishService.editDish(body);

  @override
  Future<Pagination<Dish>> getPopularDishList({
    int page = 0,
    int size = 10,
  }) =>
      _dishService.getPopularDishList(
        page: page,
        size: size,
      );

  @override
  Future<Pagination<Dish>> getDishes({
    int page = 0,
    int size = 10,
    required DishFilterRequestDTO filter,
  }) =>
      _dishService.getDishes(
        filter: filter,
        page: page,
        size: size,
      );

  @override
  Future<int> getDishesCount(DishFilterRequest filter) =>
      _dishService.getDishesCount(
        filter,
      );

  @override
  Future<Pagination<Dish>> getMyDishes({
    int page = 0,
    int size = 10,
    required DishFilterRequestDTO filter,
  }) =>
      _dishService.getMyDishes(
        page: page,
        size: size,
        filter: filter,
      );

  @override
  Future<void> changeStatusModerate({
    required String dishCardId,
  }) =>
      _dishService.changeStatusModerate(
        dishCardId: dishCardId,
      );

  @override
  Future<void> changeStatusDeactive({
    required String dishCardId,
  }) =>
      _dishService.changeStatusDeactivate(
        dishCardId: dishCardId,
      );

  @override
  Future<void> changeStatusActivate({required String dishCardId}) =>
      _dishService.changeStatusActivate(
        dishCardId: dishCardId,
      );

  @override
  Future<Dish> getDishById({
    required String dishId,
  }) =>
      _dishService.getDishById(
        dishId: dishId,
      );
}
