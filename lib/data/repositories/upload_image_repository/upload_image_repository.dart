import 'dart:io';

import 'package:home_food/data/services/upload_image_service/upload_image_service.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:injectable/injectable.dart';

abstract class UploadImageRepository {
  Future<List<UploadedImage>> uploadPhotos(List<File> photos);
}

@Injectable(as: UploadImageRepository)
class UploadImageRepositoryImpl extends UploadImageRepository {
  final UploadImageService _uploadImageService;

  UploadImageRepositoryImpl(this._uploadImageService);

  @override
  Future<List<UploadedImage>> uploadPhotos(List<File> photos) =>
      _uploadImageService.uploadPhotos(photos);
}
