import 'package:home_food/data/services/auth_service/auth_service.dart';
import 'package:home_food/models/generated_pass/generated_pass.dart';
import 'package:home_food/models/register_activate/register_activate.dart';
import 'package:home_food/models/register_user/register_user.dart';
import 'package:injectable/injectable.dart';

abstract class AuthRepository {
  Future<String> register(UserRegisterRequest body);

  Future<GeneratedPass> registerActivate(RegisterActivate body);

  Future<String> login(String phone);
}

@Injectable(as: AuthRepository)
class AuthRepositoryImpl extends AuthRepository {
  final AuthService _authService;

  AuthRepositoryImpl(this._authService);

  @override
  Future<String> register(UserRegisterRequest body) =>
      _authService.register(body);

  @override
  Future<String> login(String phone) => _authService.login(phone);

  @override
  Future<GeneratedPass> registerActivate(RegisterActivate body) =>
      _authService.registerActivate(body);
}
