import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/data/services/address_service/address_service.dart';
import 'package:home_food/models/address/address.dart';
import 'package:injectable/injectable.dart';

abstract class AddressRepository {
  Future<List<Address>> getMyAddresses();

  Future<String> addNewAddress(AddressDTO addressDTO);

  Future<String> updateAddress(AddressDTO addressDTO);

  Future<String> deleteAddress(
    String addressId,
  );
}

@Injectable(as: AddressRepository)
class AddressRepositoryImpl extends AddressRepository {
  final AddressService _addressService;

  AddressRepositoryImpl(this._addressService);

  @override
  Future<String> addNewAddress(AddressDTO addressDTO) =>
      _addressService.addNewAddress(addressDTO);

  @override
  Future<String> updateAddress(AddressDTO addressDTO) =>
      _addressService.updateAddress(addressDTO);

  @override
  Future<String> deleteAddress(String addressId) =>
      _addressService.deleteAddress(addressId);

  @override
  Future<List<Address>> getMyAddresses() => _addressService.getMyAddresses();
}
