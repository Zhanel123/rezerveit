import 'package:home_food/data/services/chat_service/chat_service.dart';
import 'package:home_food/models/chat/chat.dart';
import 'package:home_food/models/chat/chat_message.dart';
import 'package:home_food/models/chat/chat_search_result_model.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:injectable/injectable.dart';

abstract class ChatRepository {
  /// Получение списка чатов
  Future<List<ChatModel>> getChatList();

  /// Получение списка сообщений чата
  Future<List<ChatMessageModel>> getChatMessages({
    required String chatId,
  });

  /// Получение чата по заказу
  Future<String> getChatByOrderId({
    required String orderId,
    required String manufacturerId,
  });

  /// Отправить сообщение
  Future<String> sendMessage({
    required String chatId,
    required String text,
    List<UploadedImage> photoItems = const [],
  });

  ///Поиск сообщения
  Future<ChatSearchResultModel> searchByAllChat({
    required String text,
    required bool isManufacturer,
  });
}

@Injectable(as: ChatRepository)
class ChatRepositoryImpl extends ChatRepository {
  final ChatService chatService;

  ChatRepositoryImpl(this.chatService);

  /// Получение списка чатов
  @override
  Future<List<ChatModel>> getChatList() async => chatService.getChatList();

  /// Получение списка сообщений чата
  @override
  Future<List<ChatMessageModel>> getChatMessages({
    required String chatId,
  }) =>
      chatService.getChatMessages(
        chatId: chatId,
      );

  /// Получение чата по заказу
  @override
  Future<String> getChatByOrderId({
    required String orderId,
    required String manufacturerId,
  }) =>
      chatService.getChatByOrderId(
        orderId: orderId,
        manufacturerId: manufacturerId,
      );

  @override
  Future<String> sendMessage({
    required String chatId,
    required String text,
    List<UploadedImage> photoItems = const [],
  }) =>
      chatService.sendMessage(
        chatId: chatId,
        text: text,
        photoItems: photoItems,
      );

  @override
  Future<ChatSearchResultModel> searchByAllChat({
    required String text,
    required bool isManufacturer,
  }) =>
      chatService.searchByAllChat(
        text: text,
        isManufacturer: isManufacturer,
      );
}
