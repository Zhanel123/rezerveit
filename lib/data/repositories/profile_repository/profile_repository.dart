import 'package:home_food/data/dto/profile_edit_dto/profile_edit_dto.dart';
import 'package:home_food/data/services/profile_service/profile_service.dart';
import 'package:home_food/models/notification/notification.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:injectable/injectable.dart';

abstract class ProfileRepository {
  Future<Profile> updateProfile({
    required ProfileEditDTO body,
  });

  Future<List<Notification>> getRemoteNotifications();

  Future<void> setWasReaden(List<Notification> notifications);
}

@Injectable(as: ProfileRepository)
class ProfileRepositoryImpl extends ProfileRepository {
  final ProfileService _profileService;

  ProfileRepositoryImpl(
    this._profileService,
  );

  @override
  Future<Profile> updateProfile({
    required ProfileEditDTO body,
  }) =>
      _profileService.updateProfile(body: body);

  @override
  Future<List<Notification>> getRemoteNotifications() =>
      _profileService.getRemoteNotifications();

  @override
  Future<void> setWasReaden(
    List<Notification> notifications,
  ) =>
      _profileService.setWasReaden(
        notifications: notifications,
      );
}
