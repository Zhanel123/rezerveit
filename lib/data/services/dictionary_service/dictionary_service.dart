import 'package:home_food/data/dto/city_dto/mapper/city_dto_mapper.dart';
import 'package:home_food/data/dto/dish_type_dto/mapper/dish_type_dto_mapper.dart';
import 'package:home_food/data/dto/district_dto/mapper/district_mapper.dart';
import 'package:home_food/data/dto/ingredient_dto/mapper/ingredient_dto_mapper.dart';
import 'package:home_food/data/dto/preference_dto/mapper/preference_dto_mapper.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/ingredient.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/district/district.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class DictionaryService {
  Future<List<City>> getCities();

  Future<List<DishType>> getDishTypes();

  Future<List<Preference>> getPreferences();

  Future<List<Ingredient>> getIngredientList();

  Future<List<District>> getDistrictList();
}

@Injectable(as: DictionaryService)
class DictionaryServiceImpl extends DictionaryService {
  final RestClient _restClient;

  DictionaryServiceImpl(
    this._restClient,
  );

  @override
  Future<List<City>> getCities() async =>
      cityDtoListToModel(await _restClient.getCityList());

  @override
  Future<List<DishType>> getDishTypes() async =>
      dishTypeDtoListToModel(await _restClient.getDishTypes());

  @override
  Future<List<Preference>> getPreferences() async =>
      preferenceListMapper(await _restClient.getPreferencesList());

  @override
  Future<List<Ingredient>> getIngredientList() async =>
      ingredientDtoListToModel(await _restClient.getIngredientList() ?? []);

  @override
  Future<List<District>> getDistrictList() async =>
      districtListMapper(await _restClient.getDistrictList());
}
