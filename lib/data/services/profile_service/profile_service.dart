import 'package:home_food/data/dto/notification_dto/mapper/notification_mapper.dart';
import 'package:home_food/data/dto/profile_edit_dto/profile_edit_dto.dart';
import 'package:home_food/models/notification/notification.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class ProfileService {
  Future<Profile> updateProfile({
    required ProfileEditDTO body,
  });

  Future<List<Notification>> getRemoteNotifications({
    int page = 0,
  });

  Future<void> setWasReaden({
    required List<Notification> notifications,
  });
}

@Injectable(as: ProfileService)
class ProfileServiceImpl extends ProfileService {
  final RestClient _restClient;

  ProfileServiceImpl(this._restClient);

  @override
  Future<Profile> updateProfile({
    required ProfileEditDTO body,
  }) =>
      _restClient.updateProfile(
        body: body,
      );

  @override
  Future<List<Notification>> getRemoteNotifications({
    int page = 0,
  }) async {
    final response = await _restClient.getNotificationList(
      page: page,
    );

    if (response.hasNext ?? false) {
      return [
        ...notificationListMapper(response.content),
        ...(await getRemoteNotifications(page: page + 1))
      ];
    }

    return notificationListMapper(response.content);
  }

  @override
  Future<void> setWasReaden({
    required List<Notification> notifications,
  }) async =>
      _restClient.setWasReaden(
        ids: notifications.map((e) => e.id).toList(),
      );
}
