import 'package:home_food/data/dto/user_register_request_dto/mapper/user_register_request_mapper.dart';
import 'package:home_food/models/generated_pass/generated_pass.dart';
import 'package:home_food/models/register_activate/register_activate.dart';
import 'package:home_food/models/register_user/register_user.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class AuthService {
  Future<String> register(UserRegisterRequest body);

  Future<GeneratedPass> registerActivate(RegisterActivate body);

  Future<String> login(String phone);
}

@Injectable(as: AuthService)
class AuthServiceImpl extends AuthService {
  final RestClient _restClient;

  AuthServiceImpl(this._restClient);

  @override
  Future<String> register(UserRegisterRequest body) =>
      _restClient.register(userRegisterRequestMapperDTO(body));

  @override
  Future<String> login(String phone) => _restClient.login(
        phone: phone,
      );

  @override
  Future<GeneratedPass> registerActivate(RegisterActivate body) =>
      _restClient.registerActivate(body);
}
