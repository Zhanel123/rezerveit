import 'package:home_food/data/dto/grade_dto/mapper/grade_mapper.dart';
import 'package:home_food/data/dto/preference_dto/mapper/preference_dto_mapper.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/grade/grade.dart';
import 'package:home_food/network/visitor_api/visitor_api.dart';
import 'package:injectable/injectable.dart';

abstract class VisitorService {
  /// Получение списка предпочтений пользователя
  Future<List<Preference>> getUserPreferences();

  /// Изменить список предпочтений пользователя
  Future<List<String>> updateUserPreferences({
    required List<Preference> list,
  });

  /// Изменить токен файрбэйз
  Future<String> uploadFirebaseToken({
    required String firebaseToken,
  });

  Future<List<Grade>> getMerchantGrades({
    required String merchantId,
    int page = 0,
    int limit = 10,
  });
}

@Injectable(as: VisitorService)
class VisitorServiceImpl extends VisitorService {
  final VisitorApi visitorApi;

  VisitorServiceImpl(this.visitorApi);

  /// Получение списка предпочтений пользователя
  @override
  Future<List<Preference>> getUserPreferences() async =>
      preferenceListMapper(await visitorApi.getUserPreferences());

  /// Изменить список предпочтений пользователя
  @override
  Future<List<String>> updateUserPreferences({
    required List<Preference> list,
  }) =>
      visitorApi.updateUserPreferences(list: preferenceDTOListMapper(list));

  /// Изменить токен файрбэйз
  @override
  Future<String> uploadFirebaseToken({
    required String firebaseToken,
  }) =>
      visitorApi.uploadFirebaseToken(
        firebaseToken: firebaseToken,
      );

  @override
  Future<List<Grade>> getMerchantGrades({
    required String merchantId,
    int page = 0,
    int limit = 10,
  }) async {
    final response = await visitorApi.getMerchantGrades(merchantId: merchantId);

    if (response.hasNext ?? false) {
      return [
        ...gradeListMapper(response.content),
        ...(await getMerchantGrades(
          merchantId: merchantId,
          page: page + 1,
        ))
      ];
    }

    return gradeListMapper(response.content);
  }
}
