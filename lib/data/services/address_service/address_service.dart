import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/data/dto/address_dto/mapper/address_dto_mapper.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class AddressService {
  Future<List<Address>> getMyAddresses();

  Future<String> addNewAddress(AddressDTO addressDTO);

  Future<String> updateAddress(AddressDTO addressDTO);

  Future<String> deleteAddress(String addressId);
}

@Injectable(as: AddressService)
class AddressServiceImpl extends AddressService {
  final RestClient _restClient;

  AddressServiceImpl(this._restClient);

  @override
  Future<String> addNewAddress(AddressDTO addressDTO) async =>
      await _restClient.createAddress(addressDTO);

  @override
  Future<String> updateAddress(AddressDTO addressDTO) async =>
      await _restClient.updateAddress(addressDTO);

  @override
  Future<String> deleteAddress(String addressId) async =>
      _restClient.deleteAddress(addressId: addressId);

  @override
  Future<List<Address>> getMyAddresses() async =>
      addressListMapper(await _restClient.getAddressList());
}
