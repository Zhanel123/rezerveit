import 'package:home_food/data/dto/favorite_dish/mapper/favorite_dish_mapper.dart';
import 'package:home_food/models/favourite_dish/favourite_dish.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class FavoriteService {
  Future<Pagination<FavouriteDish>> getFavouriteDishes({
    int? page = 0,
    int? size = 10,
  });

  Future<String> deleteFavouriteDishById({
    required String favDishId,
  });

  Future<List<String>> deleteAllFavouriteDishes();
}

@Injectable(as: FavoriteService)
class FavoriteServiceImpl extends FavoriteService {
  final RestClient _restClient;

  FavoriteServiceImpl(this._restClient);

  @override
  Future<Pagination<FavouriteDish>> getFavouriteDishes({
    int? page = 0,
    int? size = 10,
  }) async =>
      dishDtoPaginationToModel(
        await _restClient.getFavouriteDishes(
          page: page,
          size: size,
        ),
      );

  @override
  Future<String> deleteFavouriteDishById({required String favDishId}) async =>
      await _restClient.deleteFavouriteDishById(
        favDishId: favDishId,
      );

  @override
  Future<List<String>> deleteAllFavouriteDishes() async =>
      await _restClient.deleteAllFavouriteDishes();
}
