import 'dart:io';

import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/network/chat_api/chat_api.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class MessageUploadPhotosService {
  Future<List<UploadedImage>> messageUploadPhotos(List<File> photos);
}

@Injectable(as: MessageUploadPhotosService)
class MessageUploadPhotosServiceImpl extends MessageUploadPhotosService {
  final ChatApi _chatApi;

  MessageUploadPhotosServiceImpl(this._chatApi);

  @override
  Future<List<UploadedImage>> messageUploadPhotos(List<File> photos) =>
      _chatApi.messageUploadPhotos(photos: photos);
}
