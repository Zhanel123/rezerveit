import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/dish_dto/mapper/dish_dto_mapper.dart';
import 'package:home_food/data/dto/dish_filter_request_dto/dish_filter_request_dto.dart';
import 'package:home_food/data/dto/dish_filter_request_dto/mapper/mapper.dart';
import 'package:home_food/data/dto/edit_food_request/edit_dish_request.dart';
import 'package:home_food/models/dish/create_dish_request.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/dish_list_filter_request/dish_list_filter_request.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class DishService {
  Future<dynamic> addNewDish(CreateFoodRequest body);

  Future<dynamic> editDish(EditFoodRequest body);

  Future<Pagination<Dish>> getPopularDishList({
    int page = 0,
    int size = 10,
  });

  Future<Pagination<Dish>> getDishes({
    int page = 0,
    int size = 10,
    required DishFilterRequestDTO filter,
  });

  Future<int> getDishesCount(
    DishFilterRequest filter,
  );

  Future<Pagination<Dish>> getMyDishes({
    int page = 0,
    int size = 10,
    required DishFilterRequestDTO filter,
  });

  Future<void> changeStatusActivate({
    required String dishCardId,
  });

  Future<void> changeStatusModerate({
    required String dishCardId,
  });

  Future<void> changeStatusDeactivate({
    required String dishCardId,
  });

  Future<Dish> getDishById({
    required String dishId,
  });
}

@Injectable(as: DishService)
class DishServiceImpl extends DishService {
  final RestClient _restClient;

  DishServiceImpl(this._restClient);

  @override
  Future addNewDish(CreateFoodRequest body) => _restClient.createDish(body);

  @override
  Future editDish(EditFoodRequest body) => _restClient.editDish(body);

  @override
  Future<Pagination<Dish>> getPopularDishList({
    int page = 0,
    int size = 10,
  }) async =>
      dishDtoPaginationToModel(await _restClient.getPopularDishList(
        page: page,
        size: size,
      ));

  @override
  Future<Pagination<Dish>> getDishes({
    int page = 0,
    int size = 10,
    required DishFilterRequestDTO filter,
  }) async =>
      dishDtoPaginationToModel(await _restClient.getDishListByFilter(
        body: filter,
        page: page,
        size: size,
      ));

  @override
  Future<int> getDishesCount(
    DishFilterRequest filter,
  ) async =>
      _restClient.getDishCountWithFilter(dishFilterRequestToDTO(filter));

  @override
  Future<Pagination<Dish>> getMyDishes({
    int page = 0,
    int size = 10,
    required DishFilterRequestDTO filter,
  }) async =>
      dishDtoPaginationToModel(await _restClient.getMyDishes(
        dishTypeId: filter.dishTypeId,
        hasHalfPortion: filter.hasHalfPortion,
        sortField: filter.sortBy.key,
        searchText: filter.searchText,
        page: page,
        size: size,
      ));

  @override
  Future<void> changeStatusModerate({
    required String dishCardId,
  }) async =>
      _restClient.changeDishStatusModerate(dishCardId);

  @override
  Future<void> changeStatusDeactivate({
    required String dishCardId,
  }) async =>
      _restClient.changeDishStatusDeactivate(dishCardId);

  @override
  Future<void> changeStatusActivate({
    required String dishCardId,
  }) async =>
      _restClient.activationDishCardById(dishCardId);

  @override
  Future<Dish> getDishById({
    required String dishId,
  }) async =>
      dishMapper(await _restClient.getDishById(
        id: dishId,
      ));
}
