import 'package:home_food/data/dto/order_dto/mapper/order_mapper.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:home_food/models/statistics/mapper/statistics_mapper.dart';
import 'package:home_food/models/statistics/mapper/top_dish_statistics_mapper.dart';
import 'package:home_food/models/statistics/statistics.dart';
import 'package:home_food/models/statistics/top_dish_statistics.dart';
import 'package:home_food/network/order_api/order_api.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class OrderService {
  /// Изменение данных в корзине
  /// [OrderDTO] моделька данных на изменение
  Future<OrderModel> updatePreOrder({
    required OrderModel body,
  });

  /// Получить данные о корзине
  Future<OrderModel> getPreOrder();

  /// Изменение количества блюда в корзине
  /// [positionId] айди позиции блюда в корзине
  /// [amount] количество
  Future<String> updateDishCardOrder({
    required String positionId,
    required int amount,
  });

  /// Удалить одно блюдо из корзины
  /// [positionId] позиция блюда в корзине
  Future<String> deleteDishFromBasket({
    required String positionId,
  });

  /// Подтвердить корзину в заказ
  Future<void> createOrder();

  /// Получить список заказов покупателя [UserType.CUSTOMER]
  Future<Pagination<OrderModel>> getCustomerOrders({
    int? page = 0,
    int? size = 10,
    String? status,
    String? searchText,
  });

  /// Получить список заказов мерчанта [UserType.MANUFACTURER]
  Future<Pagination<OrderModel>> getMerchantOrders({
    int? page = 0,
    int? size = 10,
    String? status,
    String? searchText,
  });

  /// Добавление блюда в корзину
  /// [dishCardId] айди блюда
  /// [amount] количество
  Future<String> putDishToPreorder({
    required String dishCardId,
    required int amount,
  });

  /// Удалить все блюдо из корзины
  Future<String> removeAllDishCardOrder();

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDERED]
  /// [orderId] айди заказа
  Future<String> changeOrderStatusToAccepted({
    required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDER_ACCEPTED]
  /// [orderId] айди заказа
  Future<String> changeOrderStatusToPreparation({
    required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.PREPARATION]
  /// [orderId] айди заказа
  Future<String> changeOrderStatusToDelivery({
    required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.DELIVERY]
  /// [orderId] айди заказа
  Future<String> changeOrderStatusToDelivered({
    required String orderId,
  });

  /// Получить заказ по айди
  /// [orderId] айди заказа
  Future<OrderModel> getOrderById({
    required String orderId,
  });

  ///Возврат заказа / Жалоба
  /// [orderId] айди заказа
  Future<String> complaintOrder({
    required String orderId,
    required String base64Image,
    required String comment,
  });

  /// Получить статистику по продажам
  Future<Statistics> getSellStatistics({
    required String startDate,
    required String endDate,
    required String period,
    int? month,
    int? year,
  });

  /// Получить статистику по топ блюдам
  Future<List<TopDishStatistics>> getTopDishStatistics({
    required String startDate,
    required String endDate,
  });
}

@Injectable(as: OrderService)
class OrderServiceImpl extends OrderService {
  final OrderApi _orderApi;

  OrderServiceImpl(this._orderApi);

  /// Изменение данных в корзине
  /// [OrderDTO] моделька данных на изменение
  @override
  Future<OrderModel> updatePreOrder({required OrderModel body}) async =>
      orderMapper(await _orderApi.updatePreOrder(body: orderDTOMapper(body)));

  /// Получить данные о корзине
  @override
  Future<OrderModel> getPreOrder() async =>
      orderMapper(await _orderApi.getPreOrder());

  /// Изменение количества блюда в корзине
  /// [positionId] айди позиции блюда в корзине
  /// [amount] количество
  @override
  Future<String> updateDishCardOrder({
    required String positionId,
    required int amount,
  }) async =>
      _orderApi.updateDishCardOrder(
        positionId: positionId,
        amount: amount,
      );

  /// Подтвердить корзину в заказ
  @override
  Future<void> createOrder() async => _orderApi.createOrder();

  /// Получить список заказов покупателя [UserType.CUSTOMER]
  @override
  Future<Pagination<OrderModel>> getCustomerOrders({
    int? page = 0,
    int? size = 10,
    String? status,
    String? searchText,
  }) async =>
      dishDtoPaginationToModel(
        await _orderApi.getCustomerOrders(
          page: page,
          status: status,
          searchText: searchText,
        ),
      );

  /// Получить список заказов мерчанта [UserType.MANUFACTURER]
  @override
  Future<Pagination<OrderModel>> getMerchantOrders({
    int? page = 0,
    int? size = 10,
    String? status,
    String? searchText,
  }) async =>
      dishDtoPaginationToModel(
        await _orderApi.getMerchantOrders(
          page: page,
          status: status,
          searchText: searchText,
        ),
      );

  /// Удалить одно блюдо из корзины
  /// [positionId] позиция блюда в корзине
  @override
  Future<String> deleteDishFromBasket({
    required String positionId,
  }) async =>
      await _orderApi.removeDishFromBasket(
        positionId,
      );

  /// Добавление блюда в корзину
  /// [dishCardId] айди блюда
  /// [amount] количество
  @override
  Future<String> putDishToPreorder({
    required String dishCardId,
    required int amount,
  }) async =>
      await _orderApi.putDishToPreorder(
        dishCardId: dishCardId,
        amount: amount,
      );

  /// Удалить все блюдо из корзины
  @override
  Future<String> removeAllDishCardOrder() async =>
      await _orderApi.removeAllDishCardOrder();

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  @override
  Future<String> changeOrderStatusToAccepted({
    required String orderId,
  }) async =>
      await _orderApi.changeOrderStatusToAccepted(
        orderId: orderId,
      );

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDER_ACCEPTED]
  /// [orderId] айди заказа
  @override
  Future<String> changeOrderStatusToPreparation({
    required String orderId,
  }) async =>
      await _orderApi.changeOrderStatusToPreparation(
        orderId: orderId,
      );

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.PREPARATION]
  /// [orderId] айди заказа
  @override
  Future<String> changeOrderStatusToDelivery({
    required String orderId,
  }) async =>
      await _orderApi.changeOrderStatusToDelivery(
        orderId: orderId,
      );

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.DELIVERY]
  /// [orderId] айди заказа
  @override
  Future<String> changeOrderStatusToDelivered({
    required String orderId,
  }) async =>
      await _orderApi.changeOrderStatusToDelivered(
        orderId: orderId,
      );

  /// Получить заказ по айди
  /// [orderId] айди заказа
  @override
  Future<OrderModel> getOrderById({
    required String orderId,
  }) async =>
      orderMapper(await _orderApi.getOrderById(
        orderId: orderId,
      ));

  /// Получить статистику по продажам
  @override
  Future<Statistics> getSellStatistics({
    required String startDate,
    required String endDate,
    required String period,
    int? month,
    int? year,
  }) async =>
      statisticsMapper(await _orderApi.getSellStatistics(
        startDate: startDate,
        endDate: endDate,
        period: period,
        month: month,
        year: year,
      ));

  /// Получить статистику по топ блюдам
  @override
  Future<List<TopDishStatistics>> getTopDishStatistics({
    required String startDate,
    required String endDate,
  }) async =>
      topDishStatisticsListMapper(await _orderApi.getTopDishStatistics(
        startDate: startDate,
        endDate: endDate,
      ));

  @override
  Future<String> complaintOrder({
    required String orderId,
    required String comment,
    required String base64Image,
  }) async =>
      await _orderApi.complaintOrder(
        orderId: orderId,
        comment: comment,
        base64Image: base64Image,
      );

}
