import 'dart:io';

import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:injectable/injectable.dart';

abstract class UploadImageService {
  Future<List<UploadedImage>> uploadPhotos(List<File> photos);
}

@Injectable(as: UploadImageService)
class UploadImageServiceImpl extends UploadImageService {
  final RestClient _restClient;

  UploadImageServiceImpl(this._restClient);

  @override
  Future<List<UploadedImage>> uploadPhotos(List<File> photos) =>
      _restClient.uploadPhotos(photos: photos);
}
