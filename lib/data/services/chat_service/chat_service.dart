import 'package:home_food/data/dto/chat_dto/mapper/chat_mapper.dart';
import 'package:home_food/data/dto/chat_dto/mapper/chat_message_mapper.dart';
import 'package:home_food/data/dto/chat_dto/mapper/chat_search_result_mapper.dart';
import 'package:home_food/models/chat/chat.dart';
import 'package:home_food/models/chat/chat_message.dart';
import 'package:home_food/models/chat/chat_search_result_model.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/network/chat_api/chat_api.dart';
import 'package:injectable/injectable.dart';

abstract class ChatService {
  /// Получение списка чатов
  Future<List<ChatModel>> getChatList();

  /// Получение списка сообщений чата
  Future<List<ChatMessageModel>> getChatMessages({
    required String chatId,
  });

  /// Получение чата по заказу
  Future<String> getChatByOrderId({
    required String orderId,
    required String manufacturerId,
  });

  /// Отправить сообщение
  Future<String> sendMessage({
    required String chatId,
    required String text,
    List<UploadedImage> photoItems = const [],
  });

  ///Поиск сообщения
  Future<ChatSearchResultModel> searchByAllChat({
    required String text,
    required bool isManufacturer,
  });
}

@Injectable(as: ChatService)
class ChatServiceImpl extends ChatService {
  final ChatApi chatApi;

  ChatServiceImpl(
    this.chatApi,
  );

  /// Получение списка чатов
  @override
  Future<List<ChatModel>> getChatList() async =>
      chatModelListMapper(await chatApi.getChatList());

  /// Получение списка сообщений чата
  @override
  Future<List<ChatMessageModel>> getChatMessages({
    required String chatId,
  }) async =>
      chatMessageModelListMapper(await chatApi.getChatMessages(
        chatId: chatId,
      ));

  @override
  Future<String> getChatByOrderId({
    required String orderId,
    required String manufacturerId,
  }) async =>
      (await chatApi.getChatByOrderId(
        orderId: orderId,
        manufacturerId: manufacturerId,
      ))
          .replaceAll('\"', '');

  @override
  Future<String> sendMessage({
    required String chatId,
    required String text,
    List<UploadedImage> photoItems = const [],
  }) =>
      chatApi.sendMessage(
        chatId: chatId,
        text: text,
        photoItems: photoItems,
      );

  @override
  Future<ChatSearchResultModel> searchByAllChat ({
    required String text,
    required bool isManufacturer,
  }) async =>
      chatSearchResultModelMapper(await chatApi.searchByAllChat(
        text: text,
        isManufacturer: isManufacturer,
      ));
}
