import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:home_food/data/dto/ingredient_dto/mapper/ingredient_dto_mapper.dart';
import 'package:home_food/data/dto/preference_dto/mapper/preference_dto_mapper.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/pagination/pagination.dart';

Dish dishMapper(DishDTO dto) => Dish(
      id: dto.id,
      name: dto.name,
      price: dto.price,
      priceWithSale: dto.priceWithSale,
      weight: dto.weight,
      portionAmount: dto.portionAmount,
      description: dto.description,
      dishTypeId: dto.dishTypeId,
      status: dto.status,
      preferenceList: preferenceListMapper(dto.preferenceList ?? []),
      ingredientList: ingredientDtoListToModel(dto.ingredientList ?? []),
      images: dto.images,
      manufacturer: dto.manufacturer,
      hasHalfPortion: dto.hasHalfPortion,
      preOrderDish: dto.preOrderDish,
      isNew: dto.isNew,
      isFavorite: dto.isFavorite,
      dishCardFavoriteId: dto.dishCardFavoriteId,
    );

DishDTO dishDTOMapper(Dish model) => DishDTO(
      id: model.id,
      name: model.name,
      price: model.price,
      priceWithSale: model.priceWithSale,
      weight: model.weight,
      portionAmount: model.portionAmount,
      description: model.description,
      dishTypeId: model.dishTypeId,
      preferenceList: preferenceDTOListMapper(model.preferenceList),
      ingredientList: ingredientListToDTO(model.ingredientList),
      images: model.images,
      manufacturer: model.manufacturer,
      isNew: model.isNew,
      hasHalfPortion: model.hasHalfPortion,
      preOrderDish: model.preOrderDish,
      isFavorite: model.isFavorite,
      dishCardFavoriteId: model.dishCardFavoriteId,
    );

List<Dish> dishDtoListToModel(List<DishDTO> list) =>
    list.map((e) => dishMapper(e)).toList();

List<DishDTO> dishListToDTO(List<Dish> list) =>
    list.map((e) => dishDTOMapper(e)).toList();

Pagination<Dish> dishDtoPaginationToModel(Pagination<DishDTO> pagination) =>
    Pagination(
      content: dishDtoListToModel(pagination.content),
      hasNext: pagination.hasNext,
      page: pagination.page,
      size: pagination.size,
    );
