import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/ingredient_dto/ingredient_dto.dart';
import 'package:home_food/data/dto/preference_dto/preference_dto.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'dish_dto.g.dart';

@JsonSerializable()
class DishDTO {
  String? id;
  String? name;
  int? price;
  int? priceWithSale;
  int? weight;
  int? portionAmount;
  String? description;
  String? dishTypeId;
  List<PreferenceDTO>? preferenceList;
  List<IngredientDTO>? ingredientList;
  List<UploadedImage>? images;
  Profile? manufacturer;
  bool? isNew;
  bool? isFavorite;
  bool? hasHalfPortion;
  bool? preOrderDish;
  DishStatusEnum? status;
  String? dishCardFavoriteId;

  DishDTO({
    this.id,
    this.name,
    this.price,
    this.priceWithSale,
    this.weight,
    this.portionAmount,
    this.description,
    this.dishTypeId,
    this.status,
    this.manufacturer,
    this.images,
    this.ingredientList,
    this.preferenceList,
    this.preOrderDish,
    this.hasHalfPortion,
    this.isNew,
    this.isFavorite,
    this.dishCardFavoriteId,
  });

  factory DishDTO.fromJson(Map<String, dynamic> json) =>
      _$DishDTOFromJson(json);

  Map<String, dynamic> toJson() => _$DishDTOToJson(this);
}
