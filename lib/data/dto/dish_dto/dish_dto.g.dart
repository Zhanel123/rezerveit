// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dish_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DishDTO _$DishDTOFromJson(Map<String, dynamic> json) => DishDTO(
      id: json['id'] as String?,
      name: json['name'] as String?,
      price: json['price'] as int?,
      priceWithSale: json['priceWithSale'] as int?,
      weight: json['weight'] as int?,
      portionAmount: json['portionAmount'] as int?,
      description: json['description'] as String?,
      dishTypeId: json['dishTypeId'] as String?,
      status: $enumDecodeNullable(_$DishStatusEnumEnumMap, json['status']),
      manufacturer: json['manufacturer'] == null
          ? null
          : Profile.fromJson(json['manufacturer'] as Map<String, dynamic>),
      images: (json['images'] as List<dynamic>?)
          ?.map((e) => UploadedImage.fromJson(e as Map<String, dynamic>))
          .toList(),
      ingredientList: (json['ingredientList'] as List<dynamic>?)
          ?.map((e) => IngredientDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      preferenceList: (json['preferenceList'] as List<dynamic>?)
          ?.map((e) => PreferenceDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      preOrderDish: json['preOrderDish'] as bool?,
      hasHalfPortion: json['hasHalfPortion'] as bool?,
      isNew: json['isNew'] as bool?,
      isFavorite: json['isFavorite'] as bool?,
      dishCardFavoriteId: json['dishCardFavoriteId'] as String?,
    );

Map<String, dynamic> _$DishDTOToJson(DishDTO instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'price': instance.price,
      'priceWithSale': instance.priceWithSale,
      'weight': instance.weight,
      'portionAmount': instance.portionAmount,
      'description': instance.description,
      'dishTypeId': instance.dishTypeId,
      'preferenceList': instance.preferenceList,
      'ingredientList': instance.ingredientList,
      'images': instance.images,
      'manufacturer': instance.manufacturer,
      'isNew': instance.isNew,
      'isFavorite': instance.isFavorite,
      'hasHalfPortion': instance.hasHalfPortion,
      'preOrderDish': instance.preOrderDish,
      'status': _$DishStatusEnumEnumMap[instance.status],
      'dishCardFavoriteId': instance.dishCardFavoriteId,
    };

const _$DishStatusEnumEnumMap = {
  DishStatusEnum.ALL: 'ALL',
  DishStatusEnum.ACTIVE: 'ACTIVE',
  DishStatusEnum.PUBLISHED: 'PUBLISHED',
  DishStatusEnum.NEW: 'NEW',
  DishStatusEnum.ON_MODERATION: 'ON_MODERATION',
  DishStatusEnum.DEACTIVATED: 'DEACTIVATED',
};
