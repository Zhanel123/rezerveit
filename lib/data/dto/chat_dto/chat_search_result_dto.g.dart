// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_search_result_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatSearchResultDTO _$ChatSearchResultDTOFromJson(Map<String, dynamic> json) =>
    ChatSearchResultDTO(
      chats: (json['chats'] as List<dynamic>?)
          ?.map((e) => ChatDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      messages: (json['messages'] as List<dynamic>?)
          ?.map((e) => ChatMessageDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ChatSearchResultDTOToJson(
        ChatSearchResultDTO instance) =>
    <String, dynamic>{
      'chats': instance.chats?.map((e) => e.toJson()).toList(),
      'messages': instance.messages?.map((e) => e.toJson()).toList(),
    };
