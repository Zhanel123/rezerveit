// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatDTO _$ChatDTOFromJson(Map<String, dynamic> json) => ChatDTO(
      id: json['id'] as String?,
      lastMessageTime: json['lastMessageTime'] == null
          ? null
          : DateTime.parse(json['lastMessageTime'] as String),
      lastMessageText: json['lastMessageText'] as String?,
      customer: json['customer'] == null
          ? null
          : Profile.fromJson(json['customer'] as Map<String, dynamic>),
      manufacturer: json['manufacturer'] == null
          ? null
          : Profile.fromJson(json['manufacturer'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatDTOToJson(ChatDTO instance) => <String, dynamic>{
      'id': instance.id,
      'lastMessageTime': instance.lastMessageTime?.toIso8601String(),
      'lastMessageText': instance.lastMessageText,
      'customer': instance.customer?.toJson(),
      'manufacturer': instance.manufacturer?.toJson(),
    };
