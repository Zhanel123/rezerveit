// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_message_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatMessageDTO _$ChatMessageDTOFromJson(Map<String, dynamic> json) =>
    ChatMessageDTO(
      id: json['id'] as String?,
      chatId: json['chatId'] as String?,
      authorId: json['authorId'] as String?,
      status: json['status'] as String?,
      text: json['text'] as String?,
      createdDate: json['createdDate'] == null
          ? null
          : DateTime.parse(json['createdDate'] as String),
      photos: (json['photos'] as List<dynamic>?)
          ?.map((e) => UploadedImage.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ChatMessageDTOToJson(ChatMessageDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'chatId': instance.chatId,
      'authorId': instance.authorId,
      'status': instance.status,
      'text': instance.text,
      'createdDate': instance.createdDate?.toIso8601String(),
      'photos': instance.photos?.map((e) => e.toJson()).toList(),
    };
