import 'package:equatable/equatable.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'chat_message_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class ChatMessageDTO extends Equatable {
  final String? id;
  final String? chatId;
  final String? authorId;
  final String? status;
  final String? text;
  final DateTime? createdDate;
  final List<UploadedImage>? photos;

  ChatMessageDTO({
    this.id,
    this.chatId,
    this.authorId,
    this.status,
    this.text,
    this.createdDate,
    this.photos,
  });

  factory ChatMessageDTO.fromJson(Map<String, dynamic> json) =>
      _$ChatMessageDTOFromJson(json);

  Map<String, dynamic> toJson() => _$ChatMessageDTOToJson(this);

  @override
  List<Object?> get props => [
        id,
        chatId,
        authorId,
        status,
        text,
        createdDate,
        photos,
      ];
}
