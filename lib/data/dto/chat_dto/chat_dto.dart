import 'package:equatable/equatable.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'chat_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class ChatDTO extends Equatable {
  final String? id;
  final DateTime? lastMessageTime;
  final String? lastMessageText;
  final Profile? customer;
  final Profile? manufacturer;

  ChatDTO({
    this.id,
    this.lastMessageTime,
    this.lastMessageText,
    this.customer,
    this.manufacturer,
  });

  factory ChatDTO.fromJson(Map<String, dynamic> json) =>
      _$ChatDTOFromJson(json);

  Map<String, dynamic> toJson() => _$ChatDTOToJson(this);

  @override
  List<Object?> get props => [
        id,
        lastMessageTime,
        lastMessageText,
        customer,
        manufacturer,
      ];
}
