import 'package:home_food/data/dto/chat_dto/chat_search_result_dto.dart';
import 'package:home_food/data/dto/chat_dto/mapper/chat_mapper.dart';
import 'package:home_food/data/dto/chat_dto/mapper/chat_message_mapper.dart';
import 'package:home_food/models/chat/chat_search_result_model.dart';

ChatSearchResultModel chatSearchResultModelMapper(ChatSearchResultDTO dto) => ChatSearchResultModel(
  chats: chatModelListMapper(dto.chats),
  messages: chatMessageModelListMapper(dto.messages),
);