import 'package:home_food/data/dto/chat_dto/chat_dto.dart';
import 'package:home_food/models/chat/chat.dart';

ChatModel chatModelMapper(ChatDTO dto) => ChatModel(
      id: dto.id,
      lastMessageTime: dto.lastMessageTime,
      lastMessageText: dto.lastMessageText,
      customer: dto.customer,
      manufacturer: dto.manufacturer,
    );

List<ChatModel> chatModelListMapper(List<ChatDTO>? list) =>
    list?.map((e) => chatModelMapper(e)).toList() ?? [];
