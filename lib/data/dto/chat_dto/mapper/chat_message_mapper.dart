import 'package:home_food/data/dto/chat_dto/chat_message_dto.dart';
import 'package:home_food/models/chat/chat_message.dart';

ChatMessageModel chatMessageModelMapper(ChatMessageDTO dto) => ChatMessageModel(
      id: dto.id,
      chatId: dto.chatId,
      authorId: dto.authorId,
      status: dto.status,
      text: dto.text,
      createdDate: dto.createdDate,
      photos: dto.photos,
    );

List<ChatMessageModel> chatMessageModelListMapper(List<ChatMessageDTO>? list) =>
    list?.map((e) => chatMessageModelMapper(e)).toList() ?? [];
