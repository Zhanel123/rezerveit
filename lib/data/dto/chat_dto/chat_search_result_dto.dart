import 'package:equatable/equatable.dart';
import 'package:home_food/data/dto/chat_dto/chat_dto.dart';
import 'package:home_food/data/dto/chat_dto/chat_message_dto.dart';
import 'package:home_food/models/chat/chat_message.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'chat_search_result_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class ChatSearchResultDTO extends Equatable {
  final List<ChatDTO>? chats;
  final List<ChatMessageDTO>? messages;

  ChatSearchResultDTO({
    this.chats,
    this.messages,
  });

  factory ChatSearchResultDTO.fromJson(Map<String, dynamic> json) =>
      _$ChatSearchResultDTOFromJson(json);

  Map<String, dynamic> toJson() => _$ChatSearchResultDTOToJson(this);

  @override
  List<Object?> get props => [
        chats,
        messages,
      ];
}
