import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:json_annotation/json_annotation.dart';

part 'dish_type_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class DishTypeDTO {
  final String? id;
  final String? nameKz;
  final String? nameRu;
  final UploadedImage? photo;

  DishTypeDTO({
    this.nameKz,
    this.nameRu,
    this.id,
    this.photo,
  });

  factory DishTypeDTO.fromJson(Map<String, dynamic> json) =>
      _$DishTypeDTOFromJson(json);

  Map<String, dynamic> toJson() => _$DishTypeDTOToJson(this);
}
