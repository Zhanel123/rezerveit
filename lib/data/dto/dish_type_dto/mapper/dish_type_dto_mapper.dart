import 'package:home_food/data/dto/dish_type_dto/dish_type_dto.dart';
import 'package:home_food/models/dish/dish_type.dart';

DishType dishTypeDtoToModel(DishTypeDTO dto) => DishType(
      id: dto.id,
      nameKz: dto.nameKz,
      nameRu: dto.nameRu,
      photo: dto.photo,
    );

List<DishType> dishTypeDtoListToModel(List<DishTypeDTO> list) =>
    list.map((e) => dishTypeDtoToModel(e)).toList();

DishTypeDTO dishTypeToDTO(DishType model) => DishTypeDTO(
      id: model.id,
      nameKz: model.nameKz,
      nameRu: model.nameRu,
    );

List<DishTypeDTO> dishTypeListToDTO(List<DishType> list) =>
    list.map((e) => dishTypeToDTO(e)).toList();
