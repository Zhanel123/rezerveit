// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dish_type_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DishTypeDTO _$DishTypeDTOFromJson(Map<String, dynamic> json) => DishTypeDTO(
      nameKz: json['nameKz'] as String?,
      nameRu: json['nameRu'] as String?,
      id: json['id'] as String?,
      photo: json['photo'] == null
          ? null
          : UploadedImage.fromJson(json['photo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DishTypeDTOToJson(DishTypeDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nameKz': instance.nameKz,
      'nameRu': instance.nameRu,
      'photo': instance.photo?.toJson(),
    };
