// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationDTO _$NotificationDTOFromJson(Map<String, dynamic> json) =>
    NotificationDTO(
      id: json['id'] as String?,
      createdDate: json['createdDate'] == null
          ? null
          : DateTime.parse(json['createdDate'] as String),
      subject: json['subject'] as String?,
      content: json['content'] as String?,
      type: json['type'] as String?,
      objectId: json['objectId'] as String?,
      status: json['status'] as String?,
      visitorId: json['visitorId'] as String?,
    );

Map<String, dynamic> _$NotificationDTOToJson(NotificationDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdDate': instance.createdDate?.toIso8601String(),
      'subject': instance.subject,
      'content': instance.content,
      'type': instance.type,
      'objectId': instance.objectId,
      'status': instance.status,
      'visitorId': instance.visitorId,
    };
