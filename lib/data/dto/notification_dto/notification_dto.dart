import 'package:json_annotation/json_annotation.dart';

part 'notification_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class NotificationDTO {
  final String? id;
  final DateTime? createdDate;
  final String? subject;
  final String? content;
  final String? type;
  final String? objectId;
  final String? status;
  final String? visitorId;

  NotificationDTO({
    this.id,
    this.createdDate,
    this.subject,
    this.content,
    this.type,
    this.objectId,
    this.status,
    this.visitorId,
  });

  factory NotificationDTO.fromJson(Map<String, dynamic> json) =>
      _$NotificationDTOFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationDTOToJson(this);
}
