import 'package:home_food/data/dto/notification_dto/notification_dto.dart';
import 'package:home_food/models/notification/notification.dart';

Notification notificationMapper(NotificationDTO dto) => Notification(
      id: dto.id,
      createdDate: dto.createdDate,
      subject: dto.subject,
      content: dto.content,
      type: dto.type,
      objectId: dto.objectId,
      wasRead: dto.status == "READ",
      visitorId: dto.visitorId,
    );

List<Notification> notificationListMapper(List<NotificationDTO> list) =>
    list.map((e) => notificationMapper(e)).toList();
