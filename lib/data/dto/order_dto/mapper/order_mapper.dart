import 'package:home_food/data/dto/address_dto/mapper/address_dto_mapper.dart';
import 'package:home_food/data/dto/grade_dto/mapper/grade_mapper.dart';
import 'package:home_food/data/dto/order_dto/order_dto.dart';
import 'package:home_food/data/dto/order_position_dto/mapper/order_position_mapper.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/models/pagination/pagination.dart';

OrderModel orderMapper(OrderDTO dto) => OrderModel(
      id: dto.id,
      address: dto.address != null ? addressMapper(dto.address!) : null,
      manufacturerName: dto.manufacturerName,
      manufacturerId: dto.manufacturerId ?? '',
      orderNumber: dto.orderNumber,
      total: dto.total,
      status: dto.status,
      timeOfDate: dto.timeOfDate,
      paymentMethod: dto.paymentMethod,
      deliveryMethod: dto.deliveryMethod,
      positionList: orderPositionListMapper(dto.positionList ?? []),
      grade: dto.grade != null ? gradeMapper(dto.grade!) : null,
      acceptedOrderTime: dto.acceptedOrderTime,
      preparationStartTime: dto.preparationStartTime,
      deliveryStartTime: dto.deliveryStartTime,
      deliveredOrderTime: dto.deliveredOrderTime,
    );

OrderDTO orderDTOMapper(OrderModel model) => OrderDTO(
      id: model.id,
      address: addressMapperDTO(model.address),
      manufacturerName: model.manufacturerName,
      orderNumber: model.orderNumber,
      timeOfDate: model.timeOfDate,
      status: model.status,
      total: model.total,
      paymentMethod: model.paymentMethod,
      silverAndNapkin: model.silverAndNapkin,
      deliveryMethod: model.deliveryMethod,
      positionList: orderPositionDTOListMapper(model.positionList),
      // grade: model.grade,
    );

List<OrderModel> orderListDTOMapper(List<OrderDTO> list) =>
    list.map((e) => orderMapper(e)).toList();

Pagination<OrderModel> dishDtoPaginationToModel(
  Pagination<OrderDTO> pagination,
) =>
    Pagination(
      content: orderListDTOMapper(pagination.content),
      hasNext: pagination.hasNext,
      page: pagination.page,
      size: pagination.size,
    );
