import 'package:easy_localization/easy_localization.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/data/dto/grade_dto/grade_dto.dart';
import 'package:home_food/data/dto/order_position_dto/order_position_dto.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/order_position/order_position.dart';
import 'package:json_annotation/json_annotation.dart';

part 'order_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class OrderDTO {
  final String? id;
  final AddressDTO? address;
  final String? manufacturerName;
  final String? manufacturerId;
  final String? orderNumber;
  final int? total;
  final bool? silverAndNapkin;
  final bool? preOrder;
  final String? paymentMethod;
  final String? deliveryMethod;
  final DateTime? timeOfDate;
  final OrderStatusEnum? status;
  final List<OrderPositionDTO>? positionList;
  @JsonKey(
    includeToJson: false,
  )
  final GradeDTO? grade;
  @JsonKey(fromJson: _StringToDate)
  final DateTime? acceptedOrderTime;
  @JsonKey(fromJson: _StringToDate)
  final DateTime? preparationStartTime;
  @JsonKey(fromJson: _StringToDate)
  final DateTime? deliveryStartTime;
  @JsonKey(fromJson: _StringToDate)
  final DateTime? deliveredOrderTime;

  OrderDTO({
    this.id,
    this.address,
    this.manufacturerName,
    this.manufacturerId,
    this.orderNumber,
    this.total,
    this.paymentMethod,
    this.silverAndNapkin,
    this.deliveryMethod,
    this.timeOfDate,
    this.preOrder,
    this.status,
    this.positionList,
    this.grade,
    this.acceptedOrderTime,
    this.preparationStartTime,
    this.deliveryStartTime,
    this.deliveredOrderTime,
  });

  factory OrderDTO.fromJson(Map<String, dynamic> json) =>
      _$OrderDTOFromJson(json);

  Map<String, dynamic> toJson() => _$OrderDTOToJson(this);

  static _StringToDate(String? json) {
    return json != null
        ? DateFormat('yyyy-MM-ddTHH:mm:ssZ').parseUTC(json).toLocal()
        : null;
  }
}
