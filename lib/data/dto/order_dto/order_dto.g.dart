// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderDTO _$OrderDTOFromJson(Map<String, dynamic> json) => OrderDTO(
      id: json['id'] as String?,
      address: json['address'] == null
          ? null
          : AddressDTO.fromJson(json['address'] as Map<String, dynamic>),
      manufacturerName: json['manufacturerName'] as String?,
      manufacturerId: json['manufacturerId'] as String?,
      orderNumber: json['orderNumber'] as String?,
      total: json['total'] as int?,
      paymentMethod: json['paymentMethod'] as String?,
      silverAndNapkin: json['silverAndNapkin'] as bool?,
      deliveryMethod: json['deliveryMethod'] as String?,
      timeOfDate: json['timeOfDate'] == null
          ? null
          : DateTime.parse(json['timeOfDate'] as String),
      preOrder: json['preOrder'] as bool?,
      status: $enumDecodeNullable(_$OrderStatusEnumEnumMap, json['status']),
      positionList: (json['positionList'] as List<dynamic>?)
          ?.map((e) => OrderPositionDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      grade: json['grade'] == null
          ? null
          : GradeDTO.fromJson(json['grade'] as Map<String, dynamic>),
      acceptedOrderTime:
          OrderDTO._StringToDate(json['acceptedOrderTime'] as String?),
      preparationStartTime:
          OrderDTO._StringToDate(json['preparationStartTime'] as String?),
      deliveryStartTime:
          OrderDTO._StringToDate(json['deliveryStartTime'] as String?),
      deliveredOrderTime:
          OrderDTO._StringToDate(json['deliveredOrderTime'] as String?),
    );

Map<String, dynamic> _$OrderDTOToJson(OrderDTO instance) => <String, dynamic>{
      'id': instance.id,
      'address': instance.address?.toJson(),
      'manufacturerName': instance.manufacturerName,
      'manufacturerId': instance.manufacturerId,
      'orderNumber': instance.orderNumber,
      'total': instance.total,
      'silverAndNapkin': instance.silverAndNapkin,
      'preOrder': instance.preOrder,
      'paymentMethod': instance.paymentMethod,
      'deliveryMethod': instance.deliveryMethod,
      'timeOfDate': instance.timeOfDate?.toIso8601String(),
      'status': _$OrderStatusEnumEnumMap[instance.status],
      'positionList': instance.positionList?.map((e) => e.toJson()).toList(),
      'acceptedOrderTime': instance.acceptedOrderTime?.toIso8601String(),
      'preparationStartTime': instance.preparationStartTime?.toIso8601String(),
      'deliveryStartTime': instance.deliveryStartTime?.toIso8601String(),
      'deliveredOrderTime': instance.deliveredOrderTime?.toIso8601String(),
    };

const _$OrderStatusEnumEnumMap = {
  OrderStatusEnum.ALL: 'ALL',
  OrderStatusEnum.PENDING: 'PENDING',
  OrderStatusEnum.ORDERED: 'ORDERED',
  OrderStatusEnum.ORDER_ACCEPTED: 'ORDER_ACCEPTED',
  OrderStatusEnum.PREPARATION: 'PREPARATION',
  OrderStatusEnum.DELIVERY: 'DELIVERY',
  OrderStatusEnum.DELIVERED: 'DELIVERED',
  OrderStatusEnum.REFUND: 'REFUND',
  OrderStatusEnum.CANCELED: 'CANCELED',
};
