// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'admin_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AdminDTO _$AdminDTOFromJson(Map<String, dynamic> json) => AdminDTO(
      iso31661Alpha3: json['iso31661Alpha3'] as String?,
      iso31661: json['iso31661'] as String?,
    );

Map<String, dynamic> _$AdminDTOToJson(AdminDTO instance) => <String, dynamic>{
      'iso31661Alpha3': instance.iso31661Alpha3,
      'iso31661': instance.iso31661,
    };
