// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'waypoints_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WayPointsDTO _$WayPointsDTOFromJson(Map<String, dynamic> json) => WayPointsDTO(
      distance: (json['distance'] as num?)?.toDouble(),
      name: json['name'] as String?,
      location: (json['location'] as List<dynamic>?)
          ?.map((e) => (e as num).toDouble())
          .toList(),
    );

Map<String, dynamic> _$WayPointsDTOToJson(WayPointsDTO instance) =>
    <String, dynamic>{
      'distance': instance.distance,
      'name': instance.name,
      'location': instance.location,
    };
