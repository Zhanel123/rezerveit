// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'intersections_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IntersectionsDTO _$IntersectionsDTOFromJson(Map<String, dynamic> json) =>
    IntersectionsDTO(
      entry: (json['entry'] as List<dynamic>?)?.map((e) => e as bool).toList(),
      bearings:
          (json['bearings'] as List<dynamic>?)?.map((e) => e as int).toList(),
      duration: (json['duration'] as num?)?.toDouble(),
      isUrban: json['isUrban'] as bool?,
      adminIndex: json['adminIndex'] as int?,
      out: json['out'] as int?,
      weight: (json['weight'] as num?)?.toDouble(),
      geometryIndex: json['geometryIndex'] as int?,
      location: (json['location'] as List<dynamic>?)
          ?.map((e) => (e as num).toDouble())
          .toList(),
      inSection: json['in'] as int?,
      turnWeight: (json['turnWeight'] as num?)?.toDouble(),
      turnDuration: (json['turnDuration'] as num?)?.toDouble(),
      stopSign: json['stopSign'] as bool?,
      lanes: (json['lanes'] as List<dynamic>?)
          ?.map((e) => LanesDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      yieldSign: json['yieldSign'] as bool?,
      trafficSignal: json['trafficSignal'] as bool?,
      classes:
          (json['classes'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$IntersectionsDTOToJson(IntersectionsDTO instance) =>
    <String, dynamic>{
      'entry': instance.entry,
      'bearings': instance.bearings,
      'duration': instance.duration,
      'isUrban': instance.isUrban,
      'adminIndex': instance.adminIndex,
      'out': instance.out,
      'weight': instance.weight,
      'geometryIndex': instance.geometryIndex,
      'location': instance.location,
      'in': instance.inSection,
      'turnWeight': instance.turnWeight,
      'turnDuration': instance.turnDuration,
      'stopSign': instance.stopSign,
      'lanes': instance.lanes?.map((e) => e.toJson()).toList(),
      'yieldSign': instance.yieldSign,
      'trafficSignal': instance.trafficSignal,
      'classes': instance.classes,
    };

LanesDTO _$LanesDTOFromJson(Map<String, dynamic> json) => LanesDTO(
      indications: (json['indications'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      validIndication: json['validIndication'] as String?,
      valid: json['valid'] as bool?,
      active: json['active'] as bool?,
    );

Map<String, dynamic> _$LanesDTOToJson(LanesDTO instance) => <String, dynamic>{
      'indications': instance.indications,
      'validIndication': instance.validIndication,
      'valid': instance.valid,
      'active': instance.active,
    };
