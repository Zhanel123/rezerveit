// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'legs_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LegsDTO _$LegsDTOFromJson(Map<String, dynamic> json) => LegsDTO(
      viaWaypoints: (json['viaWaypoints'] as List<dynamic>?)
          ?.map((e) => WayPointsDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      admins: (json['admins'] as List<dynamic>?)
          ?.map((e) => AdminDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      weight: (json['weight'] as num?)?.toDouble(),
      duration: (json['duration'] as num?)?.toDouble(),
      steps: (json['steps'] as List<dynamic>?)
          ?.map((e) => StepsDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      distance: (json['distance'] as num?)?.toDouble(),
      summary: json['summary'] as String?,
    );

Map<String, dynamic> _$LegsDTOToJson(LegsDTO instance) => <String, dynamic>{
      'viaWaypoints': instance.viaWaypoints?.map((e) => e.toJson()).toList(),
      'admins': instance.admins?.map((e) => e.toJson()).toList(),
      'weight': instance.weight,
      'duration': instance.duration,
      'steps': instance.steps?.map((e) => e.toJson()).toList(),
      'distance': instance.distance,
      'summary': instance.summary,
    };
