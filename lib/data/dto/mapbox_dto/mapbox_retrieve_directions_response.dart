import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/data/dto/mapbox_dto/routes_dto.dart';
import 'package:home_food/data/dto/mapbox_dto/waypoints_dto.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'mapbox_retrieve_directions_response.g.dart';

@JsonSerializable(explicitToJson: true)
class MapboxRetrieveDirectionsResponse {
  List<RoutesDTO>? routes;
  List<WayPointsDTO>? waypoints;
  String? code;
  String? uuid;

  MapboxRetrieveDirectionsResponse({
    this.routes,
    this.waypoints,
    this.code,
    this.uuid,
  });

  factory MapboxRetrieveDirectionsResponse.fromJson(
          Map<String, dynamic> json) =>
      _$MapboxRetrieveDirectionsResponseFromJson(json);

  Map<String, dynamic> toJson() =>
      _$MapboxRetrieveDirectionsResponseToJson(this);
}
