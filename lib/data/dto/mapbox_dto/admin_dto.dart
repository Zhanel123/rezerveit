import 'package:json_annotation/json_annotation.dart';

part 'admin_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class AdminDTO {
  String? iso31661Alpha3;
  String? iso31661;

  AdminDTO({this.iso31661Alpha3, this.iso31661});

  factory AdminDTO.fromJson(Map<String, dynamic> json) =>
      _$AdminDTOFromJson(json);

  Map<String, dynamic> toJson() => _$AdminDTOToJson(this);
}
