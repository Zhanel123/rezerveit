import 'package:json_annotation/json_annotation.dart';

part 'intersections_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class IntersectionsDTO {
  List<bool>? entry;
  List<int>? bearings;
  double? duration;
  bool? isUrban;
  int? adminIndex;
  int? out;
  double? weight;
  int? geometryIndex;
  List<double>? location;
  @JsonKey(name: 'in')
  int? inSection;
  double? turnWeight;
  double? turnDuration;
  bool? stopSign;
  List<LanesDTO>? lanes;
  bool? yieldSign;
  bool? trafficSignal;
  List<String>? classes;

  // TollCollection? tollCollection;

  IntersectionsDTO({
    this.entry,
    this.bearings,
    this.duration,
    this.isUrban,
    this.adminIndex,
    this.out,
    this.weight,
    this.geometryIndex,
    this.location,
    this.inSection,
    this.turnWeight,
    this.turnDuration,
    this.stopSign,
    this.lanes,
    this.yieldSign,
    this.trafficSignal,
    this.classes,
  });

  factory IntersectionsDTO.fromJson(Map<String, dynamic> json) =>
      _$IntersectionsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$IntersectionsDTOToJson(this);
}

@JsonSerializable(explicitToJson: true)
class LanesDTO {
  List<String>? indications;
  String? validIndication;
  bool? valid;
  bool? active;

  LanesDTO({this.indications, this.validIndication, this.valid, this.active});

  factory LanesDTO.fromJson(Map<String, dynamic> json) =>
      _$LanesDTOFromJson(json);

  Map<String, dynamic> toJson() => _$LanesDTOToJson(this);
}
