// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geometry_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GeometryDTO _$GeometryDTOFromJson(Map<String, dynamic> json) => GeometryDTO(
      coordinates: (json['coordinates'] as List<dynamic>?)
          ?.map((e) => e as List<dynamic>)
          .toList(),
      type: json['type'] as String?,
    );

Map<String, dynamic> _$GeometryDTOToJson(GeometryDTO instance) =>
    <String, dynamic>{
      'coordinates': instance.coordinates,
      'type': instance.type,
    };
