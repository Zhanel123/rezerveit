import 'package:home_food/data/dto/mapbox_dto/admin_dto.dart';
import 'package:home_food/data/dto/mapbox_dto/steps_dto.dart';
import 'package:home_food/data/dto/mapbox_dto/waypoints_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'legs_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class LegsDTO {
  List<WayPointsDTO>? viaWaypoints;
  List<AdminDTO>? admins;
  double? weight;
  double? duration;
  List<StepsDTO>? steps;
  double? distance;
  String? summary;

  LegsDTO(
      {this.viaWaypoints,
      this.admins,
      this.weight,
      this.duration,
      this.steps,
      this.distance,
      this.summary});

  factory LegsDTO.fromJson(Map<String, dynamic> json) =>
      _$LegsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$LegsDTOToJson(this);
}
