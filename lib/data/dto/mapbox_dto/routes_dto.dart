import 'package:home_food/data/dto/mapbox_dto/geometry_dto.dart';
import 'package:home_food/data/dto/mapbox_dto/legs_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'routes_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class RoutesDTO {
  String? weightName;
  double? weight;
  double? duration;
  double? distance;
  List<LegsDTO>? legs;
  GeometryDTO? geometry;

  RoutesDTO({
    this.weightName,
    this.weight,
    this.duration,
    this.distance,
    this.legs,
    this.geometry,
  });

  factory RoutesDTO.fromJson(Map<String, dynamic> json) =>
      _$RoutesDTOFromJson(json);

  Map<String, dynamic> toJson() => _$RoutesDTOToJson(this);
}
