import 'package:home_food/data/dto/mapbox_dto/geometry_dto.dart';
import 'package:home_food/data/dto/mapbox_dto/intersections_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'steps_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class StepsDTO {
  List<IntersectionsDTO>? intersections;

  // Maneuver? maneuver;
  String? name;
  double? duration;
  double? distance;
  String? drivingSide;
  double? weight;
  String? mode;
  GeometryDTO? geometry;
  String? destinations;
  String? ref;
  String? exits;

  StepsDTO({
    this.intersections,
    // this.maneuver,
    this.name,
    this.duration,
    this.distance,
    this.drivingSide,
    this.weight,
    this.mode,
    this.geometry,
    this.destinations,
    this.ref,
    this.exits,
  });

  factory StepsDTO.fromJson(Map<String, dynamic> json) =>
      _$StepsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$StepsDTOToJson(this);
}
