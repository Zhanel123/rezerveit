// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mapbox_retrieve_directions_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapboxRetrieveDirectionsResponse _$MapboxRetrieveDirectionsResponseFromJson(
        Map<String, dynamic> json) =>
    MapboxRetrieveDirectionsResponse(
      routes: (json['routes'] as List<dynamic>?)
          ?.map((e) => RoutesDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      waypoints: (json['waypoints'] as List<dynamic>?)
          ?.map((e) => WayPointsDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      code: json['code'] as String?,
      uuid: json['uuid'] as String?,
    );

Map<String, dynamic> _$MapboxRetrieveDirectionsResponseToJson(
        MapboxRetrieveDirectionsResponse instance) =>
    <String, dynamic>{
      'routes': instance.routes?.map((e) => e.toJson()).toList(),
      'waypoints': instance.waypoints?.map((e) => e.toJson()).toList(),
      'code': instance.code,
      'uuid': instance.uuid,
    };
