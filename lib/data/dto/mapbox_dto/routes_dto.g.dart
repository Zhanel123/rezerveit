// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'routes_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoutesDTO _$RoutesDTOFromJson(Map<String, dynamic> json) => RoutesDTO(
      weightName: json['weightName'] as String?,
      weight: (json['weight'] as num?)?.toDouble(),
      duration: (json['duration'] as num?)?.toDouble(),
      distance: (json['distance'] as num?)?.toDouble(),
      legs: (json['legs'] as List<dynamic>?)
          ?.map((e) => LegsDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      geometry: json['geometry'] == null
          ? null
          : GeometryDTO.fromJson(json['geometry'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RoutesDTOToJson(RoutesDTO instance) => <String, dynamic>{
      'weightName': instance.weightName,
      'weight': instance.weight,
      'duration': instance.duration,
      'distance': instance.distance,
      'legs': instance.legs?.map((e) => e.toJson()).toList(),
      'geometry': instance.geometry?.toJson(),
    };
