import 'package:json_annotation/json_annotation.dart';

part 'geometry_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class GeometryDTO {
  List<List>? coordinates;
  String? type;

  GeometryDTO({
    this.coordinates,
    this.type,
  });

  factory GeometryDTO.fromJson(Map<String, dynamic> json) =>
      _$GeometryDTOFromJson(json);

  Map<String, dynamic> toJson() => _$GeometryDTOToJson(this);
}
