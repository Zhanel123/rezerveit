import 'package:json_annotation/json_annotation.dart';

part 'waypoints_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class WayPointsDTO {
  double? distance;
  String? name;
  List<double>? location;

  WayPointsDTO({this.distance, this.name, this.location});

  factory WayPointsDTO.fromJson(Map<String, dynamic> json) =>
      _$WayPointsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$WayPointsDTOToJson(this);
}
