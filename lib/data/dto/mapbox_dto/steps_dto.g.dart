// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'steps_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StepsDTO _$StepsDTOFromJson(Map<String, dynamic> json) => StepsDTO(
      intersections: (json['intersections'] as List<dynamic>?)
          ?.map((e) => IntersectionsDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      name: json['name'] as String?,
      duration: (json['duration'] as num?)?.toDouble(),
      distance: (json['distance'] as num?)?.toDouble(),
      drivingSide: json['drivingSide'] as String?,
      weight: (json['weight'] as num?)?.toDouble(),
      mode: json['mode'] as String?,
      geometry: json['geometry'] == null
          ? null
          : GeometryDTO.fromJson(json['geometry'] as Map<String, dynamic>),
      destinations: json['destinations'] as String?,
      ref: json['ref'] as String?,
      exits: json['exits'] as String?,
    );

Map<String, dynamic> _$StepsDTOToJson(StepsDTO instance) => <String, dynamic>{
      'intersections': instance.intersections?.map((e) => e.toJson()).toList(),
      'name': instance.name,
      'duration': instance.duration,
      'distance': instance.distance,
      'drivingSide': instance.drivingSide,
      'weight': instance.weight,
      'mode': instance.mode,
      'geometry': instance.geometry?.toJson(),
      'destinations': instance.destinations,
      'ref': instance.ref,
      'exits': instance.exits,
    };
