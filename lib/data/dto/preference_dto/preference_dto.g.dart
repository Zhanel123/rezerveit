// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'preference_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PreferenceDTO _$PreferenceDTOFromJson(Map<String, dynamic> json) =>
    PreferenceDTO(
      id: json['id'] as String?,
      nameKz: json['nameKz'] as String?,
      nameRu: json['nameRu'] as String?,
    );

Map<String, dynamic> _$PreferenceDTOToJson(PreferenceDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nameKz': instance.nameKz,
      'nameRu': instance.nameRu,
    };
