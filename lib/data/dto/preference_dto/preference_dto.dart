import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:home_food/routes.dart';
import 'package:json_annotation/json_annotation.dart';

part 'preference_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class PreferenceDTO {
  final String? id;
  final String? nameKz;
  final String? nameRu;

  PreferenceDTO({
    this.id,
    this.nameKz,
    this.nameRu,
  });

  factory PreferenceDTO.fromJson(Map<String, dynamic> json) =>
      _$PreferenceDTOFromJson(json);

  Map<String, dynamic> toJson() => _$PreferenceDTOToJson(this);
}
