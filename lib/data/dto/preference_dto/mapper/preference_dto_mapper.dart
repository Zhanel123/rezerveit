import 'package:home_food/data/dto/preference_dto/preference_dto.dart';
import 'package:home_food/models/dish/preference.dart';

Preference preferenceMapper(PreferenceDTO dto) => Preference(
      id: dto.id,
      nameKz: dto.nameKz,
      nameRu: dto.nameRu,
    );

List<Preference> preferenceListMapper(List<PreferenceDTO> list) =>
    list.map((e) => preferenceMapper(e)).toList();


PreferenceDTO preferenceDTOMapper(Preference model) => PreferenceDTO(
  id: model.id,
  nameKz: model.nameKz,
  nameRu: model.nameRu,
);

List<PreferenceDTO> preferenceDTOListMapper(List<Preference> list) =>
    list.map((e) => preferenceDTOMapper(e)).toList();