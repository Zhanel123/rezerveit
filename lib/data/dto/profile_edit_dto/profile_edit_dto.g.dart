// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_edit_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileEditDTO _$ProfileEditDTOFromJson(Map<String, dynamic> json) =>
    ProfileEditDTO(
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      middleName: json['middleName'] as String?,
      birthDate: json['birthDate'] as String?,
      phoneNumber: json['phoneNumber'] as String?,
      city: json['city'] == null
          ? null
          : CityDTO.fromJson(json['city'] as Map<String, dynamic>),
      autoList: (json['autoList'] as List<dynamic>?)
          ?.map((e) => e as String?)
          .toList(),
      bonusCardNumber: json['bonusCardNumber'] as String?,
      availableBonusAmount: json['availableBonusAmount'] as int?,
      nextBonusCancellationDate: json['nextBonusCancellationDate'] as String?,
      nextBonusCancellationAmount: json['nextBonusCancellationAmount'] as int?,
      totalBonusAmount: json['totalBonusAmount'] as int?,
      totalStickersAmount: json['totalStickersAmount'] as int?,
      showActiveOrders: json['showActiveOrders'] as bool? ?? false,
      showStatisticsPanel: json['showStatisticsPanel'] as bool? ?? false,
      duplicateOrdersByEmail: json['duplicateOrdersByEmail'] as bool? ?? false,
    );

Map<String, dynamic> _$ProfileEditDTOToJson(ProfileEditDTO instance) =>
    <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'middleName': instance.middleName,
      'birthDate': instance.birthDate,
      'phoneNumber': instance.phoneNumber,
      'city': instance.city,
      'autoList': instance.autoList,
      'bonusCardNumber': instance.bonusCardNumber,
      'availableBonusAmount': instance.availableBonusAmount,
      'totalBonusAmount': instance.totalBonusAmount,
      'nextBonusCancellationDate': instance.nextBonusCancellationDate,
      'nextBonusCancellationAmount': instance.nextBonusCancellationAmount,
      'totalStickersAmount': instance.totalStickersAmount,
      'showActiveOrders': instance.showActiveOrders,
      'showStatisticsPanel': instance.showStatisticsPanel,
      'duplicateOrdersByEmail': instance.duplicateOrdersByEmail,
    };
