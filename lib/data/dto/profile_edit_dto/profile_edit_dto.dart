import 'package:home_food/data/dto/city_dto/city_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'profile_edit_dto.g.dart';

@JsonSerializable()
class ProfileEditDTO {
  String? firstName;
  String? lastName;
  String? middleName;
  String? birthDate;
  String? phoneNumber;
  CityDTO? city;
  List<String?>? autoList;
  String? bonusCardNumber;
  int? availableBonusAmount;
  int? totalBonusAmount;
  String? nextBonusCancellationDate;
  int? nextBonusCancellationAmount;
  int? totalStickersAmount;
  bool showActiveOrders;
  bool showStatisticsPanel;
  bool duplicateOrdersByEmail;

  ProfileEditDTO({
    this.firstName,
    this.lastName,
    this.middleName,
    this.birthDate,
    this.phoneNumber,
    this.city,
    this.autoList,
    this.bonusCardNumber,
    this.availableBonusAmount,
    this.nextBonusCancellationDate,
    this.nextBonusCancellationAmount,
    this.totalBonusAmount,
    this.totalStickersAmount,
    this.showActiveOrders = false,
    this.showStatisticsPanel = false,
    this.duplicateOrdersByEmail = false,
  });

  factory ProfileEditDTO.fromJson(Map<String, dynamic> json) =>
      _$ProfileEditDTOFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileEditDTOToJson(this);
}
