import 'package:home_food/data/dto/address_dto/mapper/address_dto_mapper.dart';
import 'package:home_food/data/dto/merchant_dto/merchant_dto.dart';
import 'package:home_food/models/merchant/merchant.dart';

Merchant merchantMapper(MerchantDTO dto) => Merchant(
      visitor: dto.visitor,
      addressList: addressListMapper(dto.addressList),
    );

List<Merchant> merchantListMapper(List<MerchantDTO> list) =>
    list.map((e) => merchantMapper(e)).toList();
