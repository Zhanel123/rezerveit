import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/data/dto/order_position_dto/order_position_dto.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/order_position/order_position.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'merchant_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class MerchantDTO {
  final Profile? visitor;
  final List<AddressDTO>? addressList;

  MerchantDTO({
    this.visitor,
    this.addressList,
  });

  factory MerchantDTO.fromJson(Map<String, dynamic> json) =>
      _$MerchantDTOFromJson(json);

  Map<String, dynamic> toJson() => _$MerchantDTOToJson(this);
}
