// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merchant_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MerchantDTO _$MerchantDTOFromJson(Map<String, dynamic> json) => MerchantDTO(
      visitor: json['visitor'] == null
          ? null
          : Profile.fromJson(json['visitor'] as Map<String, dynamic>),
      addressList: (json['addressList'] as List<dynamic>?)
          ?.map((e) => AddressDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MerchantDTOToJson(MerchantDTO instance) =>
    <String, dynamic>{
      'visitor': instance.visitor?.toJson(),
      'addressList': instance.addressList?.map((e) => e.toJson()).toList(),
    };
