// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dish_filter_request_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DishFilterRequestDTO _$DishFilterRequestDTOFromJson(
        Map<String, dynamic> json) =>
    DishFilterRequestDTO(
      radius: json['radius'] as int?,
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      searchText: json['searchText'] as String?,
      manufactureId: json['manufactureId'] as String?,
      dishTypeId: json['dishTypeId'] as String?,
      notDishTypeIds: (json['notDishTypeIds'] as List<dynamic>?)
          ?.map((e) => DishTypeDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      foodPreferenceIds: (json['foodPreferenceIds'] as List<dynamic>?)
          ?.map((e) => PreferenceDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      priceFrom: (json['priceFrom'] as num?)?.toDouble(),
      priceTo: (json['priceTo'] as num?)?.toDouble(),
      showNew: json['showNew'] as bool?,
      hasSale: json['hasSale'] as bool?,
      sortBy: $enumDecodeNullable(_$SortFieldEnumEnumMap, json['sortBy']),
      showPopular: json['showPopular'] as bool?,
      hasHalfPortion: json['hasHalfPortion'] as bool?,
    );

Map<String, dynamic> _$DishFilterRequestDTOToJson(
        DishFilterRequestDTO instance) =>
    <String, dynamic>{
      'radius': instance.radius,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'searchText': instance.searchText,
      'dishTypeId': instance.dishTypeId,
      'manufactureId': instance.manufactureId,
      'notDishTypeIds':
          DishFilterRequestDTO._dishTypesToList(instance.notDishTypeIds),
      'foodPreferenceIds': DishFilterRequestDTO._preferenceTypesToList(
          instance.foodPreferenceIds),
      'priceFrom': instance.priceFrom,
      'priceTo': instance.priceTo,
      'showNew': instance.showNew,
      'hasSale': instance.hasSale,
      'sortBy': _$SortFieldEnumEnumMap[instance.sortBy],
      'showPopular': instance.showPopular,
      'hasHalfPortion': instance.hasHalfPortion,
    };

const _$SortFieldEnumEnumMap = {
  SortFieldEnum.createdDate: 'createdDate',
  SortFieldEnum.buyCounter: 'buyCounter',
};
