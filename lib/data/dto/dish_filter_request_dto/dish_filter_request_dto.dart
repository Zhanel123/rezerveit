import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/dish_type_dto/dish_type_dto.dart';
import 'package:home_food/data/dto/preference_dto/preference_dto.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:json_annotation/json_annotation.dart';

part 'dish_filter_request_dto.g.dart';

@JsonSerializable()
class DishFilterRequestDTO {
  final int? radius;
  final double? latitude;
  final double? longitude;
  final String? searchText;
  final String? dishTypeId;
  final String? manufactureId;
  @JsonKey(toJson: _dishTypesToList)
  final List<DishTypeDTO>? notDishTypeIds;
  @JsonKey(toJson: _preferenceTypesToList)
  final List<PreferenceDTO>? foodPreferenceIds;
  final double? priceFrom;
  final double? priceTo;
  final bool? showNew;
  final bool? hasSale;
  final SortFieldEnum? sortBy;
  final bool? showPopular;
  final bool? hasHalfPortion;

  DishFilterRequestDTO({
    this.radius,
    this.latitude,
    this.longitude,
    this.searchText,
    this.manufactureId,
    this.dishTypeId,
    this.notDishTypeIds,
    this.foodPreferenceIds,
    this.priceFrom,
    this.priceTo,
    this.showNew,
    this.hasSale,
    this.sortBy,
    this.showPopular,
    this.hasHalfPortion,
  });

  factory DishFilterRequestDTO.fromJson(Map<String, dynamic> json) =>
      _$DishFilterRequestDTOFromJson(json);

  Map<String, dynamic> toJson() => _$DishFilterRequestDTOToJson(this);

  static _dishTypesToList(List<DishTypeDTO>? list) {
    return (list ?? []).map((e) => e.id).toList();
  }

  static _preferenceTypesToList(List<PreferenceDTO>? list) {
    return (list ?? []).map((e) => e.id).toList();
  }
}
