import 'package:home_food/data/dto/dish_filter_request_dto/dish_filter_request_dto.dart';
import 'package:home_food/data/dto/dish_type_dto/mapper/dish_type_dto_mapper.dart';
import 'package:home_food/data/dto/preference_dto/mapper/preference_dto_mapper.dart';
import 'package:home_food/models/dish_list_filter_request/dish_list_filter_request.dart';

DishFilterRequestDTO dishFilterRequestToDTO(DishFilterRequest model) =>
    DishFilterRequestDTO(
      radius: model.radius,
      latitude: model.latitude,
      longitude: model.longitude,
      searchText: model.searchText,
      dishTypeId: model.dishTypeId,
      notDishTypeIds: dishTypeListToDTO(model.notDishTypeIds),
      foodPreferenceIds: preferenceDTOListMapper(model.foodPreferenceIds),
      priceFrom: model.priceFrom,
      priceTo: model.priceTo,
      showNew: model.showNew,
      hasSale: model.hasSale,
      sortBy: model.sortBy,
      hasHalfPortion: model.hasHalfPortion,
      showPopular: model.showPopular,
    );
