import 'package:home_food/data/dto/ingredient_dto/ingredient_dto.dart';
import 'package:home_food/data/dto/preference_dto/preference_dto.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:json_annotation/json_annotation.dart';

part 'edit_dish_request.g.dart';

@JsonSerializable(explicitToJson: true)
class EditFoodRequest {
  String id;
  String? name;
  int? price;
  int? weight;
  String? description;
  int? portionAmount;
  String? dishTypeId;
  List<PreferenceDTO>? preferenceList;
  List<IngredientDTO>? ingredientList;
  List<UploadedImage>? images;

  EditFoodRequest({
    required this.id,
    this.name,
    this.price,
    this.weight,
    this.description,
    this.portionAmount,
    this.dishTypeId,
    this.preferenceList,
    this.ingredientList,
    this.images,
  });

  factory EditFoodRequest.fromJson(Map<String, dynamic> json) =>
      _$EditFoodRequestFromJson(json);

  Map<String, dynamic> toJson() => _$EditFoodRequestToJson(this);
}
