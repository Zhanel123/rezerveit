// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_dish_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EditFoodRequest _$EditFoodRequestFromJson(Map<String, dynamic> json) =>
    EditFoodRequest(
      id: json['id'] as String,
      name: json['name'] as String?,
      price: json['price'] as int?,
      weight: json['weight'] as int?,
      description: json['description'] as String?,
      portionAmount: json['portionAmount'] as int?,
      dishTypeId: json['dishTypeId'] as String?,
      preferenceList: (json['preferenceList'] as List<dynamic>?)
          ?.map((e) => PreferenceDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      ingredientList: (json['ingredientList'] as List<dynamic>?)
          ?.map((e) => IngredientDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      images: (json['images'] as List<dynamic>?)
          ?.map((e) => UploadedImage.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$EditFoodRequestToJson(EditFoodRequest instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'price': instance.price,
      'weight': instance.weight,
      'description': instance.description,
      'portionAmount': instance.portionAmount,
      'dishTypeId': instance.dishTypeId,
      'preferenceList':
          instance.preferenceList?.map((e) => e.toJson()).toList(),
      'ingredientList':
          instance.ingredientList?.map((e) => e.toJson()).toList(),
      'images': instance.images?.map((e) => e.toJson()).toList(),
    };
