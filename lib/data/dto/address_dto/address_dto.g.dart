// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressDTO _$AddressDTOFromJson(Map<String, dynamic> json) => AddressDTO(
      id: json['id'] as String?,
      address: json['address'] as String?,
      districtId: json['districtId'] as String?,
      title: json['title'] as String?,
      cityId: json['cityId'] as String?,
      visitorId: json['visitorId'] as String?,
      city: json['city'] == null
          ? null
          : CityDTO.fromJson(json['city'] as Map<String, dynamic>),
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      type: $enumDecodeNullable(_$AddressTypeEnumMap, json['type']) ??
          AddressType.CUSTOMER_ADDRESS,
    );

Map<String, dynamic> _$AddressDTOToJson(AddressDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'address': instance.address,
      'districtId': instance.districtId,
      'title': instance.title,
      'cityId': instance.cityId,
      'visitorId': instance.visitorId,
      'city': instance.city?.toJson(),
      'type': _$AddressTypeEnumMap[instance.type]!,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };

const _$AddressTypeEnumMap = {
  AddressType.MANUFACTURER_ADDRESS: 'MANUFACTURER_ADDRESS',
  AddressType.CUSTOMER_ADDRESS: 'CUSTOMER_ADDRESS',
};
