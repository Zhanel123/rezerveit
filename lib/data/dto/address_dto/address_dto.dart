import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/city_dto/city_dto.dart';
import 'package:home_food/models/city/city.dart';
import 'package:json_annotation/json_annotation.dart';

part 'address_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class AddressDTO {
  final String? id;
  final String? address;
  final String? districtId;
  final String? title;
  final String? cityId;
  final String? visitorId;
  final CityDTO? city;
  final AddressType type;
  final double? latitude;
  final double? longitude;

  AddressDTO({
    this.id,
    this.address,
    this.districtId,
    this.title,
    this.cityId,
    this.visitorId,
    this.city,
    this.latitude,
    this.longitude,
    this.type = AddressType.CUSTOMER_ADDRESS,
  });

  factory AddressDTO.fromJson(Map<String, dynamic> json) =>
      _$AddressDTOFromJson(json);

  Map<String, dynamic> toJson() => _$AddressDTOToJson(this);
}
