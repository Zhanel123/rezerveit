import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/data/dto/city_dto/mapper/city_dto_mapper.dart';
import 'package:home_food/models/address/address.dart';

Address addressMapper(AddressDTO dto) => Address(
      id: dto.id,
      address: dto.address,
      title: dto.title,
      cityId: dto.cityId,
      visitorId: dto.visitorId,
      latitude: dto.latitude,
      longitude: dto.longitude,
      type: dto.type,
      city: dto.city != null ? cityMapper(dto.city!) : null,
    );

List<Address> addressListMapper(List<AddressDTO>? list) =>
    list?.map((e) => addressMapper(e)).toList() ?? [];

AddressDTO addressMapperDTO(Address model) => AddressDTO(
      id: model.id,
      address: model.address,
      districtId: model.districtId,
      title: model.title,
      cityId: model.cityId,
      visitorId: model.visitorId,
      latitude: model.latitude,
      longitude: model.longitude,
      city: model.city != null ? cityMapperDTO(model.city!) : null,
    );
