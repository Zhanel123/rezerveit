import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_register_request_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class UserRegisterRequestDTO {
  final String firstName;
  final String? lastName;
  final String companyName;
  final UploadedImage visitorPhoto;
  final String phoneNumber;
  final UserType userType;
  final AddressDTO? address;
  final bool termsOfService;
  final bool privacyPolicy;

  UserRegisterRequestDTO({
    required this.firstName,
    this.lastName,
    required this.companyName,
    required this.visitorPhoto,
    required this.phoneNumber,
    required this.userType,
     this.address,
    required this.termsOfService,
    required this.privacyPolicy,
  });

  factory UserRegisterRequestDTO.fromJson(Map<String, dynamic> json) =>
      _$UserRegisterRequestDTOFromJson(json);

  Map<String, dynamic> toJson() => _$UserRegisterRequestDTOToJson(this);
}
