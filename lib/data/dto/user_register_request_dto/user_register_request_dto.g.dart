// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_register_request_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserRegisterRequestDTO _$UserRegisterRequestDTOFromJson(
        Map<String, dynamic> json) =>
    UserRegisterRequestDTO(
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String?,
      companyName: json['companyName'] as String,
      visitorPhoto:
          UploadedImage.fromJson(json['visitorPhoto'] as Map<String, dynamic>),
      phoneNumber: json['phoneNumber'] as String,
      userType: $enumDecode(_$UserTypeEnumMap, json['userType']),
      address: json['address'] == null
          ? null
          : AddressDTO.fromJson(json['address'] as Map<String, dynamic>),
      termsOfService: json['termsOfService'] as bool,
      privacyPolicy: json['privacyPolicy'] as bool,
    );

Map<String, dynamic> _$UserRegisterRequestDTOToJson(
        UserRegisterRequestDTO instance) =>
    <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'companyName': instance.companyName,
      'visitorPhoto': instance.visitorPhoto.toJson(),
      'phoneNumber': instance.phoneNumber,
      'userType': _$UserTypeEnumMap[instance.userType]!,
      'address': instance.address?.toJson(),
      'termsOfService': instance.termsOfService,
      'privacyPolicy': instance.privacyPolicy,
    };

const _$UserTypeEnumMap = {
  UserType.CUSTOMER: 'CUSTOMER',
  UserType.MANUFACTURER: 'MANUFACTURER',
};
