import 'package:home_food/data/dto/address_dto/mapper/address_dto_mapper.dart';
import 'package:home_food/data/dto/user_register_request_dto/user_register_request_dto.dart';
import 'package:home_food/models/register_user/register_user.dart';

UserRegisterRequestDTO userRegisterRequestMapperDTO(
        UserRegisterRequest model) =>
    UserRegisterRequestDTO(
      firstName: model.firstName,
      lastName: model.lastName,
      companyName: model.companyName,
      visitorPhoto: model.visitorPhoto,
      phoneNumber: model.phoneNumber,
      userType: model.userType,
      address: model.address != null ? addressMapperDTO(model.address!) : null,
      termsOfService: model.termsOfService,
      privacyPolicy: model.privacyPolicy,
    );
