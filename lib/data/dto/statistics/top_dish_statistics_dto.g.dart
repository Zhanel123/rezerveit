// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'top_dish_statistics_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopDishStatisticsDTO _$TopDishStatisticsDTOFromJson(
        Map<String, dynamic> json) =>
    TopDishStatisticsDTO(
      dishCardItem: json['dishCardItem'] == null
          ? null
          : DishDTO.fromJson(json['dishCardItem'] as Map<String, dynamic>),
      totalSum: json['totalSum'] as int?,
    );

Map<String, dynamic> _$TopDishStatisticsDTOToJson(
        TopDishStatisticsDTO instance) =>
    <String, dynamic>{
      'dishCardItem': instance.dishCardItem,
      'totalSum': instance.totalSum,
    };
