// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'statistics_item_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StatisticsItemDTO _$StatisticsItemDTOFromJson(Map<String, dynamic> json) =>
    StatisticsItemDTO(
      startDate: json['startDate'] == null
          ? null
          : DateTime.parse(json['startDate'] as String),
      endDate: json['endDate'] == null
          ? null
          : DateTime.parse(json['endDate'] as String),
      totalSum: json['totalSum'] as int?,
    );

Map<String, dynamic> _$StatisticsItemDTOToJson(StatisticsItemDTO instance) =>
    <String, dynamic>{
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
      'totalSum': instance.totalSum,
    };
