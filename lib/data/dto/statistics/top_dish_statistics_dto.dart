import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'top_dish_statistics_dto.g.dart';

@JsonSerializable()
class TopDishStatisticsDTO {
  DishDTO? dishCardItem;
  int? totalSum;

  TopDishStatisticsDTO({
    this.dishCardItem,
    this.totalSum,
  });

  factory TopDishStatisticsDTO.fromJson(Map<String, dynamic> json) =>
      _$TopDishStatisticsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$TopDishStatisticsDTOToJson(this);
}
