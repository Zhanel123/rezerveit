import 'package:home_food/data/dto/statistics/statistics_item_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'statistics_dto.g.dart';

@JsonSerializable()
class StatisticsDTO {
  int? totalSum;
  int? averageSum;
  double? averagePercent;
  int? changeTotalSum;
  double? changeTotalSumPercent;
  String? period;
  List<StatisticsItemDTO>? items;

  StatisticsDTO({
    this.totalSum,
    this.averageSum,
    this.averagePercent,
    this.changeTotalSum,
    this.changeTotalSumPercent,
    this.period,
    this.items,
  });

  factory StatisticsDTO.fromJson(Map<String, dynamic> json) =>
      _$StatisticsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$StatisticsDTOToJson(this);
}
