// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'statistics_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StatisticsDTO _$StatisticsDTOFromJson(Map<String, dynamic> json) =>
    StatisticsDTO(
      totalSum: json['totalSum'] as int?,
      averageSum: json['averageSum'] as int?,
      averagePercent: (json['averagePercent'] as num?)?.toDouble(),
      changeTotalSum: json['changeTotalSum'] as int?,
      changeTotalSumPercent:
          (json['changeTotalSumPercent'] as num?)?.toDouble(),
      period: json['period'] as String?,
      items: (json['items'] as List<dynamic>?)
          ?.map((e) => StatisticsItemDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$StatisticsDTOToJson(StatisticsDTO instance) =>
    <String, dynamic>{
      'totalSum': instance.totalSum,
      'averageSum': instance.averageSum,
      'averagePercent': instance.averagePercent,
      'changeTotalSum': instance.changeTotalSum,
      'changeTotalSumPercent': instance.changeTotalSumPercent,
      'period': instance.period,
      'items': instance.items,
    };
