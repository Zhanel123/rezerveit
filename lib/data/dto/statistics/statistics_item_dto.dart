import 'package:json_annotation/json_annotation.dart';

part 'statistics_item_dto.g.dart';

@JsonSerializable()
class StatisticsItemDTO {
  DateTime? startDate;
  DateTime? endDate;
  int? totalSum;

  StatisticsItemDTO({
    this.startDate,
    this.endDate,
    this.totalSum,
  });

  factory StatisticsItemDTO.fromJson(Map<String, dynamic> json) =>
      _$StatisticsItemDTOFromJson(json);

  Map<String, dynamic> toJson() => _$StatisticsItemDTOToJson(this);
}
