import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'favorite_dish_dto.g.dart';

@JsonSerializable()
class FavoriteDishDTO {
  String? id;
  DishDTO? dishCardItem;
  String? createdDate;
  Profile? visitorInfo;

  FavoriteDishDTO({
    this.id,
    this.createdDate,
    this.dishCardItem,
    this.visitorInfo,
  });

  factory FavoriteDishDTO.fromJson(Map<String, dynamic> json) =>
      _$FavoriteDishDTOFromJson(json);

  Map<String, dynamic> toJson() => _$FavoriteDishDTOToJson(this);
}
