// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_dish_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FavoriteDishDTO _$FavoriteDishDTOFromJson(Map<String, dynamic> json) =>
    FavoriteDishDTO(
      id: json['id'] as String?,
      createdDate: json['createdDate'] as String?,
      dishCardItem: json['dishCardItem'] == null
          ? null
          : DishDTO.fromJson(json['dishCardItem'] as Map<String, dynamic>),
      visitorInfo: json['visitorInfo'] == null
          ? null
          : Profile.fromJson(json['visitorInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FavoriteDishDTOToJson(FavoriteDishDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'dishCardItem': instance.dishCardItem,
      'createdDate': instance.createdDate,
      'visitorInfo': instance.visitorInfo,
    };
