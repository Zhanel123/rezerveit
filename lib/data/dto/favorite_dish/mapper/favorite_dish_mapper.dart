import 'package:home_food/data/dto/dish_dto/mapper/dish_dto_mapper.dart';
import 'package:home_food/data/dto/favorite_dish/favorite_dish_dto.dart';
import 'package:home_food/models/favourite_dish/favourite_dish.dart';
import 'package:home_food/models/pagination/pagination.dart';

FavouriteDish favoriteDishDtoToModel(FavoriteDishDTO dto) => FavouriteDish(
      id: dto.id,
      dishCardItem:
          dto.dishCardItem != null ? dishMapper(dto.dishCardItem!) : null,
      createdDate: dto.createdDate,
      visitorInfo: dto.visitorInfo,
    );

List<FavouriteDish> favoriteDishDtoListToModel(List<FavoriteDishDTO> list) =>
    list.map((e) => favoriteDishDtoToModel(e)).toList();

Pagination<FavouriteDish> dishDtoPaginationToModel(
        Pagination<FavoriteDishDTO> pagination) =>
    Pagination(
      content: favoriteDishDtoListToModel(pagination.content),
      hasNext: pagination.hasNext,
      page: pagination.page,
      size: pagination.size,
    );
