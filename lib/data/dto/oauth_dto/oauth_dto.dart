import 'package:json_annotation/json_annotation.dart';

part 'oauth_dto.g.dart';

@JsonSerializable()
class OAuthDTO {
  @JsonKey(name: 'access_token')
  String? accessToken;
  @JsonKey(name: 'token_type')
  String? tokenType;
  @JsonKey(name: 'refresh_token')
  String? refreshToken;
  @JsonKey(name: 'expires_in')
  int? expiresIn;
  @JsonKey(name: 'scope')
  String? scope;
  @JsonKey(name: 'OAuth2.SESSION_ID')
  String? sessionId;

  OAuthDTO({
    this.accessToken,
    this.tokenType,
    this.refreshToken,
    this.expiresIn,
    this.scope,
    this.sessionId,
  });

  factory OAuthDTO.fromJson(Map<String, dynamic> json) =>
      _$OAuthDTOFromJson(json);

  Map<String, dynamic> toJson() => _$OAuthDTOToJson(this);
}
