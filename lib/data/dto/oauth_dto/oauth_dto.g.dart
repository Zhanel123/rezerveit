// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oauth_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OAuthDTO _$OAuthDTOFromJson(Map<String, dynamic> json) => OAuthDTO(
      accessToken: json['access_token'] as String?,
      tokenType: json['token_type'] as String?,
      refreshToken: json['refresh_token'] as String?,
      expiresIn: json['expires_in'] as int?,
      scope: json['scope'] as String?,
      sessionId: json['OAuth2.SESSION_ID'] as String?,
    );

Map<String, dynamic> _$OAuthDTOToJson(OAuthDTO instance) => <String, dynamic>{
      'access_token': instance.accessToken,
      'token_type': instance.tokenType,
      'refresh_token': instance.refreshToken,
      'expires_in': instance.expiresIn,
      'scope': instance.scope,
      'OAuth2.SESSION_ID': instance.sessionId,
    };
