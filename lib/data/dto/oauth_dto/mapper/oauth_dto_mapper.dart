import 'package:home_food/data/dto/oauth_dto/oauth_dto.dart';
import 'package:home_food/models/profile/oauth.dart';

OAuth oauthDtoToModel(OAuthDTO dto) => OAuth(
    accessToken: dto.accessToken,
    tokenType: dto.tokenType,
    refreshToken: dto.refreshToken,
    expiresIn: dto.expiresIn,
    scope: dto.scope,
    sessionId: dto.sessionId,
);