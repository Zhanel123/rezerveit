import 'package:home_food/data/dto/ingredient_dto/ingredient_dto.dart';
import 'package:home_food/models/dish/ingredient.dart';

Ingredient ingredientDtoToModel(IngredientDTO dto) => Ingredient(
      id: dto.id,
      nameKz: dto.nameKz,
      nameRu: dto.nameRu,
    );

List<Ingredient> ingredientDtoListToModel(List<IngredientDTO> list) =>
    list.map((e) => ingredientDtoToModel(e)).toList();

IngredientDTO ingredientToDTO(Ingredient model) => IngredientDTO(
      id: model.id,
      nameKz: model.nameKz,
      nameRu: model.nameRu,
    );

List<IngredientDTO> ingredientListToDTO(List<Ingredient> list) =>
    list.map((e) => ingredientToDTO(e)).toList();
