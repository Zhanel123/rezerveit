import 'package:json_annotation/json_annotation.dart';

part 'ingredient_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class IngredientDTO {
  final String? id;
  final String? nameKz;
  final String? nameRu;

  IngredientDTO({
    this.nameKz,
    this.nameRu,
    this.id,
  });

  factory IngredientDTO.fromJson(Map<String, dynamic> json) =>
      _$IngredientDTOFromJson(json);

  Map<String, dynamic> toJson() => _$IngredientDTOToJson(this);
}
