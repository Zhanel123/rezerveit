// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ingredient_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IngredientDTO _$IngredientDTOFromJson(Map<String, dynamic> json) =>
    IngredientDTO(
      nameKz: json['nameKz'] as String?,
      nameRu: json['nameRu'] as String?,
      id: json['id'] as String?,
    );

Map<String, dynamic> _$IngredientDTOToJson(IngredientDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nameKz': instance.nameKz,
      'nameRu': instance.nameRu,
    };
