import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'order_position_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class OrderPositionDTO {
  final String? id;
  final int? amount;
  final DishDTO? dishCardItem;
  final bool? isOutOfStock;
  final bool? hasSale;

  OrderPositionDTO({
    required this.id,
    required this.amount,
    required this.dishCardItem,
    required this.isOutOfStock,
    required this.hasSale,
  });

  factory OrderPositionDTO.fromJson(Map<String, dynamic> json) =>
      _$OrderPositionDTOFromJson(json);

  Map<String, dynamic> toJson() => _$OrderPositionDTOToJson(this);
}
