// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_position_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderPositionDTO _$OrderPositionDTOFromJson(Map<String, dynamic> json) =>
    OrderPositionDTO(
      id: json['id'] as String?,
      amount: json['amount'] as int?,
      dishCardItem: json['dishCardItem'] == null
          ? null
          : DishDTO.fromJson(json['dishCardItem'] as Map<String, dynamic>),
      isOutOfStock: json['isOutOfStock'] as bool?,
      hasSale: json['hasSale'] as bool?,
    );

Map<String, dynamic> _$OrderPositionDTOToJson(OrderPositionDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'amount': instance.amount,
      'dishCardItem': instance.dishCardItem?.toJson(),
      'isOutOfStock': instance.isOutOfStock,
      'hasSale': instance.hasSale,
    };
