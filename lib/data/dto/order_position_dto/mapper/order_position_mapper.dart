import 'package:home_food/data/dto/dish_dto/mapper/dish_dto_mapper.dart';
import 'package:home_food/data/dto/order_position_dto/order_position_dto.dart';
import 'package:home_food/models/order_position/order_position.dart';

OrderPosition orderPositionMapper(OrderPositionDTO dto) => OrderPosition(
      id: dto.id,
      amount: dto.amount,
      isOutOfStock: dto.isOutOfStock,
      hasSale: dto.hasSale,
      dishCardItem:
          dto.dishCardItem != null ? dishMapper(dto.dishCardItem!) : null,
    );

OrderPositionDTO orderPositionDTOMapper(OrderPosition model) =>
    OrderPositionDTO(
      id: model.id,
      amount: model.amount,
      dishCardItem: dishDTOMapper(model.dishCardItem),
      isOutOfStock: model.isOutOfStock,
      hasSale: model.hasSale,
    );

List<OrderPosition> orderPositionListMapper(List<OrderPositionDTO> list) =>
    list.map((e) => orderPositionMapper(e)).toList();

List<OrderPositionDTO> orderPositionDTOListMapper(List<OrderPosition> list) =>
    list.map((e) => orderPositionDTOMapper(e)).toList();
