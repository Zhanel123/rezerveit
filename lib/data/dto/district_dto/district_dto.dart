import 'package:home_food/data/dto/city_dto/city_dto.dart';
import 'package:json_annotation/json_annotation.dart';

part 'district_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class DistrictDTO {
  final String? id;
  final String? nameKz;
  final String? nameRu;
  final double? latitude;
  final double? longitude;
  final CityDTO? city;

  DistrictDTO({
    this.nameKz,
    this.nameRu,
    this.id,
    this.latitude,
    this.longitude,
    this.city,
  });

  factory DistrictDTO.fromJson(Map<String, dynamic> json) =>
      _$DistrictDTOFromJson(json);

  Map<String, dynamic> toJson() => _$DistrictDTOToJson(this);
}
