// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'district_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DistrictDTO _$DistrictDTOFromJson(Map<String, dynamic> json) => DistrictDTO(
      nameKz: json['nameKz'] as String?,
      nameRu: json['nameRu'] as String?,
      id: json['id'] as String?,
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      city: json['city'] == null
          ? null
          : CityDTO.fromJson(json['city'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DistrictDTOToJson(DistrictDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nameKz': instance.nameKz,
      'nameRu': instance.nameRu,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'city': instance.city?.toJson(),
    };
