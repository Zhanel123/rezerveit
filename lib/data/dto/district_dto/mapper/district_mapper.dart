import 'package:home_food/data/dto/city_dto/mapper/city_dto_mapper.dart';
import 'package:home_food/data/dto/district_dto/district_dto.dart';
import 'package:home_food/models/district/district.dart';

District districtMapper(DistrictDTO dto) => District(
      id: dto.id,
      nameKz: dto.nameKz,
      nameRu: dto.nameRu,
      latitude: dto.latitude,
      longitude: dto.longitude,
      city: dto.city != null ? cityMapper(dto.city!) : null,
    );

List<District> districtListMapper(List<DistrictDTO>? list) =>
    list?.map((e) => districtMapper(e)).toList() ?? [];
