// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CityDTO _$CityDTOFromJson(Map<String, dynamic> json) => CityDTO(
      nameKz: json['nameKz'] as String?,
      nameRu: json['nameRu'] as String?,
      id: json['id'] as String?,
      code: json['code'] as String?,
      timeZone: json['timeZone'] as String?,
    );

Map<String, dynamic> _$CityDTOToJson(CityDTO instance) => <String, dynamic>{
      'id': instance.id,
      'nameKz': instance.nameKz,
      'nameRu': instance.nameRu,
      'code': instance.code,
      'timeZone': instance.timeZone,
    };
