import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'city_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class CityDTO extends Equatable {
  final String? id;
  final String? nameKz;
  final String? nameRu;
  final String? code;
  final String? timeZone;

  CityDTO({
    this.nameKz,
    this.nameRu,
    this.id,
    this.code,
    this.timeZone,
  });

  factory CityDTO.fromJson(Map<String, dynamic> json) =>
      _$CityDTOFromJson(json);

  Map<String, dynamic> toJson() => _$CityDTOToJson(this);

  @override
  List<Object?> get props => [
        id,
        code,
        nameKz,
        nameRu,
        timeZone,
      ];
}
