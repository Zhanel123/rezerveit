import 'package:home_food/data/dto/city_dto/city_dto.dart';
import 'package:home_food/models/city/city.dart';

City cityMapper(CityDTO dto) => City(
      id: dto.id,
      nameKz: dto.nameKz,
      nameRu: dto.nameRu,
      code: dto.code,
    );

List<City> cityDtoListToModel(List<CityDTO> list) =>
    list.map((e) => cityMapper(e)).toList();

CityDTO cityMapperDTO(City model) => CityDTO(
      id: model.id,
      nameKz: model.nameKz,
      nameRu: model.nameRu,
      code: model.code,
    );
