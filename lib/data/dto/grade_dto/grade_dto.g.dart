// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'grade_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GradeDTO _$GradeDTOFromJson(Map<String, dynamic> json) => GradeDTO(
      id: json['id'] as String?,
      comment: json['comment'] as String?,
      rating: json['rating'] as int?,
      photoPath: json['photoPath'] as String?,
      customer: json['customer'] == null
          ? null
          : Profile.fromJson(json['customer'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GradeDTOToJson(GradeDTO instance) => <String, dynamic>{
      'id': instance.id,
      'rating': instance.rating,
      'comment': instance.comment,
      'photoPath': instance.photoPath,
      'customer': instance.customer,
    };
