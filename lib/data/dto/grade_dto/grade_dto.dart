import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:json_annotation/json_annotation.dart';

part 'grade_dto.g.dart';

@JsonSerializable()
class GradeDTO {
  String? id;
  int? rating;
  String? comment;
  String? photoPath;
  Profile? customer;

  GradeDTO({
    this.id,
    this.comment,
    this.rating,
    this.photoPath,
    this.customer,
  });

  factory GradeDTO.fromJson(Map<String, dynamic> json) =>
      _$GradeDTOFromJson(json);

  Map<String, dynamic> toJson() => _$GradeDTOToJson(this);
}
