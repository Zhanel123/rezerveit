import 'package:home_food/data/dto/grade_dto/grade_dto.dart';
import 'package:home_food/models/grade/grade.dart';

Grade gradeMapper(GradeDTO dto) => Grade(
      id: dto.id,
      rating: dto.rating,
      comment: dto.comment,
      photoPath: dto.photoPath,
    );

List<Grade> gradeListMapper(List<GradeDTO> list) =>
    list.map((e) => gradeMapper(e)).toList();
