import 'package:flutter/material.dart';
import 'package:home_food/core/env.dart';

class Food {
  final String urlImage;
  final String label;
  final String price;
  final int? percent;
  final String? shopImageUrl;
  final bool showIcons;
  final bool isHot;
  final bool isNew;

  Food({
    required this.urlImage,
    required this.label,
    required this.price,
    this.percent,
    this.shopImageUrl,
    required this.showIcons,
    this.isHot = false,
    this.isNew = false,
  });

  get imageUrl => restURL + '/files?fileRef=fs://${urlImage}';
}