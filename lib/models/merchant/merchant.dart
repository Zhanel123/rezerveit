import 'package:home_food/core/enums.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:collection/collection.dart';
import 'package:latlong2/latlong.dart';

class Merchant {
  final Profile? visitor;
  final List<Address> addressList;
  final Address? address;

  Merchant({
    this.visitor,
    List<Address>? addressList,
  })  : addressList = addressList ?? const [],
        address = addressList?.firstWhereOrNull(
            (element) => element.type == AddressType.MANUFACTURER_ADDRESS);

  LatLng? get getCoordinate => address != null
      ? LatLng(address!.latitude ?? 43.25, address!.longitude ?? 77.4)
      : null;
}
