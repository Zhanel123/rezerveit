// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagination.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pagination<T> _$PaginationFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    Pagination<T>(
      content: (json['content'] as List<dynamic>?)?.map(fromJsonT).toList() ??
          const [],
      hasNext: json['hasNext'] as bool? ?? false,
      page: json['page'] as int? ?? 0,
      size: json['size'] as int? ?? 10,
    );

Map<String, dynamic> _$PaginationToJson<T>(
  Pagination<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'content': instance.content.map(toJsonT).toList(),
      'hasNext': instance.hasNext,
      'page': instance.page,
      'size': instance.size,
    };
