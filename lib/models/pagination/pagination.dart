import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:home_food/data/dto/favorite_dish/favorite_dish_dto.dart';
import 'package:home_food/data/dto/grade_dto/grade_dto.dart';
import 'package:home_food/data/dto/merchant_dto/merchant_dto.dart';
import 'package:home_food/data/dto/notification_dto/notification_dto.dart';
import 'package:json_annotation/json_annotation.dart';

import '../profile/profile.dart';

part 'pagination.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class Pagination<T> {
  final List<T> content;
  final bool? hasNext;
  final int? page;
  final int? size;

  Pagination({
    this.content = const [],
    this.hasNext = false,
    this.page = 0,
    this.size = 10,
  });

  factory Pagination.fromJson(Map<String, dynamic> json, fromJsonT) =>
      _$PaginationFromJson(json, fromJsonT);

  Map<String, dynamic> toJson() => _$PaginationToJson(this, toJsonT);

  static T fromJsonT<T>(Object? json) {
    final factoriesFromJson = <Type, Function>{
      MerchantDTO: (Map<String, dynamic> json) => MerchantDTO.fromJson(json),
      Profile: (Map<String, dynamic> json) => Profile.fromJson(json),
      GradeDTO: (Map<String, dynamic> json) => GradeDTO.fromJson(json),
      DishDTO: (Map<String, dynamic> json) => DishDTO.fromJson(json),
      NotificationDTO: (Map<String, dynamic> json) =>
          NotificationDTO.fromJson(json),
      FavoriteDishDTO: (Map<String, dynamic> json) =>
          FavoriteDishDTO.fromJson(json),
    };

    return factoriesFromJson[T] != null
        ? factoriesFromJson[T]!(json as Map<String, dynamic>)
        : null;
  }

  Object? toJsonT(T value) {
    return factoriesToJson[T]!();
  }

  final factoriesToJson = <Type, Function>{
    DishDTO: (Map<String, dynamic> json) => DishDTO.fromJson(json),
    FavoriteDishDTO: (Map<String, dynamic> json) =>
        FavoriteDishDTO.fromJson(json),
  };
}
