import 'dart:io';

import 'package:home_food/core/env.dart';
import 'package:json_annotation/json_annotation.dart';

part 'uploaded_image.g.dart';

@JsonSerializable(explicitToJson: true)
class UploadedImage {
  final String? id;
  final String? path;
  final bool? isMain;

  UploadedImage({
    this.id,
    this.path,
    this.isMain,
  });

  factory UploadedImage.fromJson(Map<String, dynamic> json) =>
      _$UploadedImageFromJson(json);

  get imageUrl => restURL + '/files?fileRef=fs://${path}';

  Map<String, dynamic> toJson() => _$UploadedImageToJson(this);

}