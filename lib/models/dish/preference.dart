import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Preference extends Equatable {
  final String id;
  final String nameKz;
  final String nameRu;

  Preference({
    String? id,
    String? nameKz,
    String? nameRu,
  })  : id = id ?? '',
        nameKz = nameKz ?? '',
        nameRu = nameRu ?? '';

  @override
  List<Object?> get props => [
        id,
        nameKz,
        nameRu,
      ];

  String name(BuildContext context) =>
      context.locale.languageCode == 'ru' ? nameRu : nameKz;
}
