import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class Ingredient {
  final String id;
  final String nameKz;
  final String nameRu;

  Ingredient({
    String? id,
    String? nameKz,
    String? nameRu,
  })  : id = id ?? '',
        nameKz = nameKz ?? '',
        nameRu = nameRu ?? '';

  String name(BuildContext context) =>
      context.locale.languageCode == 'ru' ? nameRu : nameKz;
}
