import 'package:home_food/core/enums.dart';
import 'package:home_food/models/dish/ingredient.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/profile/profile.dart';

class Dish {
  String id;
  String name;
  int price;
  int priceWithSale;
  int weight;
  int portionAmount;
  int minPortionAmount;
  String description;
  String dishTypeId;
  List<Preference> preferenceList;
  List<Ingredient> ingredientList;
  List<UploadedImage> images;
  Profile? manufacturer;
  bool isNew;
  bool isFavorite;
  bool hasHalfPortion;
  bool preOrderDish;
  int discountAfter19;
  DishStatusEnum status;
  String dishCardFavoriteId;

  Dish({
    String? id,
    String? name = '',
    int? price,
    int? priceWithSale,
    int? weight,
    int? portionAmount,
    int? minPortionAmount,
    String? description = '',
    String? dishTypeId,
    Profile? manufacturer,
    List<UploadedImage>? images,
    List<Ingredient>? ingredientList,
    List<Preference>? preferenceList,
    bool? isNew,
    bool? isFavorite,
    bool? hasHalfPortion,
    bool? preOrderDish,
    int? discountAfter19,
    DishStatusEnum? status,
    String? dishCardFavoriteId,
  })  : id = id ?? '',
        name = name ?? '',
        price = price ?? 0,
        priceWithSale = priceWithSale ?? 0,
        weight = weight ?? 0,
        portionAmount = portionAmount ?? 0,
        minPortionAmount = minPortionAmount ?? 0,
        description = description ?? '',
        dishTypeId = dishTypeId ?? '',
        preferenceList = preferenceList ?? [],
        ingredientList = ingredientList ?? [],
        images = images ?? [],
        status = status ?? DishStatusEnum.ALL,
        manufacturer = manufacturer,
        isNew = isNew ?? false,
        isFavorite = isFavorite ?? false,
        discountAfter19 = discountAfter19 ?? 0,
        hasHalfPortion = hasHalfPortion ?? false,
        preOrderDish = preOrderDish ?? false,
        dishCardFavoriteId = dishCardFavoriteId ?? '';

  Dish copyWith({
    bool? isFavorite,
    bool? hasHalfPortion,
    bool? preOrderDish,
    int? discountAfter19,
  }) =>
      Dish(
        id: id ?? this.id,
        name: name ?? this.name,
        price: price ?? this.price,
        priceWithSale: priceWithSale ?? this.priceWithSale,
        weight: weight ?? this.weight,
        portionAmount: portionAmount ?? this.portionAmount,
        description: description ?? this.description,
        dishTypeId: dishTypeId ?? this.dishTypeId,
        manufacturer: manufacturer ?? this.manufacturer,
        images: images ?? this.images,
        ingredientList: ingredientList ?? this.ingredientList,
        preferenceList: preferenceList ?? this.preferenceList,
        isNew: isNew ?? this.isNew,
        isFavorite: isFavorite ?? this.isFavorite,
        hasHalfPortion: hasHalfPortion ?? this.hasHalfPortion,
        preOrderDish: preOrderDish ?? this.preOrderDish,
        discountAfter19: discountAfter19 ?? this.discountAfter19,
        dishCardFavoriteId: dishCardFavoriteId ?? this.dishCardFavoriteId,
      );
}
