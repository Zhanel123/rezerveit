import 'package:home_food/data/dto/ingredient_dto/ingredient_dto.dart';
import 'package:home_food/data/dto/preference_dto/preference_dto.dart';
import 'package:home_food/models/dish/ingredient.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:json_annotation/json_annotation.dart';

part 'create_dish_request.g.dart';

@JsonSerializable(explicitToJson: true)
class CreateFoodRequest {
  String? name;
  int? price;
  int? weight;
  String? description;
  int? portionAmount;
  int? minPortionAmount;
  int? discountAfter19;
  String? dishTypeId;
  bool? hasHalfPortion;
  bool? preOrderDish;
  List<PreferenceDTO>? preferenceList;
  List<IngredientDTO>? ingredientList;
  List<UploadedImage>? images;

  CreateFoodRequest({
    this.name,
    this.price,
    this.weight,
    this.description,
    this.discountAfter19,
    this.portionAmount,
    this.minPortionAmount,
    this.dishTypeId,
    this.hasHalfPortion,
    this.preOrderDish,
    this.preferenceList,
    this.ingredientList,
    this.images,
  });

  factory CreateFoodRequest.fromJson(Map<String, dynamic> json) =>
      _$CreateFoodRequestFromJson(json);

  Map<String, dynamic> toJson() => _$CreateFoodRequestToJson(this);
}
