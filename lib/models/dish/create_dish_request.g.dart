// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_dish_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateFoodRequest _$CreateFoodRequestFromJson(Map<String, dynamic> json) =>
    CreateFoodRequest(
      name: json['name'] as String?,
      price: json['price'] as int?,
      weight: json['weight'] as int?,
      description: json['description'] as String?,
      discountAfter19: json['discountAfter19'] as int?,
      portionAmount: json['portionAmount'] as int?,
      minPortionAmount: json['minPortionAmount'] as int?,
      dishTypeId: json['dishTypeId'] as String?,
      hasHalfPortion: json['hasHalfPortion'] as bool?,
      preOrderDish: json['preOrderDish'] as bool?,
      preferenceList: (json['preferenceList'] as List<dynamic>?)
          ?.map((e) => PreferenceDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      ingredientList: (json['ingredientList'] as List<dynamic>?)
          ?.map((e) => IngredientDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      images: (json['images'] as List<dynamic>?)
          ?.map((e) => UploadedImage.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CreateFoodRequestToJson(CreateFoodRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'price': instance.price,
      'weight': instance.weight,
      'description': instance.description,
      'portionAmount': instance.portionAmount,
      'minPortionAmount': instance.minPortionAmount,
      'discountAfter19': instance.discountAfter19,
      'dishTypeId': instance.dishTypeId,
      'hasHalfPortion': instance.hasHalfPortion,
      'preOrderDish': instance.preOrderDish,
      'preferenceList':
          instance.preferenceList?.map((e) => e.toJson()).toList(),
      'ingredientList':
          instance.ingredientList?.map((e) => e.toJson()).toList(),
      'images': instance.images?.map((e) => e.toJson()).toList(),
    };
