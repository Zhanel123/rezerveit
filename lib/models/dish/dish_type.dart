import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:home_food/models/dish/uploaded_image.dart';

class DishType {
  final String id;
  final String nameKz;
  final String nameRu;

  final UploadedImage? photo;

  DishType({
    String? id,
    String? nameKz,
    String? nameRu,
    this.photo,
  })  : id = id ?? '',
        nameKz = nameKz ?? '',
        nameRu = nameRu ?? '';

  String name(BuildContext context) =>
      context.locale.languageCode == 'ru' ? nameRu : nameKz;
}
