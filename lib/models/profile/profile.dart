import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/city_dto/city_dto.dart';
import 'package:home_food/models/dish/uploaded_image.dart';

import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

part 'profile.g.dart';

@JsonSerializable(explicitToJson: true)
class Profile {
  String? id;
  String? firstName;
  String? lastName;
  String? middleName;
  String? companyName;
  @JsonKey(fromJson: _fromStringToDate)
  String? birthDate;
  String? phoneNumber;
  @JsonKey(
    unknownEnumValue: UserType.CUSTOMER,
    defaultValue: UserType.CUSTOMER,
  )
  UserType userType;
  CityDTO? city;
  List<String?>? autoList;
  String? bonusCardNumber;
  UploadedImage? photo;
  int? availableBonusAmount;
  int? totalBonusAmount;
  @JsonKey(fromJson: _fromStringToDate)
  String? nextBonusCancellationDate;
  int? nextBonusCancellationAmount;
  int? totalStickersAmount;
  @JsonKey(defaultValue: 0.0)
  double? rating;
  bool? showActiveOrders;
  bool? showStatisticsPanel;
  bool? duplicateOrdersByEmail;

  Profile({
    this.firstName,
    this.lastName,
    this.middleName,
    this.companyName,
    this.birthDate,
    this.phoneNumber,
    this.city,
    this.autoList,
    this.photo,
    this.bonusCardNumber,
    this.availableBonusAmount,
    this.nextBonusCancellationDate,
    this.nextBonusCancellationAmount,
    this.totalBonusAmount,
    this.rating,
    required this.userType,
    this.totalStickersAmount,
    this.showActiveOrders,
    this.showStatisticsPanel,
    this.duplicateOrdersByEmail,
  });

  factory Profile.fromJson(Map<String, dynamic> json) =>
      _$ProfileFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileToJson(this);

  static String? _fromStringToDate(String? value) {
    //2021-04-12
    return value != null
        ? DateFormat('dd-MM-yyyy')
            .format(DateFormat('yyyy-MM-dd').parseUTC(value).toLocal())
        : null;
  }
}
