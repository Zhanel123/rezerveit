// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Profile _$ProfileFromJson(Map<String, dynamic> json) => Profile(
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      middleName: json['middleName'] as String?,
      companyName: json['companyName'] as String?,
      birthDate: Profile._fromStringToDate(json['birthDate'] as String?),
      phoneNumber: json['phoneNumber'] as String?,
      city: json['city'] == null
          ? null
          : CityDTO.fromJson(json['city'] as Map<String, dynamic>),
      autoList: (json['autoList'] as List<dynamic>?)
          ?.map((e) => e as String?)
          .toList(),
      photo: json['photo'] == null
          ? null
          : UploadedImage.fromJson(json['photo'] as Map<String, dynamic>),
      bonusCardNumber: json['bonusCardNumber'] as String?,
      availableBonusAmount: json['availableBonusAmount'] as int?,
      nextBonusCancellationDate: Profile._fromStringToDate(
          json['nextBonusCancellationDate'] as String?),
      nextBonusCancellationAmount: json['nextBonusCancellationAmount'] as int?,
      totalBonusAmount: json['totalBonusAmount'] as int?,
      rating: (json['rating'] as num?)?.toDouble() ?? 0.0,
      userType: $enumDecodeNullable(_$UserTypeEnumMap, json['userType'],
              unknownValue: UserType.CUSTOMER) ??
          UserType.CUSTOMER,
      totalStickersAmount: json['totalStickersAmount'] as int?,
      showActiveOrders: json['showActiveOrders'] as bool?,
      showStatisticsPanel: json['showStatisticsPanel'] as bool?,
      duplicateOrdersByEmail: json['duplicateOrdersByEmail'] as bool?,
    )..id = json['id'] as String?;

Map<String, dynamic> _$ProfileToJson(Profile instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'middleName': instance.middleName,
      'companyName': instance.companyName,
      'birthDate': instance.birthDate,
      'phoneNumber': instance.phoneNumber,
      'userType': _$UserTypeEnumMap[instance.userType]!,
      'city': instance.city?.toJson(),
      'autoList': instance.autoList,
      'bonusCardNumber': instance.bonusCardNumber,
      'photo': instance.photo?.toJson(),
      'availableBonusAmount': instance.availableBonusAmount,
      'totalBonusAmount': instance.totalBonusAmount,
      'nextBonusCancellationDate': instance.nextBonusCancellationDate,
      'nextBonusCancellationAmount': instance.nextBonusCancellationAmount,
      'totalStickersAmount': instance.totalStickersAmount,
      'rating': instance.rating,
      'showActiveOrders': instance.showActiveOrders,
      'showStatisticsPanel': instance.showStatisticsPanel,
      'duplicateOrdersByEmail': instance.duplicateOrdersByEmail,
    };

const _$UserTypeEnumMap = {
  UserType.CUSTOMER: 'CUSTOMER',
  UserType.MANUFACTURER: 'MANUFACTURER',
};
