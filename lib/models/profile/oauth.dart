class OAuth {
  String accessToken;
  String tokenType;
  String refreshToken;
  int expiresIn;
  String scope;
  String sessionId;

  OAuth({
    String? accessToken,
    String? tokenType,
    String? refreshToken,
    int? expiresIn,
    String? scope,
    String? sessionId,
  })  : accessToken = accessToken ?? '',
        tokenType = tokenType ?? '',
        refreshToken = refreshToken ?? '',
        expiresIn = expiresIn ?? 0,
        scope = scope ?? '',
        sessionId = sessionId ?? '';
}
