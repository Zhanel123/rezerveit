import 'package:home_food/models/dish/dish.dart';

class OrderPosition {
  final String id;
  final int amount;
  final Dish dishCardItem;
  final bool isOutOfStock;
  final bool hasSale;

  OrderPosition({
    String? id,
    int? amount,
    Dish? dishCardItem,
    bool? isOutOfStock,
    bool? hasSale,
  })  : id = id ?? '',
        amount = amount ?? 0,
        isOutOfStock = isOutOfStock ?? false,
        hasSale = hasSale ?? false,
        dishCardItem = dishCardItem ?? Dish();
}
