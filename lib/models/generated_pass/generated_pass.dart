class GeneratedPass {
  String? generatedPass;

  GeneratedPass({this.generatedPass});

  GeneratedPass.fromJson(Map<String, dynamic> json) {
    generatedPass = json['generatedPass'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['generatedPass'] = this.generatedPass;
    return data;
  }
}
