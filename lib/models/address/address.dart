import 'package:equatable/equatable.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/models/city/city.dart';

class Address extends Equatable {
  final String id;
  final String title;
  final String address;
  final String addressName;
  final String fullAddressName;
  final String cityId;
  final String visitorId;
  final String districtId;
  final City? city;
  final AddressType type;
  final double? latitude;
  final double? longitude;

  Address({
    String? id,
    String? title,
    String? address,
    String? addressName,
    String? districtId,
    String? cityId,
    String? visitorId,
    AddressType? type,
    this.latitude,
    this.longitude,
    this.city,
  })  : id = id ?? '',
        address = address ?? '',
        title = title ?? '',
        type = type ?? AddressType.CUSTOMER_ADDRESS,
        addressName = addressName ?? '',
        districtId = districtId ?? '',
        cityId = cityId ?? '',
        visitorId = visitorId ?? '',
        fullAddressName =
            '${city != null ? '${city.nameRu}, ' : ''}${address ?? ''}';

  @override
  List<Object?> get props => [
        id,
        title,
        address,
        districtId,
        type,
        addressName,
        fullAddressName,
      ];
}
