import 'package:json_annotation/json_annotation.dart';

part 'get_merchants_request.g.dart';

@JsonSerializable()
class GetMerchantsRequest {
  final int? page;
  final int? size;
  final String? searchText;

  GetMerchantsRequest({
    this.page,
    this.size,
    this.searchText,
  });

  factory GetMerchantsRequest.fromJson(Map<String, dynamic> json) => _$GetMerchantsRequestFromJson(json);

  Map<String, dynamic> toJson() => _$GetMerchantsRequestToJson(this);
}