// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_merchants_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetMerchantsRequest _$GetMerchantsRequestFromJson(Map<String, dynamic> json) =>
    GetMerchantsRequest(
      page: json['page'] as int?,
      size: json['size'] as int?,
      searchText: json['searchText'] as String?,
    );

Map<String, dynamic> _$GetMerchantsRequestToJson(
        GetMerchantsRequest instance) =>
    <String, dynamic>{
      'page': instance.page,
      'size': instance.size,
      'searchText': instance.searchText,
    };
