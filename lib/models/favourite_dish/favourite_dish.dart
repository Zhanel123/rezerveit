import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/profile/profile.dart';

class FavouriteDish {
  String id;
  Dish? dishCardItem;
  String createdDate;
  Profile? visitorInfo;

  FavouriteDish({
    String? id,
    String? createdDate,
    this.dishCardItem,
    this.visitorInfo,
  })  : id = id ?? '',
        createdDate = createdDate ?? '';
}
