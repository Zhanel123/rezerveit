class Message {
  int? id;
  int? themeId;
  String? text;
  int? userId;
  String? status;
  DateTime? createTime;

  Message(
      {this.id,
        this.themeId,
        this.text,
        this.userId,
        this.status,
        this.createTime});

  Message.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    themeId = json['themeId'];
    text = json['text'];
    userId = json['userId'];
    status = json['status'];
    createTime = DateTime.tryParse(json['createTime']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['themeId'] = this.themeId;
    data['text'] = this.text;
    data['userId'] = this.userId;
    data['status'] = this.status;
    data['createTime'] = this.createTime;
    return data;
  }

}