import 'package:easy_localization/easy_localization.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/grade/grade.dart';
import 'package:home_food/models/order_position/order_position.dart';
import 'package:json_annotation/json_annotation.dart';

class OrderModel {
  final String id;
  final Address address;
  final String manufacturerName;
  final String manufacturerId;
  final bool silverAndNapkin;
  final bool preOrder;
  final String orderNumber;
  final int total;
  final String paymentMethod;
  final String deliveryMethod;
  final OrderStatusEnum status;
  final DateTime timeOfDate;
  final List<OrderPosition> positionList;
  final Grade? grade;
  @JsonKey(fromJson: _fromStringToDate)
  final DateTime? acceptedOrderTime;
  @JsonKey(fromJson: _fromStringToDate)
  final DateTime? preparationStartTime;
  @JsonKey(fromJson: _fromStringToDate)
  final DateTime? deliveryStartTime;
  @JsonKey(fromJson: _fromStringToDate)
  final DateTime? deliveredOrderTime;

  OrderModel({
    String? id,
    Address? address,
    String? manufacturerName,
    String? manufacturerId,
    bool? silverAndNapkin,
    bool? preOrder,
    String? orderNumber,
    int? total,
    String? paymentMethod,
    String? deliveryMethod,
    OrderStatusEnum? status,
    DateTime? timeOfDate,
    List<OrderPosition>? positionList,
    this.acceptedOrderTime,
    this.preparationStartTime,
    this.deliveryStartTime,
    this.deliveredOrderTime,
    this.grade,
  })  : id = id ?? '',
        address = address ?? Address(),
        manufacturerName = manufacturerName ?? '',
        manufacturerId = manufacturerId ?? '',
        silverAndNapkin = silverAndNapkin ?? false,
        preOrder = preOrder ?? false,
        orderNumber = orderNumber ?? '',
        total = total ?? 0,
        paymentMethod = paymentMethod ?? '',
        deliveryMethod = deliveryMethod ?? '',
        status = status ?? OrderStatusEnum.ALL,
        timeOfDate = timeOfDate ?? DateTime(0),
        positionList = positionList ?? [];

  OrderModel copyWith({
    Address? address,
    String? manufacturerName,
    String? orderNumber,
    int? total,
    String? paymentMethod,
    bool? silverAndNapkin,
    bool? preOrder,
    String? deliveryMethod,
    OrderStatusEnum? status,
    List<OrderPosition>? positionList,
    int? grade,
    DateTime? timeOfDate,
  }) =>
      OrderModel(
          id: this.id,
          orderNumber: this.orderNumber,
          silverAndNapkin: this.silverAndNapkin,
          status: status ?? this.status,
          preOrder: preOrder ?? this.preOrder,
          address: address ?? this.address,
          manufacturerName: manufacturerName ?? this.manufacturerName,
          manufacturerId: manufacturerId,
          total: total ?? this.total,
          paymentMethod: paymentMethod ?? this.paymentMethod,
          deliveryMethod: deliveryMethod ?? this.deliveryMethod,
          positionList: positionList ?? this.positionList,
          // grade: grade ?? this.grade,
          timeOfDate: timeOfDate ?? this.timeOfDate);

  static DateTime? _fromStringToDate(String? value) {
    //2021-04-12T09:37:10.692+0000
    return value != null
        ? DateFormat('yyyy-MM-ddTHH:mm:ssZ').parseUTC(value).toLocal()
        : null;
  }
}
