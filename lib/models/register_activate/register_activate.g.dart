// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_activate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterActivate _$RegisterActivateFromJson(Map<String, dynamic> json) =>
    RegisterActivate(
      phoneNumber: json['phoneNumber'] as String?,
      activationCode: json['activationCode'] as String?,
      isSubscribedToSms: json['isSubscribedToSms'] as bool?,
    );

Map<String, dynamic> _$RegisterActivateToJson(RegisterActivate instance) =>
    <String, dynamic>{
      'phoneNumber': instance.phoneNumber,
      'activationCode': instance.activationCode,
      'isSubscribedToSms': instance.isSubscribedToSms,
    };
