import 'package:home_food/core/enums.dart';
import 'package:home_food/models/address/address.dart';
import 'package:json_annotation/json_annotation.dart';

part 'register_activate.g.dart';

@JsonSerializable(explicitToJson: true)
class RegisterActivate {
  final String? phoneNumber;
  final String? activationCode;
  final bool? isSubscribedToSms;

  RegisterActivate({
    this.phoneNumber,
    this.activationCode,
    this.isSubscribedToSms,
  });

  factory RegisterActivate.fromJson(Map<String, dynamic> json) => _$RegisterActivateFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterActivateToJson(this);
}
