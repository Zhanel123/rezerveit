import 'package:home_food/core/enums.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/dish/uploaded_image.dart';

class UserRegisterRequest {
  final String firstName;
  final String? lastName;
  final String companyName;
  final UploadedImage visitorPhoto;
  final String phoneNumber;
  final UserType userType;
  final Address? address;
  final bool termsOfService;
  final bool privacyPolicy;

  UserRegisterRequest({
    required this.firstName,
    this.lastName,
    required this.companyName,
    required this.visitorPhoto,
    required this.phoneNumber,
    required this.userType,
    this.address,
    required this.termsOfService,
    required this.privacyPolicy,
  });
}
