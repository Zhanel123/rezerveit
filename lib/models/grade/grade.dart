import 'package:home_food/models/profile/profile.dart';

class Grade {
  String? id;
  int? rating;
  String comment;
  Profile? customer;
  String? photoPath;

  Grade({
    this.id,
    this.rating,
    this.customer,
    String? comment,
    this.photoPath,
  }) : comment = comment ?? '';
}
