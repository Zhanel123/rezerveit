import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class City extends Equatable {
  final String id;
  final String nameKz;
  final String nameRu;
  final String code;
  final String timeZone;

  const City({
    String? nameKz,
    String? nameRu,
    String? id,
    String? code,
    String? timeZone,
  })  : id = id ?? '',
        nameKz = nameKz ?? '',
        nameRu = nameRu ?? '',
        code = code ?? '',
        timeZone = timeZone ?? '';

  @override
  List<Object?> get props => [
        id,
        code,
        nameKz,
        nameRu,
        timeZone,
      ];

  String name(BuildContext context) =>
      context.locale.languageCode == 'ru' ? nameRu : nameKz;
}
