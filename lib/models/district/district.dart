import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:home_food/models/city/city.dart';

class District {
  final String id;
  final String nameKz;
  final String nameRu;
  final double? latitude;
  final double? longitude;
  final City? city;

  District({
    String? id,
    String? nameKz,
    String? nameRu,
    this.latitude,
    this.longitude,
    this.city,
  })  : id = id ?? '',
        nameKz = nameKz ?? '',
        nameRu = nameRu ?? '';

  String name(BuildContext context) =>
      context.locale.languageCode == 'ru' ? nameRu : nameKz;
}
