import 'package:home_food/models/chat/chat.dart';
import 'package:home_food/models/chat/chat_message.dart';

class ChatSearchResultModel {
  final List<ChatModel> chats;
  final List<ChatMessageModel> messages;

  ChatSearchResultModel({
    List<ChatModel>? chats,
    List<ChatMessageModel>? messages,
  })  : chats = chats ?? const [],
        messages = messages ?? const [];
}
