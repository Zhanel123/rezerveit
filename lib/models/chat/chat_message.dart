import 'package:home_food/models/dish/uploaded_image.dart';

class ChatMessageModel {
  final String? id;
  final String? chatId;
  final String? authorId;
  final String? status;
  final String? text;
  final DateTime? createdDate;
  final List<UploadedImage> photos;

  ChatMessageModel({
    this.id,
    this.chatId,
    this.authorId,
    this.status,
    this.text,
    this.createdDate,
    List<UploadedImage>? photos,
  }) : photos = photos ?? const [];
}
