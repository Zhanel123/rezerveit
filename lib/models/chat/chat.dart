import 'package:home_food/models/profile/profile.dart';

class ChatModel {
  final String? id;
  final DateTime? lastMessageTime;
  final String lastMessageText;
  final Profile? customer;
  final Profile? manufacturer;

  ChatModel({
    this.id,
    this.lastMessageTime,
    String? lastMessageText,
    this.customer,
    this.manufacturer,
  }) : lastMessageText = lastMessageText ?? '';
}
