import 'package:home_food/core/enums.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/preference.dart';

class DishFilterRequest {
  final int radius;
  final double latitude;
  final double longitude;
  final String searchText;
  final String dishTypeId;
  final List<DishType> notDishTypeIds;
  final List<Preference> foodPreferenceIds;
  final double priceFrom;
  final double priceTo;
  final bool showNew;
  final bool hasSale;
  final bool hasHalfPortion;
  final SortFieldEnum sortBy;
  final bool showPopular;

  DishFilterRequest({
    int? radius,
    double? latitude,
    double? longitude,
    String? searchText,
    String? dishTypeId,
    List<DishType>? notDishTypeIds,
    List<Preference>? foodPreferenceIds,
    double? priceFrom,
    double? priceTo,
    bool? showNew,
    bool? hasSale,
    bool? hasHalfPortion,
    SortFieldEnum? sortBy,
    bool? showPopular,
  })  : radius = radius ?? 1,
        latitude = latitude ?? 43.235835,
        longitude = longitude ?? 76.881514,
        searchText = searchText ?? '',
        dishTypeId = dishTypeId ?? '',
        notDishTypeIds = notDishTypeIds ?? [],
        foodPreferenceIds = foodPreferenceIds ?? [],
        priceFrom = priceFrom ?? 0,
        priceTo = priceTo ?? 0,
        showNew = showNew ?? false,
        hasHalfPortion = hasHalfPortion ?? false,
        hasSale = hasSale ?? false,
        sortBy = sortBy ?? SortFieldEnum.createdDate,
        showPopular = showPopular ?? false;

  DishFilterRequest copyWith({
    int? radius,
    double? latitude,
    double? longitude,
    String? searchText,
    String? dishTypeId,
    List<DishType>? notDishTypeIds,
    List<Preference>? foodPreferenceIds,
    double? priceFrom,
    double? priceTo,
    bool? showNew,
    bool? hasSale,
    bool? hasHalfPortion,
    SortFieldEnum? sortBy,
    bool? showPopular,
  }) =>
      DishFilterRequest(
        radius: radius ?? this.radius,
        latitude: latitude ?? this.latitude,
        longitude: longitude ?? this.longitude,
        searchText: searchText ?? this.searchText,
        dishTypeId: dishTypeId ?? this.dishTypeId,
        notDishTypeIds: notDishTypeIds ?? this.notDishTypeIds,
        foodPreferenceIds: foodPreferenceIds ?? this.foodPreferenceIds,
        priceFrom: priceFrom ?? this.priceFrom,
        priceTo: priceTo ?? this.priceTo,
        showNew: showNew ?? this.showNew,
        hasSale: hasSale ?? this.hasSale,
        hasHalfPortion: hasHalfPortion ?? this.hasHalfPortion,
        sortBy: sortBy ?? this.sortBy,
        showPopular: showPopular ?? this.showPopular,
      );

  DishFilterRequest copyWithDishType({
    String? dishTypeId,
  }) =>
      DishFilterRequest(
        searchText: this.searchText,
        dishTypeId: dishTypeId,
        notDishTypeIds: this.notDishTypeIds,
        foodPreferenceIds: this.foodPreferenceIds,
        priceFrom: this.priceFrom,
        priceTo: this.priceTo,
        showNew: this.showNew,
        hasSale: this.hasSale,
        hasHalfPortion: this.hasHalfPortion,
        sortBy: this.sortBy,
        showPopular: this.showPopular,
      );
}
