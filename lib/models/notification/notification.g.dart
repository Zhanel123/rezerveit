// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocalNotification _$LocalNotificationFromJson(Map<String, dynamic> json) =>
    LocalNotification(
      id: json['id'] as String?,
      title: json['title'] as String?,
      body: json['body'] as String?,
      newsUrl: json['newsUrl'] as String?,
      ruffleId: json['ruffleId'] as String?,
      createdDate: json['createdDate'] == null
          ? null
          : DateTime.parse(json['createdDate'] as String),
      readen: json['readen'] as bool? ?? false,
    );

Map<String, dynamic> _$LocalNotificationToJson(LocalNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'body': instance.body,
      'createdDate': instance.createdDate?.toIso8601String(),
      'newsUrl': instance.newsUrl,
      'ruffleId': instance.ruffleId,
      'readen': instance.readen,
    };
