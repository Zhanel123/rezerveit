import 'package:json_annotation/json_annotation.dart';

part 'notification.g.dart';

@JsonSerializable()
class LocalNotification {
  String? id;
  String? title;
  String? body;
  DateTime? createdDate;
  String? newsUrl;
  String? ruffleId;
  bool readen;

  LocalNotification({
    this.id,
    this.title,
    this.body,
    this.newsUrl,
    this.ruffleId,
    this.createdDate,
    this.readen = false,
  });

  factory LocalNotification.fromJson(Map<String, dynamic> json) =>
      _$LocalNotificationFromJson(json);

  Map<String, dynamic> toJson() => _$LocalNotificationToJson(this);
}

class Notification {
  final String id;
  final DateTime? createdDate;
  final String subject;
  final String content;
  final String type;
  final String objectId;
  final bool wasRead;
  final String visitorId;

  Notification({
    this.createdDate,
    String? id,
    String? subject,
    String? content,
    String? type,
    String? objectId,
    bool? wasRead,
    String? visitorId,
  })  : id = id ?? '',
        subject = subject ?? '',
        content = content ?? '',
        type = type ?? '',
        objectId = objectId ?? '',
        wasRead = wasRead ?? false,
        visitorId = visitorId ?? '';
}
