class StatisticsItem {
  DateTime? startDate;
  DateTime? endDate;
  int totalSum;

  StatisticsItem({
    this.startDate,
    this.endDate,
    int? totalSum,
  }) : totalSum = totalSum ?? 0;
}
