import 'package:home_food/data/dto/dish_dto/mapper/dish_dto_mapper.dart';
import 'package:home_food/data/dto/statistics/top_dish_statistics_dto.dart';
import 'package:home_food/models/statistics/top_dish_statistics.dart';

TopDishStatistics topDishStatisticsMapper(TopDishStatisticsDTO dto) =>
    TopDishStatistics(
      dishCardItem:
          dto.dishCardItem != null ? dishMapper(dto.dishCardItem!) : null,
      totalSum: dto.totalSum,
    );

List<TopDishStatistics> topDishStatisticsListMapper(
        List<TopDishStatisticsDTO> list) =>
    list.map((e) => topDishStatisticsMapper(e)).toList();
