import 'package:home_food/data/dto/statistics/statistics_dto.dart';
import 'package:home_food/data/dto/statistics/statistics_item_dto.dart';
import 'package:home_food/models/statistics/statistics.dart';
import 'package:home_food/models/statistics/statistics_item.dart';

Statistics statisticsMapper(StatisticsDTO dto) => Statistics(
      totalSum: dto.totalSum,
      averageSum: dto.averageSum,
      averagePercent: dto.averagePercent,
      changeTotalSum: dto.changeTotalSum,
      changeTotalSumPercent: dto.changeTotalSumPercent,
      period: dto.period,
      items: dto.items?.map((e) => statisticsItemMapper(e)).toList(),
    );

StatisticsItem statisticsItemMapper(StatisticsItemDTO dto) => StatisticsItem(
      startDate: dto.startDate,
      endDate: dto.endDate,
      totalSum: dto.totalSum,
    );
