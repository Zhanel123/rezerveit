import 'package:equatable/equatable.dart';
import 'package:home_food/models/statistics/statistics_item.dart';

class Statistics extends Equatable {
  final int totalSum;
  final int averageSum;
  final double averagePercent;
  final int changeTotalSum;
  final double changeTotalSumPercent;
  final String period;
  final List<StatisticsItem> items;

  const Statistics({
    int? totalSum,
    int? averageSum,
    double? averagePercent,
    int? changeTotalSum,
    double? changeTotalSumPercent,
    String? period,
    List<StatisticsItem>? items,
  })  : totalSum = totalSum ?? 0,
        averageSum = averageSum ?? 0,
        averagePercent = averagePercent ?? 0,
        changeTotalSum = changeTotalSum ?? 0,
        changeTotalSumPercent = changeTotalSumPercent ?? 0,
        period = period ?? '',
        items = items ?? const [];

  @override
  List<Object?> get props => [
        totalSum,
        averageSum,
        averagePercent,
        changeTotalSum,
        changeTotalSumPercent,
        period,
        items,
      ];
}
