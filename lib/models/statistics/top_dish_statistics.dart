import 'package:home_food/models/dish/dish.dart';

class TopDishStatistics {
  Dish? dishCardItem;
  int totalSum;

  TopDishStatistics({
    this.dishCardItem,
    int? totalSum,
  }) : totalSum = totalSum ?? 0;
}
