import 'dart:convert';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:home_food/models/notification/notification.dart';
import 'package:home_food/notification_module/notifcation_module.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:home_food/utils/shared_pref_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app.dart';

Future<void> _messageHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  RemoteNotification? notification = message.notification;
  if (notification != null) {
    if (notification.title != null) {
      try {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.reload();

        String title = prefs.getString('_title') ?? '[]';
        List<LocalNotification> notifications =
            (json.decode(title) as List? ?? [])
                .map((e) => LocalNotification.fromJson(e))
                .toList();
        notifications.add(
          LocalNotification(
            id: message.messageId,
            title: notification.title,
            body: notification.body,
            newsUrl: message.data['newsUrl'] != null
                ? message.data['newsUrl']
                : null,
            createdDate: DateTime.now(),
          ),
        );

        FlutterAppBadger.updateBadgeCount(
            notifications.where((element) => element.readen).length);
        prefs.setString('_title',
            json.encode(notifications.map((e) => e.toJson()).toList()));
        logger.w(notifications.map((e) => e.toJson()).toList());
      } catch (e) {
        logger.e(e);
      }
    }
    print("Notification received when app in background");
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_messageHandler);

  FirebaseMessaging.instance.getToken().then((value) {
    logger.w(value);
  });
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  Routes.initRoutes();
  await sharedPreference.init();
  await serviceLocatorSetup();
  HttpOverrides.global = MyHttpOverrides();
  runApp(EasyLocalization(
    supportedLocales: const [
      Locale('en'),
      Locale('ru'),
      Locale('kk'),
    ],
    path: 'assets/translations',
    fallbackLocale: Locale('en'),
    child: const MyApp(),
  ));
}

// временно обойти ошибку сертификатов SSL
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
