import 'dart:io';

import 'package:dio/dio.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/chat_dto/chat_dto.dart';
import 'package:home_food/data/dto/chat_dto/chat_message_dto.dart';
import 'package:home_food/data/dto/chat_dto/chat_search_result_dto.dart';
import 'package:home_food/data/dto/order_dto/order_dto.dart';
import 'package:home_food/data/dto/statistics/statistics_dto.dart';
import 'package:home_food/data/dto/statistics/top_dish_statistics_dto.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'chat_api.g.dart';

@singleton
@RestApi()
abstract class ChatApi {
  @factoryMethod
  factory ChatApi(
    Dio dio,
  ) = _ChatApi;

  @GET('/chat/list')
  Future<List<ChatDTO>> getChatList();

  @GET('/chat/{chatId}/messages')
  Future<List<ChatMessageDTO>> getChatMessages({
    @Path('chatId') required String chatId,
  });

  @POST('/chat')
  Future<String> getChatByOrderId({
    @Field() required String orderId,
    @Field() required String manufacturerId,
  });

  @POST('/chat/message')
  Future<String> sendMessage({
    @Field() required String chatId,
    @Field() required String text,
    @Field() List<UploadedImage> photoItems = const [],
  });

  @POST("/chat/message/photo")
  Future<List<UploadedImage>> messageUploadPhotos({
    @Part() required List<File> photos,
  });

  @GET("/chat/search/")
  Future<ChatSearchResultDTO> searchByAllChat({
    @Query("text") String? text = '',
    @Query("isManufacturer") bool? isManufacturer = false,
  });

  @POST('/chat/tech-support')
  Future<String> chatSupport();
}
