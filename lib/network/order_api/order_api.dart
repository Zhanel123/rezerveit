import 'package:dio/dio.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/order_dto/order_dto.dart';
import 'package:home_food/data/dto/statistics/statistics_dto.dart';
import 'package:home_food/data/dto/statistics/top_dish_statistics_dto.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'order_api.g.dart';

@singleton
@RestApi()
abstract class OrderApi {
  @factoryMethod
  factory OrderApi(
    Dio dio,
  ) = _OrderApi;

  /// Получить данные о корзине
  @GET("/order")
  Future<OrderDTO> getPreOrder();

  /// Изменение данных в корзине
  /// [OrderDTO] моделька данных на изменение
  @PUT("/order")
  Future<OrderDTO> updatePreOrder({
    @Body() required OrderDTO body,
  });

  /// Добавление блюда в корзину
  /// [dishCardId] айди блюда
  /// [amount] количество
  @POST("/order/dish")
  Future<String> putDishToPreorder({
    @Query('dishCardId') required String dishCardId,
    @Query('amount') required int amount,
  });

  /// Изменение количества блюда в корзине
  /// [positionId] айди позиции блюда в корзине
  /// [amount] количество
  @PUT("/order/dish")
  Future<String> updateDishCardOrder({
    @Query('positionId') required String positionId,
    @Query('amount') required int amount,
  });

  /// Подтвердить корзину в заказ
  @POST("/order")
  Future<void> createOrder();

  /// Получить список заказов покупателя [UserType.CUSTOMER]
  @GET("/order/list-by-customer")
  Future<Pagination<OrderDTO>> getCustomerOrders({
    @Query('page') int? page = 0,
    @Query('size') int? size = 10,
    @Query('status') String? status,
    @Query('searchText') String? searchText,
  });

  /// Получить список заказов мерчанта [UserType.MANUFACTURER]
  @GET("/order/list-by-manufacture")
  Future<Pagination<OrderDTO>> getMerchantOrders({
    @Query('page') int? page = 0,
    @Query('size') int? size = 10,
    @Query('status') String? status,
    @Query('searchText') String? searchText,
  });

  /// Удалить одно блюдо из корзины
  /// [positionId] позиция блюда в корзине
  @DELETE("/order/dish")
  Future<String> removeDishFromBasket(
    @Query('positionId') String positionId,
  );

  /// Удалить все блюдо из корзины
  @DELETE("/order/dish/all")
  Future<String> removeAllDishCardOrder();

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDERED]
  @PUT("/order/order-accepted")
  Future<String> changeOrderStatusToAccepted({
    @Query('orderId') required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDER_ACCEPTED]
  /// [orderId] айди заказа
  @PUT("/order/preparation")
  Future<String> changeOrderStatusToPreparation({
    @Query('orderId') required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.PREPARATION]
  /// [orderId] айди заказа
  @PUT("/order/delivery")
  Future<String> changeOrderStatusToDelivery({
    @Query('orderId') required String orderId,
  });

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.DELIVERY]
  /// [orderId] айди заказа
  @PUT("/order/delivered")
  Future<String> changeOrderStatusToDelivered({
    @Query('orderId') required String orderId,
  });

  /// Получить заказ по айди
  /// [orderId] айди заказа
  @GET('/order/{orderId}')
  Future<OrderDTO> getOrderById({
    @Path('orderId') required String orderId,
  });

  /// Отменить заказ по айди
  /// Добавить отзыв почему отменил заказ
  @POST("/order/canceled")
  Future<dynamic> cancelOrder({
    @Field() required String orderId,
    @Field() required String comment,
    @Field() required int grade,
    // @Field() required String base64Image,
    // @Field() required int grade,
    // @Field() List<String> tagList = const [],
  });

  /// Получить статистику по продажам
  @GET("/order/statistics")
  Future<StatisticsDTO> getSellStatistics({
    @Query('startDate') required String startDate,
    @Query('endDate') required String endDate,
    @Query('period') required String period,
    @Query('month') int? month,
    @Query('year') int? year,
  });

  /// Получить статистику по топ блюдам
  @GET("/order/statistics/top-dish-cards")
  Future<List<TopDishStatisticsDTO>> getTopDishStatistics({
    @Query('startDate') required String startDate,
    @Query('endDate') required String endDate,
  });

  @POST("/order/complaint")
  Future<String> complaintOrder({
    @Field() required String orderId,
    @Field() required String comment,
    @Field() required String base64Image,
  });
}
