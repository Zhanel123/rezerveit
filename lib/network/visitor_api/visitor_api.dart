import 'package:dio/dio.dart';
import 'package:home_food/data/dto/grade_dto/grade_dto.dart';
import 'package:home_food/data/dto/preference_dto/preference_dto.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'visitor_api.g.dart';

@singleton
@RestApi()
abstract class VisitorApi {
  @factoryMethod
  factory VisitorApi(
    Dio dio,
  ) = _VisitorApi;

  /// Получение списка предпочтений пользователя
  @GET('/visitor/food-preference')
  Future<List<PreferenceDTO>> getUserPreferences();

  /// Изменить список предпочтений пользователя
  @POST('/visitor/food-preference')
  Future<List<String>> updateUserPreferences({
    @Field('list') required List<PreferenceDTO> list,
  });

  /// Изменить токен файрбэйз
  @POST('/visitor/firebase/token')
  Future<String> uploadFirebaseToken({
    @Field('firebaseToken') required String firebaseToken,
  });

  /// Изменить токен файрбэйз
  @GET('/visitor/{merchantId}/grades')
  Future<Pagination<GradeDTO>> getMerchantGrades({
    @Path('merchantId') required String merchantId,
    @Query('page') int page = 0,
    @Query('limit') int limit = 10,
  });
}
