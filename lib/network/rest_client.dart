import 'dart:io';

import 'package:dio/dio.dart';
import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/data/dto/city_dto/city_dto.dart';
import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:home_food/data/dto/dish_filter_request_dto/dish_filter_request_dto.dart';
import 'package:home_food/data/dto/dish_type_dto/dish_type_dto.dart';
import 'package:home_food/data/dto/district_dto/district_dto.dart';
import 'package:home_food/data/dto/edit_food_request/edit_dish_request.dart';
import 'package:home_food/data/dto/favorite_dish/favorite_dish_dto.dart';
import 'package:home_food/data/dto/ingredient_dto/ingredient_dto.dart';
import 'package:home_food/data/dto/merchant_dto/merchant_dto.dart';
import 'package:home_food/data/dto/notification_dto/notification_dto.dart';
import 'package:home_food/data/dto/preference_dto/preference_dto.dart';
import 'package:home_food/data/dto/profile_edit_dto/profile_edit_dto.dart';
import 'package:home_food/data/dto/user_register_request_dto/user_register_request_dto.dart';
import 'package:home_food/models/dish/create_dish_request.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/generated_pass/generated_pass.dart';
import 'package:home_food/models/get_merchants_request/get_merchants_request.dart';
import 'package:home_food/models/pagination/pagination.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/models/register_activate/register_activate.dart';
import 'package:home_food/models/tag/tag.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'rest_client.g.dart';

@singleton
@RestApi()
abstract class RestClient {
  @factoryMethod
  factory RestClient(
    Dio dio,
  ) = _RestClient;

  @POST("/visitor/register")
  Future<String> register(@Body() UserRegisterRequestDTO user);

  @POST("/visitor/activate")
  Future<GeneratedPass> registerActivate(@Body() RegisterActivate body);

  @POST("/visitor/login")
  Future<String> login({
    @Field('phoneNumber') required String phone,
  });

  @POST("/visitor/activation/re-init")
  Future<dynamic> activationReInit({
    @Field('phoneNumber') required String phone,
  });

  @GET("/city/list")
  Future<List<CityDTO>> getCityList();

  @GET('/tag/list')
  Future<List<Tag>> getTagsList();

  @GET("/visitor/info")
  Future<Profile> getUserInfo();

  @POST("/visitor/photo")
  Future<UploadedImage> visitorUploadPhotos({
    @Part() required File photo,
  });

  @POST("/visitor/update")
  Future<Profile> updateProfile({
    @Body() required ProfileEditDTO body,
  });

  /// @Query - это params ?page=0
  @POST("/dish/list-by-filter")
  Future<Pagination<DishDTO>> getDishListByFilter({
    @Body() required DishFilterRequestDTO body,
    @Query('page') int? page = 0,
    @Query('size') int? size = 10,
  });

  /// @Query - это params ?page=0
  @GET("/dish/{id}")
  Future<DishDTO> getDishById({
    @Path('id') required String id,
  });

  @POST("/visitor/manufacture-list")
  Future<Pagination<MerchantDTO>> getMerchantsList(
    @Body() GetMerchantsRequest getMerchantsRequest,
  );

  /// Список карточек еды производителя
  @GET("/dish/list-by-manufacture")
  Future<Pagination<DishDTO>> getMyDishes({
    @Query('dishTypeId') String? dishTypeId,
    @Query('hasHalfPortion') bool? hasHalfPortion,
    @Query('sortField') String? sortField,
    @Query('searchText') String? searchText,
    @Query('page') int? page = 0,
    @Query('size') int? size = 10,
  });

  @POST("/dish")
  Future<dynamic> createDish(
    @Body() CreateFoodRequest body,
  );

  @PUT("/dish/on-moderation/{dishCardId}")
  Future<String> changeDishStatusModerate(
    @Path('dishCardId') String dishCardId,
  );

  @PUT("/dish/deactivation/{dishCardId}")
  Future<String> changeDishStatusDeactivate(
    @Path('dishCardId') String dishCardId,
  );

  @PUT("/dish/activation/{dishCardId}")
  Future<String> activationDishCardById(
    @Path('dishCardId') String dishCardId,
  );

  @POST("/favorite-dish-card")
  Future<String> addDishToFavorite({
    @Query('dishCardId') String? dishCardId = '',
  });

  @GET("/favorite-dish-card")
  Future<Pagination<FavoriteDishDTO>> getFavouriteDishes({
    @Query('page') int? page = 0,
    @Query('size') int? size = 10,
  });

  @DELETE("/favorite-dish-card/all")
  Future<List<String>> deleteAllFavouriteDishes();

  @DELETE("/favorite-dish-card/{id}")
  Future<String> deleteFavouriteDishById({
    @Path('id') required String favDishId,
  });

  @POST("/order/grade")
  Future<dynamic> rateOrder({
    @Field() required String orderId,
    @Field() required String comment,
    @Field() required String base64Image,
    @Field() required int grade,
    @Field() List<String> tagList = const [],
  });

  @GET("/preference/list")
  Future<List<PreferenceDTO>> getPreferencesList();

  @POST("/dish/photo")
  Future<List<UploadedImage>> uploadPhotos({
    @Part() required List<File> photos,
  });

  @GET("/dish-type/list")
  Future<List<DishTypeDTO>> getDishTypes();

  @PUT("/dish")
  Future<dynamic> editDish(
    @Body() EditFoodRequest body,
  );

  @POST("/address")
  Future<String> createAddress(
    @Body() AddressDTO address,
  );

  @GET("/address/list")
  Future<List<AddressDTO>> getAddressList();

  @PUT("/address")
  Future<String> updateAddress(@Body() AddressDTO address);

  //Params - id
  @DELETE("/address/{id}")
  Future<String> deleteAddress({
    @Path('id') required String addressId,
  });

  @GET("/ingredient/list")
  Future<List<IngredientDTO>?> getIngredientList();

  @GET("/district/list")
  Future<List<DistrictDTO>?> getDistrictList();

  /// @Query - это params ?page=0
  @GET("/dish/popular-list")
  Future<Pagination<DishDTO>> getPopularDishList({
    @Query('page') int? page = 0,
    @Query('size') int? size = 10,
  });

  @POST("/dish/list-by-filter/count")
  Future<int> getDishCountWithFilter(
    @Body() DishFilterRequestDTO body,
  );

  @GET("/notification/list")
  Future<Pagination<NotificationDTO>> getNotificationList({
    @Query('page') int? page = 0,
    @Query('size') int? size = 10,
  });

  @GET("/notification/set-was-read-true")
  Future<void> setWasReaden({
    @Query('id') List<String> ids = const [],
  });
}
