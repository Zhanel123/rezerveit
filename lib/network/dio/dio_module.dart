import 'package:dio/dio.dart';
import 'package:home_food/core/env.dart';
import 'package:home_food/network/dio/flavor_module.dart';
import 'package:home_food/network/intercepters/interceptors.dart';
import 'package:home_food/network/intercepters/log_interceptor.dart';
import 'package:injectable/injectable.dart';

@module
abstract class DioModule {
  @lazySingleton
  Dio getDio({
    required Flavor flavor,
  }) {
    final _dio = Dio()
      ..options.baseUrl = flavor.baseApiUrl;
      // ..options.connectTimeout = 30;

    // final _dio = Dio()..options.baseUrl = flavor.baseApiUrl;

    _dio.interceptors.addAll([
      authInterceptors(_dio),
      LoggerInterceptor(
        requestBody: true,
        responseBody: true,
        requestHeader: true,
      ),
    ]);

    return _dio;
  }
}

@singleton
class DioService {
  final Dio dio;

  DioService(this.dio);

  Future<Response<Map<String, dynamic>>> fetch({
    required String url,
    required String method,
    ResponseType responseType = ResponseType.json,
    FormData? data,
    Map<String, dynamic>? queryParameters,
  }) =>
      dio.fetch<Map<String, dynamic>>(
        Options(
          method: method,
          responseType: responseType,
          contentType: 'multipart/form-data',
        )
            .compose(
              dio.options,
              url,
              data: data,
              queryParameters: queryParameters,
            )
            .copyWith(
              baseUrl: restURL,
            ),
      );
}
