import 'package:home_food/core/env.dart';
import 'package:injectable/injectable.dart';

abstract class BaseFlavor {
  String get baseApiUrl;

  bool get enableAnalytics;

  bool get enableLogging;

  String get name;
}

class Flavor implements BaseFlavor {
  @override
  final String baseApiUrl;
  @override
  final bool enableAnalytics;
  @override
  final bool enableLogging;
  @override
  final String name;

  Flavor({
    required this.baseApiUrl,
    this.enableAnalytics = false,
    this.enableLogging = false,
    required this.name,
  });
}

@Injectable(as: Flavor)
class ProdFlavor extends Flavor {
  ProdFlavor()
      : super(
          name: 'PROD',
          baseApiUrl: restURL,
          enableLogging: true,
          enableAnalytics: true,
        );
}
