import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:home_food/utils/shared_pref_util.dart';

refreshTokenInterceptor(Dio dio) {
  int retries = 0;

  RequestOptions getOptions(RequestOptions options) {
    RequestOptions _options = options;

    String? authPair = sharedPreference.authPair;

    if (_options.headers['Authorization'] == null) {
      _options.headers['Authorization'] = 'Basic cmVzdDoxMjM0NQ==';
    }
    if (authPair.isNotEmpty) {
      _options.headers['AUTH-PAIR'] = authPair;
    }
    if (_options.headers['Content-Type'] != null) {
      _options.headers['Content-Type'] = 'application/json';
    }
    if (_options.headers['Accept'] != null) {
      _options.headers['Accept'] = 'application/json';
    }

    return _options;
  }

  return QueuedInterceptorsWrapper(
    onRequest: (options, handler) {
      return handler.next(getOptions(options));
    },
    onResponse: (response, handler) {
      retries = 0;
      return handler.next(response);
    },
    onError: (error, handler) async {
      // logger.i(error.response);
      return handler.next(error);
    },
  );
}

extension _AsOptions on RequestOptions {
  Options asOptions() {
    return Options(
      method: method,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
      extra: extra,
      headers: headers,
      responseType: responseType,
      contentType: contentType,
      validateStatus: validateStatus,
      receiveDataWhenStatusError: receiveDataWhenStatusError,
      followRedirects: followRedirects,
      maxRedirects: maxRedirects,
      requestEncoder: requestEncoder,
      responseDecoder: responseDecoder,
      listFormat: listFormat,
    );
  }
}
