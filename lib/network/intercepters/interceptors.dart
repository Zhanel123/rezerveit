import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:home_food/data/dto/oauth_dto/oauth_dto.dart';
import 'package:home_food/models/profile/oauth.dart';
import 'package:home_food/network/refresh_token_client.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:home_food/utils/shared_pref_util.dart';

authInterceptors(Dio dio) {
  int retries = 0;

  Future<RequestOptions> getOptions(RequestOptions options) async {
    RequestOptions _options = options;
    // final authStore = serviceLocator<AuthStore>();
    String? appCheckToken;

    String accessToken = sharedPreference.accessToken;
    await sharedPreference.reload();
    String authPair = sharedPreference.authPair;
    String _AuthHeader = _options.headers['Authorization'] ?? '';

    if (accessToken.isNotEmpty) {
      _options.headers['Authorization'] = 'Bearer $accessToken';
    }
    if (authPair.isNotEmpty) {
      _options.headers['AUTH-PAIR'] = authPair;
    }
    if (_options.headers['Content-Type'] == null) {
      _options.headers['Content-Type'] = 'application/json';
    }
    if (_options.headers['Accept'] != null) {
      _options.headers['Accept'] = 'application/json';
    }

    return _options;
  }

  return InterceptorsWrapper(
    onRequest: (options, handler) async {
      return handler.next(await getOptions(options));
    },
    onResponse: (response, handler) {
      retries = 0;
      return handler.next(response);
    },
    onError: (error, handler) async {
      if (error.response?.statusCode == 401 && retries <= 3) {
        retries += 1;
        try {
          debugPrint('TRY REFRESH TOKEN');
          await refreshToken();
          final response = await retry(dio, error.requestOptions);
          retries = 0;
          return handler.resolve(response);
        } on DioError {
          return handler.next(error);
        }
      } else if (error.response?.statusCode == 401) {
        debugPrint('3 TRIES LOGOUT');
        retries = 0;
        sharedPreference.logout();
        return handler.next(error);
      } else {
        retries = 0;
        return handler.next(error);
      }
    },
  );
}

extension _AsOptions on RequestOptions {
  Options asOptions() {
    return Options(
      method: method,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
      extra: extra,
      headers: headers,
      responseType: responseType,
      contentType: contentType,
      validateStatus: validateStatus,
      receiveDataWhenStatusError: receiveDataWhenStatusError,
      followRedirects: followRedirects,
      maxRedirects: maxRedirects,
      requestEncoder: requestEncoder,
      responseDecoder: responseDecoder,
      listFormat: listFormat,
    );
  }
}

Future<Response<dynamic>> retry(
  Dio dio,
  RequestOptions requestOptions,
) async {
  final options = new Options(
    method: requestOptions.method,
    headers: requestOptions.headers,
  );
  return dio.request<dynamic>(requestOptions.path,
      data: requestOptions.data,
      queryParameters: requestOptions.queryParameters,
      options: options);
}

Future<void> refreshToken() async {
  try {
    final authPairEncoded = sharedPreference.authPair;

    if (authPairEncoded.isEmpty) {
      throw Exception('No Auth-Pair');
    }

    OAuthDTO response = await serviceLocator<RefreshTokenClient>().authorize(
      username: 'rest',
      password: '12345',
      token: 'Basic cmVzdDoxMjM0NQ==',
    );

    if (response.accessToken != null) {
      sharedPreference.setAccessToken(response.accessToken!);
    }

    final userResponse = await serviceLocator<RefreshTokenClient>().getUserInfo(
      authPair: authPairEncoded,
      token: 'Bearer ${sharedPreference.accessToken}',
    );
    logger.w(userResponse);
  } catch (e) {
    logger.e(e);
  }
}
