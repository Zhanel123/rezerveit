import 'package:dio/dio.dart';
import 'package:home_food/data/dto/mapbox_dto/mapbox_retrieve_directions_response.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'mapbox_api.g.dart';

@singleton
@RestApi(baseUrl: 'https://api.mapbox.com/directions/v5/mapbox')
abstract class MapboxApi {
  @factoryMethod
  factory MapboxApi(
    Dio dio,
  ) = _MapboxApi;

  @POST(
    "/{profile}/{sourceLongitude},{sourceLatitude};{destinationLongitude},{destinationLatitude}",
  )
  Future<MapboxRetrieveDirectionsResponse> retrieveDirections({
    @Path('profile') required String routeProfile,
    @Path('sourceLongitude') required double sourceLongitude,
    @Path('sourceLatitude') required double sourceLatitude,
    @Path('destinationLongitude') required double destinationLongitude,
    @Path('destinationLatitude') required double destinationLatitude,
    @Query('alternatives') bool alternatives = true,
    @Query('geometries') String geometries = 'geojson',
    @Query('language') String language = 'en',
    @Query('overview') String overview = 'simplified',
    @Query('steps') bool steps = true,
    @Query('access_token')
        String accessToken = const String.fromEnvironment("ACCESS_TOKEN"),
  });
}
