// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mapbox_api.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps,no_leading_underscores_for_local_identifiers

class _MapboxApi implements MapboxApi {
  _MapboxApi(
    this._dio, {
    this.baseUrl,
  }) {
    baseUrl ??= 'https://api.mapbox.com/directions/v5/mapbox';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<MapboxRetrieveDirectionsResponse> retrieveDirections({
    required routeProfile,
    required sourceLongitude,
    required sourceLatitude,
    required destinationLongitude,
    required destinationLatitude,
    alternatives = true,
    geometries = 'geojson',
    language = 'en',
    overview = 'simplified',
    steps = true,
    accessToken = const String.fromEnvironment("ACCESS_TOKEN"),
  }) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'alternatives': alternatives,
      r'geometries': geometries,
      r'language': language,
      r'overview': overview,
      r'steps': steps,
      r'access_token': accessToken,
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<MapboxRetrieveDirectionsResponse>(Options(
      method: 'POST',
      headers: _headers,
      extra: _extra,
    )
            .compose(
              _dio.options,
              '/${routeProfile}/${sourceLongitude},${sourceLatitude};${destinationLongitude},${destinationLatitude}',
              queryParameters: queryParameters,
              data: _data,
            )
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = MapboxRetrieveDirectionsResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
