import 'package:dio/dio.dart';
import 'package:home_food/core/env.dart';
import 'package:home_food/data/dto/oauth_dto/oauth_dto.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';

import '../models/profile/oauth.dart';

part 'refresh_token_client.g.dart';

@RestApi(baseUrl: baseURL)
abstract class RefreshTokenClient {
  @factoryMethod
  factory RefreshTokenClient(Dio dio) = _RefreshTokenClient;

  @POST('/oauth/token')
  Future<OAuthDTO> authorize({
    @Query('grant_type') String grantType = 'password',
    @Query('username') required String username,
    @Query('password') required String password,
    @Header('Authorization') required String token,
  });

  @GET('/rest/visitor/info')
  Future<Profile> getUserInfo({
    @Header('AUTH-PAIR') required String authPair,
    @Header('Authorization') required String token,
  });
}
