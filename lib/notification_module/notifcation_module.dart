import 'dart:convert';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/data/repositories/order_repository/order_repository.dart';
import 'package:home_food/models/notification/notification.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/order/order_screen.dart';
import 'package:home_food/service_locator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher_string.dart';

AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_channel',
  'High Importance Notification',
  description: "This channel is for important notification",
  importance: Importance.max,
);
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class NotificationListenerProvider {
  static List<LocalNotification> localNotifications = [];

  static void getMessage() {
    FirebaseMessaging.instance.requestPermission();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;

      AppleNotification? appleNotification = notification?.apple;
      AndroidNotification? androidNotification = notification?.android;

      if (notification != null &&
          androidNotification != null &&
          Platform.isAndroid) {
        ///Show local notification
        sendNotification(
          title: notification.title!,
          body: notification.body,
          payload: jsonEncode(message.data),
        );
        FlutterAppBadger.updateBadgeCount(1);
      }

      serviceLocator<CustomerOrderCubit>().getCustomerOrders();
      serviceLocator<MerchantOrdersCubit>().getMerchantOrders();

      saveNotification(notification, message);
      // _getPayloadAndNavigate(message.data);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage event) {
      RemoteNotification? notification = event.notification;
      FlutterAppBadger.removeBadge();
      if (notification != null) {
        sendNotification(
            title: notification.title!,
            body: notification.body,
            payload: jsonEncode(event.data));
      }
      saveNotification(notification, event);
      getPayloadAndNavigate(event.data);
    });

    FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  static checkForInitialMessage() async {
    RemoteMessage? event;
    await Firebase.initializeApp();
    RemoteNotification? notification = event?.notification!;
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      if (Platform.isIOS) {
        saveNotification(
          initialMessage.notification,
          initialMessage,
        );
      }
      getPayloadAndNavigate(initialMessage.data);
    }
  }

  static Future<void> fetchFromLocalStorage() async {
    final localStorage = await SharedPreferences.getInstance()
      ..reload();
    List<String> notifications =
        localStorage.getStringList('notifications') ?? [];
    localNotifications = notifications
        .map((e) => LocalNotification.fromJson(json.decode(e)))
        .toList();
  }

  static Future<void> saveNotification(
      RemoteNotification? notification, RemoteMessage? message) async {
    await fetchFromLocalStorage();
    localNotifications.add(LocalNotification(
        title: notification?.title,
        body: notification?.body,
        newsUrl: message?.data['newsUrl'],
        ruffleId: message?.data['ruffleId'],
        createdDate: DateTime.now()));

    final localStorage = await SharedPreferences.getInstance()
      ..reload();

    localStorage.setStringList('notifications',
        localNotifications.map((e) => json.encode(e.toJson())).toList());
  }

  static void getPayloadAndNavigate(Map<String, dynamic> data) async {
    if (data['newsUrl'] != null && (data['newsUrl'] as String).isNotEmpty) {
      launchUrlString(data['newsUrl']);
    }
    if (data.containsKey('orderId')) {
      OrderModel order = await serviceLocator<OrderRepository>()
          .getOrderById(orderId: data['orderId']);
      Routes.router.navigate(
        Routes.orderScreen,
        args: OrderScreenArgs(order),
      );
    }
  }
}

void sendNotification({String? title, String? body, String? payload}) async {
  ////Set the settings for various platform
  // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('@mipmap/ic_launcher');
  const DarwinInitializationSettings initializationSettingsIOS =
      DarwinInitializationSettings(
    requestAlertPermission: true,
    requestBadgePermission: true,
    requestSoundPermission: true,
  );

  const InitializationSettings initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsIOS,
  );
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onDidReceiveNotificationResponse: (notification) async {
    if (notification.payload != null) {
      NotificationListenerProvider.getPayloadAndNavigate(
          jsonDecode(notification.payload!));
    }
  });

  /// TODO временно
  serviceLocator<MerchantOrdersCubit>().getMerchantOrders();
  serviceLocator<CustomerOrderCubit>().getCustomerOrders();

  flutterLocalNotificationsPlugin.show(
    0,
    title,
    body,
    NotificationDetails(
      android: AndroidNotificationDetails(channel.id, channel.name,
          channelDescription: channel.description),
      iOS: const DarwinNotificationDetails(
        presentAlert: true,
        presentBadge: true,
        presentSound: true,
      ),
    ),
    payload: payload,
  );
}
