import 'dart:ui';

import 'package:home_food/core/app_assets.dart';

enum UserType {
  CUSTOMER,
  MANUFACTURER,
}

enum AddressType {
  MANUFACTURER_ADDRESS,
  CUSTOMER_ADDRESS,
}

enum PaymentTypeEnum {
  CASH,
  CARD,
  TRANSFER_BY_PHONE,
}

enum PeriodTypeEnum {
  MONTH,
  BETWEEN,
  DAY,
}

enum WeightTypeEnum {
  gram,
  kilograms,
}

enum CustomerOrderStatusEnum {
  ALL,
  ACTIVE,
  COMPLETED,
}

enum OrderStatusEnum {
  ALL,
  PENDING,
  ORDERED,
  ORDER_ACCEPTED,
  PREPARATION,
  DELIVERY,
  DELIVERED,
  REFUND,
  CANCELED,
}

extension OrderStatusEnumExt on OrderStatusEnum? {
  static const keyMap = {
    OrderStatusEnum.ALL: "",
    OrderStatusEnum.PENDING: "PENDING",
    OrderStatusEnum.ORDERED: "ORDERED",
    OrderStatusEnum.ORDER_ACCEPTED: "ORDER_ACCEPTED",
    OrderStatusEnum.PREPARATION: "PREPARATION",
    OrderStatusEnum.DELIVERY: "DELIVERY",
    OrderStatusEnum.DELIVERED: "DELIVERED",
    OrderStatusEnum.REFUND: "REFUND",
    OrderStatusEnum.CANCELED: "CANCELED",
  };

  static const labelMap = {
    OrderStatusEnum.ALL: "Все",
    //Принятие убрать надо у мерчанта на странице заказов
    OrderStatusEnum.PENDING: "Принятие",
    OrderStatusEnum.ORDERED: "Заказан",
    OrderStatusEnum.ORDER_ACCEPTED: "Принят",
    //Вместо Готовка надо поставить В работе
    OrderStatusEnum.PREPARATION: "Готовка",
    OrderStatusEnum.DELIVERY: "Доставка",
    OrderStatusEnum.DELIVERED: "Доставлен",
    OrderStatusEnum.REFUND: "Возврат",
    OrderStatusEnum.CANCELED: "Отменено",
  };

  static const colorMap = {
    OrderStatusEnum.ALL: Color(0xFF37B023),
    OrderStatusEnum.PREPARATION: Color(0xFF37B023),
    OrderStatusEnum.DELIVERY: Color(0xFF37B023),
    OrderStatusEnum.DELIVERED: Color(0xFF37B023),
    OrderStatusEnum.PENDING: Color(0xFF37B023),
    OrderStatusEnum.ORDERED: Color(0xFF37B023),
    OrderStatusEnum.REFUND: Color(0xFFFF0000),
    OrderStatusEnum.CANCELED: Color(0xFFFF0000),
  };

  String? get key => keyMap[this];

  String? get label => labelMap[this];

  Color? get color => colorMap[this];
}

extension CustomerOrderStatusEnumExt on CustomerOrderStatusEnum? {
  static const keyMap = {
    CustomerOrderStatusEnum.ALL: "",
    CustomerOrderStatusEnum.ACTIVE: "ACTIVE",
    CustomerOrderStatusEnum.COMPLETED: "COMPLETED",
  };

  static const labelMap = {
    CustomerOrderStatusEnum.ALL: "Все",
    CustomerOrderStatusEnum.ACTIVE: "Активные",
    CustomerOrderStatusEnum.COMPLETED: "Завершенные",
  };

  static const colorMap = {
    CustomerOrderStatusEnum.ALL: Color(0xFF37B023),
    CustomerOrderStatusEnum.ACTIVE: Color(0xFF37B023),
    CustomerOrderStatusEnum.COMPLETED: Color(0xFFFF0000),
  };

  String? get key => keyMap[this];

  String? get label => labelMap[this];

  Color? get color => colorMap[this];
}

enum DishStatusEnum {
  ALL,
  ACTIVE,
  PUBLISHED,
  NEW,
  ON_MODERATION,
  DEACTIVATED,
}

extension DishStatusEnumExt on DishStatusEnum? {
  static const keyMap = {
    DishStatusEnum.ALL: "",
    DishStatusEnum.ACTIVE: "ACTIVE",
    DishStatusEnum.PUBLISHED: "PUBLISHED",
    DishStatusEnum.NEW: "NEW",
    DishStatusEnum.ON_MODERATION: "ON_MODERATION",
    DishStatusEnum.DEACTIVATED: "DEACTIVATED",
  };

  static const labelMap = {
    DishStatusEnum.ALL: "Все",
    DishStatusEnum.NEW: "Новый",
    DishStatusEnum.ACTIVE: "Активный",
    DishStatusEnum.ON_MODERATION: "На модерации",
    DishStatusEnum.PUBLISHED: "Опубликованный",
    DishStatusEnum.DEACTIVATED: "Деактивирован",
  };

  static const colorMap = {
    DishStatusEnum.ALL: Color(0xFF37B023),
    DishStatusEnum.NEW: Color(0xFF37B023),
    DishStatusEnum.ACTIVE: Color(0xFF37B023),
    DishStatusEnum.PUBLISHED: Color(0xFF37B023),
    DishStatusEnum.ON_MODERATION: Color(0xFF008EFF),
    DishStatusEnum.DEACTIVATED: Color(0xFFFF000F),
  };

  String? get key => keyMap[this];

  String? get label => labelMap[this];

  Color? get color => colorMap[this];
}

enum NavBarItemType {
  home,
  merchants_list,
  favourite,
  orders,
  profile,
  merchant_home,
  merchant_orders,
  merchant_foods,
  merchant_add_food,
  merchant_profile
}

enum CategoryTypeEnum {
  first_meal,
  main_dishes,
  salads,
  drinks,
}

enum SpecialTypeEnum { vegan, horsemeat, beef, hen }

extension NavBarItemTypeExt on NavBarItemType? {
  static const keyMap = {
    NavBarItemType.home: "home",
    NavBarItemType.merchants_list: "merchants_list",
    NavBarItemType.favourite: "selected",
    NavBarItemType.orders: "orders",
    NavBarItemType.profile: "profile",
    NavBarItemType.merchant_home: "merchant_home",
    NavBarItemType.merchant_orders: "merchant_orders",
    NavBarItemType.merchant_add_food: "merchant_add_food",
    NavBarItemType.merchant_foods: "merchant_foods",
    NavBarItemType.merchant_profile: "merchant_profile",
  };

  static const assetMap = {
    NavBarItemType.home: AppAssets.home_icon,
    NavBarItemType.merchants_list: AppAssets.merchants_list,
    NavBarItemType.favourite: AppAssets.selected_icon,
    NavBarItemType.orders: AppAssets.orders_icon,
    NavBarItemType.profile: AppAssets.profile_icon,
    NavBarItemType.merchant_home: AppAssets.merchant_home_icon,
    NavBarItemType.merchant_orders: AppAssets.orders_icon,
    NavBarItemType.merchant_add_food: AppAssets.merchant_add_food_icon,
    NavBarItemType.merchant_foods: AppAssets.merchant_foods_icon,
    NavBarItemType.merchant_profile: AppAssets.profile_icon,
  };

  String? get key => keyMap[this];

  String? get asset => assetMap[this];
}

extension PaymentTypeEnumExt on PaymentTypeEnum? {
  static const keyMap = {
    PaymentTypeEnum.CASH: "CASH",
    PaymentTypeEnum.CARD: "CARD",
  };

  // название на русском
  static const valueMap = {
    PaymentTypeEnum.CASH: "Наличными",
    PaymentTypeEnum.CARD: "Перевод на карту",
  };

  // иконка
  static const assetMap = {
    PaymentTypeEnum.CASH: AppAssets.visa_icon,
    // PaymentTypeEnum.CARD: AppAssets.visa_icon,
    PaymentTypeEnum.TRANSFER_BY_PHONE: AppAssets.visa_icon,
  };

  String? get value => valueMap[this!];

  String? get asset => assetMap[this!];

  String? get key => keyMap[this];
}

extension PeriodTypeEnumExt on PeriodTypeEnum? {
  static const keyMap = {
    PeriodTypeEnum.MONTH: "MONTH",
    PeriodTypeEnum.BETWEEN: "BETWEEN",
    PeriodTypeEnum.DAY: "DAY",
  };

  // название на русском
  static const valueMap = {
    PeriodTypeEnum.MONTH: "Последний  месяц",
    PeriodTypeEnum.BETWEEN: "Последняя неделя",
    PeriodTypeEnum.DAY: "Сегодня",
  };

  String? get value => valueMap[this!];

  String? get key => keyMap[this];
}

PaymentTypeEnum t = PaymentTypeEnum.CARD;

extension CategoryTypeEnumExt on CategoryTypeEnum? {
  static const keyMap = {
    CategoryTypeEnum.first_meal: "first_meal",
    CategoryTypeEnum.main_dishes: "main_dishes",
    CategoryTypeEnum.salads: "salads",
    CategoryTypeEnum.drinks: "drinks",
  };

  // название на русском
  static const valueMap = {
    CategoryTypeEnum.first_meal: "Первые блюда",
    CategoryTypeEnum.main_dishes: "Вторые блюда",
    CategoryTypeEnum.salads: "Салаты",
    CategoryTypeEnum.drinks: "Напитки",
  };

  String? get value => valueMap[this!];

  String? get key => keyMap[this];
}

extension WeightTypeEnumExt on WeightTypeEnum? {
  static const keyMap = {
    WeightTypeEnum.gram: "gram",
    WeightTypeEnum.kilograms: "kilograms",
  };

  // название на русском
  static const valueMap = {
    WeightTypeEnum.gram: "г",
    WeightTypeEnum.kilograms: "кг",
  };

  String? get value => valueMap[this!];

  String? get key => keyMap[this];
}

extension SpecialTypeEnumExt on SpecialTypeEnum? {
  static const keyMap = {
    SpecialTypeEnum.vegan: 'Vegan',
    SpecialTypeEnum.horsemeat: 'Horsemeat',
    SpecialTypeEnum.beef: 'Beef',
    SpecialTypeEnum.hen: 'Hen',
  };

  // название на русском
  static const valueMap = {
    SpecialTypeEnum.vegan: 'Vegan',
    SpecialTypeEnum.horsemeat: 'Конина',
    SpecialTypeEnum.beef: 'Говядина',
    SpecialTypeEnum.hen: 'Курица',
  };

  String? get value => valueMap[this!];

  String? get key => keyMap[this];
}

enum SortFieldEnum {
  createdDate,
  buyCounter,
}

extension SortFieldEnumExt on SortFieldEnum? {
  static const keyMap = {
    SortFieldEnum.createdDate: 'createdDate',
    SortFieldEnum.buyCounter: 'buyCounter',
  };

  // название на русском
  static const valueMap = {
    SortFieldEnum.createdDate: 'createdDate',
    SortFieldEnum.buyCounter: 'buyCounter',
  };

  String? get value => valueMap[this!];

  String? get key => keyMap[this];
}

enum MapboxRoutingProfile {
  walking,
  cycling,
  driving,
  drivingTraffic,
}

extension MapboxRoutingProfileExt on MapboxRoutingProfile? {
  static const keyMap = {
    MapboxRoutingProfile.walking: 'walking',
    MapboxRoutingProfile.cycling: 'cycling',
    MapboxRoutingProfile.driving: 'driving',
    MapboxRoutingProfile.drivingTraffic: 'driving-traffic',
  };

  static const valueMap = {
    MapboxRoutingProfile.walking: 'walking',
    MapboxRoutingProfile.cycling: 'cycling',
    MapboxRoutingProfile.driving: 'driving',
    MapboxRoutingProfile.drivingTraffic: 'drivingTraffic',
  };

  String? get value => valueMap[this!];

  String? get key => keyMap[this];
}
