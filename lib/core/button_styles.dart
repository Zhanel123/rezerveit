import 'package:flutter/material.dart';

ButtonStyle buttonWhiteRadius8 = ButtonStyle(
  backgroundColor: MaterialStatePropertyAll<Color>(Colors.white),
  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
    RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8.0),
    )
  )
);