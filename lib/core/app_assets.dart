class AppAssets {
  AppAssets._();

  static const String main_logo = 'assets/icons/main_logo.svg';
  static const String notification_icon = 'assets/icons/notification_icon.svg';
  static const String stroke = 'assets/icons/stroke.svg';
  static const String arrow_right = 'assets/icons/arrow_right.svg';
  static const String arrow_left = 'assets/icons/arrow_left.svg';
  static const String map_icon = 'assets/icons/map_icon.svg';
  static const String like_inactive = 'assets/icons/like_inactive.svg';
  static const String like_active = 'assets/icons/like_active.svg';
  static const String flame_icon = 'assets/icons/flame_icon.svg';
  static const String basket_logo = 'assets/icons/basket_icon.svg';
  static const String filter_logo = 'assets/icons/filter_logo.svg';
  static const String app_bar_logo = 'assets/icons/app_bar_logo.svg';

  static const String hot_icon = 'assets/icons/hot_icon.svg';
  static const String green_dot = 'assets/icons/green_dot.svg';
  static const String new_orders_icon = 'assets/icons/new_orders_icon.svg';
  static const String basketIconWhite = 'assets/icons/basket_icon_white.svg';
  static const String basketIcon = 'assets/icons/basket_icon.svg';
  static const String message_icon = 'assets/icons/message_icon.svg';
  static const String drop_down_icon = 'assets/icons/drop_down_icon.svg';

  static const String onboarding_1 = 'assets/images/onboarding_1.png';
  static const String onboarding_2 = 'assets/images/onboarding_2.png';
  static const String onboarding_3 = 'assets/images/onboarding_3.png';

  static const String provider_1 = 'assets/images/provider_1.png';
  static const String provider_2 = 'assets/images/provider_2.png';
  static const String slider_1 = 'assets/images/slider_1.png';
  static const String hard_rock = 'assets/images/hard_rock.png';

  static const String shop_1 = 'assets/images/shop_1.png';
  static const String shop_2 = 'assets/images/shop_2.png';
  static const String shop_3 = 'assets/images/shop_3.png';
  static const String shop_4 = 'assets/images/shop_4.png';
  static const String shop_5 = 'assets/images/shop_5.png';
  static const String shop_6 = 'assets/images/shop_6.png';
  static const String address_map = 'assets/images/address_map.png';

  static const String food_1 = 'assets/images/food_1.png';
  static const String food_2 = 'assets/images/food_2.png';
  static const String food_3 = 'assets/images/food_3.png';

  static const String food_item_1 = 'assets/images/food_item_1.png';
  static const String food_item_2 = 'assets/images/food_item_2.png';
  static const String food_item_3 = 'assets/images/food_item_3.png';
  static const String food_item_4 = 'assets/images/food_item_4.png';
  static const String food_item_5 = 'assets/images/food_item_5.png';

  static const String cart_item_1 = 'assets/images/cart_item_1.png';
  static const String cart_item_2 = 'assets/images/cart_item_2.png';

  static const String hardrock_icon = 'assets/images/hardRock_icon.png';
  static const String arrow_to_down = 'assets/icons/arrow_to_down.svg';
  static const String plus_icon = 'assets/icons/plus_icon.svg';
  static const String minus_icon = 'assets/icons/minus_icon.svg';
  static const String text_icon = 'assets/icons/text_icon.svg';
  static const String star_zero = 'assets/icons/star_zero.svg';
  static const String stars = 'assets/icons/stars.svg';
  static const String star = 'assets/icons/star.svg';

  static const String recently_1 = 'assets/images/recently_1.png';

  static const String master_card_icon = 'assets/icons/master_card_icon.svg';
  static const String visa_icon = 'assets/icons/visa_icon.svg';
  static const String copy_icon = 'assets/icons/copy_icon.svg';
  static const String money_icon = 'assets/icons/money_icon.svg';
  static const String star_icon = 'assets/icons/star_icon.svg';
  static const String review_star_icon = 'assets/icons/review_star_icon.svg';
  static const String upload_photo_icon = 'assets/icons/upload_photo_icon.svg';

  static const String home_icon = 'assets/icons/home_icon.svg';
  static const String merchants_list = 'assets/icons/preferences_icon.svg';
  static const String selected_icon = 'assets/icons/selected_icon.svg';
  static const String orders_icon = 'assets/icons/orders_icon.svg';
  static const String profile_icon = 'assets/icons/profile_icon.svg';

  static const String chat_hard_rock_icon =
      'assets/images/chat_hard_rock_icon.png';
  static const String chat_icon = 'assets/icons/chat_icon.svg';
  static const String home_food_icon = 'assets/images/home_food_icon.png';

  static const String add_button_icon = 'assets/icons/add_button_icon.svg';

  static const String merchant_home_icon =
      'assets/icons/merchant_home_icon.svg';
  static const String merchant_add_food_icon =
      'assets/icons/merchant_add_food_icon.svg';
  static const String merchant_foods_icon =
      'assets/icons/merchant_foods_icon.svg';
  static const String coffee_icon = 'assets/icons/coffee_icon.svg';

  // static const String coffee_icon = 'assets/icons/coffee_icon.svg';
  static const String question_icon = 'assets/icons/question_icon.svg';
  static const String merchant_photo_arrow_left =
      'assets/icons/merchant_photo_arrow_left.svg';
  static const String merchant_photo_arrow_right =
      'assets/icons/merchant_photo_arrow_right.svg';
  static const String merchant_food_settings =
      'assets/icons/merchant_food_settings.svg';
  static const String merchant_pink_arrow_right =
      'assets/icons/merchant_pink_arrow_right.svg';
  static const String merchant_grey_settings_icon =
      'assets/icons/merchant_grey_settings_icon.svg';
  static const String count_ellipse = 'assets/icons/count_ellipse.svg';

  static const String requirement_img = 'assets/images/requirement_img.png';
  static const String merchant_food_1 = 'assets/images/merchant_food_1.png';
  static const String return_dish = 'assets/images/return_dish.png';

  static const String add_to_cart_icon = 'assets/icons/add_to_cart_icon.svg';
  static const String merchant_like_icon = 'assets/icons/merchant_like_icon.svg';
  static const String merchant_call_icon = 'assets/icons/merchant_call_icon.svg';
  static const String question_icon_black = 'assets/icons/question_icon_black.svg';
  static const String question_icon_white = 'assets/icons/question_icon_white.svg';


  static const String address_map_icon = 'assets/icons/address_map_icon.svg';
  static const String phone_icon = 'assets/icons/phone_icon.svg';
  static const String parking_icon = 'assets/icons/parking_icon.svg';
}
