import 'package:home_food/models/dish/create_dish_request.dart';

const String baseURL = "https://4eca-87-255-201-182.ngrok-free.app";
const String restURL = "$baseURL/rest";
const String mapBoxAccessToken =
    "pk.eyJ1IjoidmFuZGVydmFpeiIsImEiOiJjbGRpbTJvcHEwOHR4M25td203cDA0aXdrIn0.mP_I0HQ84N1-SLmgnU6XUQ";
const String mapBoxStyleId = 'clg0kqrt0002s01o31xersir2';
