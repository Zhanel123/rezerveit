import 'package:flutter/material.dart';

import 'app_colors.dart';

///
///
/// text{Жирность}Size{размер}{цвет}
///
///

const TextStyle text700Size15White = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w700,
  fontSize: 15,
);

const TextStyle text700Size15Black = TextStyle(
  fontSize: 15,
  fontWeight: FontWeight.w700,
  color: Colors.black,
);

const TextStyle text700Size15BlackAccent = TextStyle(
  fontSize: 15,
  fontWeight: FontWeight.w700,
  color: Color(0x80000000),
);

const TextStyle text700Size15Green = TextStyle(
  fontSize: 15,
  fontWeight: FontWeight.w700,
  color: Color(0xff37B023),
);

const TextStyle text400Size15Black = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: 15,
  color: Colors.black,
);

const TextStyle text400Size9Pink = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: 9,
  color: AppColors.pinkColor,
);

const TextStyle text400Size15Pink = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: 15,
  color: AppColors.pinkColor,
);

const TextStyle text700Size12Black =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 12);

const TextStyle text700Size24Black =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24);

const TextStyle text700Size12White =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 12);

const TextStyle text400Size13Black = TextStyle(
    color: Color(0xff000000), fontWeight: FontWeight.w400, fontSize: 13);

const TextStyle text400Size11Black = TextStyle(
    color: Color(0xff393939), fontWeight: FontWeight.w400, fontSize: 11);

const TextStyle text400Size13White =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w400, fontSize: 13);

const TextStyle text400Size13GreyBlack = TextStyle(
    color: AppColors.greyBlackColor, fontWeight: FontWeight.w400, fontSize: 13);

const TextStyle text400Size13Grey3 = TextStyle(
    color: AppColors.greyColor3, fontWeight: FontWeight.w400, fontSize: 13);

const TextStyle text700Size18Black = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w700,
  color: Colors.black,
);

const TextStyle text700Size18Pink = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w700,
  color: AppColors.pinkColor,
);

const TextStyle text600Size16White = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.w600,
  color: Colors.white,
);

const TextStyle text600Size12White = TextStyle(
  fontSize: 12,
  fontWeight: FontWeight.w600,
  color: Colors.white,
);

const TextStyle text700Size12Pink = TextStyle(
    fontSize: 12, fontWeight: FontWeight.w700, color: AppColors.pinkColor);

const TextStyle text700Size15Pink = TextStyle(
    fontSize: 15, fontWeight: FontWeight.w700, color: AppColors.pinkColor);

const TextStyle text400Size13Grey = TextStyle(
    fontSize: 13, fontWeight: FontWeight.w400, color: Color(0xff939393));

const TextStyle text400Size13Green = TextStyle(
    fontSize: 13, fontWeight: FontWeight.w400, color: Color(0xff37B023));

const TextStyle text400Size12Green = TextStyle(
    fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xff37B023));

const TextStyle text400Size12Pink = TextStyle(
    fontSize: 12, fontWeight: FontWeight.w400, color: AppColors.pinkColor);

const TextStyle text500Size16Pink = TextStyle(
    fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.pinkColor);

const TextStyle text400Size12PinkItalic = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.pinkColor,
    fontStyle: FontStyle.italic);

const TextStyle text400Size12A2A2A2 = TextStyle(
    fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xffA2A2A2));

const TextStyle text700Size15Grey = TextStyle(
  fontSize: 15,
  fontWeight: FontWeight.w700,
  color: Color(0xff858585),
);

const TextStyle text400Size15White =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w400, fontSize: 15);

const TextStyle text400Size18White =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w400, fontSize: 18);

const TextStyle text400Size18Black =
TextStyle(color: Colors.black, fontWeight: FontWeight.w400, fontSize: 18);

const TextStyle text400Size13Pink = TextStyle(
    color: AppColors.pinkColor, fontWeight: FontWeight.w400, fontSize: 13);

const TextStyle text700Size12Grey = TextStyle(
    color: Color(0xff828282), fontWeight: FontWeight.w700, fontSize: 12);

const TextStyle text500Size13White =
    TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 13);

const TextStyle text500Size13Grey = TextStyle(
    color: Color(0xff838383), fontWeight: FontWeight.w500, fontSize: 13);

const TextStyle text500Size13Black =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 13);

const TextStyle text500Size15Black = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w500,
  fontSize: 15,
);

const TextStyle text700Size14Black =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 14);

const TextStyle text500Size10Black =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 10);

const TextStyle text500Size15BGrey =
    TextStyle(color: AppColors.grey, fontWeight: FontWeight.w500, fontSize: 15);

const TextStyle text500Size12Black =
    TextStyle(color: AppColors.grey, fontWeight: FontWeight.w500, fontSize: 12);

const TextStyle text400Size12Grey = TextStyle(
    color: Color(0xff6D6D6D), fontWeight: FontWeight.w400, fontSize: 12);

const TextStyle text400Size12Black = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w400,
  fontSize: 12,
);

const TextStyle text400Size20Black = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w400,
  fontSize: 20,
);

const TextStyle text400Size16Black = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w400,
  fontSize: 16,
);

const TextStyle text400Size24Black = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w400,
  fontSize: 24,
);
const TextStyle text400Size12GreyItalic = TextStyle(
    color: Color(0xff747474),
    fontWeight: FontWeight.w400,
    fontSize: 12,
    fontStyle: FontStyle.italic);

const TextStyle text400Size122F2F2F = TextStyle(
  color: Color(0xff2F2F2F),
  fontWeight: FontWeight.w400,
  fontSize: 12,
);

const TextStyle text400Size9Black =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w400, fontSize: 9);

final TextStyle text400Size12BlackInactive = TextStyle(
  color: Colors.black.withOpacity(0.5),
  fontWeight: FontWeight.w400,
  fontSize: 12,
);

const TextStyle text700Size13Grey = TextStyle(
  color: Color(0xff858585),
  fontWeight: FontWeight.w700,
  fontSize: 13,
);

const TextStyle text700Size11Success = TextStyle(
  color: Color(0xff37B023),
  fontWeight: FontWeight.w700,
  fontSize: 11,
);

const TextStyle text700Size13Success = TextStyle(
  color: Color(0xff37B023),
  fontWeight: FontWeight.w700,
  fontSize: 13,
);
