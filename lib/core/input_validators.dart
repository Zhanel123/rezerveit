import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/city/city.dart';

enum StringRequiredError {
  empty,
}

class StringRequired extends FormzInput<String, StringRequiredError> {
  const StringRequired.pure() : super.pure('');

  const StringRequired.dirty([String value = '']) : super.dirty(value);

  @override
  StringRequiredError? validator(String value) {
    return value.isEmpty ? StringRequiredError.empty : null;
  }
}

enum IntRequiredError { empty }

class IntRequired extends FormzInput<int, IntRequiredError> {
  const IntRequired.pure() : super.pure(0);

  const IntRequired.dirty([int value = 0]) : super.dirty(value);

  @override
  IntRequiredError? validator(int value) {
    return value == 0 ? IntRequiredError.empty : null;
  }
}

enum PhoneValidationError { invalid, lengthLimit }

class PhoneValidator extends FormzInput<String, PhoneValidationError> {
  const PhoneValidator.pure() : super.pure('');

  const PhoneValidator.dirty([String value = '']) : super.dirty(value);

  static final RegExp _phoneRegExp = RegExp(
    r'(\+7\(\d{3}\)\d{3}\d{2}\d{2})',
  );

  @override
  PhoneValidationError? validator(String? value) {
    print(_phoneRegExp.hasMatch(value ?? ''));
    return _phoneRegExp.hasMatch(value ?? '')
        ? null
        : PhoneValidationError.invalid;
  }
}

enum BoolRequiredError {
  empty,
}

class BoolRequired extends FormzInput<bool, BoolRequiredError> {
  const BoolRequired.pure() : super.pure(false);

  const BoolRequired.dirty([bool value = false]) : super.dirty(value);

  @override
  BoolRequiredError? validator(bool value) {
    // нулл когда все окей
    return value ? null : BoolRequiredError.empty;
  }
}

enum CityRequiredError {
  empty,
}

class CityRequired extends FormzInput<City?, CityRequiredError> {
  const CityRequired.pure() : super.pure(null);

  const CityRequired.dirty([City? value]) : super.dirty(value);

  @override
  CityRequiredError? validator(City? value) {
    return value != null ? null : CityRequiredError.empty;
  }
}

enum AddressRequiredError {
  empty,
}

class AddressRequired extends FormzInput<Address?, AddressRequiredError> {
  const AddressRequired.pure() : super.pure(null);

  const AddressRequired.dirty([Address? value]) : super.dirty(value);

  @override
  AddressRequiredError? validator(Address? value) {
    return value != null ? null : AddressRequiredError.empty;
  }
}

enum RequiredError { invalid }

extension RequiredErrorExt on RequiredError? {
  static const keyMap = {
    RequiredError.invalid: 'required_field',
  };

  // название на русском
  static const valueMap = {
    RequiredError.invalid: 'Обязательное поле',
  };

  String? get value => valueMap[this];

  String? get key => keyMap[this];
}

class Required<T> extends FormzInput<T?, RequiredError> {
  const Required.pure() : super.pure(null);

  const Required.dirty(T? value) : super.dirty(value);

  @override
  RequiredError? validator(T? value) {
    return value != null ? null : RequiredError.invalid;
  }
}

enum ListNotEmptyError { invalid }

extension ListNotEmptyErrorExt on ListNotEmptyError? {
  static const keyMap = {
    ListNotEmptyError.invalid: 'required_field',
  };

  // название на русском
  static const valueMap = {
    ListNotEmptyError.invalid: 'Обязательное поле',
  };

  String? get value => valueMap[this];

  String? get key => keyMap[this];
}

class ListNotEmpty<T> extends FormzInput<List<T>, ListNotEmptyError> {
  const ListNotEmpty.pure() : super.pure(const []);

  const ListNotEmpty.dirty(List<T> value) : super.dirty(value);

  @override
  ListNotEmptyError? validator(List<T> value) {
    return value.isNotEmpty ? null : ListNotEmptyError.invalid;
  }
}
