import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color primary = Color(0xFFFF4D00);
  static const Color secondary = Color(0xFFF4BA16);
  static const Color green = Color.fromRGBO(62, 106, 53, 1);
  static const Color white = Color.fromRGBO(255, 255, 255, 1);
  static const Color black = Color.fromRGBO(30, 28, 28, 0.7);
  static const Color blackPrimary = Color.fromRGBO(64, 64, 64, 1);
  static const Color blackBackground = Color.fromRGBO(64, 64, 66, 1);
  static const Color adkButtonColor = Color.fromRGBO(241, 86, 55, 1);
  static const Color grey = Color.fromRGBO(146, 146, 146, 1);
  static const Color greySecondary = Color.fromRGBO(151, 151, 151, 1);
  static const Color greyPrimary = Color.fromRGBO(196, 196, 196, 1);
  static const Color greyBackground = Color.fromRGBO(245, 245, 245, 1);
  static const Color whiteBackground = Color.fromRGBO(239, 239, 239, 1);
  static const Color subtitleGrey = Color.fromRGBO(129, 129, 129, 1);
  static const Color subtitleGreyDate = Color.fromRGBO(189, 189, 189, 1);
  static const Color tabBarColor = Color.fromRGBO(30, 28, 28, 0.3);
  static const Color receiptColor = Color.fromRGBO(0, 0, 0, 1);
  static const Color dividerColor = Color.fromRGBO(187, 187, 187, 1);
  static const Color greyColor = Color.fromRGBO(134, 134, 134, 1);
  static const Color blackSecondary = Color.fromRGBO(38, 38, 38, 1);
  static const Color adkColor = Color.fromRGBO(241, 86, 55, 1);
  static const Color checkBoxColor = Color.fromRGBO(216, 216, 216, 1);
  static const Color smsRequestColor = Color.fromRGBO(172, 172, 171, 1);
  static const Color onBoardingColor = Color.fromRGBO(236, 105, 31, 1);
  static const Color smsBorderColor = Color(0xffD8D8D8);


  static const Color hintTextColor = Color(0xff838383);
  static const Color pinkColor = Color(0xffFF5D04);
  static const Color greyColor2 = Color.fromRGBO(0, 0, 0, 0.3);

  static const Color yellowColor = Color(0xffEDBB0B);
  static const Color greyBlackColor = Color(0xff686868);
  static const Color greyColor3 = Color(0xff6A6A6A);

  static const Color whiteGrey = Color(0xffE6E6E6);

  static const Color EAGrey = Color(0xffEAEAEA);
}
