import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/routes.dart';

class FlushbarService {
  Future<dynamic> showSuccessMessage({
    BuildContext? context,
    String message = 'Успешно',
  }) {
    return Flushbar(
      message: message,
      margin: EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(8),
      icon: Icon(
        Icons.check,
        size: 28,
        color: AppColors.pinkColor,
      ),
      duration: Duration(seconds: 1),
      leftBarIndicatorColor: AppColors.pinkColor,
    ).show(context ?? Routes.router.navigatorKey!.currentContext!);
  }

  void showErrorMessage({
    BuildContext? context,
    String message = 'Ошибка',
  }) {
    BuildContext? _context =
        Routes.router.navigatorKey?.currentContext ?? context;
    Flushbar(
      message: message,
      margin: EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(8),
      icon: Icon(
        Icons.error_outline,
        size: 28,
        color: Colors.red,
      ),
      duration: Duration(seconds: 1),
      leftBarIndicatorColor: Colors.red,
    )..show(_context!);
  }
}
