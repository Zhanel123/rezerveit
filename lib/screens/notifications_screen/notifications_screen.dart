import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/data/repositories/dish_repository/dish_repository.dart';
import 'package:home_food/data/repositories/order_repository/order_repository.dart';
import 'package:home_food/data/services/dish_service/dish_service.dart';
import 'package:home_food/data/services/order_service/order_service.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/food_detaill/food_detail.dart';
import 'package:home_food/screens/order/order_screen.dart';
import 'package:home_food/service_locator.dart';

import '../../core/app_colors.dart';

class NotificationsScreen extends StatefulWidget {
  const NotificationsScreen({Key? key}) : super(key: key);

  @override
  State<NotificationsScreen> createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  @override
  void initState() {
    serviceLocator<ProfileCubit>().fetchNotifications();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Color(0xffEDD7F0),
        backgroundColor: AppColors.pinkColor,
        title: Text('Уведомления'),
      ),
      body: Container(
        height: double.infinity,
        color: Color(0xffF5F5F5),
        child: BlocBuilder<ProfileCubit, ProfileState>(
          builder: (context, state) {
            return ListView(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 24),
              children: [
                ...state.notificationList.map(
                  (e) => GestureDetector(
                    onTap: () async {
                      if (e.type == 'ORDER') {
                        final order = await serviceLocator<OrderRepository>()
                            .getOrderById(orderId: e.objectId);
                        Routes.router.navigate(
                          Routes.orderScreen,
                          args: OrderScreenArgs(order),
                        );
                      } else if (e.type == 'DISH_CARD') {
                        final dish = await serviceLocator<DishRepository>()
                            .getDishById(dishId: e.objectId);
                        Routes.router.navigate(
                          Routes.foodDetail,
                          args: FoodDetailArgs(
                            dish: dish,
                          ),
                        );
                      }
                    },
                    child: Card(
                      color: e.wasRead == true
                          ? Color(0xffCCCCCC)
                          : AppColors.pinkColor.withOpacity(0.4),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 12),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(child: Text(e.subject)),
                                  Text(DateFormat('dd-MM-yyyy')
                                      .format(e.createdDate!)),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 12),
                              child: Row(
                                children: [
                                  Expanded(child: Text(e.content)),
                                  if (e.wasRead) Icon(Icons.check)
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
