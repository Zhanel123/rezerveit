import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/chat/cubit.dart';
import 'package:home_food/blocs/chat/state.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/chat_screen/chat_screen.dart';
import 'package:home_food/screens/chats_list/widgets/chat_item.dart';

import '../../../core/app_assets.dart';
import '../../../core/text_styles.dart';
import '../../widgets/my_app_bar.dart';
import '../../widgets/search/search_field.dart';

class MerchantChatListScreen extends StatefulWidget {
  const MerchantChatListScreen({Key? key}) : super(key: key);

  @override
  State<MerchantChatListScreen> createState() => _MerchantChatListScreenState();
}

class _MerchantChatListScreenState extends State<MerchantChatListScreen>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        color: Color(0xffF5F5F5),
        child: SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 28),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: MyAppBar(),
                ),
                SizedBox(
                  height: 13,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: SearchField(),
                ),
                SizedBox(
                  height: 16,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(
                    'Чаты',
                    style: text700Size18Black,
                  ),
                ),
                BlocBuilder<ChatCubit, ChatState>(
                  builder: (context, state) {
                    return Column(
                      children: [
                        for (int i = 0; i < state.chatList.length; i++)
                          GestureDetector(
                            onTap: () => Routes.router.navigate(
                                Routes.chatScreen,
                                args: ChatScreenArgs(state.chatList[i].id!)),
                            child: ChatItem(
                              chat: state.chatList[i],
                              isMerchant: state.profile?.userType ==
                                  UserType.MANUFACTURER,
                            ),
                          ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
