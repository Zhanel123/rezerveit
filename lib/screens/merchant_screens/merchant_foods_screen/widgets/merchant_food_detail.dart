import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/blocs/favorite_dishes/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/buttons/filter_button.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/my_app_bar.dart';
import 'package:home_food/screens/widgets/search/search_field.dart';
import 'package:home_food/service_locator.dart';
import 'package:seafarer/seafarer.dart';

class MerchantFoodDetailArgs extends BaseArguments {
  final Dish dish;

  MerchantFoodDetailArgs({
    required this.dish,
  });
}

class MerchantFoodDetail extends StatefulWidget {
  final MerchantFoodDetailArgs args;

  const MerchantFoodDetail({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  State<MerchantFoodDetail> createState() => _MerchantFoodDetailState();
}

class _MerchantFoodDetailState extends State<MerchantFoodDetail> {
  int count = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffF5F5F5),
        height: double.infinity,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(top: 5),
          child: SafeArea(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: MyAppBar(),
                ),
                SizedBox(
                  height: 13,
                ),
                // Padding(
                //   padding: const EdgeInsets.symmetric(horizontal: 16.0),
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //       Expanded(child: SearchField()),
                //       FilterButton(),
                //     ],
                //   ),
                // ),
                SizedBox(
                  height: 17,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Routes.router.pop();
                        },
                        child: Container(
                          width: 33,
                          height: 33,
                          decoration: BoxDecoration(
                            color: Color(0xffF5F5F5),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          child: Center(
                              child: Icon(
                            Icons.chevron_left,
                            color: Color(0xff7A7A7A),
                          )),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      ...widget.args.dish.preferenceList.map(
                        (e) => Container(
                          height: 25,
                          margin: EdgeInsets.only(right: 5),
                          decoration: BoxDecoration(
                              color: AppColors.greyColor2,
                              borderRadius: BorderRadius.circular(50)),
                          padding: EdgeInsets.symmetric(horizontal: 24),
                          child: Center(
                            child: Text(
                              '${e.name(context)}',
                              style: text500Size13White,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 17,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Image.asset(widget.imageUrl),
                        Stack(
                          children: [
                            CarouselSlider(
                              options: CarouselOptions(
                                height: 167,
                                viewportFraction: 1,
                              ),
                              items: widget.args.dish.images.map((i) {
                                return Image.network(i.imageUrl);
                              }).toList(),
                            ),
                            Positioned(
                              top: 9,
                              bottom: 11,
                              left: 13.18,
                              right: 8,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SvgPicture.asset(
                                        AppAssets.hot_icon,
                                        width: 33,
                                        height: 33,
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      if (widget.args.dish.preOrderDish)
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(28),
                                            color: AppColors.pinkColor,
                                          ),
                                          height: 30,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 21),
                                          child: Center(
                                            child: Text(
                                              'Предзаказ',
                                              textAlign: TextAlign.center,
                                              style: text400Size15White,
                                            ),
                                          ),
                                        ),
                                      if (widget.args.dish.hasHalfPortion)
                                        Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(28),
                                              color: Colors.white),
                                          height: 30,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 21),
                                          child: Center(
                                            child: Text(
                                              'Пол порции',
                                              textAlign: TextAlign.center,
                                              style: text400Size11Black,
                                            ),
                                          ),
                                        )
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 5),
                          child: Text(
                            widget.args.dish.name,
                            style: text700Size18Black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 5),
                          child: Text(
                            '${widget.args.dish.weight} г',
                            style: text400Size13Black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 10),
                          child: Text(
                            'Состав: ${widget.args.dish.ingredientList.map((e) => e.name).join(', ')}',
                            style: text400Size13Black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 10),
                          child: Text(
                            widget.args.dish.description,
                            style: text400Size13Grey,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 13),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                NumUtils.humanizeNumber(
                                      widget.args.dish.priceWithSale,
                                      isCurrency: true,
                                    ) ??
                                    '',
                                style: text700Size18Pink,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              if (widget.args.dish.price != null)
                                Text(
                                  NumUtils.humanizeNumber(
                                        widget.args.dish.price!,
                                        isCurrency: true,
                                      ) ??
                                      '',
                                  style: TextStyle(
                                      color: Color(0xffB0B0B0),
                                      decoration: TextDecoration.lineThrough,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15),
                                )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
