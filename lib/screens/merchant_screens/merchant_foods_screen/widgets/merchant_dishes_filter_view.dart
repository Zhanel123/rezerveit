import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart';
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

class MerchantDishesFilterView extends StatelessWidget {
  const MerchantDishesFilterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: _SearchField(),
        ),
        SizedBox(
          height: 5,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 21, vertical: 20),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(8)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Просмотр и редактирование блюд',
                  style: text700Size14Black,
                  textAlign: TextAlign.left,
                ),
                SizedBox(
                  height: 12,
                ),
                Row(
                  children: [
                    Expanded(
                      child: _SortByDropdown(),
                    ),
                    SizedBox(
                      width: 22,
                    ),
                    Expanded(
                      child: _HalfPortionFilter(),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _SearchField extends StatelessWidget {
  const _SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MyDishesCubit, MyDishesState>(
      builder: (context, state) {
        return RoundedTextFormField(
          value: state.filter.searchText,
          onChange: serviceLocator<MyDishesCubit>().onSearchChanged,
          hintText: 'Поиск',
          keyboardType: TextInputType.text,
        );
      },
    );
  }
}

class _HalfPortionFilter extends StatelessWidget {
  const _HalfPortionFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MyDishesCubit, MyDishesState>(
      builder: (context, state) {
        return CheckRow(
          label: 'Пол порции',
          isCheck: state.filter.hasHalfPortion,
          widthParameter: 60,
          height: 34,
          handleValue:
              serviceLocator<MyDishesCubit>().handleHalfPortionFilterChanged,
        );
      },
    );
  }
}

class _SortByDropdown extends StatelessWidget {
  const _SortByDropdown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MyDishesCubit, MyDishesState>(
      builder: (context, state) {
        return PrimaryDropdown(
          initialOption: state.filter.sortBy,
          options: SortFieldEnum.values
              .map(
                (e) => SelectOption(
                  value: e,
                  label: e.value,
                ),
              )
              .toList(),
          onChanged: (option) => serviceLocator<MyDishesCubit>()
              .handleSortByChanged(option?.value),
        );
      },
    );
  }
}
