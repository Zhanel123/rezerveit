import 'package:flutter/material.dart';

class PlusMinusWidget extends StatefulWidget {
  final int value;
  final Function(int) onChange;

  const PlusMinusWidget({
    super.key,
    required this.value,
    required this.onChange,
  });

  @override
  _PlusMinusWidgetState createState() => _PlusMinusWidgetState();
}

class _PlusMinusWidgetState extends State<PlusMinusWidget> {
  int _value = 0;

  @override
  void initState() {
    _value = widget.value;
    super.initState();
  }

  void _incrementValue() {
    setState(() {
      _value++;
    });
    widget.onChange(_value);
  }

  void _decrementValue() {
    setState(() {
      _value--;
    });
    widget.onChange(_value);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.remove),
          onPressed: _decrementValue,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            '$_value',
            style: TextStyle(fontSize: 24),
          ),
        ),
        IconButton(
          icon: Icon(Icons.add),
          onPressed: _incrementValue,
        ),
      ],
    );
  }
}
