import 'package:flutter/material.dart';

import '../../../../core/enums.dart';
import '../../../../core/text_styles.dart';

class DropdownViewData extends StatefulWidget {
  const DropdownViewData({Key? key}) : super(key: key);

  @override
  State<DropdownViewData> createState() => _DropdownViewDataState();
}

class _DropdownViewDataState extends State<DropdownViewData> {
  String _viewByData = 'По дате добавления';
  List<String> _viewList = [];

  @override
  void initState() {
    _viewByData = 'По дате добавления';
    _viewList = ['По дате добавления', 'По дате', ' дате добавления'];
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return Container(
      height: 36,
      padding: EdgeInsets.symmetric(horizontal: 17),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Color(0xFFD8D8D8)),
        color: Colors.white,
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String?>(
          value: _viewByData,
          onChanged: (String? value) {
            // This is called when the profile selects an item.
            setState(() {
              _viewByData = value!;
            });
          },
          items: [
            ..._viewList.map((e) => DropdownMenuItem(
                value: e,
                child:Text(
                  e!,
                  style: text500Size12Black,
                )))
          ],
        ),
      ),
    );
  }
}
