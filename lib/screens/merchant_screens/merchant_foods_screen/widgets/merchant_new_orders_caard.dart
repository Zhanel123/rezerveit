import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/main_screen/main_screen.dart';
import 'package:home_food/service_locator.dart';

class MerchantNewOrdersCard extends StatefulWidget {
  const MerchantNewOrdersCard({Key? key}) : super(key: key);

  @override
  State<MerchantNewOrdersCard> createState() => _MerchantNewOrdersCardState();
}

class _MerchantNewOrdersCardState extends State<MerchantNewOrdersCard>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<Color> _myAnimation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    );

    _myAnimation = Tween<Color>(begin: Colors.white, end: Colors.black)
        .animate(CurvedAnimation(parent: _controller, curve: Curves.bounceIn));

    _controller.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _controller.repeat();
      }
    });
    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _myAnimation,
      builder: (ctx, ch) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
              color: _myAnimation.value,
              width: 1.0,
            ),
            color: Colors.white,
          ),
          height: 62,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 22.0,
                right: 13,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SvgPicture.asset(AppAssets.green_dot),
                      SizedBox(
                        width: 11,
                      ),
                      Text(
                        "Новые заказы",
                        style: text700Size14Black,
                      ).tr(),
                      SizedBox(
                        width: 8,
                      ),
                      BlocBuilder<MerchantOrdersCubit, MerchantOrdersState>(
                        builder: (context, state) {
                          return Container(
                            height: 20,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(19),
                              color: AppColors.pinkColor,
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 7, vertical: 3),
                              child: Text(
                                '+${state.orderedStatusOrdersList.length}',
                                style: text600Size12White,
                              ),
                            ),
                          );
                        },
                      )
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      serviceLocator<MerchantOrdersCubit>()
                          .handleStatusFilterChanged(2);
                      MainScreen.of(context)?.changeTab(
                        1,
                        true,
                      );
                    },
                    child: SvgPicture.asset(AppAssets.new_orders_icon),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
