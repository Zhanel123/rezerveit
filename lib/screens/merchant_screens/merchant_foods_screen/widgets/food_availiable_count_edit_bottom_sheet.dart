import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dish_catalog/cubit.dart';
import 'package:home_food/blocs/dish_edit/cubit.dart';
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/plus_minus_widget.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

class FoodAvailiableCountEditBottomSheet extends StatefulWidget {
  final Dish dish;

  const FoodAvailiableCountEditBottomSheet({
    Key? key,
    required this.dish,
  }) : super(key: key);

  @override
  State<FoodAvailiableCountEditBottomSheet> createState() =>
      _FoodAvailiableCountEditBottomSheetState();
}

class _FoodAvailiableCountEditBottomSheetState
    extends State<FoodAvailiableCountEditBottomSheet> {
  late int portionAmount;

  @override
  void initState() {
    portionAmount = widget.dish.portionAmount;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PrimaryBottomSheet(
      contentPadding: EdgeInsets.all(18),
      child: BlocConsumer<DishEditCubit, DishEditState>(
        listenWhen: (p, c) => p.status != c.status,
        listener: (context, state) {
          if (state.status.isSubmissionSuccess) {
            Navigator.popUntil(
              context,
              (route) => route.settings.name == Routes.homeScreen,
            );
            FlushbarService()
                .showSuccessMessage(message: 'Количество изменено');

            serviceLocator<MyDishesCubit>().fetchMyDishes();
          }
        },
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Routes.router.pop();
                  },
                  child: Container(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        color: Color(0xffEAEAEA),
                        borderRadius: BorderRadius.circular(28)),
                    child: Icon(
                      Icons.close,
                      color: Color(0xff7A7A7A),
                      size: 15,
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 35.0),
                child: SizedBox(
                  width: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Изменить количество доступного блюда',
                        style: text700Size15Black,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      PlusMinusWidget(
                        value: widget.dish.portionAmount,
                        onChange: (newValue) {
                          setState(() {
                            portionAmount = newValue;
                          });
                        },
                      ),
                      Container(
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: () => serviceLocator<DishEditCubit>()
                              .handleChangePortionAmount(
                            dish: widget.dish,
                            portionAmount: portionAmount,
                          ),
                          child: Text(
                            'Cохранить',
                            style: text400Size15White,
                          ),
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                            ),
                            backgroundColor: MaterialStatePropertyAll<Color>(
                                Color(0xff37B023)),
                          ),
                        ),
                      ),
                      TextButton(
                          onPressed: () {
                            Routes.router.pop();
                          },
                          child: Text(
                            'Отменить',
                            style: text400Size13Pink,
                          ))
                    ],
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
