import 'package:flutter/material.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/merchant_food_card.dart';

class MerchantDishesListView extends StatefulWidget {
  const MerchantDishesListView({
    Key? key,
    this.dishes = const [],
    required this.onScrollEnd,
  }) : super(key: key);

  final List<Dish> dishes;
  final VoidCallback? onScrollEnd;

  @override
  State<MerchantDishesListView> createState() => _MerchantDishesListViewState();
}

class _MerchantDishesListViewState extends State<MerchantDishesListView> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() {
      var triggerFetchMoreSize =
          0.7 * scrollController.position.maxScrollExtent;

      if (scrollController.position.pixels > triggerFetchMoreSize) {
        widget.onScrollEnd?.call();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: scrollController,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      children: mapDishesToWidget(),
    );
  }

  List<Widget> mapDishesToWidget() {
    final List<Widget> children = [];

    for (int i = 0; i < widget.dishes.length; i++) {
      children.add(MerchantFoodCard(
        dish: widget.dishes[i],
      ));
    }

    return children;
  }
}
