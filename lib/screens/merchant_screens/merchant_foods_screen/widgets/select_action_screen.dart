import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dish_catalog/cubit.dart';
import 'package:home_food/blocs/dish_edit/cubit.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/food_availiable_count_edit_bottom_sheet.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';

class SelectActionScreen extends StatelessWidget {
  final Dish dish;

  const SelectActionScreen({
    Key? key,
    required this.dish,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PrimaryBottomSheet(
      contentPadding: EdgeInsets.all(18),
      child: BlocConsumer<DishCatalogCubit, DishCatalogState>(
        listenWhen: (p, c) =>
            p.moderationStatus != c.moderationStatus ||
            p.deactivationStatus != c.deactivationStatus ||
            p.activationStatus != c.activationStatus,
        listener: (context, state) {
          logger.w(state.deactivationStatus);
          if (state.deactivationStatus.isSubmissionSuccess) {
            Navigator.pop(context);
            FlushbarService().showSuccessMessage(
              message: 'Блюдо деактивировано',
            );
          } else if (state.deactivationStatus.isSubmissionFailure) {
            FlushbarService().showSuccessMessage(
              context: context,
              message: state.error,
            );
          }
          if (state.activationStatus.isSubmissionSuccess) {
            Navigator.pop(context);
            FlushbarService().showSuccessMessage(
              message: 'Блюдо активировано',
            );
          } else if (state.activationStatus.isSubmissionFailure) {
            FlushbarService().showSuccessMessage(
              context: context,
              message: state.error,
            );
          }

          if (state.moderationStatus.isSubmissionSuccess) {
            Navigator.pop(context);
            FlushbarService()
                .showSuccessMessage(message: 'Отправлено на модерацию');
          } else if (state.moderationStatus.isSubmissionFailure) {
            FlushbarService().showSuccessMessage(
              context: context,
              message: state.error,
            );
          }
        },
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Routes.router.pop();
                  },
                  child: Container(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        color: Color(0xffEAEAEA),
                        borderRadius: BorderRadius.circular(28)),
                    child: Icon(
                      Icons.close,
                      color: Color(0xff7A7A7A),
                      size: 15,
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 35.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Выберите действие',
                      style: text700Size15Black,
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _FoodThumbImage(
                          images: dish.images,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Container(
                          width: 133,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                dish.name,
                                textAlign: TextAlign.left,
                                style: text400Size12Black,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  Text(
                                    NumUtils.humanizeNumber(dish.price) ?? '',
                                    style: text700Size15Black,
                                  ),
                                  SizedBox(
                                    width: 3,
                                  ),
                                  Container(
                                    width: 3,
                                    height: 3,
                                    decoration: BoxDecoration(
                                        color: AppColors.grey,
                                        borderRadius:
                                            BorderRadius.circular(50)),
                                  ),
                                  SizedBox(
                                    width: 6,
                                  ),
                                  Text('${dish.weight} г',
                                      style: text400Size12Grey)
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    if (dish.status == DishStatusEnum.PUBLISHED)
                      Container(
                        width: double.infinity,
                        height: 40,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50)),
                        child: GradientButton(
                          labelText: 'Изменить доступное количество',
                          onPressed: () => showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            isDismissible: true,
                            backgroundColor: Colors.transparent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(28.0),
                            ),
                            builder: (context) =>
                                FoodAvailiableCountEditBottomSheet(
                              dish: dish,
                            ),
                          ),
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                      ),
                    if (dish.status == DishStatusEnum.NEW)
                      Container(
                        width: double.infinity,
                        height: 40,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50)),
                        child: GradientButton(
                          labelText: 'Редактировать',
                          onPressed: () {
                            serviceLocator<DishEditCubit>()
                                .handleSelectDishCard(dish);
                            Routes.router.navigate(Routes.merchantEditScreen);
                          },
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                      ),
                    if (dish.status == DishStatusEnum.NEW)
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 10),
                        child: GradientButton(
                          onPressed: () => serviceLocator<DishCatalogCubit>()
                              .handleDishModerate(dish),
                          height: 40,
                          gradient: LinearGradient(
                            colors: [
                              Colors.blue,
                              Colors.lightBlue,
                            ],
                          ),
                          labelText: 'На модерацию',
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                    if (dish.status == DishStatusEnum.PUBLISHED)
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 10),
                        child: GradientButton(
                          onPressed: () => serviceLocator<DishCatalogCubit>()
                              .handleDishDeactivate(dish),
                          height: 40,
                          gradient: LinearGradient(
                            colors: [
                              Colors.red,
                              Colors.redAccent,
                            ],
                          ),
                          labelText: 'Деактивировать',
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                    if (dish.status != DishStatusEnum.PUBLISHED)
                      Container(
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: () => serviceLocator<DishCatalogCubit>()
                              .handleDishActivate(dish),
                          child: Text(
                            'Активировать',
                            style: text400Size15White,
                          ),
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                            ),
                            backgroundColor: MaterialStatePropertyAll<Color>(
                                Color(0xff37B023)),
                          ),
                        ),
                      ),
                    TextButton(
                        onPressed: () {
                          Routes.router.pop();
                        },
                        child: Text(
                          'Отменить',
                          style: text400Size13Pink,
                        ))
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

class _FoodThumbImage extends StatelessWidget {
  const _FoodThumbImage({
    Key? key,
    this.images = const [],
  }) : super(key: key);

  final List<UploadedImage> images;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 53,
      width: 53,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
      ),
      clipBehavior: Clip.hardEdge,
      child: images.isEmpty
          ? Container()
          : CachedNetworkImage(
              imageUrl: images.first.imageUrl,
              fit: BoxFit.cover,
            ),
    );
  }
}
