import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/food_detaill/food_detail.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/merchant_food_detail.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/select_action_screen.dart';
import 'package:home_food/screens/order/order_screen.dart';

class MerchantFoodCard extends StatelessWidget {
  final Dish dish;

  const MerchantFoodCard({
    Key? key,
    required this.dish,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      key: Key(dish.id),
      onTap: () {
        Routes.router.navigate(
          Routes.merchantFoodDetail,
          args: MerchantFoodDetailArgs(
            dish: dish,
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.all(13),
        margin: EdgeInsets.only(bottom: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                _FoodThumbImage(
                  images: dish.images,
                ),
                SizedBox(
                  width: 15,
                ),
                Container(
                  width: 133,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        dish.name,
                        textAlign: TextAlign.left,
                        style: text400Size12Black,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Text(
                            NumUtils.humanizeNumber(dish.price) ?? '',
                            style: text700Size15Black,
                          ),
                          SizedBox(
                            width: 3,
                          ),
                          Container(
                            width: 3,
                            height: 3,
                            decoration: BoxDecoration(
                              color: AppColors.grey,
                              borderRadius: BorderRadius.circular(50),
                            ),
                          ),
                          SizedBox(
                            width: 6,
                          ),
                          Text('${dish.weight} г', style: text400Size12Grey)
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  children: [
                    InkWell(
                      onTap: () => _handleSelectActionClicked(context),
                      child: SizedBox(
                        width: 40,
                        height: 40,
                        child: Align(
                          alignment: Alignment.topRight,
                          child: SvgPicture.asset(
                            AppAssets.merchant_food_settings,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 18,
                ),
                Text(
                  dish.status.label ?? '',
                  style: text400Size12Green.copyWith(
                    color: dish.status.color,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  void _handleSelectActionClicked(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
      builder: (context) => SelectActionScreen(
        dish: dish,
      ),
    );
  }
}

class _FoodThumbImage extends StatelessWidget {
  const _FoodThumbImage({
    Key? key,
    this.images = const [],
  }) : super(key: key);

  final List<UploadedImage> images;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 53,
      width: 53,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
      ),
      clipBehavior: Clip.hardEdge,
      child: images.isEmpty
          ? Container()
          : CachedNetworkImage(
              imageUrl: images.first.imageUrl,
              fit: BoxFit.cover,
            ),
    );
  }
}
