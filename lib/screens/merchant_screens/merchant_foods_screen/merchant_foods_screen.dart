import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/merchant_dishes_filter_view.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/merchant_dishes_list_view.dart';
import 'package:home_food/service_locator.dart';

import '../../widgets/my_app_bar.dart';

class MerchantFoodsScreen extends StatefulWidget {
  const MerchantFoodsScreen({Key? key}) : super(key: key);

  @override
  State<MerchantFoodsScreen> createState() => _MerchantFoodsScreenState();
}

class _MerchantFoodsScreenState extends State<MerchantFoodsScreen>
    with TickerProviderStateMixin {
  bool isHalf = false;

  final List<Tab> topTabs = <Tab>[
    new Tab(
      child: Text(
        'Все',
        style: text700Size15Grey,
      ),
    ),
    new Tab(
      child: Text(
        'Первые блюда',
        style: text700Size15Grey,
      ),
    ),
    new Tab(
      child: Text(
        'Вторые',
        style: text700Size15Grey,
      ),
    ),
    new Tab(
      child: Text(
        'Десерты',
        style: text700Size15Grey,
      ),
    ),
  ];

  late TabController _controller;

  @override
  void initState() {
    super.initState();
    isHalf = false;
    // _controller.addListener(() {
    //   print(_controller.index);
    // });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 18),
      color: Color(0xffF5F5F5),
      child: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: MyAppBar(),
              ),
              SizedBox(
                height: 13,
              ),
              MerchantDishesFilterView(),
              SizedBox(
                height: 23,
              ),
              Expanded(
                child: BlocBuilder<MyDishesCubit, MyDishesState>(
                  builder: (context, state) {
                    var selected = state.dishTypes.indexWhere(
                      (element) => element.id == state.filter.dishTypeId,
                    );
                    _controller = TabController(
                      vsync: this,
                      initialIndex: selected >= 0 ? selected + 1 : 0,
                      length: state.dishTypes.length + 1,
                    );
                    return Column(
                      children: [
                        BlocBuilder<MyDishesCubit, MyDishesState>(
                          builder: (context, state) {
                            return TabBar(
                              tabs: [null, ...state.dishTypes]
                                  .map(
                                    (e) => Tab(
                                      text: e?.name == null
                                          ? 'Все'
                                          : e?.name(context),
                                    ),
                                  )
                                  .toList(),
                              isScrollable: true,
                              controller: _controller,
                              unselectedLabelStyle: text700Size15BlackAccent,
                              labelStyle: text700Size15Black,
                              labelColor: Colors.black,
                              unselectedLabelColor:
                                  Colors.black.withOpacity(0.5),
                              onTap: serviceLocator<MyDishesCubit>()
                                  .handleDishTypeChanged,
                              indicator: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.transparent,
                              ),
                            );
                          },
                        ),
                        Expanded(
                            child: MerchantDishesListView(
                          dishes: state.myDishes,
                          onScrollEnd: () => serviceLocator<MyDishesCubit>()
                              .fetchMyDishes(append: true),
                        )),
                      ],
                    );
                  },
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  List<Widget> mapDishTypes(List<DishType> dishTypes) {
    List<Widget> tabs = [
      Tab(
        child: Text(
          'Все',
        ),
      )
    ];

    for (int i = 0; i < dishTypes.length; i++) {
      tabs.add(Tab(
        child: Text(
          dishTypes[i].name(context),
        ),
      ));
    }

    return tabs;
  }
}
