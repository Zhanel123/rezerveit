import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/create_food/cubit.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/service_locator.dart';

import '../../../../core/text_styles.dart';
import '../../../widgets/check_row.dart';

class DiscountRow extends StatefulWidget {
  const DiscountRow({Key? key}) : super(key: key);

  @override
  State<DiscountRow> createState() => _DiscountRowState();
}

class _DiscountRowState extends State<DiscountRow> {
  bool isDiscount = false;
  int _discount_percent = 10;

  List<String> _values = ["10 %", "15 %", "25 %", "35 %"];

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CheckRow(
          label: 'Скидка после 19:00',
          labelStyle: text400Size12Black,
          widthParameter: 160,
          isCheck: isDiscount,
          handleValue: (value) {
            setState(() {
              isDiscount = value ?? false;
            });
          },
        ),
        SizedBox(
          width: 24,
        ),
        Expanded(
          child: isDiscount
              ? BlocBuilder<CreateFoodCubit, CreateFoodState>(
                  builder: (context, state) {
                    return PrimaryDropdown(
                      initialOption: state.discount.value,
                      options: mapDiscounts(),
                      onChanged: (option) => serviceLocator<CreateFoodCubit>()
                          .handleDiscountChanged(option?.value),
                    );
                  },
                )
              : SizedBox(),
        ),
      ],
    );
  }

  List<SelectOption<int>> mapDiscounts() {
    List<SelectOption<int>> options = [];

    for (int i = 1; i <= 10; i++) {
      options.add(SelectOption(
        value: i * 10,
        label: '${i * 10} %',
      ));
    }

    return options;
  }
}
