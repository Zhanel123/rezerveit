import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/create_food/cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/main_screen/main_screen.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service_locator.dart';

class CreateFoodButton extends StatelessWidget {
  const CreateFoodButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 40,
            child: Center(
              child: BlocBuilder<CreateFoodCubit, CreateFoodState>(
                builder: (context, state) {
                  return GradientButton(
                    borderRadius: BorderRadius.circular(50),
                    labelText: 'Добавить блюдо',
                    isLoading:
                        state.createFoodFormzStatus.isSubmissionInProgress,
                    onPressed: serviceLocator<CreateFoodCubit>().create,
                  );
                },
              ),
            )),
        TextButton(
            onPressed: () {
              // Routes.router.pop();
            },
            child: Text(
              'Отменить',
              style: text400Size13Pink,
            ))
      ],
    );
  }
}
