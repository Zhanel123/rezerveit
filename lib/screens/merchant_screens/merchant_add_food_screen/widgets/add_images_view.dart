import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/food_photo_requirements.dart';
import 'package:home_food/screens/widgets/add_image_card.dart';
import 'package:home_food/utils/logger.dart';

class AddImagesView extends StatelessWidget {
  const AddImagesView({
    Key? key,
    this.images = const [],
    this.onDeleteImage,
    this.onSelectImage,
    this.onSelectImages,
    this.onSwapImages,
  }) : super(key: key);

  final List<UploadedImage> images;
  final Function(int)? onDeleteImage;
  final Function(File, int?)? onSelectImage;
  final Function(int, int)? onSwapImages;
  final Function(List<File>)? onSelectImages;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Wrap(
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Основная фотография',
                        style: text400Size122F2F2F,
                      ),
                      Row(
                        children: [
                          Text(
                            'jpg, png до 500 Кб',
                            style: TextStyle(
                                fontStyle: FontStyle.italic,
                                color: Color(0xff8A8A8A),
                                fontSize: 12,
                                fontWeight: FontWeight.w400),
                          ),
                          SizedBox(
                            width: 9,
                          ),
                          InkWell(
                            child: SvgPicture.asset(AppAssets.question_icon),
                            onTap: () => _showPhotoRequirementInfo(context),
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  SizedBox(
                    height: 168,
                    child: DragTarget<int>(
                      onWillAccept: (data) {
                        return true;
                      },
                      onAccept: (int index) {
                        onSwapImages?.call(
                          index,
                          0,
                        );
                      },
                      builder: (context, data, rejectedData) => Draggable<int>(
                        data: images.isNotEmpty ? 0 : null,
                        onDragUpdate: (details) {
                          var _y = details.delta.dy;
                          var _x = details.delta.dx;

                          logger.w('${_y} ${_x}');
                        },
                        feedback: SizedBox(
                          height: 168,
                          width: double.infinity,
                          child: AddImageButton(
                            image: images.isNotEmpty ? images.first : null,
                            index: 0,
                            onDeleteImage: onDeleteImage,
                            onSelectImage: onSelectImage,
                            onSelectImages: onSelectImages,
                          ),
                        ),
                        child: AddImageButton(
                          image: images.isNotEmpty ? images.first : null,
                          index: 0,
                          onDeleteImage: onDeleteImage,
                          onSelectImage: onSelectImage,
                          onSelectImages: onSelectImages,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (images.isNotEmpty)
              Column(
                children: [
                  SizedBox(
                    height: 14,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Дополнительные',
                        style: text400Size122F2F2F,
                      ),
                      Text(
                        'не более 4-х',
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Color(0xff8A8A8A),
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  Container(
                    height: 68,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: mapAdditionalUploadedImages(images),
                    ),
                  )
                ],
              ),
          ],
        ),
      ],
    );
  }

  Future<void> _showPhotoRequirementInfo(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return FoodPhotoRequirements();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.4,
        maxHeight: MediaQuery.of(context).size.height,
      ),
    );
  }

  List<Widget> mapAdditionalUploadedImages(List<UploadedImage> uploadedImages) {
    List<Widget> children = [];

    for (int i = 1; i < uploadedImages.length; i++) {
      children.add(
        DragTarget<int>(
          onWillAccept: (data) {
            return true;
          },
          onAccept: (int index) {
            onSwapImages?.call(
              index,
              i,
            );
          },
          builder: (context, data, rejectedData) => Draggable<int>(
            data: i,
            feedback: Container(
              width: 68,
              height: 68,
              margin: EdgeInsets.only(right: 10),
              child: AddImageButton(
                image: uploadedImages[i],
                index: i,
                onDeleteImage: onDeleteImage,
                onSelectImage: onSelectImage,
                onSelectImages: onSelectImages,
              ),
            ),
            child: Container(
              width: 68,
              height: 68,
              margin: EdgeInsets.only(right: 10),
              child: AddImageButton(
                image: uploadedImages[i],
                index: i,
                onDeleteImage: onDeleteImage,
                onSelectImage: onSelectImage,
                onSelectImages: onSelectImages,
              ),
            ),
          ),
        ),
      );
    }

    children.add(
      Container(
        width: 68,
        margin: EdgeInsets.only(right: 10),
        child: AddImageButton(
          onSelectImage: onSelectImage,
          onSelectImages: onSelectImages,
        ),
      ),
    );

    return children;
  }
}
