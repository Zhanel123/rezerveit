import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/merchant_image_cart.dart';

class MerchantPhotoSlider extends StatefulWidget {
  const MerchantPhotoSlider({Key? key}) : super(key: key);

  @override
  State<MerchantPhotoSlider> createState() => _MerchantPhotoSliderState();
}

class _MerchantPhotoSliderState extends State<MerchantPhotoSlider> {
  int activeIndex = 0;

  CarouselController _carouselController = CarouselController();
  PageController pageController = PageController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CarouselSlider(
          carouselController: _carouselController,
          items: [
            MerchantImageCart(),
            MerchantImageCart(),
            MerchantImageCart()
          ],
          options: CarouselOptions(
            onPageChanged: _onCarouselPageChanged,
            initialPage: activeIndex,
            enlargeCenterPage: true,
            viewportFraction: 1,
            autoPlay: true,
          ),
        ),
        Positioned(
          left: 16,
          top: 72,
          child: InkWell(
              onTap: () {
                _carouselController.previousPage();
              },
              child: SvgPicture.asset(AppAssets.merchant_photo_arrow_left)
          ),
        ),
        Positioned(
            right: 16,
          top: 72,
          child: InkWell(
              onTap: () {
                _carouselController.previousPage();
              },
              child: SvgPicture.asset(AppAssets.merchant_photo_arrow_right)
          ),
        ),
      ],
    );
  }

  _onCarouselPageChanged(index, reason) {
    if (mounted) {
      setState(() {
        activeIndex = index;
      });
    }
    // pageController.animateTo(activeIndex.toDouble(),
    //     duration: Duration(milliseconds: 600), curve: Curves.bounceIn);
  }
}
