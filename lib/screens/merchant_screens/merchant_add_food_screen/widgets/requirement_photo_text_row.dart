import 'package:flutter/cupertino.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';

class RequirementTextPhotoRow extends StatelessWidget {
  final String label;
  const RequirementTextPhotoRow({Key? key, required this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 20),
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 13),
      child: Row(
        children: [
          Container(
            width: 7,
            height: 7,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: AppColors.pinkColor
            ),
          ),
          SizedBox(width: 17,),
          Expanded(
            child: Text(
              label,
              style: text400Size12Black,
            ),
          )
        ],
      ),
    );
  }
}
