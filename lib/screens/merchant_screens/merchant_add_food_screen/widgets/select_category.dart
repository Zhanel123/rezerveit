import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/create_food/cubit.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/service_locator.dart';

class SelectCategory extends StatefulWidget {
  const SelectCategory({Key? key}) : super(key: key);

  @override
  State<SelectCategory> createState() => SelectCategoryState();
}

class SelectCategoryState extends State<SelectCategory> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      builder: (context, state) {
        return PrimaryDropdown(
          initialOption: state.dishType.value,
          options: mapDishTypes(state.dishTypes),
          // error: state.dishType.invalid ? state.dishType.error?.key ?? '' : '',
          error: state.dishType.invalid ? 'Обязательное поле' : '',
          onChanged: (option) => serviceLocator<CreateFoodCubit>()
              .handleDishTypeChanged(option?.value),
        );
      },
    );
  }

  List<SelectOption<DishType>> mapDishTypes(List<DishType> dishTypes) {
    List<SelectOption<DishType>> options = [];

    options.add(SelectOption(
      value: null,
      label: 'Выберите категорию',
    ));

    for (int i = 0; i < dishTypes.length; i++) {
      options.add(SelectOption(
        value: dishTypes[i],
        label: dishTypes[i].name(context),
      ));
    }

    return options;
  }
}
