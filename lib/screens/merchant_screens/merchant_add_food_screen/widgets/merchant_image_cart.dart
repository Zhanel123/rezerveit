import 'package:flutter/material.dart';
import 'package:home_food/core/app_assets.dart';

class MerchantImageCart extends StatefulWidget {
  const MerchantImageCart({Key? key}) : super(key: key);

  @override
  State<MerchantImageCart> createState() => _MerchantImageCartState();
}

class _MerchantImageCartState extends State<MerchantImageCart> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Stack(
        children: [
          Container(
            height: constraints.maxHeight,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
            child: Image.asset(
              AppAssets.requirement_img,
              fit: BoxFit.fill,
            ),
          ),
        ],
      );
    });
  }
}
