import 'package:flutter/material.dart';

import '../../../../core/text_styles.dart';
import '../../../widgets/check_row.dart';

class HasHalfPortionCheckbox extends StatefulWidget {
  const HasHalfPortionCheckbox({Key? key}) : super(key: key);

  @override
  State<HasHalfPortionCheckbox> createState() => _HasHalfPortionCheckboxState();
}

class _HasHalfPortionCheckboxState extends State<HasHalfPortionCheckbox> {
  bool isHalf = false;

  @override
  Widget build(BuildContext context) {
    return CheckRow(
      height: 34,
      label: 'Доступно пол порции',
      labelStyle: text500Size12Black,
      widthParameter: 200,
      isCheck: isHalf,
      handleValue: (value) {
        setState(() {
          isHalf = value ?? false;
        });
      },
    );
  }
}
