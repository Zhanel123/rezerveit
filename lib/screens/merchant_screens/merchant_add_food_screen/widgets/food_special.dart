import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/create_food/cubit.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/primary_badge_with_delete.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/service_locator.dart';

class FoodSpecial extends StatefulWidget {
  const FoodSpecial({Key? key}) : super(key: key);

  @override
  State<FoodSpecial> createState() => _FoodSpecialState();
}

class _FoodSpecialState extends State<FoodSpecial> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      builder: (context, state) {
        return Column(
          children: [
            PrimaryDropdown(
              options: mapFoodPreferences(state.preferences),
              // error: state.selectedPreferences.invalid
              //     ? state.selectedPreferences.error.key ?? ''
              //     : '',
              error:
                  state.selectedPreferences.invalid ? 'Обязательное поле' : '',
              onChanged: (option) => serviceLocator<CreateFoodCubit>()
                  .handlePreferenceSelected(option?.value),
            ),
            if (state.selectedPreferences.value.isNotEmpty)
              Padding(
                padding: EdgeInsets.symmetric(vertical: 11),
                child: SizedBox(
                  height: 30,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      for (int i = 0;
                          i < state.selectedPreferences.value.length;
                          i++)
                        PrimaryBadgeWithDelete(
                          label:
                              state.selectedPreferences.value[i].name(context),
                          onTap: () => serviceLocator<CreateFoodCubit>()
                              .deletePreference(
                                  state.selectedPreferences.value[i]),
                        ),
                    ],
                  ),
                ),
              )
          ],
        );
      },
    );
  }

  List<SelectOption<Preference>> mapFoodPreferences(
      List<Preference> preferences) {
    List<SelectOption<Preference>> options = [];

    options.add(SelectOption(
      value: null,
      label: 'Выберите особенности',
    ));

    for (int i = 0; i < preferences.length; i++) {
      options.add(SelectOption(
        value: preferences[i],
        label: preferences[i].name(context),
      ));
    }

    return options;
  }
}
