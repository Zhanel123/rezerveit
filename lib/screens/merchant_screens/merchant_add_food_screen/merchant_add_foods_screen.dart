import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/create_food/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/main_screen/main_screen.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/add_food_buttons.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/has_half_portion_checkbox.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/add_images_view.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/discount_row.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/food_description.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/food_special.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/select_category.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';

import '../../widgets/my_app_bar.dart';
import '../../widgets/rounded_text_field.dart';
import '../../widgets/search/search_field.dart';

class MerchantAddFoodScreen extends StatefulWidget {
  const MerchantAddFoodScreen({Key? key}) : super(key: key);

  @override
  State<MerchantAddFoodScreen> createState() => _MerchantAddFoodScreenState();
}

class _MerchantAddFoodScreenState extends State<MerchantAddFoodScreen> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: BlocConsumer<CreateFoodCubit, CreateFoodState>(
        buildWhen: (p, c) => p.createFoodStatus != c.createFoodStatus,
        listenWhen: (p, c) => p.createFoodStatus != c.createFoodStatus,
        listener: (context, state) {
          if (state.createFoodStatus.isSubmissionSuccess) {
            FlushbarService().showSuccessMessage();
            MainScreen.of(context)?.changeTab(3);
          } else if (state.createFoodStatus.isSubmissionFailure) {
            FlushbarService().showErrorMessage(
              context: context,
            );
          }
        },
        builder: (context, state) {
          return Container(
            height: double.infinity,
            color: Color(0xffF5F5F5),
            child: SafeArea(
              child: Stack(
                children: [
                  SingleChildScrollView(
                    padding: EdgeInsets.only(top: 18),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: MyAppBar(),
                        ),
                        SizedBox(
                          height: 13,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 21, vertical: 19),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 5.0),
                                  child: Text(
                                    'Добавление нового блюда',
                                    style: text700Size14Black,
                                  ),
                                ),
                                SizedBox(
                                  height: 11,
                                ),
                                SelectCategory(),
                                SizedBox(
                                  height: 9,
                                ),
                                _DishNameField(),
                                SizedBox(
                                  height: 9,
                                ),
                                // _IsPreOrderDish(),
                                _DishAmountField(),
                                SizedBox(
                                  height: 9,
                                ),
                                _WeightField(),
                                _HasHalfPortion(),
                                _MinDishAmountField(),
                                FoodDescription(),
                                SizedBox(
                                  height: 9,
                                ),
                                FoodSpecial(),
                                SizedBox(
                                  height: 9,
                                ),
                                _PriceField(),
                                SizedBox(
                                  height: 9,
                                ),
                                DiscountRow(),
                                SizedBox(
                                  height: 23,
                                ),
                                _DishImagesUpload(),
                                SizedBox(
                                  height: 29,
                                ),
                                CreateFoodButton(),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 29,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class _DishNameField extends StatelessWidget {
  const _DishNameField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      buildWhen: (p, c) => p.name != c.name,
      builder: (context, state) {
        return RoundedTextFormField(
          onChange: serviceLocator<CreateFoodCubit>().handleNameChanged,
          value: state.name.value,
          hasError: state.name.status == FormzInputStatus.invalid,
          errorText: state.name.error != null ? 'Обязательное поле' : null,
          hintText: 'Название блюда',
          keyboardType: TextInputType.text,
        );
      },
    );
  }
}

class _DishAmountField extends StatelessWidget {
  const _DishAmountField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      builder: (context, state) {
        return RoundedTextFormField(
          onChange:
              serviceLocator<CreateFoodCubit>().handlePortionAmountChanged,
          value: NumUtils.humanizeNumber(state.portionAmount.value),
          hasError: state.portionAmount.status == FormzInputStatus.invalid,
          errorText:
              state.portionAmount.error != null ? 'Обязательное поле' : null,
          hintText: 'Количество доступной порций',
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
            // С таким фильтром могут быть введены только числа
          ],
        );
      },
    );
  }
}

class _MinDishAmountField extends StatelessWidget {
  const _MinDishAmountField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      builder: (context, state) {
        if (!state.isPreOrderDish.value) {
          return SizedBox();
        }
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: RoundedTextFormField(
            onChange: serviceLocator<CreateFoodCubit>().handleMinDishAmount,
            value: NumUtils.humanizeNumber(state.minDishAmount.value),
            hasError: state.minDishAmount.status == FormzInputStatus.invalid,
            errorText:
                state.minDishAmount.error != null ? 'Обязательное поле' : null,
            hintText: 'Мин. количество порций',
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
              // С таким фильтром могут быть введены только числа
            ],
          ),
        );
      },
    );
  }
}

class _HasHalfPortion extends StatelessWidget {
  const _HasHalfPortion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.only(top: 12),
          width: double.infinity,
          child: CheckRow(
            height: 14,
            label: 'Доступно пол порции',
            labelStyle: text500Size12Black,
            widthParameter: 200,
            isCheck: state.isHalfPortion.value,
            handleValue: (value) => serviceLocator<CreateFoodCubit>()
                .handleIsHalfPortionChanged(value),
          ),
        );
      },
    );
  }
}

class _IsPreOrderDish extends StatelessWidget {
  const _IsPreOrderDish({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      builder: (context, state) {
        return Container(
          width: double.infinity,
          padding: EdgeInsets.only(top: 3),
          child: CheckRow(
            height: 14,
            label: 'Блюдо по предзаказу',
            labelStyle: text500Size12Black,
            widthParameter: 200,
            isCheck: state.isPreOrderDish.value,
            handleValue: (value) =>
                serviceLocator<CreateFoodCubit>().handleIsPreOrderDish(value),
          ),
        );
      },
    );
  }
}

class _PriceField extends StatelessWidget {
  const _PriceField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      buildWhen: (p, c) => p.price != c.price,
      builder: (context, state) {
        return RoundedTextFormField(
          value: NumUtils.humanizeNumber(state.price.value),
          onChange: serviceLocator<CreateFoodCubit>().handlePriceChanged,
          hasError: state.price.status == FormzInputStatus.invalid,
          errorText: state.price.error != null ? 'Обязательное поле' : null,
          hintText: 'Цена, тг',
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
            // С таким фильтром могут быть введены только числа
          ],
        );
      },
    );
  }
}

class _WeightField extends StatelessWidget {
  const _WeightField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // WeightTypeEnum _weightTypeEnum = WeightTypeEnum.gram;
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      buildWhen: (p, c) => p.weight != c.weight || p.weightType != c.weightType,
      builder: (context, state) {
        logger.w((state.weight.value ?? 0) /
            (state.weightType == WeightTypeEnum.kilograms ? 1000 : 1));
        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: RoundedTextFormField(
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                  // С таким фильтром могут быть введены только числа
                ],
                value: NumUtils.humanizeNumber(
                      (state.weight.value ?? 0) /
                          (state.weightType == WeightTypeEnum.kilograms
                              ? 1000
                              : 1),
                      withDoublePercision: true,
                    ) ??
                    '',
                onChange: serviceLocator<CreateFoodCubit>().handleWeightChanged,
                hintText: 'Вес',
                keyboardType: TextInputType.number,
                hasError: state.weight.status == FormzInputStatus.invalid,
                errorText:
                    state.weight.error != null ? 'Обязательное поле' : null,
                height: 42,
              ),
            ),
            SizedBox(
              width: 7,
            ),
            Container(
              height: 42,
              padding: EdgeInsets.symmetric(horizontal: 19),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Color(0xFFD8D8D8)),
                color: Colors.white,
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<WeightTypeEnum?>(
                  value: state.weightType,
                  onChanged:
                      serviceLocator<CreateFoodCubit>().handleWeightTypeChanged,
                  items: mapWeightTypes(),
                ),
              ),
            )
          ],
        );
      },
    );
  }

  List<DropdownMenuItem<WeightTypeEnum>> mapWeightTypes() {
    List<DropdownMenuItem<WeightTypeEnum>> items = [];

    for (int i = 0; i < WeightTypeEnum.values.length; i++) {
      items.add(
        DropdownMenuItem(
          value: WeightTypeEnum.values[i],
          child: Text(
            WeightTypeEnum.values[i].value!,
            style: text500Size15Black,
          ),
        ),
      );
    }

    return items;
  }
}

class _DishImagesUpload extends StatelessWidget {
  const _DishImagesUpload({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateFoodCubit, CreateFoodState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AddImagesView(
              images: state.uploadedImages.value,
              onDeleteImage: serviceLocator<CreateFoodCubit>().removeImage,
              onSelectImage: serviceLocator<CreateFoodCubit>().uploadImage,
              onSelectImages: serviceLocator<CreateFoodCubit>().uploadImages,
              onSwapImages: serviceLocator<CreateFoodCubit>().swapImages,
            ),
            if (state.uploadedImages.invalid
                ? state.uploadedImages.error != null
                : false)
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 10),
                child: Text(
                  state.uploadedImages.error.toString(),
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              )
          ],
        );
      },
    );
  }
}
