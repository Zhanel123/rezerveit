import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/widgets/merchant_settings_screen.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

import '../../../models/order/order.dart';
import '../../home_screen/widgets/merchant_cart.dart';
import '../../widgets/my_app_bar.dart';
import '../../widgets/search/search_field.dart';
import 'widgets/merchant_orders_list.dart';

class MerchantOrdersScreen extends StatefulWidget {
  const MerchantOrdersScreen({Key? key}) : super(key: key);

  @override
  State<MerchantOrdersScreen> createState() => _MerchantOrdersScreenState();
}

class _MerchantOrdersScreenState extends State<MerchantOrdersScreen>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    _controller = TabController(
      vsync: this,
      initialIndex: OrderStatusEnum.values.indexWhere((element) =>
          element == serviceLocator<MerchantOrdersCubit>().state.statusFilter),
      length: OrderStatusEnum.values.length,
    );

    _controller.addListener(() {
      print(_controller.index);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  late TabController _controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 18),
      color: Color(0xffF5F5F5),
      child: BlocConsumer<MerchantOrdersCubit, MerchantOrdersState>(
        listenWhen: (prev, curr) => prev.statusFilter != curr.statusFilter,
        listener: (context, state) {
          _controller.animateTo(OrderStatusEnum.values
              .indexWhere((element) => element == state.statusFilter));
        },
        builder: (context, state) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: MyAppBar(),
              ),
              SizedBox(
                height: 13,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  children: [
                    Expanded(
                      child: RoundedTextFormField(
                        value: state.searchQuery,
                        onChange: serviceLocator<MerchantOrdersCubit>()
                            .handleSearchQueryChanged,
                        hintText: 'Поиск по заказам',
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.search),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white),
                      width: 40,
                      height: 40,
                      child: Center(
                        child: InkWell(
                          onTap: () {
                            _handleFilterClicked();
                          },
                          child: SvgPicture.asset(
                              AppAssets.merchant_grey_settings_icon),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: BlocBuilder<ProfileCubit, ProfileState>(
                  builder: (context, state) {
                    if (state.profile != null) {
                      return MerchantInfoCard(
                        merchant: state.profile!,
                      );
                    }
                    return SizedBox();
                  },
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white),
                  padding: EdgeInsets.all(18),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Заказы',
                            style: text700Size14Black,
                          ),
                          // order_amount == 0
                          //     ? SizedBox()
                          //     : InkWell(
                          //         onTap: () {},
                          //         child: Text(
                          //           'К статистике',
                          //           style: text400Size12Pink,
                          //         ))
                        ],
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          state.orders.length == 0
                              ? Text(
                                  'У вас ни одного заказа на данный момент.',
                                  style: text400Size12GreyItalic,
                                )
                              : Row(
                                  children: [
                                    Container(
                                      width: 8,
                                      height: 8,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          color: Color(0xff37B023)),
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                        'Всего ${state.orders.length} заказа. ')
                                  ],
                                ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: TabBar(
                      tabs: mapOrdersStatusTab(),
                      onTap: serviceLocator<MerchantOrdersCubit>()
                          .handleStatusFilterChanged,
                      isScrollable: true,
                      controller: _controller,
                      labelColor: Color(0xFF000000),
                      unselectedLabelColor: Color(0x80000000),
                      unselectedLabelStyle: TextStyle(color: Color(0x80000000)),
                      padding: const EdgeInsets.symmetric(horizontal: 0),
                      labelStyle: TextStyle(
                        color: Color(0xFF000000),
                        fontWeight: FontWeight.w700,
                      ),
                      indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        // Creates border
                        // color: Colors.greenAccent,
                      ),
                    ),
                  ),
                ],
              ),
              if (state.orders.isNotEmpty)
                Expanded(
                  child: MerchantOrdersList(
                    orders: state.orders,
                    onScrollEnd: () => serviceLocator<MerchantOrdersCubit>()
                        .getMerchantOrders(append: true),
                  ),
                )
            ],
          );
        },
      ),
    );
  }

  void _handleFilterClicked() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return MerchantSettingsScreen();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
  }

  List<Tab> mapOrdersStatusTab() {
    List<Tab> options = [];
    for (int i = 0; i < OrderStatusEnum.values.length; i++) {
      options.add(Tab(
        text: OrderStatusEnum.values[i].label ?? '',
      ));
    }

    return options;
  }
}
