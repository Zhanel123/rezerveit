import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/chat/cubit.dart';
import 'package:home_food/blocs/order_detail/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/widgets/merchant_cancel_order_screen.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/widgets/order_dish_card.dart';
import 'package:home_food/screens/order/widgets/review_container.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service_locator.dart';
import 'package:intl/intl.dart';
import 'package:seafarer/seafarer.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/text_styles.dart';
import '../../../../models/order/order.dart';
import '../../../../routes.dart';
import '../../../order/widgets/text_container.dart';

class MerchantOrderScreenArgs extends BaseArguments {
  final OrderModel order;

  MerchantOrderScreenArgs(
    this.order,
  );
}

class MerchantOrderScreen extends StatefulWidget {
  const MerchantOrderScreen({
    Key? key,
    required this.args,
  }) : super(key: key);

  final MerchantOrderScreenArgs args;

  @override
  State<MerchantOrderScreen> createState() => _MerchantOrderScreenState();
}

class _MerchantOrderScreenState extends State<MerchantOrderScreen> {
  @override
  void initState() {
    serviceLocator<OrderDetailCubit>().handleOrderChanged(widget.args.order);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        color: Color(0xffF5F5F5),
        child: BlocConsumer<OrderDetailCubit, OrderDetailState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state.status.isSubmissionInProgress) {
              return Center(
                child: CircularProgressIndicator(
                  color: AppColors.pinkColor,
                ),
              );
            }
            return Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      padding: EdgeInsets.only(top: 48),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SvgPicture.asset(AppAssets.app_bar_logo),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 8.44),
                                      child: Text('Reserveat',
                                          style: text700Size18Black),
                                    )
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 12.0),
                                  child: InkWell(
                                    onTap: () {
                                      Routes.router.pop();
                                    },
                                    child: Container(
                                      width: 32,
                                      height: 32,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          color: Colors.white),
                                      child: Center(
                                        child: Icon(
                                          Icons.close,
                                          size: 17,
                                          color: Color(0xff7A7A7A),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Text(
                              state.order!.status != OrderStatusEnum.DELIVERED
                                  ? 'Заказ оформлен'
                                  : 'Заказ выполнен',
                              style: text700Size18Black,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                RichText(
                                  text: TextSpan(
                                      text: '#${state.order!.orderNumber}',
                                      style: text700Size15Black,
                                      children: [
                                        TextSpan(
                                          text:
                                              '   От ${state.order!.timeOfDate == DateTime(0) ? '' : _dateTimeFormatter(state.order!.timeOfDate)}',
                                          style: text400Size13Grey,
                                        )
                                      ]),
                                ),
                                if (state.order!.status !=
                                    OrderStatusEnum.CANCELED)
                                  TextButton(
                                    onPressed: () => handleCancelOrder(),
                                    child: Text(
                                      'Отменить',
                                      style: text400Size13Pink,
                                    ),
                                  )
                              ],
                            ),
                          ),
                          // ReasonButton(),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              children: [
                                ...state.order!.positionList.map(
                                  (e) => OrderDishCard(
                                    orderDish: e,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: TextContainer(
                              firstText: 'Итого оплачено',
                              secondText: NumUtils.humanizeNumber(
                                state.order!.total,
                                isCurrency: true,
                              ),
                            ),
                          ),
                          // if (true)
                          //   Padding(
                          //     padding: EdgeInsets.symmetric(horizontal: 16),
                          //     child: ToReturnWidget(
                          //       price: 1130,
                          //       amount: 2,
                          //     ),
                          //   ),
                          Padding(
                            padding: EdgeInsets.only(
                                bottom: 5, top: 3, left: 16, right: 16),
                            child: Container(
                              padding: EdgeInsets.only(
                                  top: 13, bottom: 11, left: 19, right: 23),
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(0.75),
                                  borderRadius: BorderRadius.circular(8)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Статус заказа',
                                    textAlign: TextAlign.start,
                                    style: text700Size12Black,
                                  ),
                                  SizedBox(
                                    height: 7,
                                  ),
                                  SizedBox(
                                    height: 63,
                                    child: Row(
                                      children: mapOrderStatus(state.order!),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          if (state.order!.grade != null)
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16.0)
                                      .copyWith(bottom: 0),
                              child: ReviewContainer(
                                grade: state.order!.grade!,
                              ),
                            ),
                          // Padding(
                          //   padding: const EdgeInsets.symmetric(horizontal: 16),
                          //   padding: const EdgeInsets.symmetric(horizontal: 16),
                          //   child: _ShowOnMapButton(),
                          // ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              children: [
                                if (state.order!.status ==
                                    OrderStatusEnum.ORDERED)
                                  SizedBox(
                                    width: double.infinity,
                                    child: GradientButton(
                                      gradient: LinearGradient(
                                        begin: Alignment.center,
                                        end: Alignment.bottomRight,
                                        colors: [
                                          Color(0xFF37B023),
                                          Color(0xFF72EF43),
                                        ],
                                      ),
                                      labelText: 'Принять заказ',
                                      onPressed: () =>
                                          serviceLocator<OrderDetailCubit>()
                                              .handleApproveOrder(
                                                  state.order!.id),
                                    ),
                                  ),
                                if (state.order!.status ==
                                    OrderStatusEnum.ORDER_ACCEPTED)
                                  SizedBox(
                                    width: double.infinity,
                                    child: GradientButton(
                                      gradient: LinearGradient(
                                        begin: Alignment.center,
                                        end: Alignment.bottomRight,
                                        colors: [
                                          Color(0xFF37B023),
                                          Color(0xFF72EF43),
                                        ],
                                      ),
                                      labelText: 'Сбор заказа',
                                      onPressed: () =>
                                          serviceLocator<OrderDetailCubit>()
                                              .handlePrepareOrder(
                                                  state.order!.id),
                                    ),
                                  ),
                                if (state.order!.status ==
                                    OrderStatusEnum.PREPARATION)
                                  SizedBox(
                                    width: double.infinity,
                                    child: GradientButton(
                                      gradient: LinearGradient(
                                        begin: Alignment.center,
                                        end: Alignment.bottomRight,
                                        colors: [
                                          Color(0xFF37B023),
                                          Color(0xFF72EF43),
                                        ],
                                      ),
                                      labelText: 'На доставку',
                                      onPressed: () =>
                                          serviceLocator<OrderDetailCubit>()
                                              .handleToDeliveryOrder(
                                                  state.order!.id),
                                    ),
                                  ),
                                if (state.order!.status ==
                                    OrderStatusEnum.DELIVERY)
                                  SizedBox(
                                    width: double.infinity,
                                    child: GradientButton(
                                      gradient: LinearGradient(
                                        begin: Alignment.center,
                                        end: Alignment.bottomRight,
                                        colors: [
                                          Color(0xFF37B023),
                                          Color(0xFF72EF43),
                                        ],
                                      ),
                                      labelText: 'Получен покупателем',
                                      onPressed: () =>
                                          serviceLocator<OrderDetailCubit>()
                                              .handleDeliveredOrder(
                                        state.order!.id,
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          SizedBox(height: 24),
                          SizedBox(
                            width: double.infinity,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 70),
                              child: GradientButton(
                                borderRadius: BorderRadius.circular(50),
                                labelText: 'К списку заказов',
                                onPressed: () => Routes.router.pop(),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: InkWell(
                              child: Text(
                                'Связаться с покупателем',
                                style: text400Size13Pink,
                              ),
                              onTap: () => serviceLocator<ChatCubit>()
                                  .createChat(state.order!),
                            ),
                          ),
                        ],
                      )
                      // : Column(
                      //     crossAxisAlignment: CrossAxisAlignment.start,
                      //     children: [
                      //       Padding(
                      //         padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      //         child: Row(
                      //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //           children: [
                      //             Row(
                      //               children: [
                      //                 SvgPicture.asset(AppAssets.app_bar_logo),
                      //                 Padding(
                      //                   padding: const EdgeInsets.only(left: 8.44),
                      //                   child:
                      //                       Text('HOMEFOOD', style: text700Size18Black),
                      //                 )
                      //               ],
                      //             ),
                      //             Padding(
                      //               padding: const EdgeInsets.symmetric(vertical: 12.0),
                      //               child: InkWell(
                      //                 onTap: () {
                      //                   Routes.router.pop();
                      //                 },
                      //                 child: Container(
                      //                   width: 32,
                      //                   height: 32,
                      //                   decoration: BoxDecoration(
                      //                       borderRadius: BorderRadius.circular(50),
                      //                       color: Colors.white),
                      //                   child: Center(
                      //                     child: Icon(
                      //                       Icons.close,
                      //                       size: 17,
                      //                       color: Color(0xff7A7A7A),
                      //                     ),
                      //                   ),
                      //                 ),
                      //               ),
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //       Padding(
                      //         padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      //         child: Text(
                      //           'Заказ оформлен',
                      //           // : 'Заказ выполнен',
                      //           style: text700Size18Black,
                      //         ),
                      //       ),
                      //       Padding(
                      //         padding: EdgeInsets.only(left: 16, right: 16, top: 5),
                      //         child: Row(
                      //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //           children: [
                      //             Text('От 12.02.22 15:33', style: text400Size13Grey),
                      //             TextButton(
                      //                 onPressed: () {
                      //                   Routes.router.pop();
                      //                 },
                      //                 child: Text(
                      //                   'Отменить',
                      //                   style: text400Size13Pink,
                      //                 ))
                      //           ],
                      //         ),
                      //       ),
                      //       // Padding(
                      //       //   padding: EdgeInsets.symmetric(horizontal: 16),
                      //       //   child: OrderDishCard(
                      //       //     imageUrl: AppAssets.merchant_food_1,
                      //       //     label: 'Суп гороховый с говядиной',
                      //       //     price: '1 130  ₸',
                      //       //     weight: 300,
                      //       //     amount: 2,
                      //       //     inStock: true,
                      //       //   ),
                      //       // ),
                      //       Padding(
                      //         padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      //         child: TextContainer(
                      //           firstText: 'Итого оплачено',
                      //           secondText: '3 150  ₸',
                      //         ),
                      //       ),
                      //       if (true)
                      //         Padding(
                      //           padding: EdgeInsets.symmetric(horizontal: 16),
                      //           child: ToReturnWidget(
                      //             price: 1130,
                      //             amount: 2,
                      //           ),
                      //         ),
                      //       Padding(
                      //         padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      //         child: TextContainer(
                      //           firstText: 'Время доставки',
                      //           secondText: '~ 60 мин.',
                      //           secondStyle: text700Size13Grey,
                      //         ),
                      //       ),
                      //       Padding(
                      //         padding: EdgeInsets.only(
                      //             bottom: 15, top: 3, left: 16, right: 16),
                      //         child: Container(
                      //           padding: EdgeInsets.only(
                      //               top: 13, bottom: 11, left: 19, right: 23),
                      //           width: double.infinity,
                      //           decoration: BoxDecoration(
                      //               color: Colors.white.withOpacity(0.75),
                      //               borderRadius: BorderRadius.circular(8)),
                      //           child: Column(
                      //             crossAxisAlignment: CrossAxisAlignment.start,
                      //             children: [
                      //               Text(
                      //                 'Статус заказа',
                      //                 textAlign: TextAlign.start,
                      //                 style: text700Size12Black,
                      //               ),
                      //               SizedBox(
                      //                 height: 7,
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //       ),
                      //       SizedBox(
                      //         height: 3,
                      //       ),
                      //       Padding(
                      //         padding: EdgeInsets.symmetric(horizontal: 16),
                      //         child: InkWell(
                      //             child: Container(
                      //           padding: EdgeInsets.symmetric(vertical: 12),
                      //           width: double.infinity,
                      //           decoration: BoxDecoration(
                      //               borderRadius: BorderRadius.circular(8),
                      //               color: Color(0xff37B023)),
                      //           child: Center(
                      //             child: Text(
                      //               'Получен покупателем',
                      //               style: text700Size12White,
                      //             ),
                      //           ),
                      //         )),
                      //       ),
                      //       Padding(
                      //         padding: EdgeInsets.symmetric(horizontal: 16),
                      //         child: InkWell(
                      //           child: TextContainer(
                      //             firstText: 'Показать на карте',
                      //             firstStyle: text700Size12Black,
                      //           ),
                      //         ),
                      //       ),
                      //       SizedBox(
                      //         height: 14,
                      //       ),
                      //       Center(
                      //           child: GradientButton(
                      //         labelText: 'Сохранить изменения',
                      //         onPressed: () {
                      //           Routes.router.pop();
                      //         },
                      //       )),
                      //       Center(
                      //         child: InkWell(
                      //           child: Text(
                      //             'Связаться с покупателем',
                      //             style: text400Size13Pink,
                      //           ),
                      //           onTap: () {
                      //             Routes.router.navigate(Routes.merchantChatScreen);
                      //           },
                      //         ),
                      //       )
                      //     ],
                      //   ),
                      ),
                ),
                if (state.order!.status == OrderStatusEnum.DELIVERED)
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      children: [
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //   children: [
                        //     Text(
                        //       'Добавлен отзыв',
                        //       style: text700Size15Black,
                        //     ),
                        //     TextButton(
                        //         onPressed: () {},
                        //         child: Text(
                        //           'Править',
                        //           style: text400Size13Pink,
                        //         ))
                        //   ],
                        // ),
                      ],
                    ),
                  ),
                SizedBox(height: 24),
              ],
            );
          },
        ),
      ),
    );
  }

  Future<void> handleCancelOrder() async {
    await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return MerchantCancelOrderScreen(
          order: widget.args.order,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }

  mapOrderStatus(
    OrderModel order,
  ) {
    final currentStatus = order.status;
    final List<Widget> children = [];
    final values = [
      OrderStatusEnum.ORDERED,
      OrderStatusEnum.ORDER_ACCEPTED,
      OrderStatusEnum.PREPARATION,
      OrderStatusEnum.DELIVERY,
      OrderStatusEnum.DELIVERED,
    ];
    final dateValues = [
      order.timeOfDate,
      order.acceptedOrderTime,
      order.preparationStartTime,
      order.deliveryStartTime,
      order.deliveredOrderTime,
    ];
    for (int i = 0; i < values.length; i++) {
      bool isActive = values[i] == currentStatus;
      int activeIndex =
          values.indexWhere((element) => element == currentStatus);
      children.add(Expanded(
        child: Stack(
          children: [
            Center(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      height: 5,
                      color: i == activeIndex || i <= activeIndex
                          ? Colors.green
                          : Colors.white,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      height: 5,
                      color: i < activeIndex ||
                              (i == values.length - 1 && i == activeIndex)
                          ? Colors.green
                          : Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Positioned.fill(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: i == 0
                    ? CrossAxisAlignment.start
                    : i == values.length - 1
                        ? CrossAxisAlignment.end
                        : CrossAxisAlignment.center,
                children: [
                  Text(
                    values[i].label ?? '',
                    style: isActive ? text700Size11Success : text400Size11Black,
                  ),
                  isActive
                      ? Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xFF37B023),
                              width: 5,
                            ),
                          ),
                        )
                      : Container(
                          width: 13,
                          height: 13,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(
                              color: Colors.black,
                              width: 3.25,
                            ),
                          ),
                        ),
                  dateValues[i] != null
                      ? Text('${dateValues[i]?.hour}:${dateValues[i]?.minute}')
                      : SizedBox(
                          height: 10,
                        ),
                ],
              ),
            ),
          ],
        ),
      ));
    }

    return children;
  }

  String _dateTimeFormatter(DateTime dateTime) {
    String formattedDate = DateFormat('yyyy.MM.dd kk:mm').format(dateTime);
    return formattedDate;
  }
}

class _ShowOnMapButton extends StatelessWidget {
  const _ShowOnMapButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5.0),
      child: InkWell(
        child: Container(
            width: double.infinity,
            height: 44,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.75),
              borderRadius: BorderRadius.circular(7),
              border: Border.all(
                color: Color(0xffD8D8D8), //                   <--- border color
                width: 1.0,
              ),
            ),
            child: Center(
              child: Text(
                'Показать на карте',
                style: text700Size12Black,
              ),
            )),
      ),
    );
  }
}
