import 'package:flutter/material.dart';
import 'package:home_food/core/text_styles.dart';

class ToReturnWidget extends StatelessWidget {
  final int price;
  final int amount;

  const ToReturnWidget({Key? key, required this.price, required this.amount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: 12, bottom: 11, left: 19, right: 23),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.75),
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Color(0xffD8D8D8), //                   <--- border color
          width: 1.0,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Text(
                'К возврату',
                style: text700Size12Black,
              ),
              SizedBox(
                width: 7,
              ),
              Text(
                '${price} ₸',
                style: text700Size15Pink,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'Итого',
                style: text700Size12Black,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                '${price * amount} ₸',
                style: text700Size15Green,
              )
            ],
          )
        ],
      ),
    );
  }
}
