import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

import '../../../../core/text_styles.dart';
import '../../../../routes.dart';
import '../../../widgets/check_row.dart';

class MerchantSettingsScreen extends StatefulWidget {
  const MerchantSettingsScreen({Key? key}) : super(key: key);

  @override
  State<MerchantSettingsScreen> createState() => _MerchantSettingsScreenState();
}

class _MerchantSettingsScreenState extends State<MerchantSettingsScreen> {
  bool display = false;

  @override
  void initState() {
    display = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileCubit, ProfileState>(
      listener: (context, state) {
        if (state.updateProfileStatus.isSubmissionSuccess) {
          Routes.router.pop();
          FlushbarService().showSuccessMessage(message: 'Успешно обновлено');
        }
      },
      builder: (context, state) {
        return PrimaryBottomSheet(
          contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 18),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: InkWell(
                    onTap: () {
                      Routes.router.pop();
                    },
                    child: Container(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          color: Color(0xffEAEAEA),
                          borderRadius: BorderRadius.circular(28)),
                      child: Icon(
                        Icons.close,
                        color: Color(0xff7A7A7A),
                        size: 15,
                      ),
                    )),
              ),
              Text(
                'Настройки заказов',
                style: text700Size15Black,
              ),
              SizedBox(
                height: 13,
              ),
              Text(
                'Дополнительные настройки',
                style: text400Size13Grey,
              ),
              SizedBox(
                height: 13,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CheckRow(
                      widthParameter: double.infinity,
                      label: 'Отображать сначала активные заказы',
                      isCheck: state.showActiveOrders,
                      height: 38,
                      handleValue:
                          serviceLocator<ProfileCubit>().showActiveOrdersChanged,
                    ),
                    CheckRow(
                      widthParameter: double.infinity,
                      height: 38,
                      label:
                          'Отображать заказы вместо статистики на главном экране',
                      isCheck: state.showStatisticsPanel,
                      handleValue: serviceLocator<ProfileCubit>()
                          .showStatisticsPanelChanged,
                    ),
                    // CheckRow(
                    //   widthParameter: 222,
                    //   height: 38,
                    //   label: 'Дублировать заказы на e-mail',
                    //   isCheck: state.duplicateOrdersByEmail,
                    //   handleValue: serviceLocator<ProfileCubit>()
                    //       .duplicateOrdersByEmailChanged,
                    // ),
                    SizedBox(
                      height: 13,
                    ),
                    Container(
                        width: 222,
                        child: GradientButton(
                          labelText: 'Сохранить',
                          isLoading:
                              state.updateProfileStatus.isSubmissionInProgress,
                          onPressed: () async {
                            await serviceLocator<ProfileCubit>()
                                .updateProfileSettings();
                          },
                        )),
                    // TextButton(
                    //     onPressed: () {
                    //       Routes.router.pop();
                    //     },
                    //     child: Text(
                    //       'Сбросить настройки',
                    //       style: text400Size13Pink,
                    //     ))
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
