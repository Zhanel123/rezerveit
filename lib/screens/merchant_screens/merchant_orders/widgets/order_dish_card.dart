import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/order_position/order_position.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/text_styles.dart';
import '../../../../routes.dart';
import '../../../widgets/check_row.dart';
import 'dish_replace_screen.dart';

class OrderDishCard extends StatefulWidget {
  final OrderPosition orderDish;

  const OrderDishCard({
    Key? key,
    required this.orderDish,
  }) : super(key: key);

  @override
  State<OrderDishCard> createState() => _OrderDishCardState();
}

class _OrderDishCardState extends State<OrderDishCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
      padding: EdgeInsets.all(13),
      margin: EdgeInsets.only(bottom: 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  _FoodThumbImage(
                    images: widget.orderDish.dishCardItem.images,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Container(
                    width: 133,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.orderDish.dishCardItem.name,
                          textAlign: TextAlign.left,
                          style: text400Size12Black,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              NumUtils.humanizeNumber(
                                      widget.orderDish.dishCardItem.price) ??
                                  '',
                              style: text700Size15Black,
                            ),
                            SizedBox(
                              width: 3,
                            ),
                            Container(
                              width: 3,
                              height: 3,
                              decoration: BoxDecoration(
                                  color: AppColors.grey,
                                  borderRadius: BorderRadius.circular(50)),
                            ),
                            SizedBox(
                              width: 6,
                            ),
                            Text('${widget.orderDish.dishCardItem.weight} г',
                                style: text400Size12Grey)
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Container(
                width: 46,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(
                    color: Color(0xffCFCFCF),
                    //                   <--- border color
                    width: 1.0,
                  ),
                ),
                child: Center(
                  child: Text(
                    '${widget.orderDish.amount}',
                    style: text400Size13Black,
                  ),
                ),
              )
            ],
          ),
          ///TODO: Показывать у мерчанта В наличии или нет
          // Row(
          //   crossAxisAlignment: CrossAxisAlignment.center,
          //   children: [
          //     CheckRow(
          //       label: 'Нет в наличии',
          //       // isCheck: widget.inStock,
          //       widthParameter: 100,
          //       handleValue: (value) {
          //         setState(() {
          //           // isHomeFood = value ?? false;
          //         });
          //       },
          //     ),
          //     SizedBox(
          //       width: 5,
          //     ),
          //     // if (widget.inStock == true)
          //     InkWell(
          //       onTap: () {
          //         _handleDishReplaceButton();
          //       },
          //       child: Text(
          //         'Заменить',
          //         style: text400Size13Pink,
          //       ),
          //     )
          //   ],
          // )
        ],
      ),
    );
  }

  void _handleDishReplaceButton() {
    Scaffold.of(context).showBottomSheet<void>(
      (BuildContext context) {
        return DishReplaceScreen();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.4,
        maxHeight: MediaQuery.of(context).size.height * 0.65,
      ),
    );
  }
}

class _FoodThumbImage extends StatelessWidget {
  const _FoodThumbImage({
    Key? key,
    this.images = const [],
  }) : super(key: key);

  final List<UploadedImage> images;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 53,
      width: 53,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
      ),
      clipBehavior: Clip.hardEdge,
      child: images.isEmpty
          ? Container()
          : CachedNetworkImage(
              imageUrl: images.first.imageUrl,
              fit: BoxFit.cover,
            ),
    );
  }
}
