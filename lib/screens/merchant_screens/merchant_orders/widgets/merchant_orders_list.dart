import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/widgets/merchant_order_item.dart';

class MerchantOrdersList extends StatefulWidget {
  const MerchantOrdersList({
    Key? key,
    this.orders = const [],
    this.onScrollEnd,
  }) : super(key: key);

  final List<OrderModel> orders;
  final VoidCallback? onScrollEnd;

  @override
  State<MerchantOrdersList> createState() => _MerchantOrdersListState();
}

class _MerchantOrdersListState extends State<MerchantOrdersList> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() {
      var triggerFetchMoreSize =
          0.7 * scrollController.position.maxScrollExtent;

      if (scrollController.position.pixels > triggerFetchMoreSize) {
        widget.onScrollEnd?.call();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MerchantOrdersCubit, MerchantOrdersState>(
      builder: (context, state) {
        return ListView(
          controller: scrollController,
          padding: EdgeInsets.symmetric(horizontal: 16),
          children: mapMerchantOrdersToWidget(),
        );
      },
    );
  }

  List<Widget> mapMerchantOrdersToWidget() {
    final List<Widget> children = [];

    for (int i = 0; i < widget.orders.length; i++) {
      children.add(MerchantOrderItem(
        order: widget.orders[i],
      ));
    }

    return children;
  }
}
