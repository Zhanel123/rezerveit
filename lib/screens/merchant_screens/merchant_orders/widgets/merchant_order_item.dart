import 'package:flutter/material.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/widgets/merchant_order_screen.dart';
import 'package:intl/intl.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/text_styles.dart';
import '../../../../models/order/order.dart';
import '../../../../routes.dart';

class MerchantOrderItem extends StatelessWidget {
  final OrderModel order;
  final bool status;

  const MerchantOrderItem({
    Key? key,
    required this.order,
    this.status = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Routes.router.navigate(
          Routes.merchantOrderScreen,
          args: MerchantOrderScreenArgs(
            order,
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 3),
        height: 74,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white.withOpacity(status == true ? 0.6 : 1),
        ),
        child: Row(
          children: [
            if (status != true)
              Container(
                width: 3,
                color: AppColors.pinkColor,
              ),
            Padding(
              padding: const EdgeInsets.only(left: 21.0, top: 18, bottom: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        '#${order.orderNumber}',
                        textAlign: TextAlign.start,
                        style: text700Size15Black,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      order.preOrder == true
                          ? Text(
                              'предзаказ',
                              textAlign: TextAlign.start,
                              style: text400Size12PinkItalic,
                            )
                          : SizedBox()
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    // 'От ${DateFormat('dd.MM.yy hh:mm').format(order.timeOfDate)}',
                    'От ${order.timeOfDate == DateTime(0) ? '' : _dateTimeFormatter(order.timeOfDate)}',
                    style: text400Size13Grey,
                  )
                ],
              ),
            ),
            Spacer(),
            Container(
              width: 110,
              padding: const EdgeInsets.only(right: 14.0, top: 16, bottom: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    NumUtils.humanizeNumber(
                          order.total,
                          isCurrency: true,
                        ) ??
                        '',
                    textAlign: TextAlign.start,
                    style:
                        status == true ? text700Size15Black : text700Size15Pink,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    order.status.label ?? '',
                    style: text400Size13Grey.copyWith(
                      color: order.status.color,
                    ),
                  )

                  ///TODO Merchant status order
                  // Text(
                  //   order.merchantStatus ?? '',
                  //   style: order.merchantStatus == 'Возврат'
                  //       ? text400Size13Pink
                  //       : text400Size13Green,
                  // )
                ],
              ),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: Color(0xff7A7A7A),
              size: 10,
            ),
            SizedBox(
              width: 16,
            )
          ],
        ),
      ),
    );
  }

  String _dateTimeFormatter(DateTime dateTime) {
    String formattedDate = DateFormat('yyyy.MM.dd kk:mm').format(dateTime);
    return formattedDate;
  }
}
