import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/customer_cancel_order/cubit.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/blocs/merchant_cancel_order/cubit.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/blocs/order_detail/cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

class MerchantCancelOrderScreen extends StatelessWidget {
  final OrderModel order;

  const MerchantCancelOrderScreen({
    Key? key,
    required this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MerchantCancelOrderCubit, MerchantCancelOrderState>(
      listenWhen: (p, c) => p.cancelStatus != c.cancelStatus,
      listener: (context, state) async {
        if (state.cancelStatus.isSubmissionSuccess) {
          Navigator.of(context).pop();
          FlushbarService().showSuccessMessage(
            context: context,
            message: 'Заказ отменен',
          );

          await serviceLocator<MerchantOrdersCubit>().getMerchantOrders();
          await serviceLocator<OrderDetailCubit>().getOrderById();
        } else if (state.cancelStatus.isSubmissionFailure) {
          Navigator.of(context).pop();
          FlushbarService().showErrorMessage(
            context: context,
            message: state.error,
          );
        }
      },
      builder: (context, state) {
        return PrimaryBottomSheet(
          contentPadding: const EdgeInsets.only(
            bottom: 18,
            right: 18,
            left: 18,
            top: 22,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        color: Color(0xffEAEAEA),
                        borderRadius: BorderRadius.circular(28)),
                    child: Icon(
                      Icons.close,
                      color: Color(0xff7A7A7A),
                      size: 15,
                    ),
                  )),
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        'Отмена заказа',
                        textAlign: TextAlign.center,
                        style: text700Size15Black,
                      ),
                      width: 130,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: RatingBar.builder(
                        initialRating: (state.grade.value ?? 0).toDouble(),
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: false,
                        itemCount: 5,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) =>
                            serviceLocator<MerchantCancelOrderCubit>()
                                .changeGrade(rating.toInt()),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 23, left: 21, right: 21),
                      child: RoundedTextFormField(
                        value: state.comment.value,
                        hasError:
                            state.comment.status == FormzInputStatus.invalid,
                        errorText: state.comment.error != null
                            ? 'Обязательное поле'
                            : null,
                        onChange: (value) =>
                            serviceLocator<MerchantCancelOrderCubit>()
                                .changeComment(value),
                        hintText: 'Причина отмены заказа...',
                        keyboardType: TextInputType.text,
                        minLines: 3,
                        maxLines: 6,
                        height: 84,
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            top: 23, left: 50, right: 50, bottom: 20),
                        child: GradientButton(
                          borderRadius: BorderRadius.circular(50),
                          labelText: 'Подтвердить',
                          onPressed: () =>
                              serviceLocator<MerchantCancelOrderCubit>()
                                  .cancelOrder(
                            order: order,
                          ),
                        )),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
