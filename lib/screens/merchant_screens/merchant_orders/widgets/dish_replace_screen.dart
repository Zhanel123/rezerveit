import 'package:flutter/material.dart';

import '../../../../core/text_styles.dart';
import '../../../../routes.dart';
import '../../../widgets/search/search_field.dart';
import 'dish_tab_list.dart';

class DishReplaceScreen extends StatefulWidget {
  const DishReplaceScreen({Key? key}) : super(key: key);

  @override
  State<DishReplaceScreen> createState() => _DishReplaceScreenState();
}

class _DishReplaceScreenState extends State<DishReplaceScreen> with TickerProviderStateMixin {

  final List<Tab> topTabs = <Tab>[
    new Tab(
      child: Text(
        'Все',
        style: text700Size15Grey,
      ),
    ),
    new Tab(
      child: Text(
        'Первые блюда',
        style: text700Size15Grey,
      ),
    ),
    new Tab(
      child: Text(
        'Вторые',
        style: text700Size15Grey,
      ),
    ),
    new Tab(
      child: Text(
        'Десерты',
        style: text700Size15Grey,
      ),
    ),
  ];

  late TabController _controller = TabController(vsync: this, length: 4);

  @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, length: 4);

    _controller.addListener(() {
      print(_controller.index);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
      padding: EdgeInsets.all(18),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            InkWell(
                onTap: () {
                  Routes.router.pop();
                },
                child: Container(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                      color: Color(0xffEAEAEA),
                      borderRadius: BorderRadius.circular(28)),
                  child: const Icon(
                    Icons.close,
                    color: Color(0xff7A7A7A),
                    size: 15,
                  ),
                )),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Замена блюда или добавить новое',
                    style: text700Size15Black,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SearchField(backgroundColor: Color(0xffF0F0F0), hintText: 'Поиск по наименованию',),
                  Column(
                    children: [
                      TabBar(
                          tabs: topTabs,
                          isScrollable: true,
                          controller: _controller,
                          unselectedLabelStyle: TextStyle(color: Color(0x80000000)),
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                            fontWeight: FontWeight.w700,
                          ),
                          indicator: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            // Creates border
                            // color: Colors.greenAccent,
                          )),
                      // SingleChildScrollView(child: MerchantFirstMeal()),
                      SingleChildScrollView(child: DishReplaceList())
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
