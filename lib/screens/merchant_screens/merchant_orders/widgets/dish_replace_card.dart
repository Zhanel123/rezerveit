import 'package:flutter/material.dart';
import 'package:home_food/core/app_colors.dart';

import '../../../../core/text_styles.dart';

class DishReplaceCard extends StatefulWidget {
  final String imageUrl;
  final String label;
  final String price;
  final int weight;
  final int amount;

  const DishReplaceCard(
      {Key? key, required this.imageUrl, required this.label, required this.price, required this.weight, required this.amount})
      : super(key: key);

  @override
  State<DishReplaceCard> createState() => _DishReplaceCardState();
}

class _DishReplaceCardState extends State<DishReplaceCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8)
      ),
      padding: EdgeInsets.only(left: 15, top: 13, bottom: 15, right: 17),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Image.asset(widget.imageUrl),
              SizedBox(width: 15,),
              Container(
                width: 133,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.label,
                      textAlign: TextAlign.left,
                      style: text400Size12Black,
                    ),
                    SizedBox(height: 5,),
                    RichText(
                      text: TextSpan(
                        text: widget.price,
                        style: text700Size15Black,
                        children: [
                          TextSpan(
                              text: '  ${widget.weight} г',
                              style: text400Size12Grey),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          Container(
            width: 90,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(
                color: widget.amount > 0 ? AppColors.pinkColor : Color(0xffCFCFCF), //                   <--- border color
                width: 1.0,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {},
                  child: Container(
                    width: 21.33,
                    height: 21.33,
                    decoration: BoxDecoration(
                        color: Color(0xffE9E9E9),
                        borderRadius: BorderRadius.circular(58)
                    ),
                    child: Center(
                      child: Icon(
                        Icons.remove,
                        color: Colors.black,
                        size: 10,
                      ),
                    ),
                  ),
                ),
                Text(
                  '${widget.amount}',
                  style: text400Size13Black,
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    width: 21.33,
                    height: 21.33,
                    decoration: BoxDecoration(
                        color: Color(0xffE9E9E9),
                        borderRadius: BorderRadius.circular(58)
                    ),
                    child: Center(
                        child: Text(
                          '+',
                          style: text400Size13Black,
                        )
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
