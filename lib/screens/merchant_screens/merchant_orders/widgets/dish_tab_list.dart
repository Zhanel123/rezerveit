import 'package:flutter/material.dart';

import '../../../../core/app_assets.dart';
import 'dish_replace_card.dart';

class DishReplaceList extends StatefulWidget {
  const DishReplaceList({Key? key}) : super(key: key);

  @override
  State<DishReplaceList> createState() => _DishReplaceListState();
}

class _DishReplaceListState extends State<DishReplaceList> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DishReplaceCard(
          imageUrl: AppAssets.cart_item_1,
          label: 'Пельмени свинина говядина',
          price: '1 130  ₸',
          weight: 300,
          amount: 2,
        ),
        DishReplaceCard(
          imageUrl: AppAssets.cart_item_1,
          label: 'Пельмени свинина говядина',
          price: '1 130  ₸',
          weight: 300,
          amount: 1,
        ),
        DishReplaceCard(
          imageUrl: AppAssets.cart_item_1,
          label: 'Пельмени свинина говядина',
          price: '1 130  ₸',
          weight: 300,
          amount: 0,
        ),
      ],
    );
  }
}
