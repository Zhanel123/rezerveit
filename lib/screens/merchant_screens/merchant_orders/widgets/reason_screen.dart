import 'package:flutter/material.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';

import '../../../../core/text_styles.dart';
import '../../../../routes.dart';

class ReasonScreen extends StatelessWidget {
  const ReasonScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
      padding: EdgeInsets.all(18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          InkWell(
              onTap: () {
                Routes.router.pop();
              },
              child: Container(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                    color: Color(0xffEAEAEA),
                    borderRadius: BorderRadius.circular(28)),
                child: const Icon(
                  Icons.close,
                  color: Color(0xff7A7A7A),
                  size: 15,
                ),
              )),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Покупатель потребовал возврат',
                  style: text700Size15Black,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Фотография блюда',
                  style: text400Size12Black,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  width: double.infinity,
                  height: 168,
                  child: Image.asset(AppAssets.return_dish, fit: BoxFit.fill,),
                ),
                SizedBox(height: 26,),
                Text(
                  'Причина возрата',
                  style: text400Size12Black,
                ),
                SizedBox(height: 8,),
                Text(
                  'Во-первых, все остыло. Во-вторых, что очень странно, привезли пельмени вместо мантов!',
                  style: text500Size13Black,
                ),
                SizedBox(height: 24,),
                Center(child: GradientButton(labelText: 'Вернули деньги')),
                SizedBox(height: 12,),
                Center(
                  child: InkWell(
                    child: Text(
                      'Связаться с покупателем',
                      style: text400Size13Pink,
                    ),
                    onTap: () { Routes.router.navigate(Routes.merchantChatScreen); },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
