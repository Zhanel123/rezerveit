import 'package:flutter/material.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/widgets/reason_screen.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/text_styles.dart';

class ReasonButton extends StatefulWidget {
  const ReasonButton({Key? key}) : super(key: key);

  @override
  State<ReasonButton> createState() => _ReasonButtonState();
}

class _ReasonButtonState extends State<ReasonButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Container(
          width: double.infinity,
          child: ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0),
                ),
              ),
              backgroundColor: MaterialStatePropertyAll<Color>(AppColors.pinkColor),
            ),
            onPressed: () { _handleReasonButton(); },
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 25),
              child: Text(
                'Посмотреть причину возврата',
                textAlign: TextAlign.center,
                style: text400Size15White,
              ),
            ),
          ),
        ));
  }

  void _handleReasonButton() {
    Scaffold.of(context).showBottomSheet<void>(
          (BuildContext context) {
        return const ReasonScreen();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.4,
        maxHeight: MediaQuery.of(context).size.height * 0.65,
      ),
    );
  }
}
