import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dish_edit/cubit.dart';
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/add_images_view.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/screens/widgets/my_app_bar.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'widgets/edit_food_description.dart';
import 'widgets/edit_food_discount.dart';
import 'widgets/edit_food_special.dart';

class MerchantFoodEditScreen extends StatefulWidget {
  const MerchantFoodEditScreen({Key? key}) : super(key: key);

  @override
  State<MerchantFoodEditScreen> createState() => _MerchantFoodEditScreenState();
}

class _MerchantFoodEditScreenState extends State<MerchantFoodEditScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<DishEditCubit, DishEditState>(
      listenWhen: (p, c) => p.status != c.status,
      listener: (context, state) {
        if (state.status.isSubmissionSuccess) {
          Routes.router.popUntil((p0) => p0.settings.name == Routes.homeScreen);
          FlushbarService().showSuccessMessage(
            context: context,
            message: 'Успешно',
          );
          serviceLocator<MyDishesCubit>().fetchMyDishes();
        } else if (state.status.isSubmissionFailure) {
          FlushbarService().showErrorMessage(
            context: context,
            message: state.error,
          );
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
            // height: double.infinity,
            color: Color(0xffF5F5F5),
            child: SafeArea(
              child: Stack(
                children: [
                  SingleChildScrollView(
                    padding: EdgeInsets.only(top: 18),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: MyAppBar(),
                        ),
                        SizedBox(
                          height: 13,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 21, vertical: 19),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 5.0),
                                  child: Text(
                                    'Редактирование блюда',
                                    style: text700Size14Black,
                                  ),
                                ),
                                SizedBox(
                                  height: 11,
                                ),
                                _SelectDishType(),
                                SizedBox(
                                  height: 9,
                                ),
                                _DishNameField(),
                                SizedBox(
                                  height: 9,
                                ),
                                // _IsPreOrderDish(),
                                _DishAmountField(),
                                SizedBox(
                                  height: 9,
                                ),
                                _WeightField(),
                                _HasHalfPortion(),
                                _MinDishAmountField(),
                                SizedBox(
                                  height: 9,
                                ),
                                EditFoodDescription(),
                                SizedBox(
                                  height: 9,
                                ),
                                EditFoodSpecial(),
                                SizedBox(
                                  height: 9,
                                ),
                                _PriceField(),
                                SizedBox(
                                  height: 9,
                                ),
                                EditFoodDiscount(),
                                SizedBox(
                                  height: 23,
                                ),
                                _DishImagesUpload(),
                                SizedBox(
                                  height: 29,
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 50.0),
                                  child: Center(child:
                                      BlocBuilder<DishEditCubit, DishEditState>(
                                    builder: (context, state) {
                                      return GradientButton(
                                        borderRadius: BorderRadius.circular(50),
                                        labelText: 'Сохранить',
                                        onPressed:
                                            serviceLocator<DishEditCubit>()
                                                .submitEditionDish,
                                      );
                                    },
                                  )),
                                ),
                                Center(
                                    child: TextButton(
                                        onPressed: () {
                                          Routes.router.pop();
                                        },
                                        child: Text(
                                          'Отменить',
                                          style: text400Size13Pink,
                                        )))
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class _SelectDishType extends StatelessWidget {
  const _SelectDishType({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      buildWhen: (p, c) => p.dishType != c.dishType,
      builder: (context, state) {
        return PrimaryDropdown(
          initialOption: state.dishType.value,
          options: mapDishTypes(context, state.dishTypes),
          error: state.dishType.error?.key ?? '',
          onChanged: (option) => serviceLocator<DishEditCubit>()
              .handleDishTypeChanged(option?.value),
        );
      },
    );
  }

  List<SelectOption<DishType>> mapDishTypes(
      BuildContext context, List<DishType> dishTypes) {
    List<SelectOption<DishType>> options = [];

    options.add(SelectOption(
      value: null,
      label: 'Выберите категорию',
    ));

    for (int i = 0; i < dishTypes.length; i++) {
      options.add(SelectOption(
        value: dishTypes[i],
        label: dishTypes[i].name(context),
      ));
    }

    return options;
  }
}

class _DishAmountField extends StatelessWidget {
  const _DishAmountField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      builder: (context, state) {
        return RoundedTextFormField(
          onChange: serviceLocator<DishEditCubit>().handlePortionAmountChanged,
          value: NumUtils.humanizeNumber(state.portionAmount.value),
          hasError: state.portionAmount.status == FormzInputStatus.invalid,
          errorText:
              state.portionAmount.error != null ? 'Обязательное поле' : null,
          hintText: 'Количество доступной порций',
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
            // С таким фильтром могут быть введены только числа
          ],
        );
      },
    );
  }
}

class _DishNameField extends StatelessWidget {
  const _DishNameField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      buildWhen: (p, c) => p.name != c.name,
      builder: (context, state) {
        return RoundedTextFormField(
          onChange: serviceLocator<DishEditCubit>().handleNameChanged,
          value: state.name.value,
          hasError: state.name.status == FormzInputStatus.invalid,
          errorText: state.name.error != null ? 'Обязательное поле' : null,
          hintText: 'Название блюда',
          keyboardType: TextInputType.text,
        );
      },
    );
  }
}

class _HasHalfPortion extends StatelessWidget {
  const _HasHalfPortion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.only(top: 12),
          width: double.infinity,
          child: CheckRow(
            height: 14,
            label: 'Доступно пол порции',
            labelStyle: text500Size12Black,
            widthParameter: 200,
            isCheck: state.isHalfPortion.value,
            handleValue: (value) => serviceLocator<DishEditCubit>()
                .handleIsHalfPortionChanged(value),
          ),
        );
      },
    );
  }
}

class _IsPreOrderDish extends StatelessWidget {
  const _IsPreOrderDish({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      builder: (context, state) {
        return Container(
          width: double.infinity,
          padding: EdgeInsets.only(top: 3),
          child: CheckRow(
            height: 14,
            label: 'Блюдо по предзаказу',
            labelStyle: text500Size12Black,
            widthParameter: 200,
            isCheck: state.isPreOrderDish.value,
            handleValue: (value) =>
                serviceLocator<DishEditCubit>().handleIsPreOrderDish(value),
          ),
        );
      },
    );
  }
}

class _MinDishAmountField extends StatelessWidget {
  const _MinDishAmountField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      builder: (context, state) {
        if (!state.isPreOrderDish.value) {
          return SizedBox();
        }
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: RoundedTextFormField(
            onChange: serviceLocator<DishEditCubit>().handleMinDishAmount,
            value: NumUtils.humanizeNumber(state.minDishAmount.value),
            hasError: state.minDishAmount.status == FormzInputStatus.invalid,
            errorText:
                state.minDishAmount.error != null ? 'Обязательное поле' : null,
            hintText: 'Мин. количество порций',
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
              // С таким фильтром могут быть введены только числа
            ],
          ),
        );
      },
    );
  }
}

class _PriceField extends StatelessWidget {
  const _PriceField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      buildWhen: (p, c) => p.price != c.price,
      builder: (context, state) {
        return RoundedTextFormField(
          value: NumUtils.humanizeNumber(state.price.value),
          onChange: serviceLocator<DishEditCubit>().handlePriceChanged,
          hasError: state.price.status == FormzInputStatus.invalid,
          errorText: state.price.error != null ? 'Обязательное поле' : null,
          hintText: 'Цена, тг',
          keyboardType: TextInputType.number,
        );
      },
    );
  }
}

class _WeightField extends StatelessWidget {
  const _WeightField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      buildWhen: (p, c) => p.weight != c.weight || p.weightType != c.weightType,
      builder: (context, state) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: RoundedTextFormField(
                value: NumUtils.humanizeNumber(
                      (state.weight.value ?? 0) /
                          (state.weightType == WeightTypeEnum.kilograms
                              ? 1000
                              : 1),
                      withDoublePercision: true,
                    ) ??
                    '',
                onChange: serviceLocator<DishEditCubit>().handleWeightChanged,
                hintText: 'Вес',
                keyboardType: TextInputType.number,
                hasError: state.weight.status == FormzInputStatus.invalid,
                errorText:
                    state.weight.error != null ? 'Обязательное поле' : null,
                height: 42,
              ),
            ),
            SizedBox(
              width: 7,
            ),
            Container(
              height: 42,
              padding: EdgeInsets.symmetric(horizontal: 19),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Color(0xFFD8D8D8)),
                color: Colors.white,
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<WeightTypeEnum?>(
                  value: state.weightType,
                  onChanged:
                      serviceLocator<DishEditCubit>().handleWeightTypeChanged,
                  items: mapWeightTypes(),
                ),
              ),
            )
          ],
        );
      },
    );
  }

  List<DropdownMenuItem<WeightTypeEnum>> mapWeightTypes() {
    List<DropdownMenuItem<WeightTypeEnum>> items = [];

    for (int i = 0; i < WeightTypeEnum.values.length; i++) {
      items.add(
        DropdownMenuItem(
          value: WeightTypeEnum.values[i],
          child: Text(
            WeightTypeEnum.values[i].value!,
            style: text500Size15Black,
          ),
        ),
      );
    }

    return items;
  }
}

class _DishImagesUpload extends StatelessWidget {
  const _DishImagesUpload({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AddImagesView(
              images: state.uploadedImages.value,
              onDeleteImage: serviceLocator<DishEditCubit>().removeImage,
              onSelectImage: serviceLocator<DishEditCubit>().uploadImage,
              onSelectImages: serviceLocator<DishEditCubit>().uploadImages,
              onSwapImages: serviceLocator<DishEditCubit>().onSwapImages,
            ),
            if (state.uploadedImages.error != null)
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 10),
                child: Text(
                  state.uploadedImages.error.toString(),
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              )
          ],
        );
      },
    );
  }
}
