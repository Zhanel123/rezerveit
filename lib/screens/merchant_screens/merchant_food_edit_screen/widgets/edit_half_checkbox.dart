import 'package:flutter/material.dart';

import '../../../../core/text_styles.dart';
import '../../../widgets/check_row.dart';

class EditHalfCheckbox extends StatefulWidget {
  const EditHalfCheckbox({Key? key}) : super(key: key);

  @override
  State<EditHalfCheckbox> createState() => _EditHalfCheckboxState();
}

class _EditHalfCheckboxState extends State<EditHalfCheckbox> {
  bool isHalf = false;

  @override
  Widget build(BuildContext context) {
    return CheckRow(
      height: 34,
      label: 'Доступно пол порции',
      labelStyle: text500Size12Black,
      widthParameter: 200,
      isCheck: isHalf,
      handleValue: (value) {
        setState(() {
          isHalf = value ?? false;
        });
      },
    );
  }
}
