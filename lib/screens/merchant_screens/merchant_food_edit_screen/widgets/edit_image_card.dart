import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/blocs/dish_edit/cubit.dart';
import 'package:home_food/screens/widgets/image_caputer_menu.dart';
import 'package:home_food/service_locator.dart';

import '../../../../core/app_assets.dart';

class EditImageCard extends StatelessWidget {
  const EditImageCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => handleChoosePhoto(context),
      child: Container(
        margin: EdgeInsets.only(right: 10),
        width: 68,
        height: 68,
        decoration: BoxDecoration(
            color: Color(0xffEFEFEF), borderRadius: BorderRadius.circular(8)),
        child: Center(
          child: SvgPicture.asset(
            AppAssets.merchant_add_food_icon,
            color: Color(0xffB4B4B4),
            width: 21,
            height: 21,
          ),
        ),
      ),
    );
  }

  void handleChoosePhoto(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return Container(
          margin:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Wrap(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(25),
                ),
                child: Container(
                  // constraints: minHeight != null
                  //     ? BoxConstraints(minHeight: minHeight!)
                  //     : null,
                  color: Colors.transparent,
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Container(
                              color: const Color(0xFFE5EBF0),
                              height: 4,
                              width: 40,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        ImageCaptureMenu(
                          isMultiple: true,
                          onSelectImage: (file) =>
                              serviceLocator<DishEditCubit>()
                                  .uploadImages([file]),
                          onSelectImages:
                          serviceLocator<DishEditCubit>().uploadImages,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.4,
        maxHeight: MediaQuery.of(context).size.height,
      ),
    );
  }
}
