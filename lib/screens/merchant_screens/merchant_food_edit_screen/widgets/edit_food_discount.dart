import 'package:flutter/material.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';

import '../../../../core/text_styles.dart';
import '../../../widgets/check_row.dart';

class EditFoodDiscount extends StatefulWidget {
  const EditFoodDiscount({Key? key}) : super(key: key);

  @override
  State<EditFoodDiscount> createState() => _EditFoodDiscountState();
}

class _EditFoodDiscountState extends State<EditFoodDiscount> {
  bool isDiscount = false;
  int _discount_percent = 10;

  List<String> _values = ["10 %", "15 %", "25 %", "35 %"];

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CheckRow(
          label: 'Скидка после 19:00',
          labelStyle: text400Size12Black,
          widthParameter: 150,
          isCheck: isDiscount,
          handleValue: (value) {
            setState(() {
              isDiscount = value ?? false;
            });
          },
        ),
        SizedBox(
          width: 15,
        ),
        Expanded(
          child: isDiscount
              ? PrimaryDropdown(
            initialOption: _discount_percent,
            options: mapDiscounts(),
            onChanged: (option) {
              setState(() {
                _discount_percent = option?.value;
              });
            },
          )
              : SizedBox(),
        ),
      ],
    );
  }

  List<SelectOption<int>> mapDiscounts() {
    List<SelectOption<int>> options = [];

    for (int i = 1; i <= 10; i++) {
      options.add(SelectOption(
        value: i * 10,
        label: '${i * 10} %',
      ));
    }

    return options;
  }
}
