import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/dish_edit/cubit.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/primary_badge_with_delete.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/service_locator.dart';

class EditFoodSpecial extends StatefulWidget {
  const EditFoodSpecial({Key? key}) : super(key: key);

  @override
  State<EditFoodSpecial> createState() => _EditFoodSpecialState();
}

class _EditFoodSpecialState extends State<EditFoodSpecial> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      builder: (context, state) {
        return Column(
          children: [
            PrimaryDropdown(
              options: mapFoodPreferences(state.preferences),
              error: state.selectedPreferences.error?.key ?? '',
              onChanged: (option) => serviceLocator<DishEditCubit>()
                  .handlePreferenceSelected(option?.value),
            ),
            if (state.selectedPreferences.value.isNotEmpty)
              Padding(
                padding: EdgeInsets.symmetric(vertical: 11),
                child: SizedBox(
                  height: 30,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      ...state.selectedPreferences.value.map(
                        (e) => PrimaryBadgeWithDelete(
                          label: e.name(context),
                          onTap: () => serviceLocator<DishEditCubit>()
                              .deletePreference(e),
                        ),
                      )
                    ],
                  ),
                ),
              )
          ],
        );
      },
    );
  }

  List<SelectOption<Preference>> mapFoodPreferences(
    List<Preference> preferences,
  ) {
    List<SelectOption<Preference>> options = [];

    options.add(SelectOption(
      value: null,
      label: 'Выберите особенности',
    ));

    for (int i = 0; i < preferences.length; i++) {
      options.add(SelectOption(
        value: preferences[i],
        label: preferences[i].name(context),
      ));
    }

    return options;
  }
}
