import 'package:flutter/material.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/merchant_photo_slider.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/widgets/requirement_photo_text_row.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';

import '../../../../routes.dart';

class FoodPhotoRequirements extends StatelessWidget {
  const FoodPhotoRequirements({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
      padding: EdgeInsets.only(right: 18, top: 23, left: 18, bottom: 18),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            InkWell(
                onTap: () {Routes.router.pop();},
                child: Container(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                      color: Color(0xffEAEAEA),
                      borderRadius: BorderRadius.circular(28)),
                  child: Icon(
                    Icons.close,
                    color: Color(0xff7A7A7A),
                    size: 15,
                  ),
                )),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Требования к фотографиям блюд', style: text700Size15Black,),
                  SizedBox(height: 11,),
                  Text('Примеры удачных фотографий', style: text400Size12Black,),
                  SizedBox(height: 10,),
                  MerchantPhotoSlider(),
                  SizedBox(height: 25,),
                  RequirementTextPhotoRow(label: 'Должны быть качественные фотографии, сделанные фотографом',),
                  Padding(
                    padding: const EdgeInsets.only(right: 70.0),
                    child: RequirementTextPhotoRow(label: 'Минимальное разрешение — 1280 x 720 px, не более 1 Мб',),
                  ),
                  RequirementTextPhotoRow(label: 'Фотографии не должны быть темными, блеклыми или черезчур контрастными. Обработанные фото должны выглядить реалистичными и вкусными.',),
                  RequirementTextPhotoRow(label: 'Вы можете установить одну основную (первую) и до 4-ех дополнительных фото к вашим блюдам.',),
                  RequirementTextPhotoRow(label: 'Логотип добавляется автоматически.',),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 26.0),
                    child: Center(child: GradientButton(labelText: 'Понятно')),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
