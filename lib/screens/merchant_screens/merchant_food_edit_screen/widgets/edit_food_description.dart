import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dish_edit/cubit.dart';
import 'package:home_food/service_locator.dart';

import '../../../../core/text_styles.dart';
import '../../../widgets/rounded_text_field.dart';

class EditFoodDescription extends StatelessWidget {
  const EditFoodDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishEditCubit, DishEditState>(
      builder: (context, state) {
        return RoundedTextFormField(
          onChange: serviceLocator<DishEditCubit>().handleDescriptionChanged,
          value: state.description.value,
          hasError: state.description.status == FormzInputStatus.invalid,
          errorText:
              state.description.error != null ? 'Обязательное поле' : null,
          hintText: 'Состав',
          keyboardType: TextInputType.text,
          minLines: 5,
          maxLines: 6,
          height: 115,
        );
      },
    );
  }
}
