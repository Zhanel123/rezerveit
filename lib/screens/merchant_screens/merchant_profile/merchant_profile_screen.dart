import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/screens/merchant_screens/merchant_profile/widgets/add_point_address.dart';
import 'package:home_food/service/notify_service.dart';
import '../../../core/enums.dart';
import '../../../core/text_styles.dart';
import '../../widgets/buttons/add_logo.dart';
import '../../widgets/check_row.dart';
import '../../widgets/my_app_bar.dart';
import 'widgets/merchant_address_field.dart';
import 'widgets/merchant_city_select_field.dart';
import 'widgets/merchant_company_name.dart';
import 'widgets/merchant_delivery_radius.dart';
import 'widgets/merchant_description_field.dart';
import 'widgets/merchant_is_receive_checkbox.dart';
import 'widgets/merchant_name_field.dart';
import 'widgets/merchant_phone_field.dart';
import 'widgets/merchant_pre_order_days.dart';
import 'widgets/merchant_save_button.dart';
import 'widgets/multiple_points_checkbox.dart';
import 'widgets/pre_order_checkbox.dart';

class MerchantProfileScreen extends StatefulWidget {
  const MerchantProfileScreen({Key? key}) : super(key: key);

  @override
  State<MerchantProfileScreen> createState() => _MerchantProfileScreenState();
}

class _MerchantProfileScreenState extends State<MerchantProfileScreen> {
  PeriodTypeEnum _periodTypeEnum = PeriodTypeEnum.MONTH;
  bool isReceiveEmail = false;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileCubit, ProfileState>(
      buildWhen: (p, c) => p.updateProfileStatus != c.updateProfileStatus,
      listenWhen: (p, c) => p.updateProfileStatus != c.updateProfileStatus,
      listener: (context, state) {
        if (state.updateProfileStatus.isSubmissionSuccess) {
          FlushbarService().showSuccessMessage();
        } else if (state.updateProfileStatus.isSubmissionFailure) {
          FlushbarService().showErrorMessage(
            context: context,
          );
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
            height: double.infinity,
            color: Color(0xffF5F5F5),
            child: SafeArea(
              child: SingleChildScrollView(
                padding: EdgeInsets.only(top: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: MyAppBar(),
                    ),
                    SizedBox(
                      height: 36,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Мой профиль',
                            style: text700Size18Black,
                          ),
                          Text(
                            'Профиль клиента',
                            style: text400Size13Pink,
                          ),
                        ],
                      ),
                    ),
                    MerchantNameField(),
                    MerchantPhoneField(),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: Text(
                        'Телефон подвержден!',
                        style: text400Size13Grey3,
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            top: 16.0, right: 16, left: 16),
                        child: RichText(
                          text: TextSpan(
                            text: 'Тариф «',
                            style: text400Size12Black,
                            children: [
                              TextSpan(
                                  text: 'Простой', style: text700Size15Black),
                              TextSpan(
                                  text: '»    ', style: text400Size12Black),
                              TextSpan(
                                  text: 'Оплачен до 31.02.2023',
                                  style: text400Size13Green),
                            ],
                          ),
                        )),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: Container(
                        width: double.infinity,
                        height: 42,
                        padding: EdgeInsets.symmetric(horizontal: 19),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Color(0xFFD8D8D8)),
                          color: Colors.white,
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<PeriodTypeEnum?>(
                            value: _periodTypeEnum,
                            // underline: Container(
                            //   width: double.infinity,
                            //   padding: EdgeInsets.only(left: 10, right: 19),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //     children: [
                            //       SvgPicture.asset(AppAssets.visa_icon),
                            //       SizedBox(width: 17,),
                            //       Text(
                            //         '**** **** **** 3455',
                            //         textAlign: TextAlign.start,
                            //         style: text500Size15Black,
                            //       )
                            //     ],
                            //   ),
                            // ),
                            onChanged: (PeriodTypeEnum? value) {
                              // This is called when the user selects an item.
                              setState(() {
                                _periodTypeEnum = value!;
                              });
                            },
                            // dropdownColor: Colors.red,
                            // icon on closed view
                            // icon: Icon(Icons.add),

                            /// selectedItemBuilder
                            // selectedItemBuilder: (context) {
                            //   return [
                            //
                            //   ];
                            // },
                            items: [
                              ...PeriodTypeEnum.values
                                  .map((e) => DropdownMenuItem(
                                      value: e,
                                      child: Text(
                                        e.value!,
                                        style: text500Size15Black,
                                      )))
                            ],
                          ),
                        ),
                      ),
                    ),
                    MerchantIsReceiveCheckbox(),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Компания',
                            style: text400Size13Black,
                          ),
                          Container(
                            width: 77,
                            child: Text(
                              'Логотип',
                              style: text400Size13Black,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MerchantCompanyName(),
                          SizedBox(
                            width: 8,
                          ),
                          AddLogoButton(
                            onImageSelected: (File) {},
                          )
                        ],
                      ),
                    ),
                    MerchantDescription(),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 29.0, right: 16, left: 16, bottom: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Адрес',
                            style: text400Size13Black,
                          ),
                          MultiplePointsCheckbox()
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: MerchantCitySelectField(),
                    ),
                    MerchantAddressField(),
                    Padding(
                      padding: const EdgeInsets.only(right: 8, left: 8),
                      child: AddPointAddress(),
                    ),
                    PreOrderCheckbox(),
                    MerchantPreOrderDays(),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Укажите районы доставки',
                            style: text400Size13Black,
                          ),
                          CheckRow(
                            label: 'Все',
                            isCheck: isReceiveEmail,
                            widthParameter: 27,
                            handleValue: (value) {
                              setState(() {
                                // isHomeFood = value ?? false;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: CheckRow(
                        label: 'Благовещенский',
                        isCheck: isReceiveEmail,
                        widthParameter: 120,
                        handleValue: (value) {
                          setState(() {
                            // isHomeFood = value ?? false;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: CheckRow(
                        label: 'Молодежный комплекс',
                        isCheck: isReceiveEmail,
                        widthParameter: 200,
                        handleValue: (value) {
                          setState(() {
                            // isHomeFood = value ?? false;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: CheckRow(
                        label: 'Университетский',
                        isCheck: isReceiveEmail,
                        widthParameter: 120,
                        handleValue: (value) {
                          setState(() {
                            // isHomeFood = value ?? false;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: CheckRow(
                        label: 'Центр Палладиум',
                        isCheck: isReceiveEmail,
                        widthParameter: 200,
                        handleValue: (value) {
                          setState(() {
                            // isHomeFood = value ?? false;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: CheckRow(
                        label: 'Прибрежный район',
                        isCheck: isReceiveEmail,
                        widthParameter: 200,
                        handleValue: (value) {
                          setState(() {
                            // isHomeFood = value ?? false;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 18.0, right: 16, left: 16),
                      child: Text(
                        'Или можете указать радиус доставки',
                        style: text400Size13Black,
                      ),
                    ),
                    MerchantDeliveryRadius(),
                    MerchantSaveButton(),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
