import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

class MerchantCompanyName extends StatelessWidget {
  const MerchantCompanyName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Expanded(
          child: RoundedTextFormField(
            height: 77,
            value: state.companyName.value,
            onChange: serviceLocator<ProfileCubit>().changeCompany,
            hintText: 'Компания',
            hasError: state.companyName.status == FormzInputStatus.invalid,
            errorText:
                state.companyName.error != null ? 'Обязательное поле' : '',
          ),
        );
      },
    );
  }
}
