import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class MerchantPhoneField extends StatelessWidget {
  const MerchantPhoneField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        final MaskTextInputFormatter phoneFormatter = MaskTextInputFormatter(
            mask: "+7(###)#######", initialText: state.phone.value);

        return Padding(
          padding: const EdgeInsets.only(top: 13.0, right: 16, left: 16),
          child: ProfileTextField(
            value: state.phone.value,
            hintText: '+7 999 345-55-55',
            inputFormatters: [phoneFormatter],
            textInputType: TextInputType.phone,
            onChange: serviceLocator<ProfileCubit>().handlePhoneChanged,
            hasError: state.phone.status == FormzStatus.invalid,
            errorText: state.phone.error != null ? 'Обязательное поле' : '',
          ),
        );
      },
    );
  }
}
