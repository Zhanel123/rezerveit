import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/service_locator.dart';

class MerchantCitySelectField extends StatefulWidget {
  const MerchantCitySelectField({Key? key}) : super(key: key);

  @override
  State<MerchantCitySelectField> createState() =>
      _MerchantCitySelectFieldState();
}

class _MerchantCitySelectFieldState extends State<MerchantCitySelectField> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 18),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Color(0xFFD8D8D8)),
            color: Colors.white,
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<City>(
              icon: SvgPicture.asset(AppAssets.drop_down_icon),
              value: state.city.value,
              // underline: Container(
              //   width: double.infinity,
              //   padding: EdgeInsets.only(left: 10, right: 19),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       SvgPicture.asset(AppAssets.visa_icon),
              //       SizedBox(width: 17,),
              //       Text(
              //         '**** **** **** 3455',
              //         textAlign: TextAlign.start,
              //         style: text500Size15Black,
              //       )
              //     ],
              //   ),
              // ),
              onChanged: serviceLocator<ProfileCubit>().handleCityChanged,
              // dropdownColor: Colors.red,
              // icon on closed view
              // icon: Icon(Icons.add),

              /// selectedItemBuilder
              // selectedItemBuilder: (context) {
              //   return [
              //
              //   ];
              // },
              items: [
                DropdownMenuItem(
                    value: null,
                    child: Text(
                      'Выберите город',
                      style: text500Size15Black,
                    )),
                ...state.cities.map((e) => DropdownMenuItem(
                    value: e,
                    child: Text(
                      e.name(context),
                      style: text500Size15Black,
                    )))
              ],
            ),
          ),
        );
      },
    );
  }
}
