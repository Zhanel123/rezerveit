import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/service_locator.dart';

class MerchantDeliveryRadius extends StatelessWidget {
  const MerchantDeliveryRadius({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(top: 8, right: 16, left: 16),
          child: ProfileTextField(
            textInputType: TextInputType.number,
            value: '${state.deliveryRadius.value == 0 ? '' : state.deliveryRadius.value}',
            hintText: 'Радиус доставки',
            onChange: serviceLocator<ProfileCubit>().changeDeliveryRadius,
            hasError: state.deliveryRadius.status == FormzInputStatus.invalid,
            errorText:
                state.deliveryRadius.error != null ? 'Обязательное поле' : '',
          ),
        );
      },
    );
  }
}
