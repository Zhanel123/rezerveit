import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/screens/profile_screen/widgets/update_address_screen.dart';
import 'package:home_food/service_locator.dart';

class MerchantAddressField extends StatefulWidget {
  const MerchantAddressField({Key? key}) : super(key: key);

  @override
  State<MerchantAddressField> createState() => _MerchantAddressFieldState();
}

class _MerchantAddressFieldState extends State<MerchantAddressField> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        // return Padding(
        //   padding: const EdgeInsets.only(top: 7.0, right: 16, left: 16),
        //   child: ProfileTextField(
        //     textInputType: TextInputType.text,
        //     value: '${state.addressList[0]}',
        //     hintText: 'ул. Тверская, 15А',
        //     suffixText: Text(
        //       'код 421',
        //       style: text400Size13Green,
        //     ),
        //     onChange: serviceLocator<ProfileCubit>().changeAddressList,
        //     hasError: false,
        //     errorText: '',
        //   ),
        // );
        return Column(
          children: [
            ...state.addressList.asMap().entries.map((e) => Padding(
                  padding: EdgeInsets.only(top: 7, right: 16, left: 16),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Дополнительный адрес доставки',
                            style: text400Size13Grey,
                          ),
                          InkWell(
                            onTap: () => serviceLocator<NewAddressCubit>()
                                .deleteAddress(addressId: e.value.id ?? ''),
                            child: Text(
                              'Удалить',
                              style: text400Size13Grey,
                            ),
                          )
                        ],
                      ),
                      // ProfileTextField(
                      //   textInputType: TextInputType.text,
                      //   value: '${e.value.addressName ?? ''}',
                      //   hintText: '',
                      //   disabled: true,
                      //   onChange: (value) => serviceLocator<ProfileCubit>()
                      //       .changeAddressList(value, e.key),
                      //   hasError: false,
                      // ),
                      SizedBox(
                        height: 6,
                      ),
                      ProfileTextField(
                        textInputType: TextInputType.text,
                        value: '${e.value.city?.name(context)}',
                        hintText: '',
                        disabled: true,
                        onChange: (value) => serviceLocator<ProfileCubit>()
                            .changeAddressList(value, e.key),
                        hasError: false,
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Stack(
                        children: [
                          ProfileTextField(
                            textInputType: TextInputType.text,
                            value: '${e.value.address}',
                            hintText: '',
                            disabled: true,
                            onChange: (value) => serviceLocator<ProfileCubit>()
                                .changeAddressList(value, e.key),
                            hasError: false,
                          ),
                          Positioned(
                            right: 0,
                            top: 0,
                            bottom: 0,
                            child: IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () =>
                                  _handleUpdateAddressClicked(e.value),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 6,
                      )
                    ],
                  ),
                ))
          ],
        );
      },
    );
  }

  void _handleUpdateAddressClicked(Address address) {
    serviceLocator<NewAddressCubit>().selectAddress(address);
    showModalBottomSheet(
      context: context,
      builder: (context) => UpdateAddressScreen(),
      isScrollControlled: true,
      // isDismissible: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(28)),
      ),
    );
  }
}
