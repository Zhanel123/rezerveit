import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';

import '../../../../core/enums.dart';
import '../../../../core/text_styles.dart';
import '../../../../routes.dart';
import '../../../widgets/check_row.dart';

class TariffPaymentWidget extends StatefulWidget {
  const TariffPaymentWidget({Key? key}) : super(key: key);

  @override
  State<TariffPaymentWidget> createState() => _TariffPaymentWidgetState();
}

class _TariffPaymentWidgetState extends State<TariffPaymentWidget> {
  PeriodTypeEnum _periodTypeEnum = PeriodTypeEnum.MONTH;
  PaymentTypeEnum? dropdownValue;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
      padding: EdgeInsets.all(18),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            InkWell(
                onTap: () {
                  Routes.router.pop();
                },
                child: Container(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                      color: Color(0xffEAEAEA),
                      borderRadius: BorderRadius.circular(28)),
                  child: Icon(
                    Icons.close,
                    color: Color(0xff7A7A7A),
                    size: 15,
                  ),
                )),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Оплата тарифа',
                    style: text700Size15Black,
                  ),
                  SizedBox(
                    height: 23,
                  ),
                  RichText(
                    text: TextSpan(
                      text: 'Тариф «',
                      style: text400Size12Black,
                      children: [
                        TextSpan(text: 'Простой', style: text700Size15Black),
                        TextSpan(text: '»    ', style: text400Size12Black),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 13,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 3,
                      ),
                      Container(
                        width: double.infinity,
                        height: 42,
                        padding: EdgeInsets.symmetric(horizontal: 19),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Color(0xFFD8D8D8)),
                          color: Colors.white,
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<PeriodTypeEnum?>(
                            value: _periodTypeEnum,
                            // underline: Container(
                            //   width: double.infinity,
                            //   padding: EdgeInsets.only(left: 10, right: 19),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //     children: [
                            //       SvgPicture.asset(AppAssets.visa_icon),
                            //       SizedBox(width: 17,),
                            //       Text(
                            //         '**** **** **** 3455',
                            //         textAlign: TextAlign.start,
                            //         style: text500Size15Black,
                            //       )
                            //     ],
                            //   ),
                            // ),
                            onChanged: (PeriodTypeEnum? value) {
                              // This is called when the user selects an item.
                              setState(() {
                                _periodTypeEnum = value!;
                              });
                            },
                            // dropdownColor: Colors.red,
                            // icon on closed view
                            // icon: Icon(Icons.add),

                            /// selectedItemBuilder
                            // selectedItemBuilder: (context) {
                            //   return [
                            //
                            //   ];
                            // },
                            items: [
                              ...PeriodTypeEnum.values
                                  .map((e) => DropdownMenuItem(
                                      value: e,
                                      child: Text(
                                        e.value!,
                                        style: text500Size15Black,
                                      )))
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 21,
                      ),
                      Text(
                        'Способ оплаты',
                        style: text400Size13Black,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(horizontal: 18),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Color(0xFFD8D8D8)),
                          color: Colors.white,
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<PaymentTypeEnum>(
                            value: dropdownValue,
                            // underline: Container(
                            //   width: double.infinity,
                            //   padding: EdgeInsets.only(left: 10, right: 19),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //     children: [
                            //       SvgPicture.asset(AppAssets.visa_icon),
                            //       SizedBox(width: 17,),
                            //       Text(
                            //         '**** **** **** 3455',
                            //         textAlign: TextAlign.start,
                            //         style: text500Size15Black,
                            //       )
                            //     ],
                            //   ),
                            // ),
                            onChanged: (PaymentTypeEnum? value) {
                              // This is called when the profile selects an item.
                              setState(() {
                                dropdownValue = value!;
                              });
                            },
                            // dropdownColor: Colors.red,
                            // icon on closed view
                            // icon: Icon(Icons.add),

                            /// selectedItemBuilder
                            // selectedItemBuilder: (context) {
                            //   return [
                            //
                            //   ];
                            // },
                            items: [
                              DropdownMenuItem(
                                  value: null,
                                  child: Text(
                                    'Выберите способ оплаты',
                                    style: text500Size15Black,
                                  )),
                              ...PaymentTypeEnum.values
                                  .map((e) => DropdownMenuItem(
                                      value: e,
                                      child: Row(
                                        children: [
                                          SvgPicture.asset(e.asset!),
                                          SizedBox(
                                            width: 18,
                                          ),
                                          Text(
                                            e.value!,
                                            style: text500Size15Black,
                                          )
                                        ],
                                      )))
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      InkWell(
                        onTap: () {},
                        child: Text(
                          'Добавить новую карту',
                          style: text400Size13Pink,
                        ),
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Center(
                        child: Container(
                            width: 222,
                            child: GradientButton(
                              labelText: 'Оформить',
                              onPressed: () {
                                Routes.router.pop();
                              },
                            )),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
