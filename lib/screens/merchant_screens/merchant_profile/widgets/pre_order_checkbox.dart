import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/service_locator.dart';

class PreOrderCheckbox extends StatelessWidget {
  const PreOrderCheckbox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(top: 16.0, right: 16, left: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Предзаказы',
                style: text400Size13Black,
              ),
              CheckRow(
                label: 'Доступны',
                isCheck: state.preOrderIsAccess.value,
                widthParameter: 65,
                handleValue: (value) => serviceLocator<ProfileCubit>().changePreOrderIsAccess,
              )
            ],
          ),
        );
      },
    );
  }
}
