import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/service_locator.dart';

class MerchantPreOrderDays extends StatelessWidget {
  const MerchantPreOrderDays({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(top: 8, right: 16, left: 16),
          child: ProfileTextField(
            value: '${state.preOrderMinDays.value == 0 ? '' : state.preOrderMinDays.value}',
            hintText: 'Минимум за сколько дней',
            textInputType: TextInputType.number,
            onChange: serviceLocator<ProfileCubit>().changePreOrderMinDays,
            hasError: state.preOrderMinDays.status == FormzInputStatus.invalid,
            errorText:
                state.preOrderMinDays.error != null ? 'Обязательное поле' : '',
          ),
        );
      },
    );
  }
}
