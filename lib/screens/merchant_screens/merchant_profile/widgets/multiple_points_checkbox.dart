import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/service_locator.dart';

class MultiplePointsCheckbox extends StatelessWidget {
  const MultiplePointsCheckbox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return CheckRow(
          label: 'Несколько точек продаж',
          isCheck: state.multiplePoints.value,
          widthParameter: 160,
          handleValue: (value) => serviceLocator<ProfileCubit>().handleMultiplePoints,
        );
      },
    );
  }
}
