import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/merchant_screens/merchant_profile/widgets/tariff_payment_widget.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service_locator.dart';

class MerchantSaveButton extends StatefulWidget {
  const MerchantSaveButton({Key? key}) : super(key: key);

  @override
  State<MerchantSaveButton> createState() => _MerchantSaveButtonState();
}

class _MerchantSaveButtonState extends State<MerchantSaveButton> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Center(
            child: Padding(
          padding:
              const EdgeInsets.only(top: 8.0, right: 70, left: 70, bottom: 80),
          child: SizedBox(
            width: double.infinity,
            child: GradientButton(
              labelText: 'Сохранить',
              isLoading: state.updateProfileStatus.isSubmissionInProgress,
              onPressed: state.updateProfileFormzStatus.isValid
                  ? _updateProfileInfo
                  : serviceLocator<ProfileCubit>().submissionValidate ,
            ),
          ),
        ));
      },
    );
  }

  void _updateProfileInfo() {
    serviceLocator<ProfileCubit>().update();
    serviceLocator<NewAddressCubit>().updateAddress();
  }

  void _handleReasonButton() {
    Scaffold.of(context).showBottomSheet<void>(
      (BuildContext context) {
        return const TariffPaymentWidget();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.7,
        maxHeight: MediaQuery.of(context).size.height * 0.8,
      ),
    );
  }
}
