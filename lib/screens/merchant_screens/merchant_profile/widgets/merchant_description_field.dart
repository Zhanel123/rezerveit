import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

class MerchantDescription extends StatelessWidget {
  const MerchantDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(top: 8.0, right: 16, left: 16),
          child: RoundedTextFormField(
            height: 77,
            value: state.companyDescription.value,
            onChange: serviceLocator<ProfileCubit>().changeCompanyDesc,
            hintText: 'Введите описание...',
            keyboardType: TextInputType.text,
            hasError:
                state.companyDescription.status == FormzInputStatus.invalid,
            errorText: state.companyDescription.error != null
                ? 'Обязательное поле'
                : '',
          ),
        );
      },
    );
  }
}
