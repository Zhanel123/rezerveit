import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/service_locator.dart';

class MerchantIsReceiveCheckbox extends StatelessWidget {
  const MerchantIsReceiveCheckbox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(top: 8.0, right: 16, left: 16),
          child: CheckRow(
            label: 'Получать уведомления на e-mail',
            isCheck: state.isReceiveNotification.value,
            widthParameter: 250,
            handleValue: (value) => serviceLocator<ProfileCubit>().handleIsReceiveNotification,
          ),
        );
      },
    );
  }
}
