import 'package:flutter/material.dart';

import '../../../../core/text_styles.dart';
import '../../../profile_screen/widgets/create_address_screen.dart';

class AddPointAddress extends StatefulWidget {
  const AddPointAddress({Key? key}) : super(key: key);

  @override
  State<AddPointAddress> createState() => _AddPointAddressState();
}

class _AddPointAddressState extends State<AddPointAddress> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () { _handleCreateAddressClicked(); },
      child: Text(
        'Добавить точку продаж',
        style: text400Size13Pink,
      ),
    );
  }

  void _handleCreateAddressClicked() {
    Scaffold.of(context).showBottomSheet<void>(
          (BuildContext context) {
        return CreateAddressScreen();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.4,
        maxHeight: MediaQuery.of(context).size.height * 0.65 ,
      ),
    );
  }
}
