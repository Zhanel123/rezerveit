import 'package:flutter/material.dart';

class MyBottomNavigation extends StatelessWidget {
  const MyBottomNavigation({
    Key? key,
    this.currentIndex = 0,
    required this.onTabChange,
    required this.tabs,
  }) : super(key: key);

  final int currentIndex;
  final Function(int index) onTabChange;
  final List<BottomNavigationBarItem> tabs;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: currentIndex,
      onTap: onTabChange,
      items: tabs,
      type: BottomNavigationBarType.fixed,
      showSelectedLabels: false,
      showUnselectedLabels: false,
    );
  }
}
