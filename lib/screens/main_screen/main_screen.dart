import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/customer_orders_screen/customer_orders_screen.dart';
import 'package:home_food/screens/home_screen/client_home_screen/client_home_screen.dart';
import 'package:home_food/screens/home_screen/favourite_foods/favourite_foods.dart';
import 'package:home_food/screens/home_screen/merchant_home_screen.dart';
import 'package:home_food/screens/main_screen/widgets/my_bottom_navigation.dart';
import 'package:home_food/screens/my_preferences/my_preferences_screen.dart';
import 'package:home_food/screens/profile_screen/profile_screen.dart';
import 'package:home_food/screens/widgets/clean_cart_widget.dart';
import 'package:home_food/screens/widgets/order/order_box.dart';
import 'package:home_food/service_locator.dart';
import '../merchant_screens/merchant_add_food_screen/merchant_add_foods_screen.dart';
import '../merchant_screens/merchant_foods_screen/merchant_foods_screen.dart';
import '../merchant_screens/merchant_orders/merchant_orders_screen.dart';
import '../merchant_screens/merchant_profile/merchant_profile_screen.dart';
import '../merchants_menu/merchants_list.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();

  static _MainScreenState? of(BuildContext context) =>
      context.findAncestorStateOfType<_MainScreenState>();
}

class _MainScreenState extends State<MainScreen> {
  int selectedTab = 0;
  bool autoFocus = false;
  late List<NavBarItemType> _tabs = [];

  @override
  void initState() {
    super.initState();

    serviceLocator<BasketCubit>().fetchPreOrder();
  }

  changeTab(
    int i, [
    bool autoFocus = false,
  ]) {
    setState(() {
      selectedTab = i;
      this.autoFocus = autoFocus;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BasketCubit, BasketState>(
      listenWhen: (p, c) =>
          p.merchantConflictStatus != c.merchantConflictStatus,
      listener: (context, state) {
        if (state.merchantConflictStatus.isSubmissionSuccess) {
          _showDeleteBasketWidget();
        }
      },
      builder: (context, state) {
        return BlocConsumer<BasketCubit, BasketState>(
            listenWhen: (p, c) => p.submitOrderStatus != c.submitOrderStatus,
            listener: (context, state) {
              if (state.submitOrderStatus.isSubmissionSuccess) {
                changeTab(3);
              }
            },
            builder: (context, state) {
              return BlocBuilder<ProfileCubit, ProfileState>(
                builder: (context, state) {
                  bool isManufacturer = [
                    UserType.MANUFACTURER,
                  ].contains(state.profile?.userType);

                  _tabs = [
                    if (isManufacturer) NavBarItemType.merchant_home,
                    if (!isManufacturer) NavBarItemType.home,
                    if (isManufacturer) NavBarItemType.merchant_orders,
                    if (!isManufacturer) NavBarItemType.merchants_list,
                    if (isManufacturer) NavBarItemType.merchant_add_food,
                    if (!isManufacturer) NavBarItemType.favourite,
                    if (isManufacturer) NavBarItemType.merchant_foods,
                    if (!isManufacturer) NavBarItemType.orders,
                    NavBarItemType.profile,
                  ];

                  return Scaffold(
                    body: SafeArea(
                      child: Stack(children: [
                        _TabEnumToWidget(_tabs[selectedTab]),
                        BlocBuilder<BasketCubit, BasketState>(
                          builder: (context, state) {
                            return state.order.positionList.isNotEmpty
                                ? Positioned(
                                    bottom: 12,
                                    left: 16,
                                    right: 18,
                                    // child: OrderStatus(),
                                    child: OrderBox(
                                      ordersList: state.order,
                                    ),
                                  )
                                : SizedBox();
                          },
                        )
                      ]),
                    ),
                    bottomNavigationBar: SizedBox(
                      height: 90,
                      child: MyBottomNavigation(
                        currentIndex: selectedTab,
                        onTabChange: (int index) {
                          setState(() {
                            selectedTab = index;
                          });
                        },
                        tabs: _tabs
                            .asMap()
                            .entries
                            .map(
                              (e) => BottomNavigationBarItem(
                                  icon: Center(
                                    child: SvgPicture.asset(
                                      e.value.asset!,
                                      height: 23,
                                      width: 27,
                                      color: e.key == selectedTab
                                          ? AppColors.pinkColor
                                          : Colors.grey,
                                    ),
                                  ),
                                  label: ''
                                  // label: e.value.value,
                                  ),
                            )
                            .toList(),
                      ),
                    ),
                  );
                },
              );
            });
      },
    );
  }

  _TabEnumToWidget(NavBarItemType type) {
    switch (type) {
      case NavBarItemType.home:
        return ClientHomeScreen();
      case NavBarItemType.merchant_home:
        return MerchantHomeScreen();
      case NavBarItemType.orders:
        return CustomerOrderList();
      case NavBarItemType.merchant_orders:
        return MerchantOrdersScreen();
      case NavBarItemType.merchant_add_food:
        return MerchantAddFoodScreen();
      case NavBarItemType.merchant_foods:
        return MerchantFoodsScreen();
      case NavBarItemType.favourite:
        return FavoriteFoods();
      case NavBarItemType.merchants_list:
        return MerchantsList(
          autoFocus: autoFocus,
        );
      case NavBarItemType.profile:
        return ProfileScreen();
      case NavBarItemType.merchant_home:
        return MerchantProfileScreen();
    }
  }

  void _showDeleteBasketWidget() {
    showModalBottomSheet(
      context: Routes.router.navigatorKey?.currentContext ?? context,
      builder: (BuildContext context) {
        return CleanCartWidget();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
      elevation: 1,
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.3,
        maxHeight: MediaQuery.of(context).size.height * 0.4,
      ),
    );
  }
}
