import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/merchant/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/merchants_menu/merchant_card.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

import '../widgets/buttons/filter_button.dart';
import '../widgets/buttons/settings_button.dart';
import '../widgets/my_app_bar.dart';
import '../widgets/search/search_field.dart';

class MerchantsList extends StatefulWidget {
  const MerchantsList({
    Key? key,
    this.autoFocus = false,
  }) : super(key: key);

  final bool autoFocus;

  @override
  State<MerchantsList> createState() => _MerchantsListState();
}

class _MerchantsListState extends State<MerchantsList> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    serviceLocator<MerchantCubit>().getMerchantList();
    scrollController.addListener(() {
      var triggerFetchMoreSize =
          0.7 * scrollController.position.maxScrollExtent;

      if (scrollController.position.pixels > triggerFetchMoreSize) {
        serviceLocator<MerchantCubit>().getMerchantList(append: true);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffF5F5F5),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: MyAppBar(),
              ),
              SizedBox(
                height: 13,
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: _SearchField(),
                      ),
                      FilterButton(),
                    ],
                  )),
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16.0).copyWith(
                    bottom: 0,
                  ),
                  child: BlocBuilder<MerchantCubit, MerchantState>(
                    builder: (context, state) {
                      return GridView.count(
                        primary: false,
                        controller: scrollController,
                        crossAxisCount: 2,
                        mainAxisSpacing: 6.24,
                        crossAxisSpacing: 8.24,
                        children: [
                          ...state.merchants
                              .map((e) => MerchantCard(merchant: e))
                        ],
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _SearchField extends StatelessWidget {
  final bool autoFocus;

  const _SearchField({
    Key? key,
    this.autoFocus = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MerchantCubit, MerchantState>(
      builder: (context, state) {
        return RoundedTextFormField(
          value: state.searchText,
          hintText: 'Поиск по названию',
          autofocus: autoFocus,
          onChange: serviceLocator<MerchantCubit>().handleSearchChanged,
        );
      },
    );
  }
}
