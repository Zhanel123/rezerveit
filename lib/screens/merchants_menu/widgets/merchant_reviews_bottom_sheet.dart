import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/data/repositories/visitor_repository/visitor_repository.dart';
import 'package:home_food/models/grade/grade.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/primary_image.dart';
import 'package:home_food/service_locator.dart';

class MerchantReviewsBottomSheet extends StatefulWidget {
  final String merchantId;

  const MerchantReviewsBottomSheet({
    Key? key,
    required this.merchantId,
  }) : super(key: key);

  @override
  State<MerchantReviewsBottomSheet> createState() =>
      _MerchantReviewsBottomSheetState();
}

class _MerchantReviewsBottomSheetState
    extends State<MerchantReviewsBottomSheet> {
  List<Grade> grades = [];
  bool isLoading = true;

  @override
  void initState() {
    serviceLocator<VisitorRepository>()
        .getMerchantGrades(
      merchantId: widget.merchantId,
    )
        .then((value) {
      setState(() {
        isLoading = false;
        grades = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PrimaryBottomSheet(
      contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 24),
      height: MediaQuery.of(context).size.height * 0.8,
      child: Column(
        children: [
          Text(
            'Отзывы',
            style: text700Size18Black,
          ),
          if (isLoading)
            Center(
              child: CircularProgressIndicator(
                color: AppColors.primary,
              ),
            ),
          AnimatedContainer(
              height: isLoading ? 0 : MediaQuery.of(context).size.height * 0.6,
              duration: Duration(milliseconds: 900),
              child: grades.isNotEmpty
                  ? ListView(
                      children: [
                        ...grades.map(
                          (e) => ListTile(
                            leading: SizedBox(
                              height: 30,
                              width: 30,
                              child: PrimaryImage(
                                url: e.customer?.photo?.imageUrl,
                              ),
                            ),
                            title: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${e.customer?.firstName ?? ''} ${e.customer?.lastName ?? ''}',
                                  style: text700Size15Grey,
                                ),
                                Text(
                                  e.comment,
                                  style: text700Size15Black,
                                ),
                              ],
                            ),
                            trailing: SizedBox(
                              width: 30,
                              height: 30,
                              child: Row(
                                children: [
                                  SvgPicture.asset(AppAssets.star),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "${e.rating ?? 0.0}",
                                    style: text400Size13Black,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  : Center(
                      child: Text('Нету отзывов'),
                    ))
        ],
      ),
    );
  }
}
