import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/merchant_detail/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/profile/profile.dart';

import 'package:home_food/routes.dart';
import 'package:home_food/screens/merchants_menu/widgets/merchant_reviews_bottom_sheet.dart';
import 'package:home_food/screens/widgets/primary_image.dart';
import 'package:home_food/service_locator.dart';
import 'package:seafarer/seafarer.dart';
import '../home_screen/client_home_screen/widgets/food_card.dart';
import '../widgets/my_app_bar.dart';

class MerchantMenuArgs extends BaseArguments {
  final Profile merchant;
  final List<Address> addressList;

  MerchantMenuArgs({
    required this.merchant,
    this.addressList = const [],
  });
}

class MerchantMenu extends StatefulWidget {
  final MerchantMenuArgs args;

  const MerchantMenu({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  State<MerchantMenu> createState() => _MerchantMenuState();
}

class _MerchantMenuState extends State<MerchantMenu>
    with TickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    serviceLocator<MerchantDetailCubit>().handleMerchantChanged(
      widget.args.merchant,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffF5F5F5),
        child: SafeArea(
            child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: MyAppBar(),
            ),
            SizedBox(
              height: 24,
            ),
            BlocBuilder<MerchantDetailCubit, MerchantDetailState>(
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                InkWell(
                                  onTap: () {
                                    Routes.router.pop();
                                  },
                                  child: Container(
                                    width: 25,
                                    height: 25,
                                    decoration: BoxDecoration(
                                      color: Color(0xffF5F5F5),
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    child: Center(
                                      child: SvgPicture.asset(
                                          AppAssets.arrow_left),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child: Text(
                                    widget.args.merchant.companyName ??
                                        // widget.args.merchant.firstName ??
                                        'No name company',
                                    style: text700Size18Black,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                // SvgPicture.asset(AppAssets.star),
                                // SizedBox(
                                //   width: 5,
                                // ),
                                // Padding(
                                //   padding: const EdgeInsets.only(right: 8),
                                //   child: Text(
                                //     "${(widget.args.merchant.rating ?? 0.0).toStringAsFixed(2)}",
                                //     style: text400Size13Black,
                                //   ),
                                // ),
                                // InkWell(
                                //   onTap: () {
                                //     showModalBottomSheet(
                                //       context: context,
                                //       isScrollControlled: true,
                                //       isDismissible: true,
                                //       shape: RoundedRectangleBorder(
                                //         borderRadius:
                                //             BorderRadius.circular(28.0),
                                //       ),
                                //       backgroundColor: Colors.transparent,
                                //       builder: (context) =>
                                //           MerchantReviewsBottomSheet(
                                //         merchantId: widget.args.merchant.id!,
                                //       ),
                                //     );
                                //   },
                                //   child: Text('Отзывы'),
                                // ),
                                SizedBox(
                                  width: 10,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            for (int i = 0;
                                i < widget.args.addressList.length;
                                i++)
                              Row(
                                children: [
                                  SizedBox(
                                    width: 35,
                                  ),
                                  Text(
                                    '${widget.args.addressList[i].city?.name(context)}, ${widget.args.addressList[i].address}',
                                    style: text400Size13Black,
                                  ),
                                ],
                              )
                          ],
                        ),
                      ),
                      Container(
                        width: 59,
                        height: 59,
                        clipBehavior: Clip.hardEdge,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        // child: Image.asset(AppAssets.hard_rock),
                        child: PrimaryImage(
                          url: widget.args.merchant.photo?.imageUrl,
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
            BlocBuilder<MerchantDetailCubit, MerchantDetailState>(
              builder: (context, state) {
                final index = state.dishTypes.indexWhere(
                  (element) => element.id == state.selectedDishType,
                );
                _controller = TabController(
                  vsync: this,
                  length: state.dishTypes.length,
                  initialIndex: index != -1 ? index : 0,
                );
                _controller.addListener(_tabListener);

                return Column(
                  children: [
                    TabBar(
                      tabs: state.dishTypes
                          .map(
                            (e) => Tab(
                              text: e.name == null ? 'Все' : e.name(context),
                            ),
                          )
                          .toList(),
                      onTap: serviceLocator<MerchantDetailCubit>()
                          .handleDishTypeChanged,
                      isScrollable: true,
                      controller: _controller,
                      labelColor: Colors.black,
                      unselectedLabelStyle: TextStyle(color: Color(0x80000000)),
                      labelStyle: text700Size15Black,
                      indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        // Creates border
                        // color: Colors.greenAccent,
                      ),
                    ),
                    ...state.dishes.map(
                      (e) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: FoodCard(
                          item: e,
                        ),
                      ),
                    ),
                  ],
                );
              },
            )
          ],
        )),
      ),
    );
  }

  void _tabListener() => serviceLocator<MerchantDetailCubit>()
      .handleDishTypeChanged(_controller.index);
}
