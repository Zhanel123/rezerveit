import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/merchant/merchant.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/merchant_detail/merchant_detail.dart';
import 'package:home_food/screens/widgets/primary_image.dart';

import 'merchant_menu.dart';

class MerchantCard extends StatelessWidget {
  final Merchant merchant;

  const MerchantCard({
    Key? key,
    required this.merchant,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // Routes.router.navigate(
        //   Routes.merchantScreen,
        //   args: MerchantMenuArgs(
        //     merchant: merchant.visitor!,
        //     addressList: merchant.addressList,
        //   ),
        // );
        Routes.router.navigate(
          Routes.merchantDetail,
          args: MerchantDetailArgs(
            merchant: merchant.visitor!,
            addressList: merchant.addressList,
          ),
        );
      },
      child: Container(
        width: 166.76,
        height: 166.76,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        child: Stack(
          children: [
            Positioned.fill(
              child: PrimaryImage(
                url: merchant.visitor?.photo?.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
              left: 4,
              top: 5,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Colors.white),
                width: 53,
                height: 26,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(AppAssets.star),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        merchant.visitor?.rating?.toStringAsFixed(1) ?? '',
                        style: text400Size13Black,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              left: 4,
              bottom: 5,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Colors.white),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 8,
                  ),
                  child: Text(
                      '${merchant.visitor?.companyName ?? 'No name merchant'}'),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
