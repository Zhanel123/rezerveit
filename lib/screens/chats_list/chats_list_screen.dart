import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/chat/cubit.dart';
import 'package:home_food/blocs/chat/cubit.dart';
import 'package:home_food/blocs/chat/state.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/chat/chat.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/chat_screen/chat_screen.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';
import '../widgets/my_app_bar.dart';
import '../widgets/search/search_field.dart';
import 'widgets/chat_item.dart';

class ChatsListScreen extends StatefulWidget {
  const ChatsListScreen({Key? key}) : super(key: key);

  @override
  State<ChatsListScreen> createState() => _ChatsListScreenState();
}

class _ChatsListScreenState extends State<ChatsListScreen>
    with TickerProviderStateMixin {
  @override
  void initState() {
    serviceLocator<ChatCubit>().fetchChatList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        color: Color(0xffF5F5F5),
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
                child: MyAppBar(),
              ),
              SizedBox(
                height: 13,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: _SearchField(),
              ),
              SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Чаты',
                      style: text700Size18Black,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: BlocBuilder<ChatCubit, ChatState>(
                  buildWhen: (p, c) =>
                      p.chatMessages != c.chatMessages ||
                      p.searchResult != c.searchResult,
                  builder: (context, state) {
                    if (state.searchResult != null &&
                        state.searchResult!.chats.isNotEmpty) {
                      return ListView(
                        children: [
                          for (int i = 0;
                              i < state.searchResult!.chats.length;
                              i++)
                            GestureDetector(
                              onTap: () => Routes.router.navigate(
                                  Routes.chatScreen,
                                  args: ChatScreenArgs(
                                      state.searchResult!.chats[i].id!)),
                              child: ChatItem(
                                chat: state.searchResult!.chats[i],
                                isMerchant: state.profile?.userType ==
                                    UserType.MANUFACTURER,
                              ),
                            ),
                        ],
                      );
                    } else if (state.searchResult != null &&
                        state.searchResult!.chats.isEmpty) {
                      return Center(
                        child: Text('Ничего не найдено'),
                      );
                    }
                    if (state.chatList.isEmpty) {
                      return Center(
                        child: Text('Список чатов пуст'),
                      );
                    }
                    return ListView(
                      children: [
                        for (int i = 0; i < state.chatList.length; i++)
                          GestureDetector(
                            onTap: () => Routes.router.navigate(
                                Routes.chatScreen,
                                args: ChatScreenArgs(state.chatList[i].id!)),
                            child: ChatItem(
                              chat: state.chatList[i],
                              isMerchant: state.profile?.userType ==
                                  UserType.MANUFACTURER,
                            ),
                          ),
                      ],
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 31.0,
                  left: 34,
                  right: 34,
                ),
                child: GradientButton(
                  borderRadius: BorderRadius.circular(50),
                  labelText: 'Написать в поддержку',
                  onPressed: serviceLocator<ChatCubit>().chatSupport,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _SearchField extends StatelessWidget {
  const _SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatCubit, ChatState>(
      builder: (context, state) {
        return RoundedTextFormField(
          value: state.searchText,
          onChange: serviceLocator<ChatCubit>().handleSearchText,
          hintText: 'Поиск',
          keyboardType: TextInputType.text,
        );
      },
    );
  }
}
