import 'package:flutter/material.dart';
import 'package:home_food/models/chat/chat.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/primary_image.dart';
import 'package:intl/intl.dart';

import '../../../core/app_colors.dart';
import '../../../core/text_styles.dart';

class ChatItem extends StatefulWidget {
  final ChatModel chat;
  final bool isMerchant;

  const ChatItem({
    required this.chat,
    Key? key,
    required this.isMerchant,
  }) : super(key: key);

  @override
  State<ChatItem> createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 3.0, left: 16, right: 16),
      child: Container(
        height: 74,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.only(right: 17),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                Container(
                  width: 3,
                  color: AppColors.pinkColor,
                ),
                SizedBox(
                  width: 15,
                ),
                SizedBox(
                  width: 42,
                  height: 42,
                  child: PrimaryImage(
                    url: widget.chat.customer?.photo?.imageUrl,
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Text(
                          widget.isMerchant
                              ? '${widget.chat.customer?.firstName}'
                              : '${widget.chat.manufacturer?.firstName}',
                          style: text700Size15Black,
                        ),
                        SizedBox(
                          width: 13,
                        ),
                        if (widget.chat.lastMessageTime != null)
                          Text(
                            '${DateFormat('dd.MM.yy hh:mm').format(widget.chat.lastMessageTime!)}',
                            style: text400Size13Grey,
                          )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '${widget.chat.lastMessageText}',
                      style: text400Size13Green,
                    )
                  ],
                ),
              ],
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: Color(0xff7A7A7A),
              size: 10,
            )
          ],
        ),
      ),
    );
  }
}
