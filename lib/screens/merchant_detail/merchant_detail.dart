import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/blocs/merchant_detail/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/primary_image.dart';
import 'package:home_food/service_locator.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:seafarer/seafarer.dart';

class MerchantDetailArgs extends BaseArguments {
  final Profile merchant;
  final List<Address> addressList;

  MerchantDetailArgs({
    required this.merchant,
    this.addressList = const [],
  });
}

class MerchantDetail extends StatefulWidget {
  final MerchantDetailArgs args;

  const MerchantDetail({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  State<MerchantDetail> createState() => _MerchantDetailState();
}

class _MerchantDetailState extends State<MerchantDetail> {
  @override
  void initState() {
    serviceLocator<MerchantDetailCubit>().handleMerchantChanged(
      widget.args.merchant,
    );
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final MaskTextInputFormatter phoneFormatter = MaskTextInputFormatter(
        mask: "+7(###)#######", initialText: widget.args.merchant.phoneNumber);

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Color(0xffF5F5F5),
          child: SafeArea(child:
              BlocBuilder<MerchantDetailCubit, MerchantDetailState>(
                  builder: (context, state) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0).copyWith(bottom: 34),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 300,
                    width: double.infinity,
                    child: Stack(
                      children: [
                        PrimaryImage(
                          url: widget.args.merchant.photo?.imageUrl,
                          fit: BoxFit.fitWidth,
                        ),
                        Positioned(
                          top: 25,
                          left: 20,
                          child: InkWell(
                            onTap: () {
                              Routes.router.pop();
                            },
                            child: Container(
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                color: Color(0xffF5F5F5),
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: Center(
                                child: SvgPicture.asset(AppAssets.arrow_left),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 28),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RatingBar.builder(
                            initialRating:
                                (widget.args.merchant.rating ?? 0).toDouble(),
                            direction: Axis.horizontal,
                            allowHalfRating: false,
                            itemCount: 5,
                            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Container(
                              width: 22,
                              height: 22,
                              child: Icon(
                                Icons.star,
                                size: 22,
                                color: Color(0xffFF5D04),
                              ),
                            ),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Text(
                            '${widget.args.merchant.companyName ?? 'No name company'}',
                            style: text400Size24Black,
                          ),
                          SizedBox(
                            height: 14,
                          ),
                          for (int i = 0;
                              i < widget.args.addressList.length;
                              i++)
                            Text(
                              'Адрес ${i + 1} : город ${widget.args.addressList[i].city?.name(context)}, адрес: ${widget.args.addressList[i].address}',
                              style: text400Size13Black,
                            ),
                          SizedBox(
                            height: 14,
                          ),
                          Text(
                            '7 000 тг',
                            style: text400Size16Black,
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Row(
                            children: [
                              InkWell(
                                child: SvgPicture.asset(
                                    AppAssets.merchant_like_icon),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              InkWell(
                                child: SvgPicture.asset(
                                    AppAssets.merchant_call_icon),
                              )
                            ],
                          )
                        ],
                      )),
                  Container(
                    width: double.infinity,
                    height: 1,
                    color: Color.fromRGBO(34, 34, 34, 0.3),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                    child: Column(
                      children: [
                        InkWell(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Color(0xffFF5D04),
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: 20, horizontal: 22),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Зарезервировать столик',
                                  style: text400Size18White,
                                ),
                                SvgPicture.asset(AppAssets.question_icon_white)
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(color: Color(0xffFF5D04)),
                              color: Color(0xffF5F5F5),
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: 20, horizontal: 22),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Меню / Доставка',
                                  style: text400Size18Black,
                                ),
                                SvgPicture.asset(AppAssets.question_icon_black)
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 1,
                    color: Color.fromRGBO(34, 34, 34, 0.3),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Описание ресторана',
                          style: text400Size20Black,
                        ),
                        SizedBox(
                          height: 14,
                        ),
                        Text(
                          'Итальянская остерия, где вся семья может насладиться безупречной классикой и свежими интерпретациями знакомых сочетаний. Сохранить простоту и традиции итальянской остерии, но одновременно отразить все актуальные веяния гастро-индустрии - вот что мы преследовали при создании Del Papa. В меню всегда найдется и безупречно выполненная классика.',
                          style: text400Size16Black,
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 1,
                    color: Color.fromRGBO(34, 34, 34, 0.3),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: 30, bottom: 22, left: 20, right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Детали',
                          style: text400Size20Black,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Image.asset(AppAssets.address_map),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 22),
                          child: Row(
                            children: [
                              SvgPicture.asset(AppAssets.address_map_icon),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                'Адрес заведения',
                                style: text400Size12Black,
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 1,
                          color: Color.fromRGBO(34, 34, 34, 0.3),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 22),
                          child: Row(
                            children: [
                              SvgPicture.asset(AppAssets.phone_icon),
                              SizedBox(
                                width: 20,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Телефон',
                                    style: text400Size12Black,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    '${widget.args.merchant.phoneNumber}',
                                    style: text400Size12Black,
                                  )
                                  // ProfileTextField(
                                  //   value: widget.args.merchant.phoneNumber,
                                  //   hintText: '+7 999 345-55-55',
                                  //   inputFormatters: [phoneFormatter],
                                  //   textInputType: TextInputType.phone,
                                  // ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 1,
                          color: Color.fromRGBO(34, 34, 34, 0.3),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 22),
                          child: Row(
                            children: [
                              SvgPicture.asset(AppAssets.parking_icon),
                              SizedBox(
                                width: 20,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Паркинг',
                                    style: text400Size12Black,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Нет',
                                    style: text400Size12Black,
                                  )
                                  // ProfileTextField(
                                  //   value: widget.args.merchant.phoneNumber,
                                  //   hintText: '+7 999 345-55-55',
                                  //   inputFormatters: [phoneFormatter],
                                  //   textInputType: TextInputType.phone,
                                  // ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 1,
                    color: Color.fromRGBO(34, 34, 34, 0.3),
                  ),
                ],
              ),
            );
          })),
        ),
      ),
    );
  }
}
