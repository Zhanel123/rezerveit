import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/blocs/chat/state.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/message/message.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/image_caputer_menu.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';
import 'package:seafarer/seafarer.dart';
import 'package:collection/collection.dart';

import '../../blocs/chat/cubit.dart';
import 'widgets/message_item.dart';

class ChatScreenArgs extends BaseArguments {
  final String chatId;

  ChatScreenArgs(this.chatId);
}

class ChatScreen extends StatefulWidget {
  final ChatScreenArgs args;

  const ChatScreen({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  String message = '';

  @override
  void initState() {
    serviceLocator<ChatCubit>().handleChatChanged(widget.args.chatId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        color: Color(0xffF5F5F5),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          SvgPicture.asset(AppAssets.app_bar_logo),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.44),
                            child: Text('Reserveat', style: text700Size18Black),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 12.0),
                        child: InkWell(
                          onTap: () {
                            Routes.router.pop();
                          },
                          child: Container(
                            width: 32,
                            height: 32,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white),
                            child: Center(
                              child: Icon(
                                Icons.close,
                                size: 17,
                                color: Color(0xff7A7A7A),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(28),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      BlocBuilder<ChatCubit, ChatState>(
                        builder: (context, state) {
                          return Container(
                            padding: EdgeInsets.only(
                                left: 33, right: 24, top: 17, bottom: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Чат с ${state.profile?.userType == UserType.MANUFACTURER ? state.chatList.firstWhereOrNull((element) => element.id == widget.args.chatId)?.customer?.firstName : state.chatList.firstWhereOrNull((element) => element.id == widget.args.chatId)?.manufacturer?.firstName}',
                                  style: text700Size18Black,
                                ),
                                InkWell(
                                  onTap: () {
                                    Routes.router.pop();
                                  },
                                  child: Text(
                                    'Закрыть',
                                    style: text500Size16Pink,
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                      ),
                      Container(
                        height: 1,
                        width: double.infinity,
                        decoration: BoxDecoration(color: Color(0xffD8D8D8)),
                      ),
                      Expanded(
                        child: BlocBuilder<ChatCubit, ChatState>(
                          builder: (context, state) {
                            return ListView(
                              reverse: true,
                              children: [
                                for (int i = state.chatMessages.length - 1;
                                    i >= 0;
                                    i--)
                                  MessageItem(
                                    messageItem: state.chatMessages[i],
                                    edged: false,
                                    isLast: false,
                                  )
                              ],
                            );
                          },
                        ),
                      ),
                      Container(
                        height: 1,
                        width: double.infinity,
                        decoration: BoxDecoration(color: Color(0xffD8D8D8)),
                      ),
                      Container(
                        height: 76,
                        color: Colors.white,
                        padding: EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: [
                            SizedBox(
                                height: double.infinity,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 10,
                                  ).copyWith(left: 25),
                                  child: InkWell(
                                    onTap: () => handleChooseLogo(context),
                                    child: SvgPicture.asset(
                                        AppAssets.message_icon),
                                  ),
                                )),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0,
                                ),
                                child: BlocBuilder<ChatCubit, ChatState>(
                                  builder: (context, state) {
                                    return RoundedTextFormField(
                                      value: state.newMessageText,
                                      hintText: 'Сообщение..',
                                      // hintTextAlign: TextAlign.left,
                                      height: 56,
                                      onChange: serviceLocator<ChatCubit>()
                                          .handleNewMessageTextChanged,
                                    );
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                                height: double.infinity,
                                width: 60,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 10,
                                  ).copyWith(right: 28),
                                  child: InkWell(
                                    onTap: () => serviceLocator<ChatCubit>()
                                        .sendMessage(widget.args.chatId),
                                    child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppColors.pinkColor),
                                      child: Center(
                                        child: Icon(
                                          Icons.arrow_upward_rounded,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                                // BlocBuilder<ChatCubit, ChatState>(
                                //   builder: (context, state) {
                                //     return Padding(
                                //       padding: const EdgeInsets.symmetric(
                                //         vertical: 10,
                                //       ).copyWith(right: 24),
                                //       child: InkWell(
                                //         onTap: serviceLocator<ChatCubit>()
                                //             .sendMessage,
                                //         child: Container(
                                //           width: double.infinity,
                                //           height: double.infinity,
                                //           decoration: BoxDecoration(
                                //             shape: BoxShape.circle,
                                //             color: AppColors.primary,
                                //           ),
                                //           child: Center(
                                //             child: state.isLoading
                                //                 ? Padding(
                                //               padding: const EdgeInsets.all(
                                //                   8.0),
                                //               child: CircularProgressIndicator(
                                //                 color: Colors.white,
                                //               ),
                                //             )
                                //                 : Icon(
                                //               Icons.arrow_upward_rounded,
                                //               color: Colors.white,
                                //             ),
                                //           ),
                                //         ),
                                //       ),
                                //     );
                                //   },
                                // ),
                                ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void handleChooseLogo(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return Container(
          margin:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Wrap(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(25),
                ),
                child: Container(
                  color: Colors.transparent,
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Container(
                              color: const Color(0xFFE5EBF0),
                              height: 4,
                              width: 40,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        ImageCaptureMenu(
                          onSelectImage: (file) => serviceLocator<ChatCubit>()
                              .sendImage(file, widget.args.chatId),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }
}
