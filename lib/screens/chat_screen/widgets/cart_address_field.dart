import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

class CartAddressField extends StatefulWidget {
  const CartAddressField({Key? key}) : super(key: key);

  @override
  State<CartAddressField> createState() => _CartAddressFieldState();
}

class _CartAddressFieldState extends State<CartAddressField> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewAddressCubit, NewAddressState>(
      listenWhen: (prev, current) =>
          prev.deletionStatus != current.deletionStatus,
      listener: (context, state) {
        if (state.deletionStatus.isSubmissionSuccess) {
          // Navigator.pop(context);
          serviceLocator<ProfileCubit>().getMyAddresses();
          FlushbarService().showSuccessMessage();
        }
      },
      builder: (context, state) {
        return BlocBuilder<ProfileCubit, ProfileState>(
          builder: (context, state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 17, right: 16, left: 16),
                  child: Text(
                    'Дополнительный адрес доставки',
                    style: text400Size13Grey,
                  ),
                ),
                ...state.addressList.asMap().entries.map((e) => Padding(
                      padding: EdgeInsets.only(top: 7, right: 16, left: 16),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () => serviceLocator<NewAddressCubit>()
                                    .deleteAddress(addressId: e.value.id ?? ''),
                                child: Text(
                                  'Удалить',
                                  style: text400Size13Grey,
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          RoundedTextFormField(
                            keyboardType: TextInputType.text,
                            value: '${e.value.city?.name(context)}',
                            hintText: '',
                            disabled: true,
                            onChange: (value) => serviceLocator<ProfileCubit>()
                                .changeAddressList(value, e.key),
                            hasError: false,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          RoundedTextFormField(
                            keyboardType: TextInputType.text,
                            value: e.value.address,
                            hintText: '',
                            disabled: true,
                            onChange: (value) => serviceLocator<ProfileCubit>()
                                .changeAddressList(value, e.key),
                            hasError: false,
                          ),
                          SizedBox(
                            height: 6,
                          )
                        ],
                      ),
                    ))
              ],
            );
          },
        );
      },
    );
  }
}
