import 'package:flutter/material.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/models/chat/chat_message.dart';
import 'package:home_food/models/message/message.dart';
import 'package:home_food/screens/widgets/primary_image.dart';
import 'package:home_food/service_locator.dart';
import 'package:intl/intl.dart';

import '../../../core/app_colors.dart';

class MessageItem extends StatelessWidget {
  final ChatMessageModel messageItem;
  final bool edged;
  final bool isLast;

  const MessageItem(
      {Key? key,
      required this.messageItem,
      required this.edged,
      this.isLast = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (messageItem.authorId ==
        serviceLocator<ProfileCubit>().state.profile?.id) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4)
            .copyWith(bottom: isLast ? 16 : 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            _buildMessage(
              context: context,
              isMe: true,
              isRounded: edged,
              messageItem: messageItem,
            )
          ],
        ),
      );
    }
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          _buildMessage(
            context: context,
            isMe: false,
            isRounded: edged,
            messageItem: messageItem,
          )
        ],
      ),
    );
  }

  _buildMessage({
    required ChatMessageModel messageItem,
    required bool isRounded,
    required bool isMe,
    required BuildContext context,
  }) {
    //yyyy-MM-dd'T'HH:mm:ss'Z'
    String createdTime =
        DateFormat("dd.MM.yy hh:mm").format(messageItem.createdDate!);
    // DateFormat('HH:mm', 'ru_Ru').format(messageItem.createTime!);

    return Container(
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
              color: isMe ? Color(0xff3A3A3A) : Color(0xffE8E6E5),
              borderRadius: const BorderRadius.all(Radius.circular(12)),
            ),
            padding: EdgeInsets.only(left: 12, top: 7, right: 4, bottom: 5),
            child: Container(
              constraints: BoxConstraints(minWidth: 30, maxWidth: 240),
              child: Stack(
                children: [
                  Padding(
                    padding: isMe
                        ? EdgeInsets.only(right: 80, bottom: 16, top: 5)
                        : EdgeInsets.only(right: 80, bottom: 16, top: 5),
                    child: Column(
                      children: [
                        if (messageItem.photos.isNotEmpty)
                          SizedBox(
                            height: 100,
                            width: 100,
                            child: PrimaryImage(
                              url: messageItem.photos.first.imageUrl,
                            ),
                          ),
                        Text(
                          messageItem.text?.trim() ?? '',
                          softWrap: true,
                          style:
                              Theme.of(context).textTheme.bodyText2?.copyWith(
                                    color: isMe ? Colors.white : Colors.black,
                                  ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 0,
                    bottom: 0,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Text('$createdTime',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    ?.copyWith(
                                        color: isMe
                                            ? Colors.white
                                            : Color(0xFF999999),
                                        fontSize: 10)),
                            const SizedBox(width: 4),
                            if (isMe == true && messageItem.status == "READ")
                              Icon(
                                Icons.done_all,
                                color: Color(0xFF2C5FA4),
                                size: 16,
                              ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
