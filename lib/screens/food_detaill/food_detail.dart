import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/blocs/favorite_dishes/cubit.dart';
import 'package:home_food/blocs/viewed_history/cubit.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/screens/merchants_menu/merchant_menu.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/my_app_bar.dart';
import 'package:home_food/service_locator.dart';
import 'package:seafarer/seafarer.dart';

import '../../core/app_assets.dart';
import '../../core/app_colors.dart';
import '../../routes.dart';

class FoodDetailArgs extends BaseArguments {
  final Dish dish;

  FoodDetailArgs({
    required this.dish,
  });
}

class FoodDetail extends StatefulWidget {
  final FoodDetailArgs args;

  const FoodDetail({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  State<FoodDetail> createState() => _FoodDetailState();
}

class _FoodDetailState extends State<FoodDetail> {
  int count = 1;

  @override
  void initState() {
    serviceLocator<ViewedHistoryCubit>().addDish(widget.args.dish);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffF5F5F5),
        height: double.infinity,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(top: 5),
          child: SafeArea(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: MyAppBar(),
                ),
                SizedBox(
                  height: 13,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Routes.router.pop();
                        },
                        child: Container(
                          width: 33,
                          height: 33,
                          decoration: BoxDecoration(
                            color: Color(0xffF5F5F5),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          child: Center(
                              child: Icon(
                            Icons.chevron_left,
                            color: Color(0xff7A7A7A),
                          )),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      ...widget.args.dish.preferenceList.map(
                        (e) => Container(
                          height: 25,
                          margin: EdgeInsets.only(right: 5),
                          decoration: BoxDecoration(
                              color: AppColors.greyColor2,
                              borderRadius: BorderRadius.circular(50)),
                          padding: EdgeInsets.symmetric(horizontal: 24),
                          child: Center(
                            child: Text(
                              '${e.name(context)}',
                              style: text500Size13White,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 17,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Image.asset(widget.imageUrl),
                        Stack(
                          children: [
                            CarouselSlider(
                              options: CarouselOptions(
                                height: 167,
                                viewportFraction: 1,
                              ),
                              items: widget.args.dish.images.map((i) {
                                return Image.network(i.imageUrl);
                              }).toList(),
                            ),
                            Positioned(
                              top: 9,
                              bottom: 11,
                              left: 13.18,
                              right: 8,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SvgPicture.asset(
                                        AppAssets.hot_icon,
                                        width: 33,
                                        height: 33,
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      if (widget.args.dish.preOrderDish)
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(28),
                                            color: AppColors.pinkColor,
                                          ),
                                          height: 30,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 21),
                                          child: Center(
                                            child: Text(
                                              'Предзаказ',
                                              textAlign: TextAlign.center,
                                              style: text400Size15White,
                                            ),
                                          ),
                                        ),
                                      if (widget.args.dish.hasHalfPortion)
                                        Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(28),
                                              color: Colors.white),
                                          height: 30,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 21),
                                          child: Center(
                                            child: Text(
                                              'Пол порции',
                                              textAlign: TextAlign.center,
                                              style: text400Size11Black,
                                            ),
                                          ),
                                        ),
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                widget.args.dish.name ?? '',
                                style: text700Size18Black,
                              ),
                              BlocBuilder<FavouriteDishesCubit,
                                  FavouriteDishesState>(
                                builder: (context, state) {
                                  bool isFavourite = state.favDishes.any(
                                      (element) =>
                                          element.dishCardItem?.id ==
                                          widget.args.dish.id);
                                  return InkWell(
                                      onTap: () => isFavourite
                                          ? serviceLocator<
                                                  FavouriteDishesCubit>()
                                              .deleteFavDish(
                                                  value: widget.args.dish)
                                          : serviceLocator<
                                                  FavouriteDishesCubit>()
                                              .addDishToFavorite(
                                                  widget.args.dish),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: SvgPicture.asset(
                                          isFavourite
                                              ? AppAssets.like_active
                                              : AppAssets.like_inactive,
                                        ),
                                      ));
                                },
                              ),
                            ],
                          ),
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.only(
                        //       left: 18.0, right: 17, top: 5),
                        //   child: Text(
                        //     widget.args.dish.name,
                        //     style: text700Size18Black,
                        //   ),
                        // ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 5),
                          child: Text(
                            '${widget.args.dish.weight} г',
                            style: text400Size13Black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 10),
                          child: Text(
                            'Состав: ${widget.args.dish.ingredientList.map((e) => e.name).join(', ')}',
                            style: text400Size13Black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 10),
                          child: Text(
                            widget.args.dish.description,
                            style: text400Size13Grey,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 18.0, right: 17, top: 13),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                NumUtils.humanizeNumber(
                                      widget.args.dish.priceWithSale,
                                      isCurrency: true,
                                    ) ??
                                    '',
                                style: text700Size18Pink,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              if (widget.args.dish.priceWithSale != null)
                                Text(
                                  NumUtils.humanizeNumber(
                                        widget.args.dish.price!,
                                        isCurrency: true,
                                      ) ??
                                      '',
                                  style: TextStyle(
                                      color: Color(0xffB0B0B0),
                                      decoration: TextDecoration.lineThrough,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15),
                                )
                            ],
                          ),
                        ),
                        widget.args.dish.portionAmount == 0
                            ? SizedBox()
                            : Padding(
                                padding: EdgeInsets.only(
                                    left: 18.0, right: 17, top: 15),
                                child: BlocBuilder<BasketCubit, BasketState>(
                                  builder: (context, state) {
                                    return Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        GradientButton(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            labelText: 'В корзину',
                                            isLoading: state
                                                .status.isSubmissionInProgress,
                                            icon: SvgPicture.asset(
                                                AppAssets.basketIconWhite),
                                            onPressed: () =>
                                                serviceLocator<BasketCubit>()
                                                    .putDishToPreorder(
                                                  widget.args.dish,
                                                  count,
                                                )),
                                        Container(
                                          height: 40,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 9),
                                          width: 106,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50.0),
                                            color: Colors.white,
                                            border: Border.all(
                                                color: Color(0xffE9E9E9)),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    count <= 0 ? null : count--;
                                                  });
                                                },
                                                child: Container(
                                                  width: 21.33,
                                                  height: 21.33,
                                                  decoration: BoxDecoration(
                                                      color: Color(0xffE9E9E9),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              58)),
                                                  child: Center(
                                                    child: Icon(
                                                      Icons.remove,
                                                      color: Colors.black,
                                                      size: 10,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                '${count}',
                                                style: text400Size15Black,
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    widget.args.dish
                                                                .portionAmount <=
                                                            count
                                                        ? null
                                                        : count++;
                                                  });
                                                },
                                                child: SvgPicture.asset(
                                                  AppAssets.plus_icon,
                                                  width: 25,
                                                  height: 25,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),
                        Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 17),
                            child: Row(
                              children: [
                                Text('Производитель:',
                                    style: text400Size13Black),
                                SizedBox(
                                  width: 3,
                                ),
                                InkWell(
                                  onTap: () {
                                    Routes.router.navigate(
                                      Routes.merchantMenu,
                                      args: MerchantMenuArgs(
                                        merchant:
                                            widget.args.dish.manufacturer!,
                                      ),
                                    );
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                        '${widget.args.dish.manufacturer?.firstName}',
                                        style: text400Size13Pink,
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      InkWell(
                                        onTap: () {},
                                        child: SvgPicture.asset(
                                            AppAssets.text_icon),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
