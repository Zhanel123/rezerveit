import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/login/cubit.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/buttons/default_button.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:seafarer/seafarer.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginCubit, LoginState>(
      listenWhen: (p, c) => p.loginStatus != c.loginStatus,
      listener: (context, state) {
        if (state.loginStatus.isSubmissionSuccess) {
          Routes.router.navigate(Routes.loginSmsVerificationScreen);
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
              color: Color(0xffF5F5F5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 30.0, bottom: 30),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(AppAssets.main_logo),
                          SizedBox(height: 27),
                          Text(
                            'Reserveat',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 26,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(28.0),
                      topLeft: Radius.circular(28.0),
                    ),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 37, vertical: 34),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            'Авторизация',
                            textAlign: TextAlign.left,
                            style: text700Size24Black,
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        _PhoneField(),
                        SizedBox(
                          height: 12,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 25),
                          child: DefaultButton(
                            loading: state.loginStatus.isSubmissionInProgress,
                            onPressed: serviceLocator<LoginCubit>().login,
                            label: 'Авторизоваться',
                            color: AppColors.pinkColor,
                            textColor: AppColors.white,
                          ),
                        ),
                        Center(
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 25, top: 12),
                            child: GestureDetector(
                              onTap: () => Routes.router.navigate(
                                Routes.registrationScreen,
                                navigationType: NavigationType.popAndPushNamed,
                              ),
                              child: Text(
                                'Нет аккаунта? Зарегестрироваться',
                                style: text400Size12Pink,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class _PhoneField extends StatelessWidget {
  const _PhoneField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      builder: (context, state) {
        final MaskTextInputFormatter phoneFormatter = MaskTextInputFormatter(
            mask: "+7(###)#######", initialText: state.phoneNumber.value);

        return RoundedTextFormField(
          value: state.phoneNumber.value,
          onChange: serviceLocator<LoginCubit>().changePhone,
          inputFormatters: [phoneFormatter],
          hasError: state.phoneNumber.invalid,
          autofocus: true,
          errorText: state.phoneNumber.error != null ? 'Обязательное поле' : '',
          hintText: '+7(XXX)XXXXXXX',
          keyboardType: TextInputType.phone,
          border: Border.all(color: Color(0xFFD8D8D8)),
        );
      },
    );
  }
}
