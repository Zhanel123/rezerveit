import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/login/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/login/login_pincode.dart';
import 'package:seafarer/seafarer.dart';

class LoginSmsVerificationScreen extends StatefulWidget {
  const LoginSmsVerificationScreen({Key? key}) : super(key: key);

  @override
  State<LoginSmsVerificationScreen> createState() =>
      _LoginSmsVerificationScreenState();
}

class _LoginSmsVerificationScreenState
    extends State<LoginSmsVerificationScreen> {
  late Timer _timer;
  int _start = 60;
  int minutes = 0;

  void startTimer() {
    if (mounted) {
      setState(() {
        _start = 60;
        minutes = (_start / 60).toInt();
      });
    }
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
            minutes = (_start / 60).toInt();
          });
        }
      },
    );
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginCubit, LoginState>(
      listenWhen: (prevState, currState) =>
          prevState.pinCodeStatus != currState.pinCodeStatus,
      listener: (context, state) {
        if (state.pinCodeStatus.isSubmissionSuccess) {
          Routes.router.navigate(
            Routes.homeScreen,
            navigationType: NavigationType.pushAndRemoveUntil,
            removeUntilPredicate: (route) => false,
          );
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
            color: Color(0xffF5F5F5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 66.0, bottom: 56),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(AppAssets.main_logo),
                          SizedBox(
                            height: 27,
                          ),
                          Text(
                            'Reserveat',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 26),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(28.0),
                          topLeft: Radius.circular(28.0)),
                      color: Colors.white),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 43, vertical: 38),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Введите код из СМС',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 15,
                              color: Colors.black),
                        ),
                        SizedBox(
                          height: 38,
                        ),
                        LoginPincode(),
                        SizedBox(
                          height: 12,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        _start == 0
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text('Запросить код',
                                      style: const TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff939393))),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      startTimer();
                                    },
                                    child: Text('еще раз',
                                        style: const TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xff939393))),
                                  ),
                                ],
                              )
                            : Text(
                                'Отправить повторно: через ${minutes < 1 ? '0' : ''}${minutes} : ${_start % 60}',
                                style: const TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff939393)))
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
