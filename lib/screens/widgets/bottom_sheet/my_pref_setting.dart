import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/dish_filter/cubit.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service_locator.dart';

import '../../../core/text_styles.dart';
import '../../../routes.dart';
import '../check_row.dart';

class MyPrefSetting extends StatefulWidget {
  const MyPrefSetting({Key? key}) : super(key: key);

  @override
  State<MyPrefSetting> createState() => _MyPrefSettingState();
}

class _MyPrefSettingState extends State<MyPrefSetting> {
  bool isNew = false;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishFilterCubit, DishFilterState>(
        builder: (context, state) {
      return PrimaryBottomSheet(
        contentPadding: EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 18,
        ),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () {
                  Routes.router.pop();
                },
                child: Container(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                      color: Color(0xffEAEAEA),
                      borderRadius: BorderRadius.circular(28)),
                  child: Icon(
                    Icons.close,
                    color: Color(0xff7A7A7A),
                    size: 15,
                  ),
                ),
              ),
            ),
            Text(
              'Мои предпочтения',
              style: text700Size15Black,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Отметьте галочкой нужные категории, чтобы они отображались в первую очередь',
              textAlign: TextAlign.center,
              style: text400Size13Grey,
            ),
            SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: LayoutBuilder(builder: (context, constraints) {
                return Wrap(
                  spacing: 20,
                  runSpacing: 20,
                  children: [
                    for (int i = 0; i < state.preferences.length; i++)
                      SizedBox(
                        width: constraints.maxWidth / 2 - 20,
                        height: 40,
                        child: CheckRow(
                          label: state.preferences[i].name(context),
                          isCheck: state.selectedPreferences.any((element) =>
                              element.id == state.preferences[i].id),
                          handleValue: (value) =>
                              serviceLocator<DishFilterCubit>()
                                  .foodPreferenceToggled(state.preferences[i]),
                        ),
                      )
                  ],
                );
              }),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 60),
              child: GradientButton(
                labelText: 'Сохранить',
                onPressed: () {
                  serviceLocator<DishFilterCubit>().handlePreferencesChanged();
                  Routes.router.pop();
                },
                borderRadius: BorderRadius.circular(50),
              ),
            ),
            TextButton(
                onPressed: serviceLocator<DishFilterCubit>().resetPreferences,
                child: Text(
                  'Сбросить выбор',
                  style: text400Size13Pink,
                )),
          ],
        ),
      );
    });
  }
}

// class handleCheckRow extends StatefulWidget {
//   final String label;
//   final bool isCheck;
//   final void Function(bool?) handleValue;
//   final double? widthParameter;
//
//   const handleCheckRow({Key? key, required this.label, required this.isCheck, required this.handleValue, this.widthParameter}) : super(key: key);
//
//   @override
//   State<handleCheckRow> createState() => handleCheckRowState();
// }
//
// class handleCheckRowState extends State<handleCheckRow> {
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.start,
//       children: [
//         CheckboxButton(
//           isChecked: widget.isCheck,
//           changeIsChecked: (value) => widget.handleValue(value),
//         ),
//         Container(
//           width: widget.widthParameter ?? MediaQuery.of(context).size.width * 0.2,
//           child: Text(
//             widget.label,
//             style: widget.isCheck ? text700Size12Black : text700Size12Grey,
//           ),
//         )
//       ],
//     );
//   }
// }
