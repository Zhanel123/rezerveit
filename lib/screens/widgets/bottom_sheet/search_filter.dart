import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/dish_filter/cubit.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

import '../../../routes.dart';
import '../check_row.dart';

class SearchFilter extends StatefulWidget {
  const SearchFilter({
    Key? key,
    this.onSubmit,
  }) : super(key: key);

  final Function()? onSubmit;

  @override
  State<SearchFilter> createState() => _SearchFilterState();
}

class _SearchFilterState extends State<SearchFilter> {
  @override
  Widget build(BuildContext context) {
    return PrimaryBottomSheet(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            SizedBox(height: 20),
            InkWell(
              onTap: () {
                Routes.router.pop();
              },
              child: Container(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                    color: Color(0xffEAEAEA),
                    borderRadius: BorderRadius.circular(28)),
                child: Icon(
                  Icons.close,
                  color: Color(0xff7A7A7A),
                  size: 15,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Фильтр поиска',
                    style: text700Size15Black,
                  ),
                  SizedBox(
                    height: 11,
                  ),
                  _ExcludedDishTypesFilter(),
                  SizedBox(
                    height: 28,
                  ),
                  _PreferenceTypesFilter(),
                  SizedBox(
                    height: 20,
                  ),
                  _PriceFilter(),
                  SizedBox(
                    height: 30,
                  ),
                  _AdditionalFilter(),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    child: SizedBox(
                      width: double.infinity,
                      child: GradientButton(
                        borderRadius: BorderRadius.circular(50),
                        onPressed: widget.onSubmit ??
                            () {
                              serviceLocator<DishFilterCubit>()
                                  .saveFilterChanges();
                              Routes.router.pop();
                            },
                        labelText: 'Сохранить',
                      ),
                    ),
                  ),
                  Center(
                    child: TextButton(
                        onPressed:
                            serviceLocator<DishFilterCubit>().resetFilter,
                        child: Text(
                          'Сбросить выбор',
                          style: text400Size13Pink,
                        )),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _ExcludedDishTypesFilter extends StatelessWidget {
  const _ExcludedDishTypesFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishFilterCubit, DishFilterState>(
        builder: (context, state) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Исключить категории из поиска',
            style: text400Size13Grey,
          ),
          SizedBox(height: 12),
          Container(
            height: 42,
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 19),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Color(0xFFD8D8D8)),
              color: Colors.white,
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<DishType?>(
                // value: state.dishType.value,
                hint: Text(
                  'Исключить категорию',
                  style: text500Size15Black,
                ),
                onChanged: serviceLocator<DishFilterCubit>()
                    .handleNotDishTypesSelected,
                items: state.dishTypes
                    .map(
                      (e) => DropdownMenuItem(
                        value: e,
                        child: Text(
                          e.name(context),
                          style: text500Size15Black,
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
          ),
          if (state.excludedDishTypes.isNotEmpty) const SizedBox(height: 12),
          Row(
            children: [
              ...state.excludedDishTypes.map(
                (e) => Container(
                  padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                  margin: EdgeInsets.only(right: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(28),
                    color: Color(0xFFE6E6E6),
                  ),
                  child: Row(
                    children: [
                      InkWell(
                        child: Icon(
                          Icons.close_rounded,
                          size: 12,
                        ),
                        onTap: () => serviceLocator<DishFilterCubit>()
                            .deleteDishTypeSelected(e),
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        e.name(context),
                        style: text400Size13Black,
                      ),
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      );
    });
  }
}

class _PreferenceTypesFilter extends StatelessWidget {
  const _PreferenceTypesFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Добавить в поиск',
          style: text400Size13Grey,
        ),
        SizedBox(
          height: 20,
        ),
        BlocBuilder<DishFilterCubit, DishFilterState>(
          builder: (context, state) {
            return LayoutBuilder(
              builder: (context, constraints) {
                final width = constraints.maxWidth / 2 - 20;
                return Wrap(
                  // по горизонтали
                  spacing: 20,
                  // по вертикали
                  runSpacing: 20,
                  children: [
                    ...state.preferences
                        .map(
                          (e) => Container(
                            width: width,
                            child: CheckRow(
                              label: e.name(context),
                              isCheck: state.selectedPreferences.any(
                                (element) => element.id == e.id,
                              ),
                              handleValue: (value) =>
                                  serviceLocator<DishFilterCubit>()
                                      .foodPreferenceToggled(e),
                            ),
                          ),
                        )
                        .toList(),
                  ],
                );
              },
            );
          },
        )
      ],
    );
  }
}

class _AdditionalFilter extends StatelessWidget {
  const _AdditionalFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Дополнительные фильтры',
          style: text400Size13Grey,
        ),
        SizedBox(height: 20),
        BlocBuilder<DishFilterCubit, DishFilterState>(
          builder: (context, state) {
            return LayoutBuilder(
              builder: (context, constraints) {
                final width = constraints.maxWidth;
                // final width = 30.0;
                return Wrap(
                  // по горизонтали
                  // по вертикали
                  children: [
                    Container(
                      width: width,
                      child: CheckRow(
                        widthParameter: width - 35,
                        label: 'Показывать новинки',
                        isCheck: state.showNew ?? false,
                        handleValue: (value) =>
                            serviceLocator<DishFilterCubit>()
                                .handleShowNew(value),
                      ),
                    ),
                    Container(
                      width: width,
                      child: CheckRow(
                        widthParameter: width - 35,
                        label: 'Еда со скидкой',
                        isCheck: state.hasSale ?? false,
                        handleValue: (value) =>
                            serviceLocator<DishFilterCubit>()
                                .handleHasSale(value!),
                      ),
                    ),
                    Container(
                      width: width,
                      child: CheckRow(
                        widthParameter: width - 35,
                        label: 'Показывать популярное',
                        isCheck: state.showPopular ?? false,
                        handleValue: (value) =>
                            serviceLocator<DishFilterCubit>()
                                .handleShowPopular(value!),
                      ),
                    ),
                  ],
                );
              },
            );
          },
        ),
      ],
    );
  }
}

class _PriceFilter extends StatelessWidget {
  const _PriceFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishFilterCubit, DishFilterState>(
      builder: (context, state) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Фильтр по ценам',
                  style: text400Size13Grey,
                ),
                // CheckRow(
                //   label: 'По возрастанию',
                //   isCheck: state.priceSortDirection == 'ASC',
                //   handleValue: (value) => serviceLocator<DishFilterCubit>()
                //       .handlePriceSortDirection(
                //           (value ?? false) ? 'ASC' : 'DESC'),
                //   widthParameter: 156,
                // ),
              ],
            ),
            SliderTheme(
              data: SliderThemeData(
                thumbColor: AppColors.pinkColor,
                activeTrackColor: Colors.pink.shade100,
                inactiveTrackColor: AppColors.whiteGrey,
                trackHeight: 4,
                valueIndicatorColor: Colors.white,
                valueIndicatorTextStyle: text700Size15Pink,
                valueIndicatorShape: PaddleSliderValueIndicatorShape(),
                thumbShape: RoundSliderThumbShape(
                  enabledThumbRadius: 13.0,
                ),
                overlayColor: AppColors.pinkColor.withOpacity(0.3),
              ),
              child: Column(
                children: [
                  RangeSlider(
                    min: 0,
                    max: 30000,
                    labels: RangeLabels(
                      state.priceFrom.round().toString(),
                      state.priceTo.round().toString(),
                    ),
                    // activeColor: Colors.black,
                    values: RangeValues(
                      state.priceFrom.toDouble(),
                      state.priceTo.toDouble(),
                    ),
                    onChanged: (prices) {
                      serviceLocator<DishFilterCubit>()
                          .handlePriceFrom(prices.start.round());
                      serviceLocator<DishFilterCubit>()
                          .handlePriceTo(prices.end.round());
                    },
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: RoundedTextFormField(
                          value: NumUtils.humanizeNumber(
                            state.priceFrom,
                          ),
                          hintText: '',
                          border: Border.all(color: AppColors.grey),
                          onChange: (value) {
                            serviceLocator<DishFilterCubit>().handlePriceFrom(
                                NumUtils.parseString(value)?.toInt() ?? 5);
                          },
                        ),
                      ),
                      SizedBox(width: 12),
                      Expanded(
                        child: RoundedTextFormField(
                          value: NumUtils.humanizeNumber(
                            state.priceTo,
                          ),
                          hintText: '',
                          border: Border.all(color: AppColors.grey),
                          onChange: (value) {
                            serviceLocator<DishFilterCubit>().handlePriceTo(
                                NumUtils.parseString(value)?.toInt() ?? 5);
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
