import 'package:flutter/material.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service_locator.dart';

class CleanCartWidget extends StatefulWidget {
  const CleanCartWidget({Key? key}) : super(key: key);

  @override
  State<CleanCartWidget> createState() => _CleanCartWidgetState();
}

class _CleanCartWidgetState extends State<CleanCartWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
      padding: EdgeInsets.only(bottom: 18, right: 18, left: 18, top: 22),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          InkWell(
              onTap: () {
                Routes.router.pop();
              },
              child: Container(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                    color: Color(0xffEAEAEA),
                    borderRadius: BorderRadius.circular(28)),
                child: Icon(
                  Icons.close,
                  color: Color(0xff7A7A7A),
                  size: 15,
                ),
              )),
          Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      'Недопустимо добавление блюда от другого производителя',
                      textAlign: TextAlign.center,
                      style: text700Size15Black,
                    ),
                    width: 300,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: GradientButton(
                      labelText: 'Очистить корзину',
                      onPressed: () {
                        Navigator.of(context).pop();
                        serviceLocator<BasketCubit>().deleteAllDishCard();
                      },
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
