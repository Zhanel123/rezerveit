import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';

class SearchField extends StatefulWidget {
  final String hintText;
  final Color? backgroundColor;
  final Function(String)? onChange;

  const SearchField(
      {Key? key, this.hintText = '', this.backgroundColor, this.onChange})
      : super(key: key);

  @override
  State<SearchField> createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 4),
      child: RoundedTextFormField(
        value: '',
        onChange: (String value) {
          if (widget.onChange != null) {
            widget.onChange!(value);
          }
        },
        hintText: widget.hintText,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.search),
        ),
      ),
    );
  }
}
