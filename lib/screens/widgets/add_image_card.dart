import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/image_caputer_menu.dart';

class AddImageButton extends StatelessWidget {
  const AddImageButton({
    Key? key,
    this.image,
    this.localFile,
    this.index,
    this.onDeleteImage,
    this.hintWidget,
    this.onSelectImage,
    this.onSelectImages,
  }) : super(key: key);

  final File? localFile;
  final UploadedImage? image;
  final int? index;
  final Function(int)? onDeleteImage;
  final Widget? hintWidget;
  final Function(File, int?)? onSelectImage;
  final Function(List<File>)? onSelectImages;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => handleChoosePhoto(context),
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              color: hintWidget != null ? Colors.white : Color(0xffEFEFEF),
              borderRadius: BorderRadius.circular(8),
            ),
            child: image != null
                ? CachedNetworkImage(
                    imageUrl: image!.imageUrl,
                    fit: BoxFit.cover,
                  )
                : localFile != null
                    ? Image.file(
                        localFile!,
                        fit: BoxFit.cover,
                      )
                    : hintWidget ??
                        Center(
                          child: SvgPicture.asset(
                            AppAssets.merchant_add_food_icon,
                            color: Color(0xffB4B4B4),
                            width: 21,
                            height: 21,
                          ),
                        ),
          ),
          if (image != null)
            Positioned(
              top: 0,
              right: 0,
              child: InkWell(
                onTap: () => onDeleteImage?.call(index!),
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.red.shade400,
                    ),
                    child: Icon(
                      Icons.delete_forever,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }

  Future<void> handleChoosePhoto(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return PrimaryBottomSheet(
          contentPadding: EdgeInsets.symmetric(
            vertical: 25,
            horizontal: 16,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                    color: const Color(0xFFE5EBF0),
                    height: 4,
                    width: 40,
                  ),
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              ImageCaptureMenu(
                isMultiple: index == null,
                onSelectImage: (image) => onSelectImage?.call(image, index),
                onSelectImages: index != null
                    ? null
                    : (images) => onSelectImages?.call(images),
              )
            ],
          ),
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }
}
