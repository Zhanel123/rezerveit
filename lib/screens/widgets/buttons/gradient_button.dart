import 'package:flutter/material.dart';
import 'package:home_food/core/text_styles.dart';

class GradientButton extends StatelessWidget {
  final BorderRadiusGeometry borderRadius;
  final double? width;
  final double height;
  final Gradient gradient;
  final VoidCallback? onPressed;
  final String? labelText;
  final bool isLoading;
  final Widget? child;
  final Widget? icon;
  final TextStyle? labelStyle;

  GradientButton({
    Key? key,
    this.onPressed,
    this.child,
    this.labelText,
    BorderRadiusGeometry? borderRadius,
    this.isLoading = false,
    this.width,
    this.height = 44.0,
    this.labelStyle,
    this.gradient = const LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [
        Color(0xFFF44F8B),
        Color(0xFFF4A95E),
      ],
    ),
    this.icon,
  })  : borderRadius = borderRadius ?? BorderRadius.circular(20),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Color(0xffFF5D04),
        borderRadius: borderRadius,
      ),
      child: IgnorePointer(
        ignoring: isLoading,
        child: ElevatedButton(
            onPressed: onPressed,
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.transparent,
              shadowColor: Colors.transparent,
              shape: RoundedRectangleBorder(borderRadius: borderRadius),
            ),
            child: isLoading
                ? Center(
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    ),
                  )
                : child ??
                    Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 22,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '$labelText',
                            style: text400Size15White,
                          ),
                          icon!
                        ],
                      ),
                    )),
      ),
    );
  }
}
