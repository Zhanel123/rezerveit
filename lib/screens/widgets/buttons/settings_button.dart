import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/app_assets.dart';
import '../../../core/app_colors.dart';
import '../../../core/text_styles.dart';
import '../bottom_sheet/my_pref_setting.dart';

class SettingsButton extends StatefulWidget {
  const SettingsButton({Key? key}) : super(key: key);

  @override
  State<SettingsButton> createState() => _SettingsButtonState();
}

class _SettingsButtonState extends State<SettingsButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _handleMyPreferencesClicked,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(28),
            color: AppColors.pinkColor),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 9.0),
              child: SvgPicture.asset(AppAssets.flame_icon),
            ),
            SizedBox(
              width: 28,
            ),
            Text(
              'Настроить личные предпочтения',
              textAlign: TextAlign.center,
              style: text400Size13White,
            ),
            SizedBox(
              width: 28,
            ),
          ],
        ),
      ),
    );
  }

  void _handleMyPreferencesClicked() {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return MyPrefSetting();
      },
      isDismissible: true,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }
}
