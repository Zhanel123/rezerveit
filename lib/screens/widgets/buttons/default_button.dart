import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {
  final Color color;
  final Color textColor;
  final String label;
  final Widget? child;
  final void Function()? onPressed;
  final bool loading;
  final double borderRadius;
  final double fontSize;

  const DefaultButton(
      {Key? key,
      this.color = Colors.white,
      this.textColor = const Color(0xff838383),
      this.label = '',
      this.child,
      this.onPressed,
      this.loading = false,
      this.borderRadius = 50,
      this.fontSize = 15})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: loading,
      child: GestureDetector(
        onTap: () => onPressed?.call(),
        child: TextButton(
            style: ButtonStyle(
                padding: MaterialStateProperty.all(
                    EdgeInsets.symmetric(horizontal: 30.41, vertical: 8)),
                backgroundColor: MaterialStateProperty.all<Color>(color),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(borderRadius)))),
            onPressed: onPressed,
            child: loading
                ? const Center(
                    child: SizedBox(
                      width: 15,
                      height: 15,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    ),
                  )
                : child ??
                    Center(
                      child: Text(
                        label,
                        style: TextStyle(
                            color: textColor,
                            fontSize: fontSize,
                            fontWeight: FontWeight.w400),
                      ),
                    )),
      ),
    );
  }
}
