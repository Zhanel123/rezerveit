import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/screens/widgets/image_caputer_menu.dart';
import 'package:home_food/service_locator.dart';

class AddLogoButton extends StatelessWidget {
  const AddLogoButton({
    Key? key,
    this.image,
    this.error = '',
    this.hintWidget,
    required this.onImageSelected,
  }) : super(key: key);

  final UploadedImage? image;
  final String error;
  final Widget? hintWidget;
  final Function(File) onImageSelected;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () => handleChooseLogo(context),
          child: Container(
            width: 77,
            height: 77,
            decoration: BoxDecoration(
              color: Color(0xffEFEFEF),
              border: error.isNotEmpty ? Border.all(color: Colors.red) : null,
              borderRadius: BorderRadius.circular(8),
            ),
            clipBehavior: Clip.hardEdge,
            child: image != null
                ? CachedNetworkImage(
                    imageUrl: image?.imageUrl,
                    fit: BoxFit.cover,
                  )
                : hintWidget ?? Padding(
                    padding: EdgeInsets.only(top: 19, left: 17, right: 17),
                    child: Column(
                      children: [
                        SvgPicture.asset(AppAssets.add_button_icon),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          'логотип',
                          style: text500Size10Black,
                        )
                      ],
                    ),
                  ),
          ),
        ),
        if (error.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(left: 8.0, top: 8.0),
            child: Text(
              '',
              style: TextStyle(
                color: Colors.red,
              ),
            ),
          ),
      ],
    );
  }

  void handleChooseLogo(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return Container(
          margin:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Wrap(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(25),
                ),
                child: Container(
                  color: Colors.transparent,
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Container(
                              color: const Color(0xFFE5EBF0),
                              height: 4,
                              width: 40,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        ImageCaptureMenu(
                          onSelectImage: onImageSelected,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }
}
