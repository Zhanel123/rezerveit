import 'package:flutter/material.dart';
import 'package:home_food/core/app_colors.dart';

class CheckboxButton extends StatefulWidget {
  final bool isChecked;
  final void Function(bool) changeIsChecked;

  const CheckboxButton(
      {Key? key, required this.isChecked, required this.changeIsChecked})
      : super(key: key);

  @override
  State<CheckboxButton> createState() => _CheckboxButtonState();
}

class _CheckboxButtonState extends State<CheckboxButton> {
  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.white;
      }
      return Colors.white;
    }

    return SizedBox(
      width: 20,
      height: 20,
      child: Checkbox(
        checkColor: AppColors.pinkColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        fillColor: MaterialStateProperty.resolveWith(getColor),
        side: MaterialStateBorderSide.resolveWith((states) {
          if (states.contains(MaterialState.pressed)) {
            return BorderSide(color: Color(0xffAAAAAA), width: 1);
          } else {
            return BorderSide(color: Color(0xffAAAAAA), width: 1);
          }
        }),
        value: widget.isChecked,
        onChanged: (value) => widget.changeIsChecked(value ?? false),
        // checkColor: AppColors.pinkColor,
        // hoverColor: Colors.transparent,
        // shape: RoundedRectangleBorder(
        //   borderRadius: BorderRadius.circular(4),
        // ),
        // fillColor: MaterialStateProperty.all(Colors.white),
        // side: BorderSide(
        //   width: 1,
        //   color: Color(0xffAAAAAA),
        // ),
      ),
    );
  }
}
