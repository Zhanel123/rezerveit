import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/app_assets.dart';
import '../bottom_sheet/search_filter.dart';

class FilterButton extends StatefulWidget {
  const FilterButton({Key? key}) : super(key: key);

  @override
  State<FilterButton> createState() => _FilterButtonState();
}

class _FilterButtonState extends State<FilterButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _handleFilterClicked();
      },
      child: Container(
        margin: EdgeInsets.only(left: 4),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        height: 40,
        width: 40,
        child: Center(
          child: SvgPicture.asset(AppAssets.filter_logo),
        ),
      ),
    );
  }

  void _handleFilterClicked() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return SearchFilter();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }
}
