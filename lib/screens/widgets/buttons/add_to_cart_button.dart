import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/clean_cart_widget.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

class AddToCartButton extends StatefulWidget {
  final Dish item;

  const AddToCartButton({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  State<AddToCartButton> createState() => _AddToCartButtonState();
}

class _AddToCartButtonState extends State<AddToCartButton> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BasketCubit, BasketState>(
      listenWhen: (p, c) => p.status != c.status,
      listener: (context, state) {
        if (state.status.isSubmissionSuccess) {
          FlushbarService().showSuccessMessage(message: 'Добавлено в корзину');
        }
      },
      builder: (context, state) {
        return InkWell(
          child: SvgPicture.asset(AppAssets.plus_icon),
          onTap: () =>
              serviceLocator<BasketCubit>().putDishToPreorder(widget.item, 1),
        );
      },
    );
  }
}
