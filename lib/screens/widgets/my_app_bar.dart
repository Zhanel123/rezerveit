import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/routes.dart';

import '../../service_locator.dart';

class MyAppBar extends StatefulWidget {
  final bool? showNotify;

  const MyAppBar({Key? key, this.showNotify}) : super(key: key);

  @override
  State<MyAppBar> createState() => _MyAppBarState();
}

class _MyAppBarState extends State<MyAppBar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 7),
      child: BlocBuilder<ProfileCubit, ProfileState>(
        builder: (context, state) {
          return Row(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SvgPicture.asset(AppAssets.app_bar_logo),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.44),
                    child: Text(
                      'Reserveat',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  SizedBox(
                    width: 7.5,
                  ),
                  [UserType.MANUFACTURER].contains(state.profile?.userType)
                      ? Container(
                          height: 17,
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(
                                color: Colors.black,
                              )),
                          child: Center(
                            child: Text(
                              'Merchant'.toUpperCase(),
                              style: text400Size9Black,
                            ),
                          ),
                        )
                      : SizedBox(),
                ],
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    if (![UserType.MANUFACTURER]
                        .contains(state.profile?.userType))
                      Row(
                        children: [
                          // InkWell(
                          //   onTap: () {
                          //     Routes.router
                          //         .navigate(Routes.chatsListScreen);
                          //   },
                          //   child: Container(
                          //     width: 30,
                          //     child: Stack(
                          //       children: [
                          //         SvgPicture.asset(
                          //             AppAssets.notification_icon),
                          //         // Positioned(
                          //         //     top: 0,
                          //         //     right: 3,
                          //         //     child: Container(
                          //         //       // padding: const EdgeInsets.all(4),
                          //         //       // decoration: BoxDecoration(
                          //         //       //   shape: BoxShape.circle,
                          //         //       //   color: Color(0xffF5f5f5),
                          //         //       // ),
                          //         //       child: Container(
                          //         //         width: 8,
                          //         //         height: 8,
                          //         //         decoration: BoxDecoration(
                          //         //           shape: BoxShape.circle,
                          //         //           color: AppColors.pinkColor,
                          //         //         ),
                          //         //       ),
                          //         //     ))
                          //       ],
                          //     ),
                          //   ),
                          // ),
                          SizedBox(
                            width: 15,
                          ),
                          InkWell(
                            onTap: () {
                              Routes.router.navigate(Routes.cartScreen);
                            },
                            child: Container(
                              width: 28,
                              child: Stack(
                                children: [
                                  SvgPicture.asset(AppAssets.basketIcon),
                                  // Positioned(
                                  //     top: -1,
                                  //     right: 4,
                                  //     child: Container(
                                  //       child: Container(
                                  //         width: 8,
                                  //         height: 8,
                                  //         decoration: BoxDecoration(
                                  //           shape: BoxShape.circle,
                                  //           color: AppColors.pinkColor,
                                  //         ),
                                  //       ),
                                  //     ))
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
