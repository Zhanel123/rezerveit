import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:home_food/core/app_colors.dart';

class PrimaryImage extends StatelessWidget {
  const PrimaryImage({
    Key? key,
    String? url,
    this.fit,
  })  : url = url ?? '',
        super(key: key);

  final String url;
  final BoxFit? fit;

  @override
  Widget build(BuildContext context) {
    if (url.isEmpty || url == '') {
      return _ImageError();
    }
    return CachedNetworkImage(
      imageUrl: url,
      fit: fit,
      errorWidget: (context, _, __) => _ImageError(),
      progressIndicatorBuilder: (context, _, progress) => Center(
        child: CircularProgressIndicator(
          value: progress.progress,
        ),
      ),
    );
  }
}

class _ImageError extends StatelessWidget {
  const _ImageError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: Icon(Icons.no_photography_outlined, color: AppColors.pinkColor,),
    );
  }
}
