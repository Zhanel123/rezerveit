import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service_locator.dart';

import '../../../core/text_styles.dart';
import '../../../routes.dart';

class OrderBox extends StatefulWidget {
  const OrderBox({Key? key, required this.ordersList}) : super(key: key);

  final OrderModel ordersList;

  @override
  State<OrderBox> createState() => _OrderBoxState();
}

class _OrderBoxState extends State<OrderBox> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CustomerOrderCubit, CustomerOrderState>(
      builder: (context, state) {
        return Container(
          height: 65,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Color(0x330000),
                offset: Offset(
                  0,
                  10,
                ),
                blurRadius: 40,
              )
            ],
          ),
          padding: EdgeInsets.only(left: 31, right: 21),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'your_order',
                    style: text500Size12Black,
                  ).tr(),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Text(
                        '${widget.ordersList.total}  ₸',
                        // '2000  ₸',
                        style: text700Size15Black,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '${widget.ordersList.positionList.length} блюдо',
                        style: text400Size12Grey,
                      )
                    ],
                  )
                ],
              ),
              Container(
                height: 40,
                child: GradientButton(
                  labelText: 'Оформить',
                  onPressed: () {
                    Routes.router.navigate(Routes.cartScreen);
                  },
                  borderRadius: BorderRadius.circular(50.0),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
