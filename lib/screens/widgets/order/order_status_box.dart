import 'package:flutter/material.dart';

import '../../../core/text_styles.dart';
import '../buttons/gradient_button.dart';

class OrderStatus extends StatefulWidget {
  const OrderStatus({Key? key}) : super(key: key);

  @override
  State<OrderStatus> createState() => _OrderStatusState();
}

class _OrderStatusState extends State<OrderStatus> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 65,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8)
      ),
      padding: EdgeInsets.only(left: 31, right: 21),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Заказ #379723',
                style: text700Size15Black,
              ),
              SizedBox(height: 5,),
              Text(
                'Досавляется',
                style: text700Size15Green,
              ),
            ],
          ),
          GradientButton(labelText: 'Подробнее')
        ],
      ),
    );
  }
}
