import 'package:flutter/material.dart';

import '../../core/text_styles.dart';
import 'buttons/checkbox_button.dart';

class CheckRow extends StatefulWidget {
  final String label;
  final bool isCheck;
  final void Function(bool) handleValue;
  final double? widthParameter;
  final TextStyle? labelStyle;
  final double? height;

  const CheckRow({
    Key? key,
    required this.label,
    this.isCheck = false,
    required this.handleValue,
    this.widthParameter,
    this.labelStyle,
    this.height,
  }) : super(key: key);

  @override
  State<CheckRow> createState() => _CheckRowState();
}

class _CheckRowState extends State<CheckRow> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.handleValue(!widget.isCheck),
      child: Container(
        height: widget.height,
        width: widget.widthParameter,
        margin: EdgeInsets.only(bottom: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CheckboxButton(
              isChecked: widget.isCheck,
              changeIsChecked: (value) => widget.handleValue(value),
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
              child: Text(
                widget.label,
                style: widget.isCheck
                    ? text700Size12Black
                    : widget.labelStyle ?? text400Size12Black,
              ),
            )
          ],
        ),
      ),
    );
  }
}
