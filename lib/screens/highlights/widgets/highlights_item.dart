import 'package:flutter/material.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../widgets/buttons/default_button.dart';

class HighlightsItem extends StatelessWidget {
  final String imageUrl;
  final String label;
  final Function() next;
  final Function() skip;

  final PageController controller;

  const HighlightsItem({
    required this.controller,
    Key? key,
    required this.imageUrl,
    required this.label,
    required this.next,
    required this.skip,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset(imageUrl),
        SizedBox(
          height: 71,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(
            '$label',
            textAlign: TextAlign.center,
            style: text400Size15Black,
          ),
        ),
        SizedBox(
          height: 35,
        ),
        Container(
          padding: EdgeInsets.only(bottom: 34, top: 24, left: 100, right: 100),
          child: SmoothPageIndicator(
            controller: controller,
            count: 3,
            effect: ScrollingDotsEffect(
              activeDotColor: AppColors.pinkColor,
              dotHeight: 4,
              dotColor: Color(0xffF5F5F5),
              radius: 50,
            ),
          ),
        ),
        SizedBox(
          height: 50,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 33.0),
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 44),
            child: DefaultButton(
              label: 'Далее',
              textColor: Color(0xff838383),
              onPressed: next,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 3),
          width: 176,
          child: TextButton(
            child: Text(
              'Пропустить',
              style: TextStyle(
                  color: AppColors.pinkColor,
                  fontSize: 15,
                  fontWeight: FontWeight.w400),
            ),
            onPressed: skip,
          ),
        )
      ],
    );
  }
}
