import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/buttons/default_button.dart';
import 'package:home_food/screens/highlights/widgets/highlights_item.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../core/app_assets.dart';
import '../../core/app_colors.dart';

class HighlightsScreen extends StatefulWidget {
  const HighlightsScreen({Key? key}) : super(key: key);

  @override
  State<HighlightsScreen> createState() => _HighlightsScreenState();
}

class _HighlightsScreenState extends State<HighlightsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 35),
        color: Color(0xffF5F5F5),
        child: ColumnWidget(),
      ),
    );
  }
}

class ColumnWidget extends StatelessWidget {
  const ColumnWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PageController _pageController = PageController();

    void skip() {
      Routes.router.navigate(Routes.registrationScreen);
    }

    void next() {
      if (_pageController.page == 3) {
        skip();
        return;
      }
      _pageController.nextPage(
          duration: Duration(milliseconds: 600), curve: Curves.linear);
    }

    return Column(
      children: [
        Expanded(
          child: PageView(
            controller: _pageController,
            children: <Widget>[
              HighlightsItem(
                label:
                    'Все рестораны твоего города всегда под рукой',
                imageUrl: AppAssets.onboarding_1,
                next: next,
                skip: skip,
                controller: _pageController,
              ),
              HighlightsItem(
                label: ' Можете заказать доставку любимых блюд',
                imageUrl: AppAssets.onboarding_2,
                next: next,
                skip: skip,
                controller: _pageController,
              ),
              HighlightsItem(
                label: 'Депозитная система бронирования столов',
                imageUrl: AppAssets.onboarding_3,
                next: next,
                skip: skip,
                controller: _pageController,
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(bottom: 34, top: 24, left: 100, right: 100),
          child: SmoothPageIndicator(
            controller: _pageController,
            count: 3,
            effect: ScrollingDotsEffect(
              activeDotColor: AppColors.pinkColor,
              dotHeight: 4,
              dotColor: Colors.white,
            ),
          ),
        )
      ],
    );
  }
}