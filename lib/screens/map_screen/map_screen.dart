import 'dart:math';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/maps_cubit/maps_cubit_cubit.dart';
import 'package:home_food/blocs/maps_cubit/maps_cubit_state.dart';
import 'package:home_food/blocs/merchant/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/merchant/merchant.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/map_screen/widgets/marker.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:home_food/utils/shared_pref_util.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key}) : super(key: key);

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  late final MapboxMapController mapController;
  LatLng latlng = sharedPreference.getLatLngFromSharedPrefs();
  late CameraPosition _initialCameraPosition;
  List<Marker> _markers = [];

  @override
  void initState() {
    super.initState();
    _initialCameraPosition = CameraPosition(target: latlng, zoom: 20);
  }

  // @override
  // void dispose() {
  //   mapController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,

        color: Color(0xffF5F5F5),
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 16.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset(AppAssets.app_bar_logo),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.44),
                          child: Text(
                            'Reserveat',
                            style: text700Size18Black,
                          ),
                        )
                      ],
                    ),
                    InkWell(
                      onTap: Routes.router.pop,
                      child: Container(
                        width: 32,
                        height: 32,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white),
                        child: Center(
                          child: Icon(
                            Icons.close,
                            size: 17,
                            color: Color(0xff7A7A7A),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: BlocConsumer<MapsCubitCubit, MapsCubitState>(
                  listener: (context, state) {
                    setState(() {
                      // _markers = state.merchants
                      //     .map((e) => Marker(
                      //         e.visitor!.id!, e.getCoordinate!, _initialPositionf, (p0) {}))
                      //     .toList();
                    });
                  },
                  builder: (context, state) {
                    return Stack(
                      children: [
                        MapboxMap(
                          accessToken:
                              const String.fromEnvironment("ACCESS_TOKEN"),
                          onMapCreated: _onMapCreated,
                          onStyleLoadedCallback: _onStyleLoadedCallback,
                          myLocationEnabled: true,
                          myLocationTrackingMode:
                              MyLocationTrackingMode.TrackingGPS,
                          // minMaxZoomPreference: MinMaxZoomPreference(14, 40),
                          initialCameraPosition: CameraPosition(
                            target: LatLng(0, 0),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onMapCreated(MapboxMapController controller) {
    setState(() {
      mapController = controller;
    });

    controller.addListener(() {
      if (controller.isCameraMoving) {
        _updateMarkerPosition();
      }
    });
    mapController.onFeatureTapped.add(_onFeatureTap);
  }

  void _onFeatureTap(featureId, Point<double> point, LatLng latLng) {
    logger.i(mapController.symbolManager?.byId(featureId)?.data);

    Merchant? merchant = mapController.symbolManager
        ?.byId(featureId)
        ?.data?['merchant'] as Merchant?;

    if (merchant != null)
      showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => Column(
          children: [
            Container(
              height: 60,
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 16),
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  for (var address in merchant.addressList)
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Text(
                            address.fullAddressName,
                            style: text700Size18Black,
                          ),
                          Text(
                              '${calculateDistanceFromMyLocation(address.latitude, address.longitude).toStringAsFixed(2)} km')
                        ],
                      ),
                    )
                ],
              ),
            ),
            Expanded(
              child: PrimaryBottomSheet(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 24, horizontal: 16),
                child: Column(
                  children: [],
                ),
              ),
            ),
          ],
        ),
      );
    // final snackBar = SnackBar(
    //   content: Text(
    //     'Tapped feature with id $featureId, point $point, LatLng $latLng',
    //     style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
    //   ),
    //   backgroundColor: Theme.of(context).primaryColor,
    // );
    // ScaffoldMessenger.of(context).clearSnackBars();
    // ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _updateMarkerPosition() {
    final coordinates = <LatLng>[];

    final MerchantCubit _merchantCubit = serviceLocator<MerchantCubit>();

    for (final markerState in _merchantCubit.state.merchants) {
      // if (markerState.getCoordinate != null) {
      //   coordinates.add(markerState.getCoordinate!);
      // }
    }

    // mapController.toScreenLocationBatch(coordinates).then((points) {
    //   _markerStates.asMap().forEach((i, value) {
    //     _markerStates[i].updatePosition(points[i]);
    //   });
    // });
  }

  Future<void> _onStyleLoadedCallback() async {
    final MerchantCubit _merchantCubit = serviceLocator<MerchantCubit>();

    final ByteData bytes =
        await rootBundle.load("assets/images/merchant_default_icon.png");
    final Uint8List list = bytes.buffer.asUint8List();
    await mapController.addImage("assetImage", list);
    for (final markerState in _merchantCubit.state.merchants) {
      if (markerState.getCoordinate != null) {
        await mapController.addSymbol(
            SymbolOptions(
              geometry: LatLng(markerState.getCoordinate!.latitude,
                  markerState.getCoordinate!.longitude),
              iconSize: 1,
              iconImage: 'assetImage',
            ),
            {
              'merchant': markerState,
            });
      }
    }
  }

  double calculateDistanceFromMyLocation(
    double? lat2,
    double? lon2,
  ) {
    if (lat2 == null || lon2 == null) {
      return 0;
    }
    LatLng latLng = sharedPreference.getLatLngFromSharedPrefs();
    return calculateDistance(
      latLng.latitude,
      latLng.longitude,
      lat2,
      lon2,
    );
  }

  double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
    const double earthRadius = 6371; // Radius of the earth in kilometers

    // Convert latitude and longitude values from degrees to radians
    double lat1Rad = degreesToRadians(lat1);
    double lon1Rad = degreesToRadians(lon1);
    double lat2Rad = degreesToRadians(lat2);
    double lon2Rad = degreesToRadians(lon2);

    // Calculate the differences between coordinates
    double dLat = lat2Rad - lat1Rad;
    double dLon = lon2Rad - lon1Rad;

    // Apply the Haversine formula
    double a = sin(dLat / 2) * sin(dLat / 2) +
        cos(lat1Rad) * cos(lat2Rad) * sin(dLon / 2) * sin(dLon / 2);
    double c = 2 * asin(sqrt(a));
    double distance = earthRadius * c;

    return distance;
  }

  double degreesToRadians(double degrees) {
    return degrees * (pi / 180);
  }
}
