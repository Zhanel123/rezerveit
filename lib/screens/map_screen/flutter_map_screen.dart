import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:home_food/blocs/maps_cubit/maps_cubit_cubit.dart';
import 'package:home_food/blocs/maps_cubit/maps_cubit_cubit.dart';
import 'package:home_food/blocs/maps_cubit/maps_cubit_state.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:latlong2/latlong.dart';

final LatLng _myLocation = LatLng(43.25, 77.4);

class MapBoxMapsScreen extends StatelessWidget {
  const MapBoxMapsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MapsCubitCubit, MapsCubitState>(
      builder: (context, state) {
        return FlutterMap(
          options: MapOptions(
            minZoom: 5,
            // maxZoom: 16,
            center: _myLocation,
            zoom: 13,
          ),
          nonRotatedChildren: [
            TileLayer(
              urlTemplate:
                  'https://api.mapbox.com/styles/v1/vandervaiz/{id}/tiles/256/{z}/{x}/{y}@2x?access_token={accessToken}',
              additionalOptions: {
                'accessToken': const String.fromEnvironment("ACCESS_TOKEN"),
                'id': 'clg0kqrt0002s01o31xersir2',
              },
            ),
            MarkerLayer(
              markers: [
                for (int i = 0; i < state.merchants.length; i++)
                  if (state.merchants[i].getCoordinate != null)
                    Marker(
                        point: state.merchants[i].getCoordinate!,
                        builder: (_) {
                          return GestureDetector(
                            onTap: () {},
                            child: Icon(Icons.home),
                          );
                        })
              ],
            ),
            MarkerLayer(
              markers: [
                Marker(
                    point: _myLocation,
                    builder: (_) {
                      return _MyLocationMarker();
                    })
              ],
            )
          ],
        );
      },
    );
  }
}

class _MyLocationMarker extends StatelessWidget {
  const _MyLocationMarker({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
        color: AppColors.primary,
        shape: BoxShape.circle,
      ),
    );
  }
}
