import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/blocs/customer_refund_order/cubit.dart';
import 'package:home_food/blocs/order_detail/cubit.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/screens/order/widgets/refund_success_screen.dart';
import 'package:home_food/screens/widgets/add_image_card.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/add_logo.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:injectable/injectable.dart';
import 'package:injectable/injectable.dart';
import 'package:injectable/injectable.dart';

import '../../../core/app_assets.dart';
import '../../../core/text_styles.dart';
import '../../../routes.dart';
import '../../widgets/buttons/gradient_button.dart';
import '../../widgets/rounded_text_field.dart';

class ComplainToOrderScreen extends StatefulWidget {
  final OrderModel order;

  const ComplainToOrderScreen({
    Key? key,
    required this.order,
  }) : super(key: key);

  @override
  State<ComplainToOrderScreen> createState() => _ComplainToOrderScreenState();
}

class _ComplainToOrderScreenState extends State<ComplainToOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RefundOrderCubit, RefundOrderState>(
      listener: (context, state) async {
        if (state.refundStatus.isSubmissionSuccess) {
          Navigator.of(context).pop();
          FlushbarService().showSuccessMessage(
            message: 'order_canceled'.tr(),
          );

          await serviceLocator<CustomerOrderCubit>().getCustomerOrders();
          await serviceLocator<OrderDetailCubit>().getOrderById();
        } else if (state.refundStatus.isSubmissionFailure) {
          Navigator.of(context).pop();
          FlushbarService().showErrorMessage(
            context: context,
            message: state.error,
          );
        }
      },
      listenWhen: (p, c) => p.refundStatus != c.refundStatus,
      builder: (context, state) {
        return PrimaryBottomSheet(
          contentPadding:
              EdgeInsets.only(bottom: 18, right: 18, left: 18, top: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Routes.router.pop();
                  },
                  child: Container(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        color: Color(0xffEAEAEA),
                        borderRadius: BorderRadius.circular(28)),
                    child: Icon(
                      Icons.close,
                      color: Color(0xff7A7A7A),
                      size: 15,
                    ),
                  )),
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        'Пожаловаться на заказ',
                        textAlign: TextAlign.center,
                        style: text700Size15Black,
                      ),
                      width: 130,
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: EdgeInsets.only(top: 0, left: 8, right: 8, bottom: 10),
                      child: RoundedTextFormField(
                        value: state.comment.value,
                        hasError:
                            state.comment.status == FormzInputStatus.invalid,
                        errorText: state.comment.error != null
                            ? 'Обязательное поле'
                            : null,
                        onChange: (value) => serviceLocator<RefundOrderCubit>()
                            .changeComment(value),
                        hintText: 'Напишите, что пошло не так...',
                        keyboardType: TextInputType.text,
                        minLines: 3,
                        maxLines: 6,
                        height: 84,
                      ),
                    ),
                    Container(
                      height: 100,
                      padding: EdgeInsets.only(left: 21, right: 21),
                      child: AddImageButton(
                        index: 0,
                        localFile: state.uploadedImage.value,
                        onSelectImage: serviceLocator<RefundOrderCubit>()
                            .handleImageSelected,
                        hintWidget: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Прикрепить фото',
                              style: text400Size15Pink,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            SvgPicture.asset(AppAssets.upload_photo_icon)
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: 18,
                        left: 21,
                        right: 21,
                        bottom: 18,
                      ),
                      child: GradientButton(
                        labelText: 'Запрос на возврат',
                        isLoading: state.refundStatus.isSubmissionInProgress,
                        borderRadius: BorderRadius.circular(50),
                        onPressed: () => serviceLocator<RefundOrderCubit>()
                            .handleCustomerRefundOrder(
                          order: widget.order,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
