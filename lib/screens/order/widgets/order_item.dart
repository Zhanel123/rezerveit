import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/order_position/order_position.dart';

import '../../../core/text_styles.dart';

class OrderItem extends StatelessWidget {
  final OrderPosition orderDish;

  const OrderItem({
    Key? key,
    required this.orderDish,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 3.0, left: 16, right: 16),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.only(left: 15, top: 13, bottom: 15, right: 17),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                _FoodThumbImage(
                  images: orderDish.dishCardItem.images,
                ),
                SizedBox(
                  width: 15,
                ),
                Container(
                  width: 133,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        orderDish.dishCardItem.name,
                        textAlign: TextAlign.left,
                        style: text400Size12Black,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      RichText(
                        text: TextSpan(
                          text: NumUtils.humanizeNumber(
                              orderDish.dishCardItem.price),
                          style: text700Size15Black,
                          children: [
                            TextSpan(
                                text:
                                    '  ${NumUtils.humanizeNumber(orderDish.dishCardItem.weight)} г',
                                style: text400Size12Grey),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            Container(
              width: 46,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                border: Border.all(
                  color: Color(0xffCFCFCF),
                  //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: Center(
                child: Text(
                  '${NumUtils.humanizeNumber(orderDish.amount)}',
                  style: text400Size13Black,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _FoodThumbImage extends StatelessWidget {
  const _FoodThumbImage({
    Key? key,
    this.images = const [],
  }) : super(key: key);

  final List<UploadedImage> images;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 53,
      width: 53,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
      ),
      clipBehavior: Clip.hardEdge,
      child: images.isEmpty
          ? Container()
          : CachedNetworkImage(
              imageUrl: images.first.imageUrl,
              fit: BoxFit.cover,
            ),
    );
  }
}
