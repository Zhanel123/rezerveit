import 'package:flutter/material.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/order/order.dart';

import '../complain_to_order_screen.dart';

class ComplainToOrderButton extends StatefulWidget {
  final OrderModel order;

  const ComplainToOrderButton({
    Key? key,
    required this.order,
  }) : super(key: key);

  @override
  State<ComplainToOrderButton> createState() => _ComplainToOrderButtonState();
}

class _ComplainToOrderButtonState extends State<ComplainToOrderButton> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {
          handleRefundClicked();
        },
        child: Text(
          'Пожаловаться на заказ',
          style: text400Size15Pink,
        ));
  }

  void handleRefundClicked() {
    showModalBottomSheet(
      context: context,
      isDismissible: true,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return ComplainToOrderScreen(
          order: widget.order,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }
}
