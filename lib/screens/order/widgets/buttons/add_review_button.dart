import 'package:flutter/material.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/text_styles.dart';
import '../add_review_screen.dart';

class AddReviewButton extends StatefulWidget {
  final OrderModel order;

  const AddReviewButton({
    Key? key,
    required this.order,
  }) : super(key: key);

  @override
  State<AddReviewButton> createState() => _AddReviewButtonState();
}

class _AddReviewButtonState extends State<AddReviewButton> {
  @override
  Widget build(BuildContext context) {
    return GradientButton(
      labelText: 'Добавить отзыв',
      onPressed: () {
        handleCreateReviewClicked();
      },
    );
  }

  void handleCreateReviewClicked() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return AddReviewScreen(
          order: widget.order,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }
}
