import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/rate_cubit/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

import '../../../routes.dart';
import '../../widgets/rounded_text_field.dart';
import 'buttons/complain_to_order_button.dart';

class RateSuccessScreen extends StatefulWidget {
  const RateSuccessScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<RateSuccessScreen> createState() => _RateSuccessScreenState();
}

class _RateSuccessScreenState extends State<RateSuccessScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RateOrderCubit, RateCubitState>(
      builder: (context, state) {
        return PrimaryBottomSheet(
          contentPadding:
              EdgeInsets.only(bottom: 18, right: 18, left: 18, top: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Routes.router.pop();
                  },
                  child: Container(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        color: Color(0xffEAEAEA),
                        borderRadius: BorderRadius.circular(28)),
                    child: Icon(
                      Icons.close,
                      color: Color(0xff7A7A7A),
                      size: 15,
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.only(
                  top: 24.0,
                  bottom: 80,
                ),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Text(
                          'Спасибо за отзыв к заказу',
                          textAlign: TextAlign.center,
                          style: text700Size15Black,
                        ),
                        width: 150,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: RatingBar.builder(
                          initialRating: (state.grade.value ?? 0).toDouble(),
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: false,
                          itemCount: 5,
                          itemSize: 37,
                          ignoreGestures: true,
                          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (rating) {},
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: GradientButton(
                          labelText: 'Окей',
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
