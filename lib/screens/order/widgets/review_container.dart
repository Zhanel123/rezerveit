import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/models/grade/grade.dart';

import '../../../core/text_styles.dart';

class ReviewContainer extends StatelessWidget {
  const ReviewContainer({
    Key? key,
    required this.grade,
  }) : super(key: key);

  final Grade grade;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
      width: double.infinity,
      padding: EdgeInsets.only(left: 20, right: 25, top: 13, bottom: 13),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            grade.comment,
            style: text400Size12Black,
            textAlign: TextAlign.left,
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              RatingBar.builder(
                initialRating: (grade.rating ?? 0).toDouble(),
                minRating: 1,
                direction: Axis.horizontal,
                ignoreGestures: true,
                itemSize: 18,
                allowHalfRating: false,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {},
              ),
              SizedBox(
                width: 8,
              ),
              Text(
                '${grade.rating?.toDouble() ?? 0}',
                style: text400Size13Black,
              ),
            ],
          )
        ],
      ),
    );
  }
}
