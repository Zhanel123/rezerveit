import 'package:flutter/material.dart';
import 'package:home_food/core/text_styles.dart';

class TextContainer extends StatefulWidget {
  final String firstText;
  final TextStyle? firstStyle;
  final String? secondText;
  final TextStyle? secondStyle;
  final bool? preOrder;

  const TextContainer(
      {Key? key,
      required this.firstText,
      this.secondText,
      this.firstStyle,
      this.secondStyle,
      this.preOrder})
      : super(key: key);

  @override
  State<TextContainer> createState() => _TextContainerState();
}

class _TextContainerState extends State<TextContainer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 3),
      child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(top: 12, bottom: 11, left: 19, right: 23),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.75),
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
              color: Color(0xffD8D8D8), //                   <--- border color
              width: 1.0,
            ),
          ),
          child: widget.secondText != null
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      widget.firstText,
                      style: widget.firstStyle ?? text700Size12Black,
                    ),
                    Text(
                      widget.secondText!,
                      style: widget.secondStyle ?? text700Size15Black,
                    )
                  ],
                )
              : widget.preOrder == true
                  ? Row(
                      children: [
                        Text(
                          widget.firstText,
                          style: widget.firstStyle ?? text700Size12Black,
                        ),
                        SizedBox(width: 15,),
                        Text(
                          'мин. за 2 дня',
                          style: text400Size13Pink,
                        )
                      ],
                    )
                  : Text(
                      widget.firstText,
                      textAlign: TextAlign.center,
                    )),
    );
  }
}
