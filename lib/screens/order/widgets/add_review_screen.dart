import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/blocs/order_detail/cubit.dart';
import 'package:home_food/blocs/rate_cubit/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/screens/order/order_screen.dart';
import 'package:home_food/screens/order/widgets/rate_success_screen.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:seafarer/seafarer.dart';

import '../../../routes.dart';
import '../../widgets/rounded_text_field.dart';
import 'buttons/complain_to_order_button.dart';

class AddReviewScreen extends StatefulWidget {
  final OrderModel order;

  const AddReviewScreen({
    Key? key,
    required this.order,
  }) : super(key: key);

  @override
  State<AddReviewScreen> createState() => _AddReviewScreenState();
}

class _AddReviewScreenState extends State<AddReviewScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RateOrderCubit, RateCubitState>(
      listenWhen: (p, c) => p.gradeStatus != c.gradeStatus,
      listener: (context, state) async {
        if (state.gradeStatus.isSubmissionSuccess) {
          Navigator.of(context).pop();
          await showModalBottomSheet(
            context: context,
            isDismissible: true,
            isScrollControlled: true,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return RateSuccessScreen();
            },
          );

          await serviceLocator<CustomerOrderCubit>().getCustomerOrders();
          await serviceLocator<OrderDetailCubit>().getOrderById();
        } else if (state.gradeStatus.isSubmissionFailure) {
          Navigator.of(context).pop();
          FlushbarService().showErrorMessage(
            context: context,
            message: state.error,
          );
        }
      },
      builder: (context, state) {
        return PrimaryBottomSheet(
          contentPadding: const EdgeInsets.only(
            bottom: 18,
            right: 18,
            left: 18,
            top: 22,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        color: Color(0xffEAEAEA),
                        borderRadius: BorderRadius.circular(28)),
                    child: Icon(
                      Icons.close,
                      color: Color(0xff7A7A7A),
                      size: 15,
                    ),
                  )),
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        'Оставить отзыв к заказу',
                        textAlign: TextAlign.center,
                        style: text700Size15Black,
                      ),
                      width: 130,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: RatingBar.builder(
                        initialRating: (state.grade.value ?? 0).toDouble(),
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: false,
                        itemCount: 5,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) =>
                            serviceLocator<RateOrderCubit>()
                                .changeGrade(rating.toInt()),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 23, left: 21, right: 21),
                      child: RoundedTextFormField(
                        value: state.comment.value,
                        hasError:
                            state.comment.status == FormzInputStatus.invalid,
                        errorText: state.comment.error != null
                            ? 'Обязательное поле'
                            : null,
                        onChange: (value) => serviceLocator<RateOrderCubit>()
                            .changeComment(value),
                        hintText: 'При желании оставьте отзыв...',
                        keyboardType: TextInputType.text,
                        minLines: 3,
                        maxLines: 6,
                        height: 84,
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 23, left: 50, right: 50),
                        child: GradientButton(
                          borderRadius: BorderRadius.circular(50),
                          labelText: 'Добавить',
                          onPressed: () =>
                              serviceLocator<RateOrderCubit>().handleOrderRate(
                            order: widget.order,
                          ),
                        )),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20, top: 16),
                      child: InkWell(
                        child: Text(
                          'Пожаловаться',
                          style: TextStyle(
                              color: AppColors.pinkColor,
                              fontSize: 15,
                              fontWeight: FontWeight.w400),
                        ),
                        onTap: () {},
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
      padding: EdgeInsets.only(bottom: 18, right: 18, left: 18, top: 22),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          InkWell(
              onTap: () {
                Routes.router.pop();
              },
              child: Container(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                    color: Color(0xffEAEAEA),
                    borderRadius: BorderRadius.circular(28)),
                child: Icon(
                  Icons.close,
                  color: Color(0xff7A7A7A),
                  size: 15,
                ),
              )),
          Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      'Спасибо за отзыв к заказу',
                      textAlign: TextAlign.center,
                      style: text700Size15Black,
                    ),
                    width: 150,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: SvgPicture.asset(
                            AppAssets.star_icon,
                            color: Color(0xffF1C731),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: SvgPicture.asset(
                            AppAssets.star_icon,
                            color: Color(0xffF1C731),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: SvgPicture.asset(
                            AppAssets.star_icon,
                            color: Color(0xffF1C731),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: SvgPicture.asset(
                            AppAssets.star_icon,
                            color: Color(0xffF1C731),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: SvgPicture.asset(
                            AppAssets.star_icon,
                            color: Color(0xffF1C731),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: GradientButton(
                      labelText: 'Окей',
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
