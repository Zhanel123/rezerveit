import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/chat/cubit.dart';
import 'package:home_food/blocs/order_detail/cubit.dart';
import 'package:home_food/blocs/order_detail/cubit.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/order/widgets/buttons/add_review_button.dart';
import 'package:home_food/screens/order/widgets/add_review_screen.dart';
import 'package:home_food/screens/order/widgets/buttons/complain_to_order_button.dart';
import 'package:home_food/screens/order/widgets/customer_cancel_order_screen.dart';
import 'package:home_food/screens/order/widgets/order_item.dart';
import 'package:home_food/screens/order/widgets/review_container.dart';
import 'package:home_food/screens/order/widgets/text_container.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service_locator.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:seafarer/seafarer.dart';

import '../../core/app_assets.dart';
import '../../core/text_styles.dart';

class OrderScreenArgs extends BaseArguments {
  final OrderModel order;

  OrderScreenArgs(
    this.order,
  );
}

class OrderScreen extends StatefulWidget {
  const OrderScreen({
    Key? key,
    required this.args,
  }) : super(key: key);

  final OrderScreenArgs args;

  @override
  State<OrderScreen> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  @override
  void initState() {
    serviceLocator<OrderDetailCubit>().handleOrderChanged(widget.args.order);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        color: Color(0xffF5F5F5),
        child: BlocConsumer<OrderDetailCubit, OrderDetailState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            if (state.status.isSubmissionInProgress) {
              return Center(
                child: CircularProgressIndicator(
                  color: AppColors.pinkColor,
                ),
              );
            }
            return SingleChildScrollView(
              padding: EdgeInsets.only(top: 48),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            SvgPicture.asset(AppAssets.app_bar_logo),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.44),
                              child:
                                  Text('Reserveat', style: text700Size18Black),
                            )
                          ],
                        ),
                        InkWell(
                          onTap: () {
                            Routes.router.pop();
                          },
                          child: Container(
                            width: 32,
                            height: 32,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white),
                            child: Center(
                              child: Icon(
                                Icons.close,
                                size: 17,
                                color: Color(0xff7A7A7A),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 34),
                    child: Text(
                      'Заказ ${widget.args.order.status.label}',
                      style: text700Size18Black,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: 16,
                      right: 16,
                      top: 5,
                      bottom: 8,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: '#${widget.args.order.orderNumber}',
                            style: text700Size15Black,
                            children: [
                              TextSpan(
                                text:
                                    '  От ${widget.args.order.timeOfDate == DateTime(0) ? '' : _dateTimeFormatter(widget.args.order.timeOfDate)}',
                                style: text400Size13Grey,
                              ),
                            ],
                          ),
                        ),
                        if (state.order!.status != OrderStatusEnum.CANCELED)
                          TextButton(
                            onPressed: () {
                              handleCancelOrder();
                            },
                            child: Text(
                              'Отменить',
                              style: text400Size13Pink,
                            ),
                          )
                      ],
                    ),
                  ),
                  ...state.order!.positionList.map(
                    (e) => OrderItem(
                      orderDish: e,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 3),
                    child: Container(
                      decoration: BoxDecoration(color: Color(0xffD8D8D8)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: TextContainer(
                      firstText: 'Итого оплачено',
                      secondText: NumUtils.humanizeNumber(
                        state.order!.total,
                        isCurrency: true,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: 15, top: 3, left: 16, right: 16),
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 13, bottom: 11, left: 19, right: 23),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.75),
                          borderRadius: BorderRadius.circular(8)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Статус заказа',
                            textAlign: TextAlign.start,
                            style: text700Size12Black,
                          ),
                          SizedBox(
                            height: 7,
                          ),
                          SizedBox(
                            height: 63,
                            child: Row(
                              children: mapOrderStatus(state.order!),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // if (state.order!.status != OrderStatusEnum.DELIVERED)
                  //   Padding(
                  //     padding: EdgeInsets.symmetric(horizontal: 16),
                  //     child: TextContainer(
                  //       firstText: 'Показать на карте',
                  //       firstStyle: text700Size12Black,
                  //     ),
                  //   ),
                  SizedBox(
                    height: 16,
                  ),
                  if (state.order!.status != OrderStatusEnum.DELIVERED)
                    GestureDetector(
                      onTap: () =>
                          serviceLocator<ChatCubit>().createChat(state.order!),
                      child: Container(
                        padding:
                            EdgeInsets.only(left: 16, right: 16, bottom: 16),
                        child: Center(
                          child: Text(
                            'Связаться с продавцом ',
                            style: text400Size13Pink,
                          ),
                        ),
                      ),
                    ),
                  if (state.order!.grade != null)
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Добавлен отзыв',
                                style: text700Size15Black,
                              ),
                              TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    'Править',
                                    style: text400Size13Pink,
                                  ))
                            ],
                          ),
                          ReviewContainer(
                            grade: state.order!.grade!,
                          ),
                        ],
                      ),
                    ),
                  SizedBox(
                    height: 16,
                  ),
                  if (state.order!.status == OrderStatusEnum.DELIVERED &&
                      state.order!.grade == null)
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Center(
                        child: AddReviewButton(
                          order: state.order!,
                        ),
                      ),
                    ),
                  if (state.order!.status == OrderStatusEnum.DELIVERED)
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Center(
                        child: ComplainToOrderButton(
                          order: state.order!,
                        ),
                      ),
                    ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  void handleCreateReviewClicked(OrderModel order) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return AddReviewScreen(
          order: order,
        );
      },
      isDismissible: true,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
  }

  mapOrderStatus(
    OrderModel order,
  ) {
    final currentStatus = order.status;
    final List<Widget> children = [];
    final values = [
      OrderStatusEnum.ORDERED,
      OrderStatusEnum.ORDER_ACCEPTED,
      OrderStatusEnum.PREPARATION,
      OrderStatusEnum.DELIVERY,
      OrderStatusEnum.DELIVERED,
    ];

    final dateValues = [
      order.timeOfDate,
      order.acceptedOrderTime,
      order.preparationStartTime,
      order.deliveryStartTime,
      order.deliveredOrderTime,
    ];

    for (int i = 0; i < values.length; i++) {
      bool isActive = values[i] == currentStatus;
      int activeIndex =
          values.indexWhere((element) => element == currentStatus);
      children.add(Expanded(
        child: Stack(
          children: [
            Center(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      height: 5,
                      color: i == activeIndex || i <= activeIndex
                          ? Colors.green
                          : Colors.white,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      height: 5,
                      color: i < activeIndex ||
                              (i == values.length - 1 && i == activeIndex)
                          ? Colors.green
                          : Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Positioned.fill(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: i == 0
                    ? CrossAxisAlignment.start
                    : i == values.length - 1
                        ? CrossAxisAlignment.end
                        : CrossAxisAlignment.center,
                children: [
                  Text(
                    values[i].label ?? '',
                    style: isActive ? text700Size11Success : text400Size11Black,
                  ),
                  isActive
                      ? Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xFF37B023),
                              width: 5,
                            ),
                          ),
                        )
                      : Container(
                          width: 13,
                          height: 13,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(
                              color: Colors.black,
                              width: 3.25,
                            ),
                          ),
                        ),
                  dateValues[i] != null
                      ? Text('${dateValues[i]?.hour}:${dateValues[i]?.minute}')
                      : SizedBox(
                          height: 10,
                        ),
                ],
              ),
            ),
          ],
        ),
      ));
    }

    return children;
  }

  String _dateTimeFormatter(DateTime dateTime) {
    String formattedDate = DateFormat('yyyy.MM.dd kk:mm').format(dateTime);
    return formattedDate;
  }

  void handleCancelOrder() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return CustomerCancelOrderScreen(
          order: widget.args.order,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    );
  }
}
