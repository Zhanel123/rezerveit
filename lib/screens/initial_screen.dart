import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/notification_module/notifcation_module.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/login/login_screen.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/shared_pref_util.dart';
import 'package:seafarer/seafarer.dart';

import 'registration/registration_screen.dart';

class InitialScreen extends StatefulWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  State<InitialScreen> createState() => _InitialScreenState();
}

class _InitialScreenState extends State<InitialScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_prepareAppServices);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileCubit, ProfileState>(
      listenWhen: (p, c) => p.isLogging != c.isLogging,
      listener: (context, state) {
        if (state.isLogging.isSubmissionSuccess) {
          Routes.router.navigate(
            Routes.homeScreen,
            navigationType: NavigationType.pushReplace,
          );
        } else if (state.isLogging.isSubmissionFailure) {
          Routes.router.navigate(
            Routes.loginScreen,
            navigationType: NavigationType.pushReplace,
          );
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
            width: double.infinity,
            color: Color(0xffF5F5F5),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(AppAssets.main_logo),
                  SizedBox(
                    height: 27,
                  ),
                  Text(
                    'Reserveat',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 26),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    width: 117,
                    child: LinearProgressIndicator(
                      color: AppColors.pinkColor,
                      backgroundColor: Colors.white,
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _handleIndicatorFinished() {
    Scaffold.of(context).showBottomSheet<void>(
      (BuildContext context) {
        return LoginScreen();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.4,
        maxHeight: MediaQuery.of(context).size.height * 0.6,
      ),
    );
    print('object');
  }

  Future<void> _prepareAppServices(Duration timeStamp) async {
    await serviceLocator.allReady();
    serviceLocator<ProfileCubit>().isAuthenticated();
    serviceLocator<NewAddressCubit>().fetchCities();

    NotificationListenerProvider.getMessage();
    NotificationListenerProvider.checkForInitialMessage();
    await Future.delayed(Duration(seconds: 3));
    if (sharedPreference.showOnBoarding) {
      Routes.router.navigate(Routes.highlightsScreen);
      sharedPreference.setShowOnBoarding(false);
      return;
    }
  }
}
