import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/dish_catalog/cubit.dart';
import 'package:home_food/blocs/dish_filter/cubit.dart';
import 'package:home_food/blocs/viewed_history/cubit.dart';
import 'package:home_food/blocs/viewed_history/state.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/buttons/show_on_map_button.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/footer.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/primary_badge.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/home_slider.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/popular_foods_list.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/providers_list.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/recently_watched.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/type_of_food_slider.dart';
import 'package:home_food/screens/main_screen/main_screen.dart';
import 'package:home_food/screens/widgets/bottom_sheet/my_pref_setting.dart';
import 'package:home_food/screens/widgets/bottom_sheet/search_filter.dart';
import 'package:home_food/screens/widgets/my_app_bar.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

import '../../../core/app_colors.dart';
import '../../../core/text_styles.dart';
import 'widgets/select_radius.dart';

class ClientHomeScreen extends StatefulWidget {
  const ClientHomeScreen({Key? key}) : super(key: key);

  @override
  State<ClientHomeScreen> createState() => _ClientHomeScreenState();
}

class _ClientHomeScreenState extends State<ClientHomeScreen> {
  @override
  void initState() {
    super.initState();
    serviceLocator<DictionaryCubit>().getTagsList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffF5F5F5),
      child: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.only(top: 18),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: MyAppBar(),
              ),
              SizedBox(
                height: 13,
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: _SearchField(),
                      ),
                      _FilterButton()
                    ],
                  )),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: SizedBox(
                  height: 200,
                  child: ShopProvidersList(),
                ),
              ),
              SizedBox(
                height: 34,
              ),
              PopularFoodsList(),
              SizedBox(
                height: 30,
              ),
              BlocBuilder<ViewedHistoryCubit, ViewedHistoryState>(
                builder: (context, state) {
                  return state.viewedHistory.isNotEmpty
                      ? Padding(
                          padding: EdgeInsets.only(left: 16),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Недавно просмотренные',
                                style: text700Size18Black,
                                textAlign: TextAlign.left,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              ResentlyWatched(),
                            ],
                          ),
                        )
                      : SizedBox();
                },
              ),
              Footer()
            ],
          ),
        ),
      ),
    );
  }
}

class _SearchField extends StatelessWidget {
  const _SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishCatalogCubit, DishCatalogState>(
      builder: (context, state) {
        return RoundedTextFormField(
          hintText: 'Поиск по еде',
          value: state.searchText,
          onTap: () {
            MainScreen.of(context)?.changeTab(
              1,
              true,
            );
          },
          onChange: serviceLocator<DishCatalogCubit>().handleSearchChanged,
        );
      },
    );
  }
}

class _FilterButton extends StatelessWidget {
  const _FilterButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _handleFilterClicked(context, () => onSubmit(context));
        // MainScreen.of(context)?.changeTab(
        //   1,
        //   true,
        // );

        /// Добавить открытие фильтра сразу после нажатия
        /// не знаю
      },
      child: Container(
        margin: EdgeInsets.only(left: 4),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        height: 40,
        width: 40,
        child: Center(
          child: SvgPicture.asset(AppAssets.filter_logo),
        ),
      ),
    );
  }

  void onSubmit(BuildContext context) {
    MainScreen.of(context)?.changeTab(
      1,
      true,
    );
    Routes.router.pop();
    serviceLocator<DishFilterCubit>().saveFilterChanges();
  }

  void _handleFilterClicked(BuildContext context, Function() onSubmit) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return SearchFilter(
          onSubmit: onSubmit,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.4,
        maxHeight: MediaQuery.of(context).size.height,
      ),
    );
  }
}

class _MyPreferencesButton extends StatelessWidget {
  const _MyPreferencesButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _handlePreferencesClicked(
          context,
          () => onSubmitPreferences(context),
        );
      },
      child: Container(
        padding: EdgeInsets.only(left: 8, right: 9, top: 5, bottom: 7.71),
        margin: EdgeInsets.only(right: 6),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(28),
          color: AppColors.pinkColor,
        ),
        width: 30,
        height: 30,
        child: SvgPicture.asset(AppAssets.flame_icon),
      ),
    );
  }

  void onSubmitPreferences(BuildContext context) {
    MainScreen.of(context)?.changeTab(
      1,
      true,
    );
    Routes.router.pop();
    serviceLocator<DishFilterCubit>().saveFilterChanges();
  }

  void _handlePreferencesClicked(BuildContext context, Function() onSubmit) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return MyPrefSetting(
            // onSubmit: onSubmit,
            );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
      constraints: BoxConstraints(
        minHeight: MediaQuery.of(context).size.height * 0.4,
        maxHeight: MediaQuery.of(context).size.height,
      ),
    );
  }
}
