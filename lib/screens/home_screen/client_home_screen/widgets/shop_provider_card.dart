import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/models/merchant/merchant.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/merchants_menu/merchant_menu.dart';
import 'package:home_food/screens/widgets/primary_image.dart';

import '../../../../core/text_styles.dart';

class ShopProviderCard extends StatelessWidget {
  final Merchant merchant;

  const ShopProviderCard({
    Key? key,
    required this.merchant,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 9),
      child: Container(
        width: 200,
        height: 200,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        child: Column(
          children: [
            Container(
              width: 200,
              height: 160,
              child: PrimaryImage(
                url: merchant.visitor?.photo?.imageUrl ?? '',
                fit: BoxFit.cover,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 13.0, right: 9.6),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(merchant.visitor?.companyName ?? '',
                            style: text700Size12Black),
                        SizedBox(
                          width: 8,
                        ),
                        SvgPicture.asset(AppAssets.star),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          merchant.visitor?.rating.toString() ?? '',
                          style: text400Size13Black,
                        ),
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        Routes.router.navigate(
                          Routes.merchantMenu,
                          args: MerchantMenuArgs(
                            merchant: merchant.visitor!,
                            addressList: merchant.addressList,
                          ),
                        );
                      },
                      child: Container(
                        child: SvgPicture.asset(AppAssets.arrow_right),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
