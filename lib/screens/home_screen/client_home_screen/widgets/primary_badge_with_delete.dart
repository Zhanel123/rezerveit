import 'package:flutter/material.dart';

class PrimaryBadgeWithDelete extends StatelessWidget {
  final String label;
  final Function()? onTap;

  const PrimaryBadgeWithDelete({
    Key? key,
    required this.label,
     this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        color: Color(0xffD3CCDD),
      ),
      child: Row(
        children: [
          InkWell(
            child: Icon(
              Icons.close_rounded,
              size: 12,
            ),
            onTap: onTap,
          ),
          SizedBox(
            width: 9,
          ),
          Text(
            '$label',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Color(0xff393939),
                fontSize: 13,
                fontWeight: FontWeight.w400),
          ),
        ],
      ),
    );
  }
}
