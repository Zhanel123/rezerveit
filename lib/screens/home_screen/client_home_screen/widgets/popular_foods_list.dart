import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/popular_dishes/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/models/food/food.dart';
import 'package:home_food/service_locator.dart';

import '../../../../core/text_styles.dart';
import 'buttons/show_more_button.dart';
import 'food_card.dart';

class PopularFoodsList extends StatefulWidget {
  const PopularFoodsList({Key? key}) : super(key: key);

  @override
  State<PopularFoodsList> createState() => _PopularFoodsListState();
}

class _PopularFoodsListState extends State<PopularFoodsList> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    serviceLocator<PopularDishesCubit>().getPopularDishes(append: false);

    scrollController.addListener(scroollListener);
    super.initState();
  }

  void scroollListener() {
    // когда доходим до 70%
    var triggerFetchMoreSize = 0.7 * scrollController.position.maxScrollExtent;

    if (scrollController.position.pixels > triggerFetchMoreSize) {
      serviceLocator<PopularDishesCubit>().getPopularDishes(append: true);
    }
  }

  bool showMoreFoods = false;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PopularDishesCubit, PopularDishesState>(
        builder: (context, state) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'Популярные блюда',
              textAlign: TextAlign.left,
              style: text700Size18Black,
            ),
            SizedBox(
              height: 15,
            ),
            ...state.popularDishes.map(
              (e) => FoodCard(
                item: e,
              ),
            ),
            ShowMoreButton()
          ],
        ),
      );
    });
  }
}
