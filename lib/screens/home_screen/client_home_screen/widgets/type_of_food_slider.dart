import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/dish_catalog/cubit.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/main_screen/main_screen.dart';
import 'package:home_food/service_locator.dart';

import '../../../../core/app_assets.dart';
import 'food_type_card.dart';

class TypeOfFoodSlider extends StatefulWidget {
  const TypeOfFoodSlider({Key? key}) : super(key: key);

  @override
  State<TypeOfFoodSlider> createState() => _TypeOfFoodSliderState();
}

class _TypeOfFoodSliderState extends State<TypeOfFoodSlider> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0),
      child: SizedBox(
        height: 100,
        width: double.infinity,
        child: BlocBuilder<DishCatalogCubit, DishCatalogState>(
          builder: (context, state) {
            return ListView(
              scrollDirection: Axis.horizontal,
              children: [
                for (var i = 0; i < state.dishTypes.length; i++)
                  InkWell(
                    onTap: () {
                      serviceLocator<DishCatalogCubit>()
                          .handleDishTypeChanged(i + 1);
                      MainScreen.of(context)?.changeTab(
                        1,
                        true,
                      );
                    },
                    child: FoodTypeCard(
                      foodType: state.dishTypes[i],
                    ),
                  ),
              ],
            );
          },
        ),
      ),
    );
  }

  _handleDishTypeClicked() {
    Routes.router.navigate(Routes.myPreferencesScreen);
  }
}
