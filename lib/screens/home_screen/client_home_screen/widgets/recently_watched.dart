import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/viewed_history/cubit.dart';
import 'package:home_food/blocs/viewed_history/state.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/food/food.dart';
import 'package:home_food/routes.dart';

import 'food_card.dart';

class ResentlyWatched extends StatefulWidget {
  const ResentlyWatched({Key? key}) : super(key: key);

  @override
  State<ResentlyWatched> createState() => _ResentlyWatchedState();
}

class _ResentlyWatchedState extends State<ResentlyWatched> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ViewedHistoryCubit, ViewedHistoryState>(
      builder: (context, state) {
        return SizedBox(
          height: 209,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              ...state.viewedHistory.map(
                (e) => SizedBox(
                  width: 273,
                  height: double.infinity,
                  child: FoodCard(
                    item: e,
                    key: Key('viewed_history ${e.id}'),
                    margin: EdgeInsets.only(bottom: 41, right: 10),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
