import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';

import '../../../../core/text_styles.dart';


class Footer extends StatelessWidget {
  const Footer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.only(top: 36, bottom: 45),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 250,
            child: Text(
              'Сервис доставки еды и бронирование столиков в ресторанах Reserveat Copyright © 2022',
              textAlign: TextAlign.center,
              style: text400Size13Grey,
            ),
          ),
          SizedBox(height: 19,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(AppAssets.visa_icon),
              SizedBox(width: 25,),
              SvgPicture.asset(AppAssets.master_card_icon)
            ],
          )
        ],
      ),
    );
  }
}
