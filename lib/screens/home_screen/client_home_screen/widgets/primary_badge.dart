import 'package:flutter/material.dart';

class PrimaryBadge extends StatelessWidget {
  final String label;

  const PrimaryBadge({
    Key? key,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 6),
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 14),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        color: Color(0xffD3CCDD),
      ),
      height: 30,
      child: Row(
        children: [
          Text(
            '$label',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Color(0xff393939),
                fontSize: 13,
                fontWeight: FontWeight.w400),
          ),
        ],
      ),
    );
  }
}
