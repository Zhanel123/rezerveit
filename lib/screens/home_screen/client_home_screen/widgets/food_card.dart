import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dish_catalog/cubit.dart';
import 'package:home_food/blocs/favorite_dishes/cubit.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/favourite_dish/favourite_dish.dart';
import 'package:home_food/models/food/food.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/food_detaill/food_detail.dart';
import 'package:home_food/screens/widgets/buttons/add_to_cart_button.dart';
import 'package:home_food/screens/widgets/primary_image.dart';
import 'package:home_food/service_locator.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/text_styles.dart';

class FoodCard extends StatefulWidget {
  final Dish item;
  final EdgeInsets margin;

  // final FavouriteDish? favouriteDish;

  const FoodCard({
    Key? key,
    required this.item,
    this.margin = const EdgeInsets.only(bottom: 13),
    // this.favouriteDish,
  }) : super(key: key);

  @override
  State<FoodCard> createState() => _FoodCardState();
}

class _FoodCardState extends State<FoodCard> {
  int currentImageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Routes.router.navigate(
          Routes.foodDetail,
          args: FoodDetailArgs(dish: widget.item),
        );
      },
      child: Container(
        margin: widget.margin,
        height: 209,
        width: double.infinity,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  LayoutBuilder(builder: (context, constraints) {
                    if (widget.item.images.isEmpty) {
                      return Container(
                        width: double.infinity,
                        height: constraints.maxHeight,
                        child: PrimaryImage(),
                      );
                    }
                    return Container(
                      width: double.infinity,
                      height: constraints.maxHeight,
                      child: Stack(
                        children: [
                          SizedBox(
                            width: double.infinity,
                            child: CarouselSlider(
                              options: CarouselOptions(
                                  height: 400.0,
                                  viewportFraction: 1,
                                  enableInfiniteScroll: false,
                                  onPageChanged: (index, reason) {
                                    setState(() {
                                      currentImageIndex = index;
                                    });
                                  }),
                              items: widget.item.images.map((i) {
                                return SizedBox(
                                  height: double.infinity,
                                  width: double.infinity,
                                  child: PrimaryImage(
                                    url: i.imageUrl,
                                    fit: BoxFit.fitWidth,
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                          if (widget.item.images.isNotEmpty)
                            Positioned(
                                right: 10,
                                bottom: 10,
                                child: Container(
                                  height: 12,
                                  padding: const EdgeInsets.only(
                                    left: 8,
                                    top: 4,
                                    bottom: 4,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: Row(
                                    children: [
                                      for (int i = 0;
                                          i < widget.item.images.length;
                                          i++)
                                        Container(
                                          width: 5,
                                          height: 5,
                                          margin: EdgeInsets.only(right: 8),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: currentImageIndex == i
                                                ? Colors.white
                                                : Colors.white12,
                                          ),
                                        )
                                    ],
                                  ),
                                ))
                        ],
                      ),
                    );
                  }),
                  Positioned(
                      top: 9,
                      bottom: 9,
                      left: 8,
                      right: 8,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _Labels(
                            item: widget.item,
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    top: 4, bottom: 4, right: 8, left: 8),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.black.withOpacity(0.5),
                                ),
                                child: Text(
                                  NumUtils.humanizeNumber(
                                        widget.item.price,
                                        isCurrency: true,
                                      ) ??
                                      '',
                                  style: text600Size16White,
                                ),
                              )
                            ],
                          )
                        ],
                      ))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(widget.item.name ?? '', style: text700Size12Black),
                  Row(
                    children: [
                      // if (item.showIcons == true)
                      // BlocBuilder<OrderCubit, OrderState>(
                      //   builder: (context, state) {
                      //     return Text(
                      //       '${state.order?.positionList?[0]?.amount != null ? '+ ${state.order?.positionList?[0]?.amount}' : ''}',
                      //       style: text700Size12Pink,
                      //     );
                      //   },
                      // ),
                      SizedBox(
                        width: 8.85,
                      ),
                      // if (item.showIcons == true)
                      widget.item.portionAmount != 0
                          // ? InkWell(
                          //     onTap: () => serviceLocator<OrderCubit>()
                          //         .putDishToPreorder(item, 1),
                          //     child: SvgPicture.asset(AppAssets.plus_icon))
                          ? AddToCartButton(
                              item: widget.item,
                            )
                          : SizedBox(),
                      SizedBox(
                        width: 8,
                      ),
                      BlocBuilder<FavouriteDishesCubit, FavouriteDishesState>(
                        builder: (context, state) {
                          bool isFavourite = state.favDishes.any((element) =>
                              element.dishCardItem?.id == widget.item.id);
                          return InkWell(
                              onTap: () => isFavourite
                                  ? serviceLocator<FavouriteDishesCubit>()
                                      .deleteFavDish(value: widget.item)
                                  : serviceLocator<FavouriteDishesCubit>()
                                      .addDishToFavorite(widget.item),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SvgPicture.asset(
                                  isFavourite
                                      ? AppAssets.like_active
                                      : AppAssets.like_inactive,
                                ),
                              ));
                        },
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _Labels extends StatelessWidget {
  final Dish item;

  const _Labels({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // if (item.isHot)
            // Padding(
            //   padding: const EdgeInsets.only(right: 7.0),
            //   child: _Label(
            //     text: 'hot',
            //     color: AppColors.yellowColor,
            //   ),
            // ),
            if (item.isNew)
              Padding(
                  padding: const EdgeInsets.only(right: 7.0),
                  child: _Label(
                    text: 'new',
                    color: AppColors.pinkColor,
                  )),
            if (item.discountAfter19 > 0)
              _Label(
                text: '-${item.discountAfter19}%',
                color: AppColors.pinkColor,
              )
          ],
        ),
        if (item.manufacturer?.photo?.path != null)
          Container(
            height: 42,
            width: 42,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
            ),
            child: PrimaryImage(
              url: item.manufacturer?.photo?.imageUrl,
              fit: BoxFit.cover,
            ),
          ),
      ],
    );
  }
}

class _Label extends StatelessWidget {
  final String text;
  final Color color;

  const _Label({Key? key, required this.text, required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 33,
      height: 33,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Center(
        child: Text(
          text,
          style: text400Size13White,
        ),
      ),
    );
  }
}
