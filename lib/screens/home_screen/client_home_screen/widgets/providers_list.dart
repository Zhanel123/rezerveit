import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/merchant/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/shop_provider_card.dart';
import 'package:home_food/service_locator.dart';

class ShopProvidersList extends StatefulWidget {
  const ShopProvidersList({Key? key}) : super(key: key);

  @override
  State<ShopProvidersList> createState() => _ShopProvidersListState();
}

class _ShopProvidersListState extends State<ShopProvidersList> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    serviceLocator<MerchantCubit>().getMerchantList(append: false);

    scrollController.addListener(scroollListener);
    super.initState();
  }

  void scroollListener() {
    // когда доходим до 70%
    var triggerFetchMoreSize = 0.7 * scrollController.position.maxScrollExtent;

    if (scrollController.position.pixels > triggerFetchMoreSize) {
      serviceLocator<MerchantCubit>().getMerchantList(append: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MerchantCubit, MerchantState>(
      builder: (context, state) {
        return ListView(
          controller: scrollController,
          scrollDirection: Axis.horizontal,
          children: [
            ...state.merchants.map((e) => ShopProviderCard(
                  merchant: e,
                ))
          ],
        );
      },
    );
  }
}
