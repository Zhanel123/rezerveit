import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/dish_catalog/cubit.dart';
import 'package:home_food/screens/main_screen/main_screen.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/service_locator.dart';

class SelectRadius extends StatefulWidget {
  const SelectRadius({Key? key}) : super(key: key);

  @override
  State<SelectRadius> createState() => _SelectRadiusState();
}

class _SelectRadiusState extends State<SelectRadius> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishCatalogCubit, DishCatalogState>(
      builder: (context, state) {
        return PrimaryDropdown(
          height: 40,
          initialOption: state.radius,
          options: mapDishTypes([1, 2, 3, 4]),
          // error: state.dishType.invalid ? state.dishType.error?.key ?? '' : '',
          // error: state.radius ? 'Обязательное поле' : '',
          onChanged: (option) {
            serviceLocator<DishCatalogCubit>()
                .handleRadiusSelected(option?.value);
          },
        );
      },
    );
  }

  List<SelectOption<int>> mapDishTypes(List<int> radiusList) {
    List<SelectOption<int>> options = [];

    options.add(SelectOption(
      value: null,
      label: 'Выберите радиус',
    ));

    for (int i = 0; i < radiusList.length; i++) {
      options.add(SelectOption(
        value: radiusList[i],
        label: "Близ ${radiusList[i]} км",
      ));
    }

    return options;
  }
}
