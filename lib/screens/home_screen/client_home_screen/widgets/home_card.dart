import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/material.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/widgets/order/order_box.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class HomeCard extends StatefulWidget {
  const HomeCard({Key? key}) : super(key: key);

  @override
  State<HomeCard> createState() => _HomeCardState();
}

class _HomeCardState extends State<HomeCard> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Stack(
        children: [
          Container(
            height: constraints.maxHeight,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
            child: Image.asset(
              AppAssets.slider_1,
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            child: Center(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: Text(
                  'Скидки 10% на все комплексные обеды',
                  textAlign: TextAlign.center,
                  style: text700Size15White,
                ),
              ),
            ),
          )
        ],
      );
    });
  }
}
