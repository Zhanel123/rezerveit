import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'home_card.dart';

class HomeSlider extends StatefulWidget {
  const HomeSlider({Key? key}) : super(key: key);

  @override
  State<HomeSlider> createState() => _HomeSliderState();
}

class _HomeSliderState extends State<HomeSlider> {
  int activeIndex = 0;

  CarouselController _carouselController = CarouselController();
  PageController pageController = PageController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CarouselSlider(
          carouselController: _carouselController,
          items: [
            HomeCard(),
            HomeCard(),
            HomeCard(),
          ],
          options: CarouselOptions(
            onPageChanged: _onCarouselPageChanged,
            initialPage: activeIndex,
            enlargeCenterPage: true,
            viewportFraction: 1,
            autoPlay: true,
          ),
        ),
        Positioned(
          top: 16,
          left: 14,
          bottom: 16,
          child: IconButton(
              onPressed: () {
                _carouselController.previousPage();
              },
              icon: Icon(
                Icons.keyboard_arrow_left,
                color: Colors.white,
              )),
        ),
        Positioned(
            top: 16,
            right: 14,
            bottom: 16,
            child: IconButton(
                onPressed: () {
                  _carouselController.nextPage();
                },
                icon: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.white,
                ))),
        Positioned(
            bottom: 15,
            left: 0,
            right: 0,
            child: Center(
              child: Container(
                padding: EdgeInsets.only(top: 37),
                child: SmoothPageIndicator(
                  controller: pageController,
                  onDotClicked: _onDotClicked,
                  count: 4,
                  effect: ScrollingDotsEffect(
                    activeDotColor: Color(0xffF44F8B),
                    dotColor: Colors.white,
                    dotHeight: 6.94,
                    dotWidth: 6.94,
                  ),
                ),
              ),
            ))
      ],
    );
  }

  void _onDotClicked(int index) {
    _carouselController.jumpToPage(index);
  }

  _onCarouselPageChanged(index, reason) {
    if (mounted) {
      setState(() {
        activeIndex = index;
      });
    }

    //почему то тут ошибка
    // pageController.animateTo(activeIndex.toDouble(),
    //     duration: Duration(milliseconds: 600), curve: Curves.bounceIn);
  }
}
