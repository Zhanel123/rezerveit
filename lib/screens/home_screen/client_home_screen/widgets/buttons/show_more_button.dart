import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/blocs/popular_dishes/cubit.dart';
import 'package:home_food/service_locator.dart';

import '../../../../../core/app_assets.dart';
import '../../../../../core/button_styles.dart';
import '../../../../../core/text_styles.dart';

class ShowMoreButton extends StatefulWidget {
  const ShowMoreButton({
    Key? key,
  }) : super(key: key);

  @override
  State<ShowMoreButton> createState() => _ShowMoreButtonState();
}

class _ShowMoreButtonState extends State<ShowMoreButton> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PopularDishesCubit, PopularDishesState>(
      builder: (context, state) {
        if (state.hasNext == false) {
          return SizedBox();
        }
        return SizedBox(
            width: double.infinity,
            height: 40,
            child: OutlinedButton(
              onPressed: () => serviceLocator<PopularDishesCubit>()
                  .getPopularDishes(append: true),
              style: buttonWhiteRadius8,
              child: Padding(
                padding: const EdgeInsets.only(top: 7, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Показать еще 5',
                      style: text400Size13Black,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    SvgPicture.asset(AppAssets.arrow_to_down)
                  ],
                ),
              ),
            ));
      },
    );
  }
}
