import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/map_screen/flutter_map_screen.dart';

import '../../../../../core/button_styles.dart';

class ShowOnMapButton extends StatelessWidget {
  const ShowOnMapButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () => Routes.router.navigate(Routes.mapScreen),
      style: buttonWhiteRadius8,
      child: Padding(
        padding: const EdgeInsets.only(left: 6.0, top: 8, bottom: 8, right: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              AppAssets.map_icon,
              color: Color(0xffD7D7D7),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'Показать на карте',
              style: text400Size13Black,
            ),
          ],
        ),
      ),
    );
  }
}
