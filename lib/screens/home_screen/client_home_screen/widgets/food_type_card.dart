import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/screens/widgets/primary_image.dart';

import '../../../../core/text_styles.dart';

class FoodTypeCard extends StatelessWidget {
  final DishType foodType;

  const FoodTypeCard({
    Key? key,
    required this.foodType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      padding: EdgeInsets.only(top: 17, bottom: 5),
      margin: EdgeInsets.only(right: 9),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
        border: Border.all(color: AppColors.greyBackground),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 40,
            height: 40,
            child: PrimaryImage(
              url: foodType.photo?.imageUrl,

              /// TODO fit: BoxFit.cover учи
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            width: 87,
            child: Text(foodType.name(context),
                textAlign: TextAlign.center, style: text700Size12Black),
          )
        ],
      ),
    );
  }
}
