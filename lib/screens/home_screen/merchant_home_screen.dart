import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/home_screen/widgets/merchant_cart.dart';
import 'package:home_food/screens/home_screen/widgets/top_dishes_view.dart';
import 'package:home_food/screens/home_screen/widgets/volume_sales_view.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/merchant_new_orders_caard.dart';
// import 'package:mapbox_gl/mapbox_gl.dart';

import '../widgets/my_app_bar.dart';
import '../widgets/search/search_field.dart';

class MerchantHomeScreen extends StatefulWidget {
  const MerchantHomeScreen({Key? key}) : super(key: key);

  @override
  State<MerchantHomeScreen> createState() => _MerchantHomeScreenState();
}

class _MerchantHomeScreenState extends State<MerchantHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffF5F5F5),
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 18),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: MyAppBar(),
            ),
            SizedBox(
              height: 13,
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: BlocBuilder<ProfileCubit, ProfileState>(
                builder: (context, state) {
                  if (state.profile != null) {
                    return MerchantInfoCard(
                      merchant: state.profile!,
                    );
                  }
                  return SizedBox();
                },
              ),
            ),
            BlocBuilder<MerchantOrdersCubit, MerchantOrdersState>(
              builder: (context, state) {
                return Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 5,
                  ),
                  child: MerchantNewOrdersCard(),
                );
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: VolumeSales(),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TopDishesView(),
            ),
          ],
        ),
      ),
    );
  }
}
