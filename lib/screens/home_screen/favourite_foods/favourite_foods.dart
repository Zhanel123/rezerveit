import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/favorite_dishes/cubit.dart';
import 'package:home_food/screens/home_screen/favourite_foods/fav_dishes.dart';
import 'package:home_food/screens/widgets/my_app_bar.dart';
import 'package:home_food/screens/widgets/search/search_field.dart';
import 'package:home_food/service_locator.dart';

import '../../../core/text_styles.dart';

class FavoriteFoods extends StatefulWidget {
  const FavoriteFoods({Key? key}) : super(key: key);

  @override
  State<FavoriteFoods> createState() => _FavoriteFoodsState();
}

class _FavoriteFoodsState extends State<FavoriteFoods>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, length: 4);

    _controller.addListener(() {
      print(_controller.index);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  late TabController _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        color: Color(0xffF5F5F5),
        child: SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 18),
            child: BlocBuilder<FavouriteDishesCubit, FavouriteDishesState>(
              builder: (context, state) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: MyAppBar(),
                    ),
                    SizedBox(
                      height: 13,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: SearchField(
                        hintText: "Поиск в избранном",
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Избранные блюда',
                            style: text700Size18Black,
                          ),
                          state.favDishes.isNotEmpty
                              ? TextButton(
                              onPressed:
                              serviceLocator<FavouriteDishesCubit>()
                                  .deleteAllFavDishes,
                              child: Text(
                                'Удалить все',
                                style: text400Size13Pink,
                              ))
                              : SizedBox()
                        ],
                      ),
                    ),
                    FavDishes()
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
