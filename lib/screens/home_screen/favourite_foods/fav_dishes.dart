import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/favorite_dishes/cubit.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/food_card.dart';
import 'package:home_food/service_locator.dart';

class FavDishes extends StatefulWidget {
  const FavDishes({Key? key}) : super(key: key);

  @override
  State<FavDishes> createState() => _FavDishesState();
}

class _FavDishesState extends State<FavDishes> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    serviceLocator<FavouriteDishesCubit>().fetchFavouriteDishes(append: false);

    scrollController.addListener(scroollListener);
    super.initState();
  }

  void scroollListener() {
    // когда доходим до 70%
    var triggerFetchMoreSize = 0.7 * scrollController.position.maxScrollExtent;

    if (scrollController.position.pixels > triggerFetchMoreSize) {
      serviceLocator<FavouriteDishesCubit>().fetchFavouriteDishes(append: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavouriteDishesCubit, FavouriteDishesState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            children: [
              ...state.favDishes.map(
                (e) => FoodCard(
                  item: e.dishCardItem!,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
