import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/merchant_statistics/cubit.dart';
import 'package:home_food/blocs/merchant_statistics/cubit.dart';
import 'package:home_food/blocs/merchant_statistics/state.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/chat_data/chat_data.dart';
import 'package:home_food/models/statistics/statistics_item.dart';
import 'package:home_food/service_locator.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class VolumeSales extends StatefulWidget {
  const VolumeSales({Key? key}) : super(key: key);

  @override
  State<VolumeSales> createState() => _VolumeSalesState();
}

class _VolumeSalesState extends State<VolumeSales> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MerchantStatisticsCubit, MerchantStatisticsState>(
      builder: (context, state) {
        return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
            ),
            padding: const EdgeInsets.symmetric(
              vertical: 16,
              horizontal: 22,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Объем продаж',
                      style: text700Size14Black,
                    ),
                    DropdownButtonHideUnderline(
                      child: DropdownButton<PeriodTypeEnum?>(
                        value: state.sellStatisticsPeriodType,
                        onChanged: serviceLocator<MerchantStatisticsCubit>()
                            .sellStatisticsPeriodTypeChanged,
                        items: [
                          ...PeriodTypeEnum.values.map((e) => DropdownMenuItem(
                              value: e,
                              child: Text(
                                e.value!,
                                style: text500Size15Black,
                              )))
                        ],
                      ),
                    )
                  ],
                ),
                if (state.statistics.items.isEmpty)
                  Center(
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 90),
                      child: Text(
                        'Нет данных для отображения\nстатистики.',
                        textAlign: TextAlign.center,
                        style: text400Size12GreyItalic,
                      ),
                    ),
                  ),
                if (state.statistics.items.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: state.sellStatisticsPeriodType == PeriodTypeEnum.DAY
                        ? _SplineChart(chartData: state.statistics.items)
                        : _ColumnChart(chartData: state.statistics.items),
                  ),
                RichText(
                  text: TextSpan(
                    text: 'Всего продано на:',
                    style: text400Size12Grey,
                    children: [
                      TextSpan(
                        text: ' ${state.statistics.totalSum} ₸',
                        style: text400Size12Black,
                      )
                    ],
                  ),
                ),
                SizedBox(height: 4),
                RichText(
                  text: TextSpan(
                    text: 'В среднем за 1 день:  ',
                    style: text400Size12Grey,
                    children: [
                      TextSpan(
                        text: '${state.statistics.averageSum} ₸',
                        style: text400Size12Black,
                      ),
                      TextSpan(
                        text:
                            ' ${state.statistics.averagePercent.toStringAsFixed(2)} %',
                        style: state.statistics.averagePercent >= 0
                            ? text400Size12Green
                            : text400Size12Pink,
                      ),
                    ],
                  ),
                )
              ],
            ));
      },
    );
  }
}

class _ColumnChart extends StatefulWidget {
  const _ColumnChart({Key? key, required this.chartData}) : super(key: key);

  final List<StatisticsItem> chartData;

  @override
  State<_ColumnChart> createState() => _ColumnChartState();
}

class _ColumnChartState extends State<_ColumnChart> {
  int? currentColumn;

  final SelectionBehavior _selectionBehavior = SelectionBehavior(
    enable: true,
    selectedColor: AppColors.pinkColor,
    unselectedColor: Color(0xFFD9D9D9),
  );

  // final DataLabelSettings _dataLabelSettings = ;

  final TooltipBehavior _tooltipBehavior = TooltipBehavior(
    enable: false,
    shouldAlwaysShow: true,
  );

  @override
  Widget build(BuildContext context) {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      primaryYAxis: NumericAxis(
        isVisible: true,
      ),
      primaryXAxis: DateTimeCategoryAxis(
        isVisible: true,
      ),
      tooltipBehavior: _tooltipBehavior,
      series: <CartesianSeries<StatisticsItem, DateTime?>>[
        ColumnSeries<StatisticsItem, DateTime>(
          dataSource: widget.chartData,
          selectionBehavior: _selectionBehavior,
          xValueMapper: (StatisticsItem data, _) => data.startDate,
          yValueMapper: (StatisticsItem data, _) =>
              data.totalSum == 0 ? null : data.totalSum,
          // Sets the corner radius
          width: 0.5,
          // isTrackVisible: true,
          color: AppColors.pinkColor,
          trackBorderWidth: 0,
          trackPadding: 3,
          dataLabelSettings: DataLabelSettings(
            isVisible: true,
            alignment: ChartAlignment.far,
            labelAlignment: ChartDataLabelAlignment.top,
            offset: Offset(30, 0),
            builder: (
              dynamic data,
              dynamic point,
              dynamic series,
              int pointIndex,
              int seriesIndex,
            ) {
              StatisticsItem _data = data as StatisticsItem;
              return currentColumn == pointIndex
                  ? Container(
                      constraints: BoxConstraints(
                        maxHeight: 50,
                        maxWidth: 50,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                      ),
                      child: Center(
                        child: Column(
                          children: [
                            Text(
                              DateFormat('dd MMM').format(_data.startDate!),
                              style: text400Size9Pink,
                            ),
                            Text(
                              NumUtils.humanizeNumber(
                                    _data.totalSum,
                                    isCurrency: true,
                                  ) ??
                                  "",
                              style: text400Size12Black,
                            ),
                          ],
                        ),
                      ),
                    )
                  : SizedBox();
            },
          ),
          emptyPointSettings: EmptyPointSettings(
            mode: EmptyPointMode.zero,
            borderWidth: 1,
          ),
        ),
      ],
      onDataLabelRender: _onDataLabelRender,
      onSelectionChanged: _onColumnSelectionChanged,
    );
  }

  void _onColumnSelectionChanged(SelectionArgs args) {
    setState(() {
      currentColumn = args.pointIndex;
    });
  }

  void _onDataLabelRender(DataLabelRenderArgs args) {
    if (args.pointIndex == currentColumn) {
    } else {
      args.text = '';
    }
  }
}

class _SplineChart extends StatefulWidget {
  const _SplineChart({Key? key, required this.chartData}) : super(key: key);

  final List<StatisticsItem> chartData;

  @override
  State<_SplineChart> createState() => _SplineChartState();
}

class _SplineChartState extends State<_SplineChart> {
  @override
  Widget build(BuildContext context) {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      primaryYAxis: NumericAxis(
        isVisible: true,
      ),
      primaryXAxis: DateTimeCategoryAxis(
        isVisible: true,
        dateFormat: DateFormat('HH:mm'),
      ),
      series: <ChartSeries>[
        AreaSeries<StatisticsItem, DateTime?>(
          dataSource: widget.chartData,
          xValueMapper: (StatisticsItem data, _) => data.startDate,
          yValueMapper: (StatisticsItem data, _) => data.totalSum,
          borderWidth: 2,
          borderColor: Color(0xFF50BA3E),
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0x3850BA3E), Color(0x50BA3E)],
            stops: [0.0, 1.0],
          ),
        )
      ],
    );
  }
}
