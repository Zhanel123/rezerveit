import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/screens/widgets/primary_image.dart';

import '../../../core/text_styles.dart';

class TopDishCard extends StatelessWidget {
  final Dish dish;
  final int totalSum;

  const TopDishCard({
    Key? key,
    required this.dish,
    required this.totalSum,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
      padding: EdgeInsets.only(bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                width: 53,
                clipBehavior: Clip.hardEdge,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: CarouselSlider(
                  options: CarouselOptions(
                      height: 53.0,
                      viewportFraction: 1,
                      enableInfiniteScroll: false,
                      onPageChanged: (index, reason) {
                        // setState(() {
                        //   currentImageIndex = index;
                        // });
                      }),
                  items: dish.images.map((i) {
                    return SizedBox(
                      height: double.infinity,
                      width: double.infinity,
                      child: PrimaryImage(
                        url: i.imageUrl,
                        fit: BoxFit.fitWidth,
                      ),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Container(
                width: 133,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      dish.name,
                      textAlign: TextAlign.left,
                      style: text400Size12Black,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    RichText(
                      text: TextSpan(
                        text: '',
                        style: text700Size15Black,
                        children: [
                          TextSpan(
                              text: '${dish.weight} г',
                              style: text400Size12Grey),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          Text(
            '+ $totalSum',
            style: text400Size12Green,
          )
        ],
      ),
    );
  }
}
