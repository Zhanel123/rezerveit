import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/screens/widgets/primary_image.dart';

class MerchantInfoCard extends StatelessWidget {
  const MerchantInfoCard({
    Key? key,
    required this.merchant,
  }) : super(key: key);

  final Profile merchant;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      height: 84,
      padding: EdgeInsets.only(right: 13, left: 17, top: 12, bottom: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 56,
            width: 56,
            decoration: BoxDecoration(
                color: AppColors.pinkColor,
                borderRadius: BorderRadius.circular(28)),
            child: Container(
              height: 42,
              width: 42,

              /// TODO clipBehavior: Clip.hardEdge, учи
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: PrimaryImage(
                url: merchant.photo?.imageUrl,

                /// TODO fit: BoxFit.cover учи
                fit: BoxFit.cover,
              ),
            ),
            // child: PrimaryImage(
            //   url: merchant.photo?.imageUrl,
            // ),
          ),
          SizedBox(width: 16),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Text(
                        merchant.firstName ?? '',
                        style: text700Size14Black,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.star,
                        color: Colors.yellow,
                      ),
                      Text('${merchant.rating ?? 0}'),
                    ],
                  ),
                ),
                Text(
                  'Тариф оплачен',
                  style: text400Size12Green,
                )
              ],
            ),
          ),
          Spacer(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 21,
                width: 21,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xffE9E9E9),
                ),
                child: Center(
                  child: Icon(
                    Icons.chevron_right,
                    color: Color(0xFF7A7A7A),
                    size: 15,
                  ),
                ),
              ),
              Text('до 01.05.23')
            ],
          )
        ],
      ),
    );
  }
}
