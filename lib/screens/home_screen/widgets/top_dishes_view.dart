import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/merchant_statistics/cubit.dart';
import 'package:home_food/blocs/merchant_statistics/cubit.dart';
import 'package:home_food/blocs/merchant_statistics/state.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/home_screen/widgets/top_dish_card.dart';
import 'package:home_food/service_locator.dart';

class TopDishesView extends StatefulWidget {
  const TopDishesView({Key? key}) : super(key: key);

  @override
  State<TopDishesView> createState() => _TopDishesViewState();
}

class _TopDishesViewState extends State<TopDishesView> {

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MerchantStatisticsCubit, MerchantStatisticsState>(
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.white,
          ),
          padding: const EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 22,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'ТОП-5 блюд',
                    style: text700Size14Black,
                  ),
                  DropdownButtonHideUnderline(
                    child: DropdownButton<PeriodTypeEnum?>(
                      value: state.topDishStatisticsPeriodType,
                      onChanged: serviceLocator<MerchantStatisticsCubit>()
                          .topDishStatisticsPeriodTypeChanged,
                      items: [
                        ...PeriodTypeEnum.values.map((e) => DropdownMenuItem(
                            value: e,
                            child: Text(
                              e.value!,
                              style: text500Size15Black,
                            )))
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 8,
              ),
              if (state.topDishStatistics.isEmpty)
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 28),
                  child: Text(
                    'Нет данных для отображения\nстатистики.',
                    textAlign: TextAlign.center,
                    style: text400Size12GreyItalic,
                  ),
                ),
              if (state.topDishStatistics.isNotEmpty)
                for (int i = 0; i < state.topDishStatistics.length; i++)
                  Column(
                    children: [
                      TopDishCard(
                        dish: state.topDishStatistics[i].dishCardItem!,
                        totalSum: state.topDishStatistics[i].totalSum,
                      ),
                      if (i != state.topDishStatistics.length - 1) Divider(),
                    ],
                  ),
            ],
          ),
        );
      },
    );
  }
}
