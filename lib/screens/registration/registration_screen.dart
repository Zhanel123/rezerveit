import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/login/cubit.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/models/district/district.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:seafarer/seafarer.dart';

import '../../core/app_assets.dart';
import '../../core/app_colors.dart';
import '../widgets/buttons/add_logo.dart';
import '../widgets/buttons/checkbox_button.dart';
import '../widgets/buttons/default_button.dart';

class RegistrationWidget extends StatefulWidget {
  const RegistrationWidget({Key? key}) : super(key: key);

  @override
  State<RegistrationWidget> createState() => _RegistrationWidgetState();
}

class _RegistrationWidgetState extends State<RegistrationWidget> {
  String? name;
  String? phoneNumber;
  bool check = false;
  bool isLoading = false;
  bool isCustomer = false;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RegistrationCubit, RegistrationState>(
      listenWhen: (p, c) => p.registrationStatus != c.registrationStatus,
      listener: (context, state) {
        if (state.registrationStatus.isSubmissionSuccess) {
          Routes.router.navigate(Routes.registrationSmsVerificationScreen);
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
            color: Color(0xffF5F5F5,),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 30.0, bottom: 0),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(AppAssets.main_logo),
                          SizedBox(height: 27),
                          Text(
                            'Reserveat',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 26,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(28.0),
                      topLeft: Radius.circular(28.0),
                    ),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 22, vertical: 22),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            'Зарегистрироваться',
                            textAlign: TextAlign.left,
                            style: text700Size24Black,
                          ),
                        ),
                        SizedBox(
                          height: 9,
                        ),
                        _RoleSelectView(),
                        SizedBox(
                          height: 9,
                        ),
                        _NameField(),
                        SizedBox(
                          height: 9,
                        ),
                        _PhoneField(),
                        state.isSalesman ? _CompanyField() : SizedBox(),
                        SizedBox(
                          height: 18,
                        ),
                        // _CitySelect(),
                        SizedBox(
                          height: 18,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0),
                          child: _PrivacyPolicy(),
                        ),
                        SizedBox(
                          height: 35,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 25),
                          child: DefaultButton(
                            loading:
                                state.registrationStatus.isSubmissionInProgress,
                            onPressed:
                                serviceLocator<RegistrationCubit>().register,
                            label: 'Зарегистрироваться',
                            color: AppColors.pinkColor,
                            textColor: AppColors.white,
                          ),
                        ),
                        Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 25),
                            child: GestureDetector(
                              onTap: () => Routes.router.navigate(
                                Routes.loginScreen,
                                navigationType: NavigationType.popAndPushNamed,
                              ),
                              child: Text(
                                'Уже есть аккаунт',
                                style: text400Size12Pink,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class _RoleSelectView extends StatelessWidget {
  const _RoleSelectView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationCubit, RegistrationState>(
      builder: (context, state) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Я — покупатель',
              style: !state.isSalesman
                  ? text400Size12Black
                  : text400Size12BlackInactive,
            ),
            SizedBox(
              width: 8,
            ),
            Transform.scale(
              scale: 0.8,
              child: CupertinoSwitch(
                thumbColor: AppColors.pinkColor,
                trackColor: AppColors.EAGrey,
                activeColor: AppColors.EAGrey,
                value: state.isSalesman,
                onChanged: serviceLocator<RegistrationCubit>().changeRole,
              ),
            ),
            SizedBox(
              width: 11,
            ),
            Text(
              'Я — продавец',
              style: state.isSalesman
                  ? text400Size12Black
                  : text400Size12BlackInactive,
            ),
          ],
        );
      },
    );
  }
}

class _CompanyField extends StatelessWidget {
  const _CompanyField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationCubit, RegistrationState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(top: 13.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: RoundedTextFormField(
                  height: 77,
                  value: state.companyName.value,
                  onChange: serviceLocator<RegistrationCubit>().changeCompany,
                  hintText: 'Компания',
                  border: Border.all(color: Color(0xFFD8D8D8)),
                  keyboardType: TextInputType.text,
                  hasError:
                      state.companyName.status == FormzInputStatus.invalid,
                  errorText: state.companyName.error != null
                      ? 'Обязательное поле'
                      : '',
                ),
              ),
              SizedBox(
                width: 8,
              ),
              AddLogoButton(
                image: state.uploadedImage.value,
                error: state.uploadedImage.invalid ? 'Обязательное поле' : '',
                onImageSelected:
                    serviceLocator<RegistrationCubit>().uploadImage,
              )
            ],
          ),
        );
      },
    );
  }
}

class _NameField extends StatelessWidget {
  const _NameField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationCubit, RegistrationState>(
      buildWhen: (prev, curr) => prev.name != curr.name,
      builder: (context, state) {
        return RoundedTextFormField(
          value: state.name.value,
          onChange: serviceLocator<RegistrationCubit>().changeName,
          hintText: 'Имя',
          border: Border.all(color: Color(0xFFD8D8D8)),
          keyboardType: TextInputType.text,
          hasError: state.name.invalid,
          errorText: state.name.error != null ? 'Обязательное поле' : '',
        );
      },
    );
  }
}

class _PhoneField extends StatelessWidget {
  const _PhoneField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationCubit, RegistrationState>(
      builder: (context, state) {
        final MaskTextInputFormatter phoneFormatter = MaskTextInputFormatter(
          mask: "+7(###)#######",
          initialText: state.phoneNumber.value,
        );

        return RoundedTextFormField(
          value: state.phoneNumber.value,
          onChange: serviceLocator<RegistrationCubit>().changePhone,
          inputFormatters: [phoneFormatter],
          hasError: state.phoneNumber.invalid,
          errorText: state.phoneNumber.error != null ? 'Обязательное поле' : '',
          hintText: '+7(XXX)XXXXXXX',
          // keyboardType: TextInputType.phone,
          border: Border.all(color: Color(0xFFD8D8D8)),
        );
      },
    );
  }
}

class _PrivacyPolicy extends StatefulWidget {
  const _PrivacyPolicy({Key? key}) : super(key: key);

  @override
  State<_PrivacyPolicy> createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<_PrivacyPolicy> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationCubit, RegistrationState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CheckboxButton(
                  isChecked: state.privacyPolicy.value,
                  changeIsChecked: (value) {
                    serviceLocator<RegistrationCubit>()
                        .handlePrivacyPolicyAndTermsOfService(value);
                  },
                ),
                SizedBox(width: 14),
                Expanded(
                  child: RichText(
                    text: TextSpan(
                      text: 'Я принимаю ',
                      style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff939393)),
                      children: [
                        TextSpan(
                          text:
                              'условия использования и политики конфиденциальности',
                          style:
                              TextStyle(decoration: TextDecoration.underline),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            if (state.privacyPolicy.invalid)
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                child: Text(
                  'Обязательное поле',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              ),
          ],
        );
      },
    );
  }
}

class _CitySelect extends StatelessWidget {
  const _CitySelect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationCubit, RegistrationState>(
      builder: (context, state) {
        return PrimaryDropdown(
          initialOption: state.city.value,
          options: mapCities(context, state.cities),
          error: state.city.invalid ? 'Обязательное поле' : '',
          onChanged: (option) => serviceLocator<RegistrationCubit>()
              .handleCityChanged(option?.value),
        );
      },
    );
  }

  List<SelectOption<City>> mapCities(BuildContext context, List<City> cities) {
    List<SelectOption<City>> options = [];

    if (cities.isEmpty) {
      serviceLocator<DictionaryCubit>().getCityList();
    }

    options.add(SelectOption(
      value: null,
      label: 'Выберите город',
    ));

    for (int i = 0; i < cities.length; i++) {
      options.add(SelectOption(
        value: cities[i],
        label: cities[i].name(context),
      ));
    }

    return options;
  }
}

class _DistrictSelect extends StatelessWidget {
  const _DistrictSelect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationCubit, RegistrationState>(
      builder: (context, state) {
        return PrimaryDropdown<District>(
          initialOption: state.district.value,
          hintText: 'Выберите ЖК',
          options: mapDistricts(context, state.districts),
          disabled: state.city.value == null,
          error: state.city.invalid ? 'Обязательное поле' : '',
          onChanged: (option) => serviceLocator<RegistrationCubit>()
              .handleDistrictChanged(option?.value),
        );
      },
    );
  }

  List<SelectOption<District>> mapDistricts(
      BuildContext context, List<District> districts) {
    List<SelectOption<District>> options = [];

    if (districts.isEmpty) {
      serviceLocator<DictionaryCubit>().getCityList();
    }

    for (int i = 0; i < districts.length; i++) {
      options.add(SelectOption(
        value: districts[i],
        label: districts[i].name(context),
      ));
    }

    return options;
  }
}
