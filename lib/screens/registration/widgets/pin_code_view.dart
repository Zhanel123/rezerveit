import 'package:flutter/material.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/service_locator.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinCodeView extends StatefulWidget {
  const PinCodeView({Key? key}) : super(key: key);

  @override
  State<PinCodeView> createState() => _PinCodeViewState();
}

class _PinCodeViewState extends State<PinCodeView> {
  bool hasError = false;

  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      appContext: context,
      length: 4,
      onChanged: (String value) {},
      onCompleted: serviceLocator<RegistrationCubit>().handlePinCodeCompleted,
      autoFocus: true,
      obscureText: false,
      pinTheme: PinTheme(
        selectedFillColor: Colors.white,
        inactiveFillColor: Colors.white,
        activeFillColor: Colors.white,
        inactiveColor: AppColors.smsBorderColor,
        activeColor: AppColors.smsBorderColor,
        selectedColor: AppColors.smsBorderColor,
        shape: PinCodeFieldShape.box,
        fieldOuterPadding: EdgeInsets.symmetric(horizontal: 7),
        borderRadius: BorderRadius.circular(8),
        borderWidth: 1,
        fieldHeight: 73,
        fieldWidth: 58,
      ),
      mainAxisAlignment: MainAxisAlignment.center,
      backgroundColor: Colors.white,
      enableActiveFill: true,
      obscuringCharacter: '•',
      hintCharacter: '•',
      hintStyle: TextStyle(
          color: Colors.grey, fontWeight: FontWeight.w700, fontSize: 36),
      textStyle: TextStyle(
          color: Colors.grey, fontWeight: FontWeight.w700, fontSize: 36),
      animationDuration: Duration(milliseconds: 100),
      keyboardType: TextInputType.number,
    );
  }
}
