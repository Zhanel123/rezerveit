import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/blocs/create_food/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/service_locator.dart';

class SelectDeliveryPrice extends StatelessWidget {
  const SelectDeliveryPrice({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 42,
      padding: EdgeInsets.symmetric(horizontal: 19),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Color(0xFFD8D8D8)),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('Доставка курьером'),
          DropdownButtonHideUnderline(
            child: DropdownButton<int?>(
                icon: SvgPicture.asset(AppAssets.drop_down_icon),
                value: 2,
                onChanged: null,
                items: <int?>[250, 550, 750]
                    .map<DropdownMenuItem<int>>((int? value) {
                  return DropdownMenuItem<int>(
                    child: Text('$value'),
                    value: value,
                  );
                }).toList()),
          ),
        ],
      ),
    );
  }
}
