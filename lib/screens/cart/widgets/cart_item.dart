import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/order_position/order_position.dart';
import 'package:home_food/screens/widgets/primary_image.dart';
import 'package:home_food/service_locator.dart';

class CartItem extends StatefulWidget {
  final OrderPosition item;

  const CartItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  State<CartItem> createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 3.0, left: 16, right: 16),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.only(left: 15, top: 13, bottom: 15, right: 17),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                LayoutBuilder(builder: (context, constraints) {
                  return Container(
                    width: 53,
                    height: 53,
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: 400.0,
                        viewportFraction: 1,
                      ),
                      items: widget.item.dishCardItem.images.map((i) {
                        return PrimaryImage(
                          url: i.imageUrl,
                          fit: BoxFit.fitWidth,
                        );
                      }).toList(),
                    ),
                  );
                }),
                SizedBox(
                  width: 15,
                ),
                Container(
                  width: 133,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.item.dishCardItem.name,
                        textAlign: TextAlign.left,
                        style: text400Size12Black,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      RichText(
                        text: TextSpan(
                          text: '',
                          children: [
                            if (widget.item.dishCardItem.priceWithSale > 0)
                              TextSpan(
                                text:
                                    '${widget.item.dishCardItem.priceWithSale} ',
                                style: text700Size15Pink,
                              ),
                            TextSpan(
                              text: '${widget.item.dishCardItem.price}',
                              style: text700Size15Black.copyWith(
                                decoration:
                                    widget.item.dishCardItem.priceWithSale > 0
                                        ? TextDecoration.lineThrough
                                        : null,
                              ),
                            ),
                            TextSpan(
                              text: '  ${widget.item.dishCardItem.weight} г',
                              style: text400Size12Grey,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            Container(
              width: 90,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                border: Border.all(
                  color: Color(0xffCFCFCF),
                  //                   <--- border color
                  width: 1.0,
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () =>
                        serviceLocator<BasketCubit>().decreaseOrderAmount(
                      orderPosition: widget.item,
                      amount: widget.item.amount,
                    ),
                    child: Container(
                      width: 21.33,
                      height: 21.33,
                      decoration: BoxDecoration(
                          color: Color(0xffE9E9E9),
                          borderRadius: BorderRadius.circular(58)),
                      child: Center(
                        child: Icon(
                          Icons.remove,
                          color: Colors.black,
                          size: 10,
                        ),
                      ),
                    ),
                  ),
                  Text(
                    '${widget.item.amount}',
                    style: text400Size13Black,
                  ),
                  InkWell(
                    onTap: () =>
                        serviceLocator<BasketCubit>().increaseOrderAmount(
                      orderPosition: widget.item,
                      amount: widget.item.amount,
                    ),
                    child: Container(
                      width: 21.33,
                      height: 21.33,
                      decoration: BoxDecoration(
                          color: Color(0xffE9E9E9),
                          borderRadius: BorderRadius.circular(58)),
                      child: Center(
                          child: Text(
                        '+',
                        style: text400Size13Black,
                      )),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
