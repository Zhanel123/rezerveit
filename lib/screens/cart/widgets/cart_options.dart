import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_food/core/text_styles.dart';

class Options extends StatefulWidget {
  final String label;
  final bool state;
  final Function(bool? value) function;

  const Options({
    Key? key,
    required this.label,
    required this.state,
    required this.function,
  }) : super(key: key);

  @override
  State<Options> createState() => _OptionsState();
}

class _OptionsState extends State<Options> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 42,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.75),
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Color(0xffD8D8D8), //                   <--- border color
          width: 1.0,
        ),
      ),
      padding: EdgeInsets.only(top: 12, bottom: 11, left: 19, right: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.label,
            style: text700Size12Black,
          ),
          Container(
            height: 24,
            child: Transform.scale(
              scale: 0.8,
              child: CupertinoSwitch(
                  value: widget.state,
                  onChanged: (bool? value) {
                    widget.function(value);
                  }),
            ),
          ),
        ],
      ),
    );
  }
}
