import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/service_locator.dart';

class CartFooter extends StatefulWidget {
  const CartFooter({Key? key}) : super(key: key);

  @override
  State<CartFooter> createState() => _CartFooterState();
}

class _CartFooterState extends State<CartFooter> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BasketCubit, BasketState>(
      builder: (context, state) {
        return Container(
          color: Colors.white,
          height: 80,
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 13),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Row(
                  children: [
                    Text(
                      '${state.order.total} ₸',
                      style: text700Size18Black,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: GradientButton(
                  borderRadius: BorderRadius.circular(50),
                  labelText: 'Оформить',
                  onPressed: serviceLocator<BasketCubit>().submitOrder,
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
