import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/order_position/order_position.dart';
import 'package:home_food/screens/cart/widgets/cart_footer.dart';
import 'package:home_food/screens/cart/widgets/cart_options.dart';
import 'package:home_food/screens/main_screen/main_screen.dart';
import 'package:home_food/screens/order/widgets/text_container.dart';
import 'package:home_food/screens/profile_screen/buttons/add_address_button.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

import '../../core/app_assets.dart';
import '../../routes.dart';
import 'buttons/payment_method_button.dart';
import 'widgets/cart_item.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  void initState() {
    serviceLocator<BasketCubit>().fetchPreOrder();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BasketCubit, BasketState>(
      listener: (context, state) {
        if (state.submitOrderStatus.isSubmissionSuccess) {
          serviceLocator<CustomerOrderCubit>().getCustomerOrders();
          Routes.router.popUntil(
            (route) => route.settings.name == Routes.homeScreen,
          );
          FlushbarService().showSuccessMessage();
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
            height: double.infinity,
            color: Color(0xffF5F5F5),
            child: BlocBuilder<BasketCubit, BasketState>(
              builder: (context, state) {
                return Stack(children: [
                  SingleChildScrollView(
                    padding: EdgeInsets.only(top: 48),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  SvgPicture.asset(AppAssets.app_bar_logo),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.44),
                                    child: Text('Reserveat',
                                        style: text700Size18Black),
                                  )
                                ],
                              ),
                              InkWell(
                                onTap: () {
                                  Routes.router.pop();
                                },
                                child: Container(
                                  width: 32,
                                  height: 32,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: Colors.white),
                                  child: Center(
                                    child: Icon(
                                      Icons.close,
                                      size: 17,
                                      color: Color(0xff7A7A7A),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 16.0, right: 16.0, top: 34),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Моя корзина',
                                style: text700Size18Black,
                              ),
                              if (state.order.positionList.isNotEmpty)
                                InkWell(
                                  child: Text(
                                    'Очистить корзину',
                                    style: text400Size13Pink,
                                  ),
                                  onTap: () {
                                    // Navigator.of(context).pop();
                                    serviceLocator<BasketCubit>()
                                        .deleteAllDishCard();
                                  },
                                ),
                              InkWell(
                                child: Text(
                                  'Добавить еду',
                                  style: text400Size13Pink,
                                ),
                                onTap: () {
                                  Routes.router.popUntil((p0) =>
                                      p0.settings.name == Routes.homeScreen);
                                  MainScreen.of(context)?.changeTab(
                                    1,
                                    true,
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                        state.order.positionList.isNotEmpty
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    children: mapCartElements(
                                      state.order.positionList,
                                    ),
                                  ),
                                  // Padding(
                                  //   padding: const EdgeInsets.symmetric(
                                  //       horizontal: 16.0),
                                  //   child: TextContainer(
                                  //     firstText: 'Предзаказ',
                                  //     preOrder: true,
                                  //   ),
                                  // ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: 16, left: 16, top: 3),
                                    child: Options(
                                      label: 'Приборы и салфетки',
                                      state: state.silverAndNapkin,
                                      function: serviceLocator<BasketCubit>()
                                          .handleSilverAndNapkinChanged,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                            horizontal: 16.0)
                                        .copyWith(
                                      top: 3,
                                    ),
                                    child:
                                        BlocBuilder<BasketCubit, BasketState>(
                                      builder: (context, state) {
                                        return Options(
                                          label: 'Самовывоз',
                                          state: state.deliveryMethod.value ==
                                                  'Самовывоз'
                                              ? true
                                              : false,
                                          function: (value) {
                                            serviceLocator<BasketCubit>()
                                                .changeDeliveryMethod(
                                                    deliveryMethod:
                                                        value == true
                                                            ? 'Самовывоз'
                                                            : 'Доставкой');
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height: 16,
                                  ),
                                  if (state.deliveryMethod.value != 'Самовывоз')
                                    _AddressSelectView(),
                                  if (state.deliveryMethod.value != 'Самовывоз')
                                    AddAddressButton(),

                                  ///TODO под вопросом
                                  // state.deliveryMethod.value ==
                                  //     'Самовывоз' ? SizedBox() : _AddressSelectView(),
                                  // state.deliveryMethod.value ==
                                  //     'Самовывоз' ? SizedBox() : AddAddressButton(),
                                  // Padding(
                                  //   padding: const EdgeInsets.symmetric(
                                  //       horizontal: 16.0),
                                  //   child: DeliveryMethod(),
                                  // ),
                                  // _PickupDeliveryMethod(),

                                  SizedBox(
                                    height: 8,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16.0),
                                    child: Text(
                                      'Способ оплаты',
                                      style: text400Size13Black,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16.0),
                                    child: PaymentMethod(),
                                  ),
                                ],
                              )
                            : Container(
                                height: 500,
                                child: Center(
                                  child: Text('Корзина пустая :('),
                                )),
                      ],
                    ),
                  ),
                  state.order.positionList.isNotEmpty
                      ? Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: CartFooter(),
                        )
                      : SizedBox()
                ]);
              },
            ),
          ),
        );
      },
    );
  }

  List<Widget> mapCartElements(List<OrderPosition> positionList) {
    List<Widget> children = [];

    for (int i = 0; i < positionList.length; i++) {
      children.add(
        Padding(
          padding: EdgeInsets.only(top: 3),
          child: CartItem(
            item: positionList[i],
          ),
        ),
      );
    }
    return children;
  }
}

class _AddressSelectView extends StatelessWidget {
  const _AddressSelectView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BasketCubit, BasketState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                'Адрес доставки',
                textAlign: TextAlign.start,
                style: text400Size13Black,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: PrimaryDropdown<Address>(
                initialOption: state.selectedAddress.value,
                options: mapAddresses(state.myAddressList),
                error: state.selectedAddress.invalid ? 'Обязательное поле' : '',
                onChanged: (option) => serviceLocator<BasketCubit>()
                    .onAddressChanged(option?.value),
              ),
            ),
            if (state.selectedAddress.value != null)
              SizedBox(
                height: 7,
              ),
            if (state.selectedAddress.value != null)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: Color(0xffD8D8D8),
                      //                   <--- border color
                      width: 1.0,
                    ),
                  ),
                  padding:
                      EdgeInsets.only(left: 20, top: 10, bottom: 9, right: 17),
                  child: Text(
                    '${state.selectedAddress.value?.fullAddressName}',
                    style: text500Size15BGrey,
                  ),
                ),
              )
          ],
        );
      },
    );
  }

  List<SelectOption<Address>> mapAddresses(List<Address> myAddressList) {
    List<SelectOption<Address>> options = [];
    options.add(SelectOption(
      value: null,
      label: 'Выберите адрес',
    ));
    for (int i = 0; i < myAddressList.length; i++) {
      options.add(SelectOption(
        value: myAddressList[i],
        label: myAddressList[i].address,
      ));
    }

    return options;
  }
}

class DeliveryMethod extends StatefulWidget {
  const DeliveryMethod({Key? key}) : super(key: key);

  @override
  State<DeliveryMethod> createState() => _DeliveryMethodState();
}

class _DeliveryMethodState extends State<DeliveryMethod> {
  PaymentTypeEnum? dropdownValue;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BasketCubit, BasketState>(
      builder: (context, state) {
        return PrimaryDropdown<String>(
          initialOption: state.order.deliveryMethod,
          options: mapDeliveryMethods(),
          onChanged: serviceLocator<BasketCubit>().changeDeliveryMethod(
            deliveryMethod: '',
          ),
        );
      },
    );
  }

  List<SelectOption<String>> mapDeliveryMethods() {
    List<String> methods = ['Самовывоз'];
    List<SelectOption<String>> options = [];

    options.add(SelectOption(
      value: null,
      label: 'Выберите способ доставки',
    ));

    for (int i = 0; i < methods.length; i++) {
      options.add(SelectOption(
        value: methods[i],
        label: methods[i],
      ));
    }

    return options;
  }
}

// class _PickupDeliveryMethod extends StatelessWidget {
//   const _PickupDeliveryMethod({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<BasketCubit, BasketState>(
//       builder: (context, state) {
//         return Options(
//           label: 'Самовывоз',
//           state: false,
//           function: () {},
//         );
//       },
//     );
//   }
// }
