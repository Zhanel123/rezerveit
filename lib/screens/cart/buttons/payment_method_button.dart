import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/service_locator.dart';

class PaymentMethod extends StatefulWidget {
  const PaymentMethod({Key? key}) : super(key: key);

  @override
  State<PaymentMethod> createState() => _PaymentMethodState();
}

class _PaymentMethodState extends State<PaymentMethod> {
  PaymentTypeEnum? dropdownValue;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BasketCubit, BasketState>(
      builder: (context, state) {
        return PrimaryDropdown<String>(
          error: state.paymentMethod.invalid ? 'Обязательное поле' : '',
          initialOption: state.paymentMethod.value,
          options: mapPaymentMethods(),
          onChanged: (option) =>
              serviceLocator<BasketCubit>().updatePaymentMethodOrder(
            paymentMethod: option?.value,
          ),
        );
      },
    );
  }

  List<SelectOption<String>> mapPaymentMethods() {
    List<SelectOption<String>> options = [];

    options.add(SelectOption(
      value: null,
      label: 'Выберите способ оплаты',
    ));

    for (int i = 0; i < PaymentTypeEnum.values.length; i++) {
      options.add(SelectOption(
        value: PaymentTypeEnum.values[i].value,
        label: PaymentTypeEnum.values[i].value,
      ));
    }

    return options;
  }
}
