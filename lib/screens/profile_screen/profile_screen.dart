import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/profile_screen/widgets/name_field.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:seafarer/seafarer.dart';

import '../../core/text_styles.dart';
import '../widgets/my_app_bar.dart';
import 'buttons/add_address_button.dart';
import 'widgets/address_field.dart';
import 'widgets/bank_cards.dart';
import 'widgets/city_selected_field.dart';
import 'widgets/phone_field.dart';
import 'widgets/receive_notification_checkbox.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool isCheck = false;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileCubit, ProfileState>(
      listenWhen: (p, c) => p.updateProfileStatus != c.updateProfileStatus,
      listener: (context, state) {
        if (state.updateProfileStatus.isSubmissionSuccess) {
          FlushbarService().showSuccessMessage();
        } else if (state.updateProfileStatus.isSubmissionFailure) {
          FlushbarService().showErrorMessage(
            context: context,
          );
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Container(
            width: double.infinity,
            height: double.infinity,
            color: Color(0xffF5F5F5),
            child: SafeArea(
              child: SingleChildScrollView(
                padding: EdgeInsets.only(top: 19),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: MyAppBar(),
                    ),
                    // SizedBox(
                    //   height: 13,
                    // ),
                    // Padding(
                    //   padding: const EdgeInsets.only(
                    //       left: 16.0, right: 16.0, top: 8),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.end,
                    //     children: [
                    //       InkWell(
                    //         onTap: () async {
                    //           await context.setLocale(Locale('ru'));
                    //         },
                    //         child: Text('Рус',
                    //             style: TextStyle(
                    //               fontSize: 15,
                    //               color: Localizations.localeOf(context) ==
                    //                   Locale('ru')
                    //                   ? AppColors.pinkColor
                    //                   : AppColors.greyColor,
                    //               fontWeight: FontWeight.w500,
                    //             )),
                    //       ),
                    //       SizedBox(width: 20,),
                    //       InkWell(
                    //         onTap: () async {
                    //           await context.setLocale(Locale('kk'));
                    //         },
                    //         child: Text('Қаз',
                    //             style: TextStyle(
                    //               fontSize: 15,
                    //               color: Localizations.localeOf(context) ==
                    //                   Locale('kk')
                    //                   ? AppColors.pinkColor
                    //                   : AppColors.greyColor,
                    //               fontWeight: FontWeight.w500,
                    //             )),
                    //       ),
                    //
                    //     ],
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 16.0, right: 16.0, top: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "my_profile".tr(),
                            style: text700Size18Black,
                          ),
                          if ([
                            UserType.MANUFACTURER,
                          ].contains(state.profile?.userType))
                            InkWell(
                              onTap: () => serviceLocator<ProfileCubit>()
                                  .changeLocalRole(),
                              child: Text(
                                'Стать клиентом',
                                style: text700Size18Pink,
                              ),
                            ),
                          // if (state.profile?.userType == UserType.CUSTOMER)
                          //   InkWell(
                          //     child: Text(
                          //       'Стать продавцом',
                          //       style: text700Size18Pink,
                          //     ),
                          //   ),
                        ],
                      ),
                    ),
                    NameField(),
                    PhoneField(),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                      child: Text(
                        'Телефон подвержден!',
                        style: text400Size13Grey3,
                      ),
                    ),
                    // Padding(
                    //   padding:
                    //       const EdgeInsets.only(top: 24.0, right: 16, left: 16),
                    //   child: Text(
                    //     'Основной адрес доставки',
                    //     style: text400Size13Black,
                    //   ),
                    // ),
                    // Container(
                    //   child: CitySelectField(),
                    //   height: 50,
                    // ),
                    BlocConsumer<NewAddressCubit, NewAddressState>(
                      listenWhen: (prev, current) =>
                          prev.newAddressStatus != current.newAddressStatus ||
                          prev.changeAddressStatus !=
                              current.changeAddressStatus ||
                          prev.deletionStatus != current.deletionStatus,
                      listener: (context, state) async {
                        if (state.newAddressStatus.isSubmissionSuccess) {
                          Navigator.pop(context);
                          serviceLocator<ProfileCubit>().getMyAddresses();
                          FlushbarService().showSuccessMessage();
                        }
                        if (state.deletionStatus.isSubmissionSuccess) {
                          FlushbarService().showSuccessMessage();
                          serviceLocator<ProfileCubit>().getMyAddresses();
                        }
                        if (state.changeAddressStatus.isSubmissionSuccess) {
                          await FlushbarService()
                              .showSuccessMessage(context: context);
                          Navigator.pop(context);
                          serviceLocator<ProfileCubit>().getMyAddresses();
                        }
                      },
                      builder: (context, state) {
                        return AddressField();
                      },
                    ),
                    // AdditionalAddress(),
                    AddAddressButton(),
                    // _PreOrderDay(),
                    // ReceiveNotificationCheckbox(),
                    // Padding(
                    //   padding:
                    //       const EdgeInsets.only(left: 16, right: 16),
                    //   child: BankCards(),
                    // ),
                    SizedBox(
                      height: 24,
                    ),
                    BlocBuilder<ProfileCubit, ProfileState>(
                      builder: (context, state) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 70),
                          child: SizedBox(
                            width: double.infinity,
                            child: GradientButton(
                              labelText: 'Сохранить',
                              borderRadius: BorderRadius.circular(50),
                              isLoading: state
                                  .updateProfileStatus.isSubmissionInProgress,
                              onPressed: state.updateProfileFormzStatus.isValid
                                  ? serviceLocator<ProfileCubit>().update
                                  : serviceLocator<ProfileCubit>()
                                      .submissionValidate,
                            ),
                          ),
                        );
                      },
                    ),

                    InkWell(
                      onTap: () async {
                        await serviceLocator<ProfileCubit>().logOut();
                        Routes.router.navigate(
                          Routes.loginScreen,
                          navigationType: NavigationType.pushAndRemoveUntil,
                          removeUntilPredicate: (route) => false,
                        );
                      },
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(40.0),
                          child: Text(
                            'Выйти',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: AppColors.pinkColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 60,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class _PreOrderDay extends StatelessWidget {
  const _PreOrderDay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 8),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Предзаказы',
                    style: text400Size13Black,
                  ),
                  CheckRow(
                    label: 'Доступны',
                    handleValue: (value) {},
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              ProfileTextField(
                value: state.name.value,
                hintText: 'Александр',
                textInputType: TextInputType.name,
                onChange: serviceLocator<ProfileCubit>().handleNameChanged,
                hasError: state.name.status == FormzInputStatus.invalid,
                errorText: state.name.error != null ? 'Обязательное поле' : '',
              ),
            ],
          ),
        );
      },
    );
  }
}
