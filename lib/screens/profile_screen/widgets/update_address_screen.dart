import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

import '../../../core/text_styles.dart';
import '../../../routes.dart';

class UpdateAddressScreen extends StatefulWidget {
  const UpdateAddressScreen({Key? key}) : super(key: key);

  @override
  State<UpdateAddressScreen> createState() => _UpdateAddressScreenState();
}

class _UpdateAddressScreenState extends State<UpdateAddressScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewAddressCubit, NewAddressState>(
      listenWhen: (prev, current) =>
          prev.newAddressStatus != current.newAddressStatus,
      listener: (context, state) {
        if (state.newAddressStatus.isSubmissionSuccess) {
          Routes.router.popUntil(
            (route) => route.settings.name != Routes.homeScreen,
          );
          serviceLocator<ProfileCubit>().getMyAddresses();
          FlushbarService().showSuccessMessage();
        }
      },
      builder: (context, state) {
        return Container(
          width: double.infinity,
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height * 0.2,
            // maxHeight: MediaQuery.of(context).size.height * 0.6,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(28)),
            color: Colors.white,
          ),
          padding: EdgeInsets.all(18),
          margin:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: [
                InkWell(
                    onTap: () {
                      Routes.router.pop();
                    },
                    child: Container(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          color: Color(0xffEAEAEA),
                          borderRadius: BorderRadius.circular(28)),
                      child: Icon(
                        Icons.close,
                        color: Color(0xff7A7A7A),
                        size: 15,
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 28.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          'Изменить адрес доставки',
                          style: text700Size15Black,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'При оформлении заказа это очень удобно, чтобы не вводить каждый раз заново',
                        textAlign: TextAlign.center,
                        style: text400Size13Grey,
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: BlocBuilder<NewAddressCubit, NewAddressState>(
                          builder: (context, state) {
                            return ProfileTextField(
                              value: state.title.value,
                              hintText: 'Название доставки',
                              onChange: serviceLocator<NewAddressCubit>()
                                  .handleAddressTitleChanged,
                              hasError: state.title.status ==
                                  FormzInputStatus.invalid,
                              errorText: state.title.error != null
                                  ? 'Обязательное поле'
                                  : null,
                            );
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5, left: 16, right: 16),
                        child: Text(
                          'Например: Офис',
                          textAlign: TextAlign.start,
                          style: text400Size13Grey,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 11, left: 16, right: 16),
                        child: _CitySelect(),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 9, left: 16, right: 16),
                        child: BlocBuilder<NewAddressCubit, NewAddressState>(
                          builder: (context, state) {
                            return ProfileTextField(
                              value: state.address.value?.address ?? '',
                              hintText: 'Введите адрес',
                              onChange: serviceLocator<NewAddressCubit>()
                                  .handleChangeAddress,
                              hasError: state.address.status ==
                                  FormzInputStatus.invalid,
                              errorText: state.address.error != null
                                  ? 'Обязательное поле'
                                  : null,
                            );
                          },
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: 34, left: 26, right: 26, bottom: 30),
                          child: BlocBuilder<NewAddressCubit, NewAddressState>(
                            builder: (context, state) {
                              return GradientButton(
                                borderRadius: BorderRadius.circular(50),
                                isLoading: state
                                    .newAddressStatus.isSubmissionInProgress,
                                labelText: 'Изменить адрес',
                                onPressed: serviceLocator<NewAddressCubit>()
                                    .updateAddress,
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class _CitySelect extends StatelessWidget {
  const _CitySelect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewAddressCubit, NewAddressState>(
      builder: (context, state) {
        return PrimaryDropdown<City>(
          initialOption: state.newCity.value,
          options: state.cities
              .map((e) => SelectOption(
                    value: e,
                    label: e.name(context),
                  ))
              .toList(),
          onChanged: (option) => serviceLocator<NewAddressCubit>()
              .handleNewCityAdded(option?.value),
        );
      },
    );
  }
}
