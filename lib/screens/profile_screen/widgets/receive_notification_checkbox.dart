import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/widgets/check_row.dart';
import 'package:home_food/service_locator.dart';

class ReceiveNotificationCheckbox extends StatefulWidget {
  const ReceiveNotificationCheckbox({Key? key}) : super(key: key);

  @override
  State<ReceiveNotificationCheckbox> createState() =>
      _ReceiveNotificationCheckboxState();
}

class _ReceiveNotificationCheckboxState
    extends State<ReceiveNotificationCheckbox> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: CheckRow(
            label: 'Получать уведомления об акциях, новинках и скидках',
            isCheck: state.isReceiveNotification.value,
            handleValue: (value) => serviceLocator<ProfileCubit>().handleIsReceiveNotification,
            widthParameter: 230,
          ),
        );
      },
    );
  }
}
