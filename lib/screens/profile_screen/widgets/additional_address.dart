import 'package:flutter/material.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';

import '../../../core/text_styles.dart';

class AdditionalAddress extends StatelessWidget {
  const AdditionalAddress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 20, right: 16, left: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Дополнительный адрес доставки',
                  style: text400Size13Grey,
                ),
                TextButton(
                    onPressed: () {},
                    child: Text(
                      'Удалить',
                      style: text400Size13Grey,
                    )),
              ],
            ),
            ProfileTextField(
              value: 'Дом',
              onChange: (String) {},
              hasError: false,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 7.0),
              child: ProfileTextField(
                value: 'Алматы',
                onChange: (String) {},
                hasError: false,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 7.0),
              child: ProfileTextField(
                value: 'ул. Благородная, д. 5, кв. 7',
                onChange: (String) {},
                hasError: false,
              ),
            ),
          ],
        ));
  }
}
