import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';

class BankCards extends StatelessWidget {
  const BankCards({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Банковские карты',
            style: text400Size13Grey,
          ),
          SizedBox(height: 10,),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white
            ),
            height: 42,
            padding: EdgeInsets.only(left: 10, right: 13),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Row(
                    children: [
                        SvgPicture.asset(AppAssets.visa_icon),
                        SizedBox(width: 17,),
                        Text(
                          '**** **** **** 3455',
                          style: text500Size15BGrey,
                        ),
                    ],
                  ),
                  SvgPicture.asset(AppAssets.copy_icon)
                ],
              ),
            ),
          SizedBox(height: 10,),
          Text(
            'Добавить новую карту',
            style: text400Size13Pink,
          )
        ],
    )
    ,
    );
  }
}
