import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/screens/widgets/primary_dropdown.dart';
import 'package:home_food/service_locator.dart';

class CitySelectField extends StatefulWidget {
  const CitySelectField({Key? key}) : super(key: key);

  @override
  State<CitySelectField> createState() => _CitySelectFieldState();
}

class _CitySelectFieldState extends State<CitySelectField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 18, right: 18, top: 8),
      child: BlocBuilder<ProfileCubit, ProfileState>(
        builder: (context, state) {
          return PrimaryDropdown<City?>(
            options: [null, ...state.cities]
                .map(
                  (e) => SelectOption(
                    value: e,
                    label: e == null ? 'Выберите город' : e.name(context),
                  ),
                )
                .toList(),
            onChanged: (option) =>
                serviceLocator<ProfileCubit>().handleCityChanged(option?.value),
          );
        },
      ),
    );
  }
}
