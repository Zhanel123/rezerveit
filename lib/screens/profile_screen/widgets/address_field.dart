import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/screens/profile_screen/widgets/create_address_screen.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'update_address_screen.dart';

class AddressField extends StatelessWidget {
  const AddressField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      buildWhen: (p, c) =>
          p.addressList != c.addressList || p.addressStatus != c.addressStatus,
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 17, right: 16, left: 16),
              child: Text(
                'Дополнительный адрес доставки',
                style: text400Size13Grey,
              ),
            ),
            if (state.addressStatus.isSubmissionInProgress)
              Center(
                child: CircularProgressIndicator(
                  color: AppColors.primary,
                ),
              ),
            if (!state.addressStatus.isSubmissionInProgress)
              Column(
                children: mapAddressList(context, state.addressList),
              )
          ],
        );
      },
    );
  }

  void _handleUpdateAddressClicked(BuildContext context, Address address) {
    serviceLocator<NewAddressCubit>().selectAddress(address);
    showModalBottomSheet(
      context: context,
      builder: (context) => UpdateAddressScreen(),
      isScrollControlled: true,
      isDismissible: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(28)),
      ),
    );
  }

  mapAddressList(BuildContext context, List<Address> addressList) {
    List<Widget> children = [];
    for (int i = 0; i < addressList.length; i++) {
      children.add(Padding(
        key: Key(addressList[i].id + addressList[i].title),
        padding: EdgeInsets.only(top: 7, right: 16, left: 16),
        child: Column(
          children: [
            SizedBox(
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    serviceLocator<NewAddressCubit>()
                        .deleteAddress(addressId: addressList[i].id ?? '');
                  },
                  child: Text(
                    'Удалить',
                    style: text400Size13Grey,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 6,
            ),
            ProfileTextField(
              textInputType: TextInputType.text,
              value: addressList[i].title,
              hintText: '',
              disabled: true,
              onChange: (value) =>
                  serviceLocator<ProfileCubit>().changeAddressList(value, i),
              hasError: false,
            ),
            SizedBox(
              height: 6,
            ),
            Stack(
              children: [
                ProfileTextField(
                  textInputType: TextInputType.text,
                  value:
                      '${addressList[i].city?.name(context)}, ${addressList[i].address}',
                  hintText: '',
                  disabled: true,
                  onChange: (value) =>
                      serviceLocator<ProfileCubit>().changeAddressList(
                    value,
                    i,
                  ),
                  hasError: false,
                ),
                Positioned(
                  right: 0,
                  top: 0,
                  bottom: 0,
                  child: IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () =>
                        _handleUpdateAddressClicked(context, addressList[i]),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 6,
            )
          ],
        ),
      ));
    }

    return children;
  }
}
