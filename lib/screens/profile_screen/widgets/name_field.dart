import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/service_locator.dart';

class NameField extends StatelessWidget {
  const NameField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(top: 16.0, right: 16, left: 16),
          child: ProfileTextField(
            value: state.name.value,
            hintText: 'Александр',
            textInputType: TextInputType.name,
            onChange: serviceLocator<ProfileCubit>().handleNameChanged,
            hasError: state.name.status == FormzInputStatus.invalid,
            errorText: state.name.error != null ? 'Обязательное поле' : '',
          ),
        );
      },
    );
  }
}
