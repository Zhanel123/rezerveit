import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:home_food/core/text_styles.dart';

class ProfileTextField extends StatefulWidget {
  final String? value;
  final String? hintText;
  final double? width;
  final Widget? suffixWidget;
  final Function(String) onChange;
  final bool hasError;
  final String? errorText;
  final TextInputType? textInputType;
  final Border? border;
  final bool disabled;
  final List<TextInputFormatter>? inputFormatters;

  const ProfileTextField({
    Key? key,
    this.value = '',
    this.hintText,
    this.width = 0,
    this.suffixWidget,
    required this.onChange,
    this.hasError = false,
    this.errorText,
    this.textInputType,
    this.border,
    this.disabled = false,
    this.inputFormatters,
  }) : super(key: key);

  @override
  State<ProfileTextField> createState() => _ProfileTextFieldState();
}

class _ProfileTextFieldState extends State<ProfileTextField> {
  late TextEditingController _controller;

  /// Таймер для подсчета времени ввода
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: widget.value);

    _controller.addListener(() {
      if (_timer?.isActive ?? false) {
        _timer?.cancel();
      }
      _timer = Timer(Duration(milliseconds: 300), () {
        widget.onChange(_controller.text);
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: widget.hasError ? Border.all(color: Colors.red) : widget.border,
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      height: 40,
      width: double.infinity,
      child: TextField(
        keyboardType: widget.textInputType,
        inputFormatters: widget.inputFormatters,
        controller: _controller,
        enabled: !widget.disabled,
        decoration: InputDecoration(
          suffix: widget.suffixWidget,
          hintText: widget.hintText ?? '',
          hintStyle: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 15,
            color: Color(0xff838383),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(width: 1, color: Color(0xFFD8D8D8))),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(width: 1, color: Color(0xFFD8D8D8))),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(width: 1, color: Color(0xFFD8D8D8))),
          fillColor: Colors.white,
          filled: true,
          contentPadding:
              const EdgeInsets.only(left: 20, top: 10, bottom: 9, right: 14),
          // labelText: widget.value
        ),
        style: text500Size15Black,
        textAlign: TextAlign.start,
      ),
    );
  }
}
