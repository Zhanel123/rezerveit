import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/screens/widgets/bottom_sheet/primary_bottom_sheet.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';

import '../../../core/text_styles.dart';
import '../../../routes.dart';

class CreateAddressScreen extends StatefulWidget {
  const CreateAddressScreen({Key? key}) : super(key: key);

  @override
  State<CreateAddressScreen> createState() => _CreateAddressScreenState();
}

class _CreateAddressScreenState extends State<CreateAddressScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewAddressCubit, NewAddressState>(
      listenWhen: (prev, current) =>
          prev.newAddressStatus != current.newAddressStatus,
      listener: (context, state) {
        if (state.newAddressStatus.isSubmissionSuccess) {
          Routes.router.popUntil(
            (route) => route.settings.name != Routes.homeScreen,
          );
          serviceLocator<ProfileCubit>().getMyAddresses();
          FlushbarService().showSuccessMessage();
        }
      },
      builder: (context, state) {
        return PrimaryBottomSheet(
          contentPadding: EdgeInsets.all(18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Routes.router.pop();
                },
                child: Container(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                      color: Color(0xffEAEAEA),
                      borderRadius: BorderRadius.circular(28)),
                  child: Icon(
                    Icons.close,
                    color: Color(0xff7A7A7A),
                    size: 15,
                  ),
                ),
              ),
              SizedBox(height: 25),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 22.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(
                        'Добавить адрес доставки',
                        style: text700Size15Black,
                      ),
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    Text(
                      'При оформлении заказа это очень удобно, чтобы не вводить каждый раз заново',
                      textAlign: TextAlign.center,
                      style: text400Size13Grey,
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    BlocBuilder<NewAddressCubit, NewAddressState>(
                      builder: (context, state) {
                        return RoundedTextFormField(
                          value: state.title.value,
                          hintText: 'Название доставки',
                          onChange: serviceLocator<NewAddressCubit>()
                              .handleAddressTitleChanged,
                          hasError:
                              state.title.status == FormzInputStatus.invalid,
                          errorText: state.title.error != null
                              ? 'Обязательное поле'
                              : null,
                        );
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 5),
                      child: Text(
                        'Например: Офис',
                        textAlign: TextAlign.start,
                        style: text400Size13Grey,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: 11,
                      ),
                      child: _CitySelect(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: 9,
                      ),
                      child: BlocBuilder<NewAddressCubit, NewAddressState>(
                        builder: (context, state) {
                          return ProfileTextField(
                            disabled: state.newCity.invalid,
                            value: '${state.address.value?.address ?? ''}',
                            hintText: 'Введите адрес',
                            onChange: serviceLocator<NewAddressCubit>()
                                .handleChangeAddress,
                            hasError: state.address.status ==
                                FormzInputStatus.invalid,
                            errorText: state.address.error != null
                                ? 'Обязательное поле'
                                : null,
                          );
                        },
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                          top: 34,
                          left: 30,
                          right: 30,
                          bottom: 20,
                        ),
                        child: BlocBuilder<NewAddressCubit, NewAddressState>(
                            builder: (context, state) {
                          return SizedBox(
                            width: double.infinity,
                            child: GradientButton(
                              borderRadius: BorderRadius.circular(50),
                              isLoading:
                                  state.newAddressStatus.isSubmissionInProgress,
                              labelText: 'Добавить',
                              onPressed: serviceLocator<NewAddressCubit>()
                                  .createAddress,
                            ),
                          );
                        })),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class _CitySelect extends StatelessWidget {
  const _CitySelect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewAddressCubit, NewAddressState>(
      builder: (context, state) {
        return Container(
          height: 42,
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 18),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Color(0xFFD8D8D8)),
            color: Colors.white,
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<City>(
              icon: SvgPicture.asset(AppAssets.drop_down_icon),
              value: state.newCity.value,
              onChanged: serviceLocator<NewAddressCubit>().handleNewCityAdded,
              items: [
                DropdownMenuItem(
                  value: null,
                  child: Text(
                    'Выберите город',
                    style: text500Size15Black,
                  ),
                ),
                ...state.cities.map((e) => DropdownMenuItem(
                    value: e,
                    child: Text(
                      e.name(context),
                      style: text500Size15Black,
                    )))
              ],
            ),
          ),
        );
      },
    );
  }
}
