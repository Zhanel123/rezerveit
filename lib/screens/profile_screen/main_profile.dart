import 'package:flutter/material.dart';
import 'package:home_food/screens/profile_screen/widgets/profile_text_field.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';

import '../../core/text_styles.dart';
import '../widgets/bottom_sheet/my_pref_setting.dart';
import '../widgets/check_row.dart';
import '../widgets/my_app_bar.dart';
import 'buttons/add_address_button.dart';

class MainProfile extends StatefulWidget {
  const MainProfile({Key? key}) : super(key: key);

  @override
  State<MainProfile> createState() => _MainProfileState();
}

class _MainProfileState extends State<MainProfile> {
  bool isCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color(0xffF5F5F5),
        padding: EdgeInsets.only(top: 19),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: MyAppBar(),
                ),
                SizedBox(
                  height: 13,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, bottom: 12, top: 4),
                  child: Text(
                    'Мой профиль',
                    textAlign: TextAlign.start,
                    style: text700Size18Black,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    children: const [
                      Text(
                        'Кабинет продавца',
                        style: text400Size13Pink,
                      ),
                      SizedBox(
                        width: 32,
                      ),
                      Text(
                        'Стать контроллером',
                        style: text400Size13Pink,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 16.0, right: 16, left: 16),
                  child: ProfileTextField(
                    value: 'Александр',
                    onChange: (String) {},
                    hasError: false,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 13.0, right: 16, left: 16),
                  child: ProfileTextField(
                    value: '+7 999 345-55-55',
                    onChange: (String) {},
                    hasError: false,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, right: 16, left: 16),
                  child: Text(
                    'Телефон подвержден!',
                    style: text400Size13Grey3,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 24.0, right: 16, left: 16),
                  child: Text(
                    'Основной адрес доставки',
                    style: text400Size13Black,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 13.0, right: 16, left: 16),
                  child: ProfileTextField(
                    value: 'Астана',
                    onChange: (String) {},
                    hasError: false,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 7.0, right: 16, left: 16),
                  child: ProfileTextField(
                    value: 'ул. Тверская, 15А',
                    onChange: (String) {},
                    hasError: false,
                  ),
                ),
                AddAddressButton(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: CheckRow(
                    label: 'Получать уведомления об акциях, новинках и скидках',
                    isCheck: isCheck,
                    handleValue: (value) {
                      setState(() {
                        isCheck = value ?? false;
                      });
                    },
                    widthParameter: 300,
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 33, left: 16, right: 16),
              child: Center(
                  child: GradientButton(
                labelText: 'Сохранить',
              )),
            ),
          ],
        ),
      ),
    );
  }
}
