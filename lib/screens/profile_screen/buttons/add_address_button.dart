import 'package:flutter/material.dart';

import '../../../core/text_styles.dart';
import '../widgets/create_address_screen.dart';

class AddAddressButton extends StatefulWidget {
  const AddAddressButton({Key? key}) : super(key: key);

  @override
  State<AddAddressButton> createState() => _AddAddressButtonState();
}

class _AddAddressButtonState extends State<AddAddressButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 16, left: 16),
      child: TextButton(
        onPressed: _handleCreateAddressClicked,
        child: Text(
          'Добавить дополнительный адрес',
          style: text400Size13Pink,
        ),
      ),
    );
  }

  void _handleCreateAddressClicked() {
    showModalBottomSheet(
      context: context,
      builder: (context) => CreateAddressScreen(),
      isScrollControlled: true,
      isDismissible: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(28)),
      ),
    );
    // Scaffold.of(context).showBottomSheet<void>(
    //   (BuildContext context) {
    //     return CreateAddressScreen();
    //   },
    //   constraints: BoxConstraints(
    //     minHeight: MediaQuery.of(context).size.height * 0.2,
    //     maxHeight: MediaQuery.of(context).size.height * 0.3,
    //   ),
    // );
  }
}
