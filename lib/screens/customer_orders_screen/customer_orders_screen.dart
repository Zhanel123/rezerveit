import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/screens/customer_orders_screen/widgets/customer_orders_list_view.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

import '../widgets/my_app_bar.dart';
import '../widgets/search/search_field.dart';

class CustomerOrderList extends StatefulWidget {
  const CustomerOrderList({Key? key}) : super(key: key);

  @override
  State<CustomerOrderList> createState() => _CustomerOrderListState();
}

class _CustomerOrderListState extends State<CustomerOrderList>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    _controller = TabController(
      vsync: this,
      length: CustomerOrderStatusEnum.values.length,
    );

    _controller.addListener(() {
      print(_controller.index);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  late TabController _controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 18),
      height: double.infinity,
      color: Color(0xffF5F5F5),
      child: SafeArea(
        child: BlocBuilder<CustomerOrderCubit, CustomerOrderState>(
          builder: (context, state) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: MyAppBar(),
                ),
                SizedBox(
                  height: 13,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: RoundedTextFormField(
                    value: state.searchQuery,
                    onChange: serviceLocator<CustomerOrderCubit>()
                        .handleSearchQueryChanged,
                    hintText: 'Поиск по номеру заказа',
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 21.0, top: 19),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 16.0),
                        child: Text(
                          'Список заказов',
                          style: text700Size18Black,
                        ),
                      ),
                      BlocBuilder<CustomerOrderCubit, CustomerOrderState>(
                        builder: (context, state) {
                          return Text(
                            'Всего ${state.orders.length}',
                            style: text400Size13GreyBlack,
                          );
                        },
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      Column(
                        children: [
                          SizedBox(
                            width: double.infinity,
                            child: TabBar(
                              tabs: mapOrdersStatusTab(),
                              onTap: serviceLocator<CustomerOrderCubit>()
                                  .handleStatusFilterChanged,
                              isScrollable: true,
                              controller: _controller,
                              labelColor: Color(0xFF000000),
                              unselectedLabelColor: Color(0x80000000),
                              unselectedLabelStyle:
                                  TextStyle(color: Color(0x80000000)),
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 0),
                              labelStyle: TextStyle(
                                color: Color(0xFF000000),
                                fontWeight: FontWeight.w700,
                              ),
                              indicator: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                // Creates border
                                // color: Colors.greenAccent,
                              ),
                            ),
                          ),
                        ],
                      ),
                      if (state.orders.isEmpty)
                        Column(
                          children: [
                            Center(
                              child: Text(
                                'У Вас еще нет заказов.',
                                style: text400Size13GreyBlack,
                              ),
                            )
                          ],
                        ),
                      if (state.orders.isNotEmpty)
                        Expanded(
                          child: BlocBuilder<CustomerOrderCubit,
                                  CustomerOrderState>(
                              builder: (context, state) {
                                return CustomerOrdersListView(
                                  orders: state.orders,
                                  onScrollEnd: () =>
                                      serviceLocator<CustomerOrderCubit>()
                                          .getCustomerOrders(append: true),
                                );
                              }),
                        )
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  List<Tab> mapOrdersStatusTab() {
    List<Tab> options = [];
    for (int i = 0; i < CustomerOrderStatusEnum.values.length; i++) {
      options.add(Tab(
        text: CustomerOrderStatusEnum.values[i].label ?? '',
      ));
    }

    return options;
  }
}
