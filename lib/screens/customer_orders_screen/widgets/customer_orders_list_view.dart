import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/models/order/order.dart';

import 'order_list_item.dart';

class CustomerOrdersListView extends StatefulWidget {
  const CustomerOrdersListView({
    Key? key,
    this.orders = const [],
    this.onScrollEnd,
  }) : super(key: key);

  final List<OrderModel> orders;
  final VoidCallback? onScrollEnd;

  @override
  State<CustomerOrdersListView> createState() => _CustomerOrdersListViewState();
}

class _CustomerOrdersListViewState extends State<CustomerOrdersListView> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() {
      var triggerFetchMoreSize =
          0.7 * scrollController.position.maxScrollExtent;

      if (scrollController.position.pixels > triggerFetchMoreSize) {
        widget.onScrollEnd?.call();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: scrollController,
      padding: EdgeInsets.symmetric(horizontal: 16),
      children: mapOrdersToWidget(),
    );
  }

  List<Widget> mapOrdersToWidget() {
    final List<Widget> children = [];

    for (int i = 0; i < widget.orders.length; i++) {
      children.add(OrderListItem(
        order: widget.orders[i],
      ));
    }

    return children;
  }
}
