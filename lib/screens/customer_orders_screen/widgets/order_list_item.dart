import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/order/order_screen.dart';
import 'package:intl/intl.dart';

class OrderListItem extends StatelessWidget {
  final OrderModel order;

  const OrderListItem({
    Key? key,
    required this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Routes.router.navigate(Routes.orderScreen,
            args: OrderScreenArgs(
              order,
            ));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 3),
        height: 74,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white
              .withOpacity(order.status == OrderStatusEnum.DELIVERED ? 0.6 : 1),
        ),
        child: Row(
          children: [
            if (order.status != OrderStatusEnum.DELIVERED &&
                order.status != OrderStatusEnum.CANCELED)
              Container(
                width: 3,
                color: AppColors.pinkColor,
              ),
            Padding(
              padding: const EdgeInsets.only(left: 21.0, top: 18, bottom: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    // '#${order.orderNumber}',
                    // '#3789983',
                    '#${order.orderNumber}',
                    textAlign: TextAlign.start,
                    style: text700Size15Black,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  // Text(
                  //   'От ${DateFormat('dd.MM.yy hh:mm').format(order.timeOfDate)}',
                  //   style: text400Size13Grey,
                  // ),
                  Text(
                    'От ${order.timeOfDate == DateTime(0) ? '' : _dateTimeFormatter(order.timeOfDate)}',
                    style: text400Size13Grey,
                  )
                ],
              ),
            ),
            Spacer(),
            Container(
              width: 110,
              padding: const EdgeInsets.only(right: 14.0, top: 16, bottom: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${order.total} ₸',
                    textAlign: TextAlign.start,
                    style: order.status == OrderStatusEnum.DELIVERED
                        ? text700Size15Black
                        : text700Size15Pink,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    order.status.label ?? '',
                    style: text400Size13Grey.copyWith(
                      color: order.status.color,
                    ),
                  )
                ],
              ),
            ),
            // SizedBox(width: 16,),
            (![
              OrderStatusEnum.DELIVERED,
              OrderStatusEnum.CANCELED,
              OrderStatusEnum.REFUND,
            ].contains(order.status))
                ? Icon(
                    Icons.arrow_forward_ios,
                    color: Color(0xff7A7A7A),
                    size: 10,
                  )
                : order.grade?.rating == null
                    ? InkWell(
                        onTap: () {},
                        child: SvgPicture.asset(AppAssets.star_zero),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {},
                            child: SvgPicture.asset(AppAssets.stars),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            '${order.grade?.rating?.toDouble() ?? 0}',
                            style: text400Size13Black,
                          )
                        ],
                      ),
            // else if (status == true && order.rating == null)
            //   InkWell(
            //     onTap: () {},
            //     child: SvgPicture.asset(AppAssets.star_zero),
            //   )
            // else if (status == true && order.rating != null)
            //   Padding(
            //     padding: const EdgeInsets.only(top: 18, bottom: 14),
            //     child: Column(
            //       children: [
            //         InkWell(
            //           onTap: () {},
            //           child: SvgPicture.asset(AppAssets.stars),
            //         ),
            //         SizedBox(
            //           height: 5,
            //         ),
            //         ///TODO Rating of order
            //         // Text(
            //         //   '${order.rating}',
            //         //   style: text400Size13Black,
            //         // )
            //       ],
            //     ),
            //   ),
            SizedBox(
              width: 14,
            )
          ],
        ),
      ),
    );
  }

  String _dateTimeFormatter(DateTime dateTime) {
    String formattedDate = DateFormat('yyyy.MM.dd kk:mm').format(dateTime);
    return formattedDate;
  }
}
