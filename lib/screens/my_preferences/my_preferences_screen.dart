import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/dish_catalog/cubit.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/food_card.dart';
import 'package:home_food/screens/my_preferences/widgets/shops_button.dart';
import 'package:home_food/screens/widgets/rounded_text_field.dart';
import 'package:home_food/service_locator.dart';

import '../../core/app_colors.dart';
import '../../core/text_styles.dart';
import '../widgets/buttons/filter_button.dart';
import '../widgets/buttons/settings_button.dart';
import '../widgets/my_app_bar.dart';

class MyPreferencesScreen extends StatefulWidget {
  const MyPreferencesScreen({
    Key? key,
    this.autoFocus = false,
  }) : super(key: key);

  final bool autoFocus;

  @override
  State<MyPreferencesScreen> createState() => _MyPreferencesScreenState();
}

class _MyPreferencesScreenState extends State<MyPreferencesScreen>
    with TickerProviderStateMixin {
  late TabController _controller;
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() {
      var triggerFetchMoreSize =
          0.7 * scrollController.position.maxScrollExtent;

      if (scrollController.position.pixels > triggerFetchMoreSize) {
        serviceLocator<DishCatalogCubit>().getDishes(append: true);
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 18),
        color: Color(0xffF5F5F5),
        child: SafeArea(
          child: ListView(
            controller: scrollController,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: MyAppBar(),
              ),
              SizedBox(
                height: 13,
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: _SearchField(
                          autoFocus: widget.autoFocus,
                        ),
                      ),
                      FilterButton(),
                    ],
                  )),
              SizedBox(
                height: 8,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    // Text(
                    //   'Популярные блюда',
                    //   style: text700Size18Black,
                    // ),
                    // SizedBox(
                    //   width: 13,
                    // ),
                    ShopsButton(),
                  ],
                ),
              ),
              // SizedBox(
              //   height: 9,
              // ),
              // Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 16.0),
              //   child: PopularFoods(),
              // ),
              // Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 16.0),
              //   child: MerchantsButton(),
              // ),
              BlocBuilder<DishCatalogCubit, DishCatalogState>(
                builder: (context, state) {
                  final index = state.selectedDishType != ""
                      ? state.dishTypes.indexWhere(
                            (element) => element.id == state.selectedDishType,
                          ) +
                          1
                      : 0;
                  _controller = TabController(
                    vsync: this,
                    length: state.dishTypes.length + 1,
                    initialIndex: index != -1 ? index : 0,
                  );
                  _controller.addListener(_tabListener);

                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      children: [
                        TabBar(
                          tabs: [
                            null,
                            ...state.dishTypes,
                          ]
                              .map(
                                (e) => Tab(
                                  text: e?.name == null
                                      ? 'Все'
                                      : e?.name(context),
                                ),
                              )
                              .toList(),
                          onTap: serviceLocator<DishCatalogCubit>()
                              .handleDishTypeChanged,
                          isScrollable: true,
                          controller: _controller,
                          labelColor: Colors.black,
                          unselectedLabelStyle: TextStyle(
                            color: Color(0x80000000),
                          ),
                          labelStyle: text700Size15Black,
                          indicator: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            // Creates border
                            // color: Colors.greenAccent,
                          ),
                        ),
                        ...state.dishes.map(
                          (e) => FoodCard(
                            item: e,
                          ),
                        ),
                        if (state.isLoading)
                          CircularProgressIndicator(
                            color: AppColors.pinkColor,
                          )
                      ],
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void _tabListener() => serviceLocator<DishCatalogCubit>()
      .handleDishTypeChanged(_controller.index);
}

class _SearchField extends StatelessWidget {
  final bool autoFocus;

  const _SearchField({
    Key? key,
    this.autoFocus = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DishCatalogCubit, DishCatalogState>(
      builder: (context, state) {
        return RoundedTextFormField(
          value: state.searchText,
          hintText: 'Поиск по еде',
          autofocus: autoFocus,
          onChange: serviceLocator<DishCatalogCubit>().handleSearchChanged,
        );
      },
    );
  }
}
