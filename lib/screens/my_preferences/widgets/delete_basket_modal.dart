import 'package:flutter/material.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/widgets/buttons/gradient_button.dart';

class DeleteBasketModal extends StatelessWidget {
  const DeleteBasketModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(28)),
        color: Colors.white,
      ),
      padding: EdgeInsets.all(18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          InkWell(
              onTap: () {
                Routes.router.pop();
              },
              child: Container(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                    color: Color(0xffEAEAEA),
                    borderRadius: BorderRadius.circular(28)),
                child: Icon(
                  Icons.close,
                  color: Color(0xff7A7A7A),
                  size: 15,
                ),
              )),
          Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      'Удалить все продукты из корзины',
                      textAlign: TextAlign.center,
                      style: text700Size15Black,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: GradientButton(
                      labelText: 'Окей',
                      onPressed: () { },
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
