import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/popular_dishes/cubit.dart';
import 'package:home_food/core/app_assets.dart';
import 'package:home_food/models/food/food.dart';
import 'package:home_food/screens/home_screen/client_home_screen/widgets/food_card.dart';
import 'package:home_food/service_locator.dart';

class PopularFoods extends StatefulWidget {
  const PopularFoods({Key? key}) : super(key: key);

  @override
  State<PopularFoods> createState() => _PopularFoodsState();
}

class _PopularFoodsState extends State<PopularFoods> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    serviceLocator<PopularDishesCubit>().getPopularDishes(append: false);

    scrollController.addListener(scroollListener);
    super.initState();
  }

  void scroollListener() {
    // когда доходим до 70%
    var triggerFetchMoreSize = 0.7 * scrollController.position.maxScrollExtent;

    if (scrollController.position.pixels > triggerFetchMoreSize) {
      serviceLocator<PopularDishesCubit>().getPopularDishes(append: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PopularDishesCubit, PopularDishesState>(
      builder: (context, state) {
        return SizedBox(
          height: 209,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              ...state.popularDishes.map(
                (e) => SizedBox(
                  width: 273,
                  height: 209,
                  child: FoodCard(
                    item: e,
                    margin: EdgeInsets.only(bottom: 41, right: 10),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
