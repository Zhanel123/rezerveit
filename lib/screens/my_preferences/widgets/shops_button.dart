import 'package:flutter/material.dart';
import 'package:home_food/core/app_colors.dart';
import 'package:home_food/core/text_styles.dart';
import 'package:home_food/routes.dart';

class ShopsButton extends StatelessWidget {
  const ShopsButton({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
        side: BorderSide(color: AppColors.pinkColor, width: 1),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(28.0)
        )
      ),
        onPressed: () { Routes.router.navigate(Routes.shopsList); },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 3),
          child: Text(
            'Поставщики',
            textAlign: TextAlign.center,
            style: text400Size13Black,
          ),
        )
    );
  }
}
