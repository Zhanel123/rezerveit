import 'package:home_food/screens/cart/cart_screen.dart';
import 'package:home_food/screens/chat_screen/chat_screen.dart';
import 'package:home_food/screens/food_detaill/food_detail.dart';
import 'package:home_food/screens/highlights/highlights_screen.dart';
import 'package:home_food/screens/home_screen/merchant_home_screen.dart';
import 'package:home_food/screens/initial_screen.dart';
import 'package:home_food/screens/login/login_screen.dart';
import 'package:home_food/screens/login/login_sms_verification_screen.dart';
import 'package:home_food/screens/main_screen/main_screen.dart';
import 'package:home_food/screens/map_screen/flutter_map_screen.dart';
import 'package:home_food/screens/map_screen/map_screen.dart';
import 'package:home_food/screens/merchant_screens/merchant_add_food_screen/merchant_add_foods_screen.dart';
import 'package:home_food/screens/merchant_screens/merchant_chats_screen/merchant_chats_list_screen.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/merchant_foods_screen.dart';
import 'package:home_food/screens/merchant_screens/merchant_foods_screen/widgets/merchant_food_detail.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/merchant_orders_screen.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/widgets/merchant_order_screen.dart';
import 'package:home_food/screens/merchant_screens/merchant_profile/merchant_profile_screen.dart';
import 'package:home_food/screens/merchants_menu/merchant_menu.dart';
import 'package:home_food/screens/merchants_menu/merchants_list.dart';
import 'package:home_food/screens/my_preferences/my_preferences_screen.dart';
import 'package:home_food/screens/notifications_screen/notifications_screen.dart';
import 'package:home_food/screens/order/order_screen.dart';
import 'package:home_food/screens/profile_screen/profile_screen.dart';
import 'package:home_food/screens/registration/registration_screen.dart';
import 'package:home_food/screens/registration/registration_sms_verification_screen.dart';
import 'package:seafarer/seafarer.dart';

import 'screens/chats_list/chats_list_screen.dart';
import 'screens/customer_orders_screen/customer_orders_screen.dart';
import 'screens/home_screen/favourite_foods/favourite_foods.dart';
import 'screens/merchant_detail/merchant_detail.dart';
import 'screens/merchant_screens/merchant_food_edit_screen/merchant_food_edit_screen.dart';

class Routes {
  Routes._();

  static final router = Seafarer(
    options: const SeafarerOptions(
      defaultTransitionDuration: Duration(milliseconds: 100),
    ),
  );

  static initRoutes() {
    router.addRoutes([
      SeafarerRoute(
        name: registrationScreen,
        builder: (context, args, params) => RegistrationWidget(),
      ),
      SeafarerRoute(
        name: loginScreen,
        builder: (context, args, params) => LoginScreen(),
      ),
      SeafarerRoute(
        name: registrationSmsVerificationScreen,
        builder: (context, args, params) => RegistrationSmsVerificationScreen(),
      ),
      SeafarerRoute(
        name: loginSmsVerificationScreen,
        builder: (context, args, params) => LoginSmsVerificationScreen(),
      ),
      SeafarerRoute(
        name: loginSmsVerificationScreen,
        builder: (context, args, params) => LoginSmsVerificationScreen(),
      ),
      SeafarerRoute(
        name: initialScreen,
        builder: (context, args, params) => InitialScreen(),
      ),
      SeafarerRoute(
        name: highlightsScreen,
        builder: (context, args, params) => HighlightsScreen(),
      ),
      SeafarerRoute(
        name: homeScreen,
        builder: (context, args, params) => MainScreen(),
      ),
      SeafarerRoute(
        name: myPreferencesScreen,
        builder: (context, args, params) => MyPreferencesScreen(),
      ),
      SeafarerRoute(
        name: shopsList,
        builder: (context, args, params) => MerchantsList(),
      ),
      SeafarerRoute(
        name: merchantMenu,
        builder: (context, args, params) => MerchantMenu(
          args: args as MerchantMenuArgs,
        ),
      ),
      SeafarerRoute(
        name: merchantDetail,
        builder: (context, args, params) => MerchantDetail(
          args: args as MerchantDetailArgs,
        ),
      ),
      SeafarerRoute(
        name: favoriteFoods,
        builder: (context, args, params) => FavoriteFoods(),
      ),
      SeafarerRoute(
        name: orderList,
        builder: (context, args, params) => CustomerOrderList(),
      ),
      SeafarerRoute(
        name: profileScreen,
        builder: (context, args, params) => ProfileScreen(),
      ),
      SeafarerRoute(
        name: cartScreen,
        builder: (context, args, params) => CartScreen(),
      ),
      SeafarerRoute(
        name: chatsListScreen,
        builder: (context, args, params) => ChatsListScreen(),
      ),
      SeafarerRoute(
        name: foodDetail,
        builder: (context, args, params) => FoodDetail(
          args: args as FoodDetailArgs,
        ),
      ),
      SeafarerRoute(
        name: merchantFoodDetail,
        builder: (context, args, params) => MerchantFoodDetail(
          args: args as MerchantFoodDetailArgs,
        ),
      ),
      SeafarerRoute(
        name: orderScreen,
        builder: (context, args, params) => OrderScreen(
          args: args as OrderScreenArgs,
        ),
      ),
      SeafarerRoute(
        name: merchantHomeScreen,
        builder: (context, args, params) => MerchantHomeScreen(),
      ),
      SeafarerRoute(
        name: merchantOrdersScreen,
        builder: (context, args, params) => MerchantOrdersScreen(),
      ),
      SeafarerRoute(
        name: merchantFoodsScreen,
        builder: (context, args, params) => MerchantFoodsScreen(),
      ),
      SeafarerRoute(
        name: merchantAddFoodScreen,
        builder: (context, args, params) => MerchantAddFoodScreen(),
      ),
      SeafarerRoute(
        name: merchantProfileScreen,
        builder: (context, args, params) => MerchantProfileScreen(),
      ),
      SeafarerRoute(
        name: merchantEditScreen,
        builder: (context, args, params) => MerchantFoodEditScreen(),
      ),
      SeafarerRoute(
        name: merchantOrderScreen,
        builder: (context, args, params) => MerchantOrderScreen(
          args: args as MerchantOrderScreenArgs,
        ),
      ),
      SeafarerRoute(
        name: merchantChatScreen,
        builder: (context, args, params) => MerchantChatListScreen(),
      ),
      SeafarerRoute(
        name: chatScreen,
        builder: (context, args, params) => ChatScreen(
          args: args as ChatScreenArgs,
        ),
      ),
      SeafarerRoute(
        name: mapGlScreen,
        builder: (context, args, params) => MapBoxMapsScreen(),
      ),
      SeafarerRoute(
        name: mapScreen,
        builder: (context, args, params) => MapScreen(),
      ),
      SeafarerRoute(
        name: notificationsScreen,
        builder: (context, args, params) => NotificationsScreen(),
      ),
    ]);
  }

  static final String mapScreen = '/mapScreen';
  static final String mapGlScreen = '/mapGlScreen';
  static final String registrationScreen = '/registrationScreen';
  static final String loginScreen = '/loginScreen';
  static final String registrationSmsVerificationScreen =
      '/registrationSmsVerificationScreen';
  static final String loginSmsVerificationScreen =
      '/loginSmsVerificationScreen';
  static final String initialScreen = '/initialScreen';
  static final String highlightsScreen = '/highlightsScreen';
  static final String homeScreen = '/homeScreen';
  static final String myPreferencesScreen = '/myPreferencesScreen';
  static final String shopsList = '/shopsList';
  static final String merchantMenu = '/merchantMenu';
  static final String merchantScreen = '/merchantScreen';
  static final String merchantDetail = '/merchantDetail';
  static final String foodDetail = '/foodDetail';
  static final String merchantFoodDetail = '/merchantFoodDetail';
  static final String favoriteFoods = '/favoriteFoods';
  static final String orderList = '/orderList';
  static final String profileScreen = '/profileScreen';
  static final String cartScreen = '/cartScreen';
  static final String chatsListScreen = '/chatsListScreen';
  static final String notificationsScreen = '/notificationsScreen';
  static final String orderScreen = '/orderScreen';

  static final String merchantHomeScreen = '/merchantHomeScreen';
  static final String merchantOrdersScreen = '/merchantHomeScreen';
  static final String merchantFoodsScreen = '/merchantFoodsScreen';
  static final String merchantAddFoodScreen = '/merchantAddFoodScreen';
  static final String merchantProfileScreen = '/merchantProfileScreen';
  static final String merchantEditScreen = '/merchantEditScreen';
  static final String merchantOrderScreen = '/merchantOrderScreen';
  static final String mechantDishReplace = '/merchantOrderScreen';
  static final String merchantChatScreen = '/merchantChatScreen';
  static final String chatScreen = '/chatScreen';
}
