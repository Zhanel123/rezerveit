part of 'cubit.dart';

class MerchantDetailState extends Equatable {
  final Profile? merchant;
  final bool hasNext;
  final int page;
  final int size;
  final bool isLoading;
  final String? searchText;
  final String? selectedDishType;
  final List<Dish> dishes;
  final List<DishType> dishTypes;
  final List<DishType> excludedDishTypes;
  final List<Preference> preferences;
  final List<Preference> selectedPreferences;
  final double? priceFrom;
  final double? priceTo;
  final bool? showNew;
  final bool? hasSale;
  final SortFieldEnum priceSortDirection;
  final bool? showPopular;
  final int? dishCount;

  MerchantDetailState({
    this.merchant,
    this.hasNext = true,
    this.page = 0,
    this.size = 0,
    this.isLoading = false,
    this.searchText = '',
    this.selectedDishType = '',
    this.dishTypes = const [],
    this.dishes = const [],
    this.excludedDishTypes = const [],
    this.preferences = const [],
    this.selectedPreferences = const [],
    this.priceFrom = 5,
    this.priceTo = 30000,
    this.showNew = false,
    this.hasSale = false,
    this.priceSortDirection = SortFieldEnum.createdDate,
    this.showPopular = true,
    this.dishCount = 0,
  });

  @override
  List<Object?> get props => [
        page,
        size,
        merchant,
        dishes,
        hasNext,
        isLoading,
        searchText,
        selectedDishType,
        excludedDishTypes,
        selectedPreferences,
        priceFrom,
        priceTo,
        showNew,
        hasSale,
        priceSortDirection,
        showPopular,
        dishCount,
        dishTypes,
      ];

  MerchantDetailState copyWith({
    Profile? merchant,
    int? page,
    int? size,
    bool? hasNext,
    bool? isLoading,
    String? searchText,
    String? selectedDishType,
    List<DishType>? dishTypes,
    List<DishType>? excludedDishTypes,
    List<Dish>? dishes,
    List<Preference>? preferences,
    List<Preference>? selectedPreferences,
    double? priceFrom,
    double? priceTo,
    bool? showNew,
    bool? hasSale,
    SortFieldEnum? priceSortDirection,
    bool? showPopular,
    int? dishCount,
  }) =>
      MerchantDetailState(
        page: page ?? this.page,
        size: size ?? this.size,
        dishes: dishes ?? this.dishes,
        merchant: merchant ?? this.merchant,
        hasNext: hasNext ?? this.hasNext,
        isLoading: isLoading ?? this.isLoading,
        searchText: searchText ?? this.searchText,
        preferences: preferences ?? this.preferences,
        selectedDishType: selectedDishType ?? this.selectedDishType,
        dishTypes: dishTypes ?? this.dishTypes,
        excludedDishTypes: excludedDishTypes ?? this.excludedDishTypes,
        selectedPreferences: selectedPreferences ?? this.selectedPreferences,
        priceFrom: priceFrom ?? this.priceFrom,
        priceTo: priceTo ?? this.priceTo,
        showNew: showNew ?? this.showNew,
        hasSale: hasSale ?? this.hasSale,
        priceSortDirection: priceSortDirection ?? this.priceSortDirection,
        showPopular: showPopular ?? this.showPopular,
        dishCount: dishCount ?? this.dishCount,
      );
}
