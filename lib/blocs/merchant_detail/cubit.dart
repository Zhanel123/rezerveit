import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/dish_filter_request_dto/dish_filter_request_dto.dart';
import 'package:home_food/data/dto/dish_type_dto/mapper/dish_type_dto_mapper.dart';
import 'package:home_food/data/dto/preference_dto/mapper/preference_dto_mapper.dart';
import 'package:home_food/data/repositories/dish_repository/dish_repository.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/profile/profile.dart';

import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class MerchantDetailCubit extends Cubit<MerchantDetailState> {
  final DishRepository _dishRepository;
  final DictionaryCubit _dictionaryCubit;

  MerchantDetailCubit(
    this._dishRepository,
    this._dictionaryCubit,
  ) : super(MerchantDetailState()) {
    emit(state.copyWith(
      dishTypes: _dictionaryCubit.state.dishTypes,
      preferences: _dictionaryCubit.state.preferences,
    ));
    _dictionaryCubit.stream.listen((event) {
      emit(state.copyWith(
        dishTypes: event.dishTypes,
        selectedDishType:
            event.dishTypes.isNotEmpty ? event.dishTypes.first.id : null,
        preferences: event.preferences,
      ));
    });
    _dictionaryCubit.getDishTypes();
    _dictionaryCubit.getPreferences();
    fetchDishCardsByMerchant();
  }

  void handleMerchantChanged(Profile merchant) {
    emit(state.copyWith(
      merchant: merchant,
    ));
    handleDishTypeChanged(0);
  }

  Future<void> fetchDishCardsByMerchant({
    bool append = false,
  }) async {
    if ((append && !state.hasNext) || state.isLoading) {
      return;
    }
    if (append) {
      emit(state.copyWith(page: state.page + 1));
    } else {
      emit(state.copyWith(
        page: 0,
      ));
    }
    emit(state.copyWith(isLoading: true));
    try {
      final response = await _dishRepository.getDishes(
        page: state.page,
        size: 10,
        filter: DishFilterRequestDTO(
          searchText: state.searchText,
          manufactureId: state.merchant?.id,
          dishTypeId: state.selectedDishType,
          notDishTypeIds: dishTypeListToDTO(state.excludedDishTypes),
          foodPreferenceIds: preferenceDTOListMapper(state.selectedPreferences),
          priceFrom: state.priceFrom,
          priceTo: state.priceTo,
          showNew: state.showNew,
          hasSale: state.hasSale,
          sortBy: state.priceSortDirection,
          showPopular: state.showPopular,
        ),
      );

      emit(state.copyWith(
        dishes: append
            ? [
                ...state.dishes,
                ...response.content,
              ]
            : response.content,
        hasNext: response.hasNext ?? false,
      ));

      if (response.hasNext ?? false) {
        fetchDishCardsByMerchant(append: true);
      }
    } catch (e) {
      logger.e(e);
    }
    emit(state.copyWith(
      isLoading: false,
    ));
  }

  void handleDishTypeChanged(int index) {
    emit(state.copyWith(
      selectedDishType: state.dishTypes[index].id,
    ));

    fetchDishCardsByMerchant();
  }
}
