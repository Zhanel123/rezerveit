import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:home_food/data/dto/merchant_dto/mapper/merchant_mapper.dart';
import 'package:home_food/models/get_merchants_request/get_merchants_request.dart';
import 'package:home_food/models/merchant/merchant.dart';
import 'package:home_food/models/profile/profile.dart';

import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class MerchantCubit extends Cubit<MerchantState> {
  MerchantCubit() : super(MerchantState());

  Timer? _searchTimer;

  getMerchantList({
    bool append = false,
  }) async {
    if ((append && state.hasNext == false) || state.isLoading) {
      return;
    }
    if (append) {
      emit(state.copyWith(page: state.page + 1));
    } else {
      emit(state.copyWith(
        page: 0,
      ));
    }
    emit(state.copyWith(isLoading: true));

    try {
      final response = await serviceLocator<RestClient>()
          .getMerchantsList(GetMerchantsRequest(
        page: state.page,
        size: 10,
        searchText: state.searchText,
      ));

      List<Merchant> responseContent = merchantListMapper(response.content);
      List<Merchant> _merchants = append
          ? [
              ...state.merchants,
              ...responseContent,
            ]
          : responseContent;

      emit(state.copyWith(
        merchants: _merchants,
        hasNext: response.hasNext ?? false,
      ));

      emit(state.copyWith(isLoading: false));
      if (response.hasNext ?? false) {
        getMerchantList(append: true);
      }
    } catch (e) {
      logger.e(e);
    }

    emit(state.copyWith(isLoading: false));
  }

  handleSearchChanged(String value) {
    emit(state.copyWith(
      searchText: value,
    ));
    if (_searchTimer?.isActive ?? false) {
      _searchTimer!.cancel();
    }
    _searchTimer = Timer(Duration(milliseconds: 600), getMerchantList);
  }
}
