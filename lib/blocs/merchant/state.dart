part of 'cubit.dart';

class MerchantState extends Equatable {
  final List<Merchant> merchants;
  final bool hasNext;
  final String? searchText;
  final int page;
  final int size;
  final bool isLoading;

  MerchantState({
    this.merchants = const [],
    this.hasNext = true,
    this.searchText,
    this.page = 0,
    this.size = 0,
    this.isLoading = false,
  });

  @override
  List<Object?> get props => [
        searchText,
        page,
        size,
        merchants,
        hasNext,
        isLoading,
      ];

  MerchantState copyWith({
    List<Merchant>? merchants,
    String? searchText,
    int? page,
    int? size,
    bool? hasNext,
    bool? isLoading,
  }) =>
      MerchantState(
        searchText: searchText ?? this.searchText,
        page: page ?? this.page,
        size: size ?? this.size,
        merchants: merchants ?? this.merchants,
        hasNext: hasNext ?? this.hasNext,
        isLoading: isLoading ?? this.isLoading,
      );
}
