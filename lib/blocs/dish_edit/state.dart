part of 'cubit.dart';

class DishEditState extends Equatable {
  final Required<String> id;
  final Required<String> name;
  final Required<int> price;
  final Required<int> weight;
  final Required<String> description;
  final Required<int> portionAmount;
  final Required<int> minDishAmount;
  final String dishTypeId;
  final WeightTypeEnum weightType;
  final ListNotEmpty<UploadedImage> uploadedImages;
  final List<DishType> dishTypes;
  final Required<DishType> dishType;
  final List<Ingredient> ingredients;
  final ListNotEmpty<Ingredient> selectedIngredients;
  final List<Preference> preferences;
  final ListNotEmpty<Preference> selectedPreferences;
  final FormzStatus status;
  final BoolRequired isHalfPortion;
  final BoolRequired isPreOrderDish;
  final String error;

  DishEditState({
    this.id = const Required<String>.pure(),
    this.name = const Required<String>.pure(),
    this.price = const Required<int>.pure(),
    this.weight = const Required.pure(),
    this.description = const Required<String>.pure(),
    this.portionAmount = const Required<int>.pure(),
    this.minDishAmount = const Required<int>.pure(),
    this.dishTypeId = '',
    this.error = '',
    this.weightType = WeightTypeEnum.gram,
    this.uploadedImages = const ListNotEmpty.pure(),
    this.dishTypes = const [],
    this.dishType = const Required.pure(),
    this.ingredients = const [],
    this.selectedIngredients = const ListNotEmpty.pure(),
    this.preferences = const [],
    this.selectedPreferences = const ListNotEmpty.pure(),
    this.status = FormzStatus.pure,
    this.isHalfPortion = const BoolRequired.pure(),
    this.isPreOrderDish = const BoolRequired.pure(),
  });

  @override
  List<Object?> get props => [
        id,
        name,
        price,
        weight,
        weightType,
        description,
        portionAmount,
    minDishAmount,
        dishTypeId,
        uploadedImages,
        dishTypes,
        dishType,
        ingredients,
        selectedIngredients,
        preferences,
        selectedPreferences,
        status,
        error,
    isHalfPortion,
    isPreOrderDish,
      ];

  DishEditState copyWith({
    Required<String>? id,
    Required<String>? name,
    Required<int>? price,
    Required<int>? weight,
    WeightTypeEnum? weightType,
    Required<String>? description,
    Required<int>? portionAmount,
    Required<int>? minDishAmount,
    String? dishTypeId,
    String? error,
    ListNotEmpty<UploadedImage>? uploadedImages,
    List<DishType>? dishTypes,
    Required<DishType>? dishType,
    List<Ingredient>? ingredients,
    ListNotEmpty<Ingredient>? selectedIngredients,
    List<Preference>? preferences,
    ListNotEmpty<Preference>? selectedPreferences,
    FormzStatus? status,
    BoolRequired? isHalfPortion,
    BoolRequired? isPreOrderDish,
  }) =>
      DishEditState(
        id: id ?? this.id,
        name: name ?? this.name,
        price: price ?? this.price,
        weight: weight ?? this.weight,
        weightType: weightType ?? this.weightType,
        description: description ?? this.description,
        portionAmount: portionAmount ?? this.portionAmount,
        minDishAmount: minDishAmount ?? this.minDishAmount,
        dishTypeId: dishTypeId ?? this.dishTypeId,
        uploadedImages: uploadedImages ?? this.uploadedImages,
        dishTypes: dishTypes ?? this.dishTypes,
        dishType: dishType ?? this.dishType,
        ingredients: ingredients ?? this.ingredients,
        selectedIngredients: selectedIngredients ?? this.selectedIngredients,
        preferences: preferences ?? this.preferences,
        selectedPreferences: selectedPreferences ?? this.selectedPreferences,
        status: status ?? this.status,
        error: error ?? this.error,
        isHalfPortion: isHalfPortion ?? this.isHalfPortion,
        isPreOrderDish: isPreOrderDish ?? this.isPreOrderDish,
      );
}
