import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/data/dto/edit_food_request/edit_dish_request.dart';
import 'package:home_food/data/dto/ingredient_dto/mapper/ingredient_dto_mapper.dart';
import 'package:home_food/data/dto/preference_dto/mapper/preference_dto_mapper.dart';
import 'package:home_food/data/repositories/dictionary_repository/dictionary_repository.dart';
import 'package:home_food/data/repositories/dish_repository/dish_repository.dart';
import 'package:home_food/data/repositories/upload_image_repository/upload_image_repository.dart';
import 'package:home_food/models/dish/create_dish_request.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/ingredient.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class DishEditCubit extends Cubit<DishEditState> {
  final DictionaryRepository _dictionaryRepository;
  final DictionaryCubit _dictionaryCubit;
  final DishRepository _dishRepository;
  final UploadImageRepository _uploadImageRepository;

  DishEditCubit(
    this._dictionaryCubit,
    this._dictionaryRepository,
    this._dishRepository,
    this._uploadImageRepository,
  ) : super(DishEditState()) {
    _dictionaryCubit.getPreferences();
    _dictionaryCubit.fetchDishIngredients();
    _dictionaryCubit.getDishTypes();

    _dictionaryCubit.stream.listen((event) {
      emit(state.copyWith(
        dishTypes: event.dishTypes,
        preferences: event.preferences,
        ingredients: event.ingredients,
      ));
    });
  }

  Future<void> handleSelectDishCard(Dish dish) async {
    final response = await _dictionaryRepository.getDishTypes();

    emit(state.copyWith(
      id: Required.dirty(dish.id),
      name: Required.dirty(dish.name),
      price: Required.dirty(dish.price),
      weight: Required.dirty(dish.weight),
      description: Required.dirty(dish.description),
      portionAmount: Required.dirty(dish.portionAmount),
      dishTypeId: dish.dishTypeId,
      dishType: Required.dirty(
        state.dishTypes.isNotEmpty
            ? state.dishTypes
                .firstWhere((element) => element.id == dish.dishTypeId)
            : response.firstWhere((element) => element.id == dish.dishTypeId),
      ),
      selectedIngredients: ListNotEmpty.dirty(dish.ingredientList),
      selectedPreferences: ListNotEmpty.dirty(dish.preferenceList),
      uploadedImages: ListNotEmpty.dirty(dish.images),
    ));
  }

  Future<void> handleNameChanged(String value) async {
    emit(state.copyWith(name: Required.dirty(value)));
  }

  Future<void> handlePriceChanged(String value) async {
    emit(
      state.copyWith(
        price: Required.dirty(NumUtils.parseString(value)?.toInt() ?? 0),
      ),
    );
  }

  Future<void> handleWeightChanged(String value) async {
    num _weightValue = NumUtils.parseString(value) ?? 0;

    if (state.weightType == WeightTypeEnum.kilograms) {
      _weightValue *= 1000;
    }

    Required<int> weight = Required.dirty(_weightValue.toInt());

    emit(state.copyWith(weight: weight));
  }

  Future<void> handleDescriptionChanged(String value) async {
    emit(state.copyWith(description: Required.dirty(value)));
  }

  Future<void> handlePortionAmountChanged(String value) async {
    emit(state.copyWith(
        portionAmount: Required.dirty(NumUtils.parseString(value)?.toInt())));
  }

  void uploadImages(List<File> images) async {
    try {
      final response = await _uploadImageRepository.uploadPhotos(images);

      ListNotEmpty<UploadedImage> uploadedImages = ListNotEmpty.dirty([
        ...state.uploadedImages.value,
        ...response,
      ]);

      emit(state.copyWith(uploadedImages: uploadedImages));
    } catch (e) {
      logger.e(e);
    }
  }

  void uploadImage(
    File image,
    int? index,
  ) async {
    try {
      final response = await _uploadImageRepository.uploadPhotos([image]);

      List<UploadedImage> uploadedImages = [];

      for (int i = 0; i < state.uploadedImages.value.length; i++) {
        if (index != null && i == index) {
          uploadedImages.add(response.first);
        } else {
          uploadedImages.add(state.uploadedImages.value[i]);
        }
      }
      if (index == null || state.uploadedImages.value.isEmpty) {
        uploadedImages.add(response.first);
      }

      emit(
        state.copyWith(
          uploadedImages: ListNotEmpty.dirty(uploadedImages),
        ),
      );
    } catch (e) {
      logger.e(e);
    }
  }

  void handleDishTypeChanged(DishType? value) {
    emit(state.copyWith(dishType: Required.dirty(value)));
  }

  void handlePreferenceSelected(Preference value) {
    if (state.selectedPreferences.value
        .any((element) => element.id == value.id)) {
      return;
    }

    ListNotEmpty<Preference> preferences = ListNotEmpty.dirty([
      ...state.selectedPreferences.value,
      value,
    ]);
    emit(state.copyWith(selectedPreferences: preferences));
  }

  void validate() {
    emit(state.copyWith(
      status: Formz.validate([
        state.name,
        state.description,
        state.price,
        state.weight,
        state.dishType,
        state.selectedPreferences,
        // state.selectedIngredients,
        state.uploadedImages,
      ]),
    ));
  }

  void submissionValidate() {
    emit(state.copyWith(
      name: Required.dirty(state.name.value),
      description: Required.dirty(state.description.value),
      price: Required.dirty(state.price.value),
      weight: Required.dirty(state.weight.value),
      dishType: Required<DishType>.dirty(state.dishType.value),
    ));

    validate();
  }

  void submitEditionDish() async {
    submissionValidate();
    if (!state.status.isValid) {
      return;
    }
    emit(state.copyWith(status: FormzStatus.submissionInProgress));

    try {
      final response = await _dishRepository.editDish(EditFoodRequest(
        id: state.id.value ?? '',
        name: state.name.value,
        price: state.price.value,
        weight: state.weight.value,
        description: state.description.value,
        portionAmount: state.portionAmount.value ?? 1,
        dishTypeId: state.dishType.value?.id,
        preferenceList:
            preferenceDTOListMapper(state.selectedPreferences.value),
        ingredientList: ingredientListToDTO([
          state.ingredients.first,
        ]),
        images: state.uploadedImages.value,
      ));

      emit(state.copyWith(
        status: FormzStatus.submissionSuccess,
      ));
    } on DioError catch (e) {
      logger.e(e);
      emit(state.copyWith(
        error: e.response?.data['message'],
        status: FormzStatus.submissionFailure,
      ));
    } catch (e) {
      emit(state.copyWith(
        error: e.toString(),
        status: FormzStatus.submissionFailure,
      ));
    }
    emit(state.copyWith(
      status: FormzStatus.pure,
    ));
  }

  void handleWeightTypeChanged(WeightTypeEnum? value) {
    emit(state.copyWith(
      weightType: value,
    ));
  }

  handleIsHalfPortionChanged(bool value) {
    BoolRequired _isHalfPortion = BoolRequired.dirty(value);
    emit(state.copyWith(isHalfPortion: _isHalfPortion));
  }

  void handleMinDishAmount(String value) {
    emit(
      state.copyWith(
          minDishAmount:
              Required.dirty(NumUtils.parseString(value)?.toInt() ?? 0)),
    );
  }

  handleIsPreOrderDish(bool value) {
    BoolRequired _isPreOrderDish = BoolRequired.dirty(value);
    emit(state.copyWith(isPreOrderDish: _isPreOrderDish));
  }

  removeImage(int index) {
    List<UploadedImage> _images = [
      ...state.uploadedImages.value,
    ];
    _images.removeAt(index);

    emit(state.copyWith(
      uploadedImages: ListNotEmpty.dirty(_images),
    ));
  }

  deletePreference(Preference preference) {
    // сначала вывожу из ListNotEmpty обычный список List<Preference>
    // потом его меняю и обяз ставить условие

    List<Preference> _preferences = [...state.selectedPreferences.value];
    _preferences.removeWhere((element) => element.id == preference.id);

    emit(state.copyWith(
      selectedPreferences: ListNotEmpty.dirty(_preferences),
    ));
  }

  onSwapImages(int firstImageIndex, int secondImageIndex) {
    List<UploadedImage> _images = [...state.uploadedImages.value];
    final _item = _images[firstImageIndex];
    _images
      ..removeAt(firstImageIndex)
      ..insert(secondImageIndex, _item);
    emit(state.copyWith(
      uploadedImages: ListNotEmpty.dirty(_images),
    ));
  }

  Future<void> handleChangePortionAmount({
    required dish,
    required int portionAmount,
  }) async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));

    try {
      final response = await _dishRepository.editDish(EditFoodRequest(
        id: dish.id,
        portionAmount: portionAmount,
      ));

      emit(state.copyWith(
        status: FormzStatus.submissionSuccess,
      ));
    } on DioError catch (e) {
      logger.e(e);
      emit(state.copyWith(
        error: e.response?.data['message'],
        status: FormzStatus.submissionFailure,
      ));
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(
        error: e.toString(),
        status: FormzStatus.submissionFailure,
      ));
    }
    emit(state.copyWith(
      status: FormzStatus.pure,
    ));
  }
}
