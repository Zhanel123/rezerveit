import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/models/statistics/statistics.dart';
import 'package:home_food/models/statistics/top_dish_statistics.dart';

class MerchantStatisticsState extends Equatable {
  final Statistics statistics;
  final DateTime sellStatisticsStartDate;
  final DateTime sellStatisticsEndDate;
  final PeriodTypeEnum sellStatisticsPeriodType;
  final List<TopDishStatistics> topDishStatistics;
  final DateTime topDishStatisticsStartDate;
  final DateTime topDishStatisticsEndDate;
  final PeriodTypeEnum topDishStatisticsPeriodType;
  final FormzStatus statisticsStatus;

  MerchantStatisticsState({
    this.statistics = const Statistics(),
    this.statisticsStatus = FormzStatus.pure,
    this.topDishStatistics = const [],
    DateTime? sellStatisticsStartDate,
    DateTime? sellStatisticsEndDate,
    this.sellStatisticsPeriodType = PeriodTypeEnum.MONTH,
    DateTime? topDishStatisticsStartDate,
    DateTime? topDishStatisticsEndDate,
    this.topDishStatisticsPeriodType = PeriodTypeEnum.MONTH,
  })  : topDishStatisticsStartDate = topDishStatisticsStartDate ??
            DateTime.now().subtract(Duration(days: 31)),
        topDishStatisticsEndDate = topDishStatisticsEndDate ?? DateTime.now(),
        sellStatisticsStartDate = sellStatisticsStartDate ??
            DateTime.now().subtract(Duration(days: 31)),
        sellStatisticsEndDate = sellStatisticsEndDate ?? DateTime.now();

  @override
  List<Object?> get props => [
        statistics,
        statisticsStatus,
        sellStatisticsStartDate,
        sellStatisticsEndDate,
        sellStatisticsPeriodType,
        topDishStatistics,
        topDishStatisticsStartDate,
        topDishStatisticsEndDate,
        topDishStatisticsPeriodType,
      ];

  MerchantStatisticsState copyWith({
    Statistics? statistics,
    FormzStatus? statisticsStatus,
    DateTime? sellStatisticsStartDate,
    DateTime? sellStatisticsEndDate,
    PeriodTypeEnum? sellStatisticsPeriodType,
    List<TopDishStatistics>? topDishStatistics,
    DateTime? topDishStatisticsStartDate,
    DateTime? topDishStatisticsEndDate,
    PeriodTypeEnum? topDishStatisticsPeriodType,
  }) =>
      MerchantStatisticsState(
        statistics: statistics ?? this.statistics,
        statisticsStatus: statisticsStatus ?? this.statisticsStatus,
        sellStatisticsStartDate:
            sellStatisticsStartDate ?? this.sellStatisticsStartDate,
        sellStatisticsEndDate:
            sellStatisticsEndDate ?? this.sellStatisticsEndDate,
        sellStatisticsPeriodType:
            sellStatisticsPeriodType ?? this.sellStatisticsPeriodType,
        topDishStatistics: topDishStatistics ?? this.topDishStatistics,
        topDishStatisticsStartDate:
            topDishStatisticsStartDate ?? this.topDishStatisticsStartDate,
        topDishStatisticsEndDate:
            topDishStatisticsEndDate ?? this.topDishStatisticsEndDate,
        topDishStatisticsPeriodType:
            topDishStatisticsPeriodType ?? this.topDishStatisticsPeriodType,
      );
}
