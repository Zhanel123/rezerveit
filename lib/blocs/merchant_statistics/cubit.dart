import 'package:bloc/bloc.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/repositories/order_repository/order_repository.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';

import 'state.dart';

@LazySingleton()
class MerchantStatisticsCubit extends Cubit<MerchantStatisticsState> {
  final OrderRepository _orderRepository;
  final ProfileCubit _profileCubit;

  MerchantStatisticsCubit(
    this._orderRepository,
    this._profileCubit,
  ) : super(MerchantStatisticsState()) {
    _profileCubit.stream.listen((event) {
      if (event.profile != null) {
        getTopDishStatistics();
        getSellStatistics();
      }
    });
    if (_profileCubit.state.profile != null) {
      getSellStatistics();
      getTopDishStatistics();
    }
  }

  Future<void> getSellStatistics() async {
    emit(state.copyWith(
      statisticsStatus: FormzStatus.submissionInProgress,
    ));
    try {
      final response = await _orderRepository.getSellStatistics(
        startDate:
            DateFormat('dd.MM.yyyy').format(state.sellStatisticsStartDate),
        endDate: DateFormat('dd.MM.yyyy').format(state.sellStatisticsEndDate),
        period: state.sellStatisticsPeriodType.key!,
        month: state.sellStatisticsPeriodType == PeriodTypeEnum.MONTH
            ? DateTime.now().month
            : null,
        year: state.sellStatisticsPeriodType == PeriodTypeEnum.MONTH
            ? DateTime.now().year
            : null,
      );

      emit(state.copyWith(
        statistics: response,
        statisticsStatus: FormzStatus.submissionSuccess,
      ));
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(
        statisticsStatus: FormzStatus.submissionFailure,
      ));
    }
    emit(state.copyWith(
      statisticsStatus: FormzStatus.pure,
    ));
  }

  Future<void> getTopDishStatistics() async {
    try {
      final response = await _orderRepository.getTopDishStatistics(
        startDate:
            DateFormat('dd.MM.yyyy').format(state.topDishStatisticsStartDate),
        endDate:
            DateFormat('dd.MM.yyyy').format(state.topDishStatisticsEndDate),
      );

      emit(state.copyWith(
        topDishStatistics: response,
      ));
    } catch (e) {
      logger.e(e);
    }
  }

  void sellStatisticsPeriodTypeChanged(PeriodTypeEnum? value) {
    emit(state.copyWith(
      sellStatisticsPeriodType: value,
    ));

    if (value == PeriodTypeEnum.BETWEEN) {
      emit(state.copyWith(
        sellStatisticsStartDate: DateTime.now().subtract(Duration(
          days: 14,
        )),
        sellStatisticsEndDate: DateTime.now(),
      ));
    } else if (value == PeriodTypeEnum.DAY) {
      emit(state.copyWith(
        sellStatisticsStartDate: DateTime.now(),
        sellStatisticsEndDate: DateTime.now(),
      ));
    } else if (value == PeriodTypeEnum.MONTH) {
      emit(state.copyWith(
        sellStatisticsStartDate: DateTime.now().subtract(Duration(
          days: 31,
        )),
        sellStatisticsEndDate: DateTime.now(),
      ));
    }
    getSellStatistics();
  }

  void topDishStatisticsPeriodTypeChanged(PeriodTypeEnum? value) {
    emit(state.copyWith(
      topDishStatisticsPeriodType: value,
    ));

    if (value == PeriodTypeEnum.BETWEEN) {
      emit(state.copyWith(
        topDishStatisticsStartDate: DateTime.now().subtract(Duration(
          days: 14,
        )),
        sellStatisticsEndDate: DateTime.now(),
      ));
    } else if (value == PeriodTypeEnum.DAY) {
      emit(state.copyWith(
        topDishStatisticsStartDate: DateTime.now(),
        topDishStatisticsEndDate: DateTime.now(),
      ));
    } else if (value == PeriodTypeEnum.MONTH) {
      emit(state.copyWith(
        topDishStatisticsStartDate: DateTime.now().subtract(Duration(
          days: 31,
        )),
        topDishStatisticsEndDate: DateTime.now(),
      ));
    }
    getTopDishStatistics();
  }
}
