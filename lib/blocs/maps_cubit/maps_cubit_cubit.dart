import 'package:bloc/bloc.dart';
import 'package:home_food/blocs/merchant/cubit.dart';
import 'package:injectable/injectable.dart';
import 'package:location/location.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'maps_cubit_state.dart';

@lazySingleton
class MapsCubitCubit extends Cubit<MapsCubitState> {
  final MerchantCubit _merchantCubit;

  MapsCubitCubit(
    this._merchantCubit,
  ) : super(MapsCubitState()) {
    emit(state.copyWith(
      merchants: _merchantCubit.state.merchants,
    ));
    _merchantCubit.stream.listen((event) {
      emit(state.copyWith(
        merchants: event.merchants,
      ));
    });
    initializeLocationAndStore();
  }

  Future<void> initializeLocationAndStore() async {
    Location _location = Location();
    bool? _serivceEnabled;
    PermissionStatus? _permissionGranted;

    _serivceEnabled = await _location.serviceEnabled();
    if (!_serivceEnabled) {
      _serivceEnabled = await _location.requestService();
    }

    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
    }

    LocationData _locationData = await _location.getLocation();
    LatLng currentLatLng =
        LatLng(_locationData.latitude!, _locationData.longitude!);

    _saveCurrentPosition(currentLatLng);
  }

  Future<void> _saveCurrentPosition(LatLng position) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    storage.setDouble('latitude', position.latitude);
    storage.setDouble('longitude', position.longitude);

    emit(state.copyWith(
      currentLatLng: position,
    ));
  }

  void onMapCreated(MapboxMapController controller) {
    emit(state.copyWith(
      mapController: controller,
    ));
  }
}
