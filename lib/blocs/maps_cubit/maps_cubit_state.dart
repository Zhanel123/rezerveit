import 'package:equatable/equatable.dart';
import 'package:home_food/models/merchant/merchant.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class MapsCubitState extends Equatable {
  final bool isLocationServiceEnabled;
  final LatLng? currentLatLng;
  late final MapboxMapController? mapController;
  final List<Merchant> merchants;

  MapsCubitState({
    this.isLocationServiceEnabled = false,
    this.currentLatLng,
    this.mapController,
    this.merchants = const [],
  });

  @override
  List<Object?> get props => [
        isLocationServiceEnabled,
        currentLatLng,
        merchants,
        mapController,
      ];

  MapsCubitState copyWith({
    bool? isLocationServiceEnabled,
    LatLng? currentLatLng,
    List<Merchant>? merchants,
    MapboxMapController? mapController,
  }) =>
      MapsCubitState(
        isLocationServiceEnabled:
            isLocationServiceEnabled ?? this.isLocationServiceEnabled,
        currentLatLng: currentLatLng ?? this.currentLatLng,
        merchants: merchants ?? this.merchants,
        mapController: mapController ?? this.mapController,
      );
}
