part of 'cubit.dart';

class NewAddressState extends Equatable {
  final String addressId;
  final Required<City> newCity;
  final StringRequired title;
  final List<City> cities;
  final FormzStatus newAddressFormzStatus;
  final FormzStatus newAddressStatus;
  final FormzStatus deletionStatus;
  final FormzStatus changeAddressStatus;
  final AddressRequired address;
  final UserType userType;

  NewAddressState({
    this.addressId = '',
    this.title = const StringRequired.pure(),
    this.newCity = const Required.pure(),
    this.cities = const [],
    this.newAddressFormzStatus = FormzStatus.pure,
    this.newAddressStatus = FormzStatus.pure,
    this.deletionStatus = FormzStatus.pure,
    this.changeAddressStatus = FormzStatus.pure,
    this.address = const AddressRequired.pure(),
    this.userType = UserType.CUSTOMER,
  });

  @override
  List<Object?> get props => [
        addressId,
        newCity,
        title,
        cities,
        newAddressFormzStatus,
        newAddressStatus,
        address,
        userType,
        deletionStatus,
    changeAddressStatus,
      ];

  NewAddressState copyWith({
    String? addressId,
    Required<City>? newCity,
    StringRequired? title,
    List<City>? cities,
    FormzStatus? newAddressFormzStatus,
    FormzStatus? newAddressStatus,
    FormzStatus? deletionStatus,
    FormzStatus? changeAddressStatus,
    AddressRequired? address,
    UserType? userType,
  }) =>
      NewAddressState(
          addressId: addressId ?? this.addressId,
          newCity: newCity ?? this.newCity,
          title: title ?? this.title,
          cities: cities ?? this.cities,
          userType: userType ?? this.userType,
          newAddressFormzStatus:
              newAddressFormzStatus ?? this.newAddressFormzStatus,
          newAddressStatus: newAddressStatus ?? this.newAddressStatus,
          address: address ?? this.address,
          changeAddressStatus: changeAddressStatus ?? this.changeAddressStatus,
          deletionStatus: deletionStatus ?? this.deletionStatus);
}
