import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/data/dto/address_dto/address_dto.dart';
import 'package:home_food/data/repositories/address_repository/address_repository.dart';
import 'package:home_food/data/repositories/dictionary_repository/dictionary_repository.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/service_locator.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class NewAddressCubit extends Cubit<NewAddressState> {
  final ProfileCubit _profileCubit;
  final DictionaryRepository _dictionaryRepository;
  final AddressRepository _addressRepository;

  NewAddressCubit(
    this._dictionaryRepository,
    this._addressRepository,
    this._profileCubit,
  ) : super(NewAddressState()) {
    fetchCities();

    _profileCubit.stream.listen((event) {
      state.copyWith(
        userType: event.profile?.userType,
      );
    });
  }

  void fetchCities() async {
    final response = await _dictionaryRepository.getCities();
    emit(state.copyWith(cities: response));
  }

  void handleNewCityAdded(City? value) {
    Required<City> _city = Required.dirty(value);
    emit(state.copyWith(newCity: _city));
    // validate();
  }

  handleAddressTitleChanged(String value) {
    StringRequired _title = StringRequired.dirty(value);
    emit(state.copyWith(title: _title));
  }

  handleChangeAddress(String? value) {
    Address a = Address(
      address: value ?? '',
      cityId: state.newCity.value?.id,
      city: state.newCity.value!,
    );
    AddressRequired _address = AddressRequired.dirty(a);
    emit(state.copyWith(address: _address));
  }

  void validateAddress() {
    emit(state.copyWith(
      newAddressFormzStatus: Formz.validate([
        state.address,
        state.title,
        state.newCity,
      ]),
    ));
  }

  void createAddress() async {
    emit(
      state.copyWith(
        address: AddressRequired.dirty(state.address.value),
        title: StringRequired.dirty(state.title.value),
        newCity: Required.dirty(state.newCity.value),
      ),
    );
    validateAddress();
    if (!state.newAddressFormzStatus.isValid) {
      return;
    }
    emit(state.copyWith(newAddressStatus: FormzStatus.submissionInProgress));
    try {
      final response = await _addressRepository.addNewAddress(
        AddressDTO(
          address: state.address.value?.address,
          cityId: state.newCity.value?.id,
          title: state.title.value,
          type: state.userType == UserType.CUSTOMER
              ? AddressType.CUSTOMER_ADDRESS
              : AddressType.MANUFACTURER_ADDRESS,
        ),
      );

      emit(state.copyWith(
        newAddressStatus: FormzStatus.submissionSuccess,
        addressId: response,
      ));
    } catch (e) {
      emit(state.copyWith(newAddressStatus: FormzStatus.submissionFailure));
    }
    emit(NewAddressState(
      cities: state.cities,
    ));
  }

  handleSelectAddress(Address address) {
    emit(state.copyWith(
        // address: AddressRequired.dirty.(address),
        ));
  }

  void updateAddress() async {
    emit(
      state.copyWith(
        address: AddressRequired.dirty(state.address.value),
        title: StringRequired.dirty(state.title.value),
        newCity: Required.dirty(state.newCity.value),
      ),
    );
    validateAddress();
    if (!state.newAddressFormzStatus.isValid) {
      return;
    }
    emit(state.copyWith(changeAddressStatus: FormzStatus.submissionInProgress));

    try {
      final response = await _addressRepository.updateAddress(AddressDTO(
        id: state.addressId,
        address: state.address.value?.address,
        title: state.title.value,
        cityId: state.newCity.value?.id,
        type: state.userType == UserType.CUSTOMER
            ? AddressType.CUSTOMER_ADDRESS
            : AddressType.MANUFACTURER_ADDRESS,
      ));

      emit(state.copyWith(
        changeAddressStatus: FormzStatus.submissionSuccess,
      ));

      serviceLocator<ProfileCubit>().getMyAddresses();
    } catch (e) {
      emit(state.copyWith(changeAddressStatus: FormzStatus.submissionFailure));
    }

    emit(state.copyWith(
      changeAddressStatus: FormzStatus.pure,
    ));
  }

  void deleteAddress({
    required String addressId,
  }) async {
    emit(state.copyWith(deletionStatus: FormzStatus.submissionInProgress));

    try {
      final response = await _addressRepository.deleteAddress(addressId);

      emit(state.copyWith(
        deletionStatus: FormzStatus.submissionSuccess,
      ));
    } catch (e) {
      emit(state.copyWith(deletionStatus: FormzStatus.submissionFailure));
    }
    emit(state.copyWith(deletionStatus: FormzStatus.pure));
  }

  void emptyState() {
    emit(state.copyWith(
      newCity: const Required.pure(),
      title: const StringRequired.pure(),
      address: const AddressRequired.pure(),
    ));
  }

  void selectAddress(Address address) {
    emit(
      state.copyWith(
        title: StringRequired.dirty(address.title),
        addressId: address.id,
        newCity: Required.dirty(address.city),
        address: AddressRequired.dirty(address),
      ),
    );
  }
}
