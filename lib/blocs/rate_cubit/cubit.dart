import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class RateOrderCubit extends Cubit<RateCubitState> {
  RateOrderCubit() : super(RateCubitState());

  Future<void> handleOrderRate({
    required OrderModel order,
  }) async {
    emit(state.copyWith(
      comment: Required.dirty(state.comment.value),
      grade: Required.dirty(state.grade.value),
    ));
    emit(state.copyWith(
      gradeStatus: Formz.validate([
        state.comment,
        state.grade,
      ]),
    ));

    if (!state.gradeStatus.isValid) {
      return;
    }
    emit(state.copyWith(gradeStatus: FormzStatus.submissionInProgress));
    try {
      await serviceLocator<RestClient>().rateOrder(
        orderId: order.id,
        comment: state.comment.value ?? '',
        base64Image: '',
        grade: state.grade.value!,
      );

      emit(state.copyWith(gradeStatus: FormzStatus.submissionSuccess));
    } on DioError catch (e) {
      emit(state.copyWith(
        gradeStatus: FormzStatus.submissionFailure,
        error: e.response?.data['message'],
      ));
    }
  }

  changeComment(String comment) {
    Required<String> _comment = Required.dirty(comment);
    emit(state.copyWith(
      comment: _comment,
    ));
  }

  changeGrade(int grade) {
    Required<int> _grade = Required.dirty(grade);
    emit(state.copyWith(
      grade: _grade,
    ));
  }
}
