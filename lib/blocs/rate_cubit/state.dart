part of 'cubit.dart';

class RateCubitState extends Equatable {
  final Required<String> comment;
  final Required<int> grade;
  final FormzStatus gradeStatus;
  final String error;

  RateCubitState({
    this.comment = const Required.pure(),
    this.grade = const Required.pure(),
    this.gradeStatus = FormzStatus.pure,
    this.error = '',
  });

  @override
  List<Object?> get props => [
        comment,
        grade,
        gradeStatus,
        error,
      ];

  RateCubitState copyWith({
    Required<String>? comment,
    Required<int>? grade,
    FormzStatus? gradeStatus,
    String? error,
  }) =>
      RateCubitState(
        comment: comment ?? this.comment,
        grade: grade ?? this.grade,
        error: error ?? this.error,
        gradeStatus: gradeStatus ?? this.gradeStatus,
      );
}
