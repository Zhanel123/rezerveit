import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/core/number.dart';
import 'package:home_food/data/dto/ingredient_dto/mapper/ingredient_dto_mapper.dart';
import 'package:home_food/data/dto/preference_dto/mapper/preference_dto_mapper.dart';
import 'package:home_food/data/repositories/dish_repository/dish_repository.dart';
import 'package:home_food/data/repositories/upload_image_repository/upload_image_repository.dart';
import 'package:home_food/models/dish/create_dish_request.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/ingredient.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class CreateFoodCubit extends Cubit<CreateFoodState> {
  final DictionaryCubit _dictionaryCubit;
  final DishRepository _dishRepository;
  final UploadImageRepository _uploadImageRepository;

  CreateFoodCubit(
    this._dictionaryCubit,
    this._dishRepository,
    this._uploadImageRepository,
  ) : super(CreateFoodState()) {
    _dictionaryCubit.stream.listen((event) {
      emit(state.copyWith(
        dishTypes: event.dishTypes,
        preferences: event.preferences,
        ingredients: event.ingredients,
      ));
    });
    _dictionaryCubit.getPreferences();
  }

  Future<void> handleNameChanged(String value) async {
    emit(state.copyWith(name: Required.dirty(value)));
  }

  Future<void> handlePriceChanged(String value) async {
    emit(
      state.copyWith(
        price: Required.dirty(NumUtils.parseString(value)?.toInt() ?? 0),
      ),
    );
  }

  void handleMinDishAmount(String value) {
    emit(
      state.copyWith(
          minDishAmount:
              Required.dirty(NumUtils.parseString(value)?.toInt() ?? 0)),
    );
  }

  Future<void> handleWeightChanged(String value) async {
    num _weightValue = NumUtils.parseString(value) ?? 0;

    if (state.weightType == WeightTypeEnum.kilograms) {
      _weightValue *= 1000;
    }

    Required<int> weight = Required.dirty(_weightValue.toInt());

    emit(state.copyWith(weight: weight));
  }

  Future<void> handleDescriptionChanged(String value) async {
    emit(state.copyWith(description: Required.dirty(value)));
  }

  Future<void> handlePortionAmountChanged(String value) async {
    emit(state.copyWith(
        portionAmount:
            Required.dirty(NumUtils.parseString(value)?.toInt() ?? 0)));
  }

  void uploadImages(List<File> images) async {
    try {
      final response = await _uploadImageRepository.uploadPhotos(images);

      ListNotEmpty<UploadedImage> uploadedImages = ListNotEmpty.dirty([
        ...state.uploadedImages.value,
        ...response,
      ]);

      emit(state.copyWith(uploadedImages: uploadedImages));
    } catch (e) {
      logger.e(e);
    }
  }

  void uploadImage(
    File image,
    int? index,
  ) async {
    try {
      final response = await _uploadImageRepository.uploadPhotos([image]);

      List<UploadedImage> uploadedImages = [];

      for (int i = 0; i < state.uploadedImages.value.length; i++) {
        if (index != null && i == index) {
          uploadedImages.add(response.first);
        } else {
          uploadedImages.add(state.uploadedImages.value[i]);
        }
      }
      if (index == null || state.uploadedImages.value.isEmpty) {
        uploadedImages.add(response.first);
      }

      emit(
        state.copyWith(
          uploadedImages: ListNotEmpty.dirty(uploadedImages),
        ),
      );
    } catch (e) {
      logger.e(e);
    }
  }

  void handleDishTypeChanged(DishType? value) {
    emit(state.copyWith(dishType: Required.dirty(value)));
  }

  handleIsHalfPortionChanged(bool value) {
    BoolRequired _isHalfPortion = BoolRequired.dirty(value);
    emit(state.copyWith(isHalfPortion: _isHalfPortion));
  }

  handleIsPreOrderDish(bool value) {
    BoolRequired _isPreOrderDish = BoolRequired.dirty(value);
    emit(state.copyWith(isPreOrderDish: _isPreOrderDish));
  }

  Future<void> handleMainImageChanged(File file) async {
    try {
      final response = await _uploadImageRepository.uploadPhotos([file]);

      List<UploadedImage> _temp = state.uploadedImages.value;

      _temp = _temp.length > 1 ? _temp.sublist(1) : [];

      _temp = [
        response.first,
        ..._temp,
      ];

      ListNotEmpty<UploadedImage> uploadedImages = ListNotEmpty.dirty(_temp);

      emit(state.copyWith(uploadedImages: uploadedImages));
    } catch (e) {
      logger.e(e);
    }
  }

  void handlePreferenceSelected(Preference value) {
    if (state.selectedPreferences.value
        .any((element) => element.id == value.id)) {
      return;
    }

    ListNotEmpty<Preference> preferences = ListNotEmpty.dirty([
      ...state.selectedPreferences.value,
      value,
    ]);
    emit(state.copyWith(selectedPreferences: preferences));
  }

  void validate() {
    emit(state.copyWith(
      createFoodFormzStatus: Formz.validate([
        state.name,
        state.description,
        state.price,
        state.weight,
        state.dishType,
        // state.selectedPreferences,
        state.portionAmount,
        state.uploadedImages,
        if (state.isPreOrderDish.value) state.minDishAmount,
      ]),
    ));
  }

  void submissionValidate() {
    emit(state.copyWith(
      name: Required.dirty(state.name.value),
      description: Required.dirty(state.description.value),
      price: Required.dirty(state.price.value),
      weight: Required.dirty(state.weight.value),
      dishType: Required<DishType>.dirty(state.dishType.value),
      minDishAmount: Required.dirty(state.minDishAmount.value),
      portionAmount: Required.dirty(state.portionAmount.value),
    ));

    validate();
  }

  void create() async {
    submissionValidate();
    if (!state.createFoodFormzStatus.isValid) {
      return;
    }
    emit(state.copyWith(createFoodStatus: FormzStatus.submissionInProgress));

    try {
      final response = await _dishRepository.addNewDish(CreateFoodRequest(
        name: state.name.value,
        price: state.price.value,
        weight: state.weight.value,
        discountAfter19: state.discount.value,
        description: state.description.value,
        portionAmount: state.portionAmount.value ?? 1,
        minPortionAmount: state.minDishAmount.value ?? 1,
        dishTypeId: state.dishType.value?.id,
        preferenceList:
            preferenceDTOListMapper(state.selectedPreferences.value),
        ingredientList: ingredientListToDTO([state.ingredients.first]),
        // ingredientList: state.selectedIngredients.value,
        images: state.uploadedImages.value,
        hasHalfPortion: state.isHalfPortion.value,
        preOrderDish: state.isPreOrderDish.value,
      ));

      serviceLocator<MyDishesCubit>().fetchMyDishes();

      emit(state.copyWith(
        createFoodStatus: FormzStatus.submissionSuccess,
      ));
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(
        createFoodStatus: FormzStatus.submissionFailure,
      ));
    }
    emit(state.clear());
  }

  void handleWeightTypeChanged(WeightTypeEnum? value) {
    emit(state.copyWith(
      weightType: value,
    ));
  }

  deletePreference(Preference preference) {
    //сначала вывожу из ListNotEmpty обычный список List<Preference>
    // потом его меняю и обяз ставить условие

    List<Preference> _preferences = [...state.selectedPreferences.value];
    _preferences.removeWhere((element) => element.id == preference.id);

    emit(state.copyWith(
      selectedPreferences: ListNotEmpty.dirty(_preferences),
    ));
  }

  removeImage(int index) {
    List<UploadedImage> _images = [...state.uploadedImages.value];
    _images.removeAt(index);

    emit(state.copyWith(
      uploadedImages: ListNotEmpty.dirty(_images),
    ));
  }

  handleDiscountChanged(int value) {
    emit(state.copyWith(
      discount: Required.dirty(value),
    ));
  }

  swapImages(int firstImageIndex, int secondImageIndex) {
    List<UploadedImage> _images = [...state.uploadedImages.value];
    final _item = _images[firstImageIndex];
    _images
      ..removeAt(firstImageIndex)
      ..insert(secondImageIndex, _item);
    emit(state.copyWith(
      uploadedImages: ListNotEmpty.dirty(_images),
    ));
  }
}
