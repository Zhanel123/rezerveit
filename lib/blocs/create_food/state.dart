part of 'cubit.dart';

class CreateFoodState extends Equatable {
  final Required<String> name;
  final Required<int> price;
  final Required<int> weight;
  final Required<String> description;
  final Required<int> portionAmount;
  final Required<int> minDishAmount;
  final Required<int> discount;
  final String dishTypeId;
  final WeightTypeEnum weightType;
  final ListNotEmpty<UploadedImage> uploadedImages;
  final List<DishType> dishTypes;
  final Required<DishType> dishType;
  final List<Ingredient> ingredients;
  final ListNotEmpty<Ingredient> selectedIngredients;
  final List<Preference> preferences;
  final ListNotEmpty<Preference> selectedPreferences;
  final BoolRequired isHalfPortion;
  final BoolRequired isPreOrderDish;
  final FormzStatus createFoodFormzStatus;
  final FormzStatus createFoodStatus;

  CreateFoodState({
    this.name = const Required<String>.pure(),
    this.price = const Required<int>.pure(),
    this.weight = const Required.pure(),
    this.discount = const Required.pure(),
    this.description = const Required<String>.pure(),
    this.portionAmount = const Required<int>.pure(),
    this.minDishAmount = const Required<int>.pure(),
    this.dishTypeId = '',
    this.weightType = WeightTypeEnum.gram,
    this.uploadedImages = const ListNotEmpty.pure(),
    this.dishTypes = const [],
    this.dishType = const Required.pure(),
    this.ingredients = const [],
    this.selectedIngredients = const ListNotEmpty.pure(),
    this.preferences = const [],
    this.selectedPreferences = const ListNotEmpty.pure(),
    this.isHalfPortion = const BoolRequired.pure(),
    this.isPreOrderDish = const BoolRequired.pure(),
    this.createFoodFormzStatus = FormzStatus.pure,
    this.createFoodStatus = FormzStatus.pure,
  });

  @override
  List<Object?> get props => [
        name,
        price,
        weight,
        discount,
        weightType,
        description,
        portionAmount,
        minDishAmount,
        dishTypeId,
        uploadedImages,
        dishTypes,
        dishType,
        ingredients,
        selectedIngredients,
        preferences,
        selectedPreferences,
        isHalfPortion,
        isPreOrderDish,
        createFoodFormzStatus,
        createFoodStatus
      ];

  CreateFoodState clear() => CreateFoodState(
        dishTypes: dishTypes,
        ingredients: ingredients,
        preferences: preferences,
      );

  CreateFoodState copyWith({
    Required<String>? name,
    Required<int>? price,
    Required<int>? weight,
    Required<int>? discount,
    WeightTypeEnum? weightType,
    Required<String>? description,
    Required<int>? portionAmount,
    Required<int>? minDishAmount,
    String? dishTypeId,
    ListNotEmpty<UploadedImage>? uploadedImages,
    List<DishType>? dishTypes,
    Required<DishType>? dishType,
    List<Ingredient>? ingredients,
    ListNotEmpty<Ingredient>? selectedIngredients,
    List<Preference>? preferences,
    ListNotEmpty<Preference>? selectedPreferences,
    BoolRequired? isHalfPortion,
    BoolRequired? isPreOrderDish,
    FormzStatus? createFoodFormzStatus,
    FormzStatus? createFoodStatus,
  }) =>
      CreateFoodState(
        name: name ?? this.name,
        price: price ?? this.price,
        weight: weight ?? this.weight,
        discount: discount ?? this.discount,
        weightType: weightType ?? this.weightType,
        description: description ?? this.description,
        portionAmount: portionAmount ?? this.portionAmount,
        minDishAmount: minDishAmount ?? this.minDishAmount,
        dishTypeId: dishTypeId ?? this.dishTypeId,
        uploadedImages: uploadedImages ?? this.uploadedImages,
        dishTypes: dishTypes ?? this.dishTypes,
        dishType: dishType ?? this.dishType,
        ingredients: ingredients ?? this.ingredients,
        selectedIngredients: selectedIngredients ?? this.selectedIngredients,
        preferences: preferences ?? this.preferences,
        selectedPreferences: selectedPreferences ?? this.selectedPreferences,
        isHalfPortion: isHalfPortion ?? this.isHalfPortion,
        isPreOrderDish: isPreOrderDish ?? this.isPreOrderDish,
        createFoodFormzStatus:
            createFoodFormzStatus ?? this.createFoodFormzStatus,
        createFoodStatus: createFoodStatus ?? this.createFoodStatus,
      );
}
