import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/dish_catalog/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/data/repositories/visitor_repository/visitor_repository.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/dish_list_filter_request/dish_list_filter_request.dart';
import 'package:home_food/service_locator.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class DishFilterCubit extends Cubit<DishFilterState> {
  final DictionaryCubit _dictionaryCubit;
  final VisitorRepository _visitorRepository;

  DishFilterCubit(
    this._dictionaryCubit,
    this._visitorRepository,
  ) : super(DishFilterState()) {
    emit(state.copyWith(
      preferences: _dictionaryCubit.state.preferences,
      dishTypes: _dictionaryCubit.state.dishTypes,
    ));
    _dictionaryCubit.stream.listen((event) {
      emit(state.copyWith(
        preferences: event.preferences,
        dishTypes: event.dishTypes,
      ));
    });
    fetchUserPreferences();
  }

  Future<void> fetchUserPreferences() async {
    final response = await _visitorRepository.getUserPreferences();
    emit(state.copyWith(
      selectedPreferences: response,
    ));
    handlePreferencesChanged();
  }

  Future<void> handleSearchTextChanged(String value) async {
    emit(state.copyWith(searchText: value));
  }

  void handleDishTypeChanged(DishType? value) {
    emit(state.copyWith(dishType: Required.dirty(value)));
  }

  Future<void> handleNotDishTypesSelected(DishType? value) async {
    if (state.excludedDishTypes.any((element) => element.id == value?.id) ||
        value == null) {
      return;
    }
    emit(
      state.copyWith(
        excludedDishTypes: [
          ...state.excludedDishTypes,
          value,
        ],
      ),
    );
  }

  Future<void> deleteDishTypeSelected(DishType? value) async {
    List<DishType> deletedDishTypes = [...state.excludedDishTypes];
    deletedDishTypes.removeWhere((element) => element.id == value?.id);

    emit(state.copyWith(excludedDishTypes: [...deletedDishTypes]));
  }

  Future<void> foodPreferenceToggled(Preference value) async {
    var _selectedPreferenses = [...state.selectedPreferences];
    if (_selectedPreferenses.any((element) => element.id == value.id)) {
      _selectedPreferenses.removeWhere((element) => element.id == value.id);
    } else {
      _selectedPreferenses.add(value);
    }

    emit(state.copyWith(selectedPreferences: _selectedPreferenses));
  }

  Future<void> handlePriceFrom(int value) async {
    emit(state.copyWith(priceFrom: value));
  }

  Future<void> handlePriceTo(int value) async {
    emit(state.copyWith(priceTo: value));
  }

  Future<void> handleShowNew(bool value) async {
    emit(state.copyWith(showNew: value));
  }

  Future<void> handleHasSale(bool value) async {
    emit(state.copyWith(hasSale: value));
  }

  Future<void> handlePriceSortDirection(SortFieldEnum value) async {
    emit(state.copyWith(priceSortDirection: value));
  }

  Future<void> handleShowPopular(bool value) async {
    emit(state.copyWith(showPopular: value));
  }

  saveFilterChanges() {
    serviceLocator<DishCatalogCubit>().saveFilters(DishFilterRequest(
      searchText: state.searchText,
      dishTypeId: state.dishType.value?.id,
      notDishTypeIds: state.excludedDishTypes,
      foodPreferenceIds: state.selectedPreferences,
      priceFrom: state.priceFrom.toDouble(),
      priceTo: state.priceTo.toDouble(),
      showNew: state.showNew,
      hasSale: state.hasSale,
      sortBy: state.priceSortDirection,
      showPopular: state.showPopular,
    ));
  }

  void resetFilter() {
    emit(DishFilterState().copyWith(
      preferences: state.preferences,
      dishTypes: state.dishTypes,
    ));
  }

  Future<void> handlePreferencesChanged() async {
    serviceLocator<DishCatalogCubit>().saveFilters(DishFilterRequest(
      foodPreferenceIds: state.selectedPreferences,
    ));

    await _visitorRepository.updateUserPreferences(
      list: state.selectedPreferences,
    );
  }

  void resetPreferences() {
    emit(DishFilterState().copyWith(
      preferences: state.preferences,
    ));
  }
}
