part of 'cubit.dart';

class DishFilterState extends Equatable {
  final String? searchText;
  final String? dishTypeId;
  final Required<DishType> dishType;
  final List<DishType> dishTypes;
  final List<DishType> excludedDishTypes;
  final List<Preference> selectedPreferences;
  final List<Preference> preferences;
  final int priceFrom;
  final int priceTo;
  final bool? showNew;
  final bool? hasSale;
  final SortFieldEnum priceSortDirection;
  final bool? showPopular;

  DishFilterState({
    this.searchText = '',
    this.dishTypeId = '',
    this.dishType = const Required.pure(),
    this.excludedDishTypes = const [],
    this.selectedPreferences = const [],
    this.preferences = const [],
    this.dishTypes = const [],
    this.priceFrom = 5,
    this.priceTo = 30000,
    this.showNew = false,
    this.hasSale = false,
    this.priceSortDirection = SortFieldEnum.createdDate,
    this.showPopular = true,
  });

  @override
  List<Object?> get props => [
        searchText,
        dishTypeId,
        excludedDishTypes,
        selectedPreferences,
        preferences,
        dishType,
        dishTypes,
        priceFrom,
        priceTo,
        showNew,
        hasSale,
        priceSortDirection,
        showPopular,
      ];

  DishFilterState copyWith({
    String? searchText,
    String? dishTypeId,
    Required<DishType>? dishType,
    List<DishType>? excludedDishTypes,
    List<DishType>? dishTypes,
    List<Preference>? selectedPreferences,
    List<Preference>? preferences,
    int? priceFrom,
    int? priceTo,
    bool? showNew,
    bool? hasSale,
    SortFieldEnum? priceSortDirection,
    bool? showPopular,
  }) =>
      DishFilterState(
        searchText: searchText ?? this.searchText,
        dishTypeId: dishTypeId ?? this.dishTypeId,
        dishType: dishType ?? this.dishType,
        excludedDishTypes: excludedDishTypes ?? this.excludedDishTypes,
        dishTypes: dishTypes ?? this.dishTypes,
        selectedPreferences: selectedPreferences ?? this.selectedPreferences,
        preferences: preferences ?? this.preferences,
        priceFrom: priceFrom ?? this.priceFrom,
        priceTo: priceTo ?? this.priceTo,
        showNew: showNew ?? this.showNew,
        hasSale: hasSale ?? this.hasSale,
        priceSortDirection: priceSortDirection ?? this.priceSortDirection,
        showPopular: showPopular ?? this.showPopular,
      );
}
