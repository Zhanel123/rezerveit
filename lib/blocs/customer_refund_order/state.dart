part of 'cubit.dart';

class RefundOrderState {
  final Required<String> comment;
  final Required<int> grade;
  final Required<File> uploadedImage;
  final FormzStatus refundStatus;
  final String error;

  RefundOrderState({
    this.comment = const Required.pure(),
    this.grade = const Required.pure(),
    this.uploadedImage = const Required.pure(),
    this.refundStatus = FormzStatus.pure,
    this.error = '',
  });

  @override
  List<Object?> get props => [
        comment,
        grade,
        uploadedImage,
        refundStatus,
        error,
      ];


  RefundOrderState copyWith({
    Required<String>? comment,
    Required<int>? grade,
    Required<File>? uploadedImage,
    FormzStatus? refundStatus,
    String? error,
  }) =>
      RefundOrderState(
        comment: comment ?? this.comment,
        grade: grade ?? this.grade,
        error: error ?? this.error,
        uploadedImage: uploadedImage ?? this.uploadedImage,
        refundStatus: refundStatus ?? this.refundStatus,
      );
}
