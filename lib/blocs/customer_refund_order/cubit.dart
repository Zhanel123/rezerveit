import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:formz/formz.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/network/order_api/order_api.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:image/image.dart';
import 'package:injectable/injectable.dart';
import 'package:path_provider/path_provider.dart';

part 'state.dart';

@LazySingleton()
class RefundOrderCubit extends Cubit<RefundOrderState> {
  RefundOrderCubit() : super(RefundOrderState());

  Future<void> handleCustomerRefundOrder({required OrderModel order}) async {
    emit(state.copyWith(
      comment: Required.dirty(state.comment.value),
    ));

    emit(state.copyWith(
        refundStatus: Formz.validate(
      [
        state.comment,
        state.uploadedImage,
      ],
    )));

    if (!state.refundStatus.isValid) {
      return;
    }

    emit(state.copyWith(refundStatus: FormzStatus.submissionInProgress));

    try {
      await serviceLocator<OrderApi>().complaintOrder(
        orderId: order.id,
        comment: state.comment.value ?? '',
        base64Image:
            state.uploadedImage.value != null ? await _FileToBase64() : null,
      );

      emit(state.copyWith(refundStatus: FormzStatus.submissionSuccess));
    } on DioError catch (e) {
      emit(state.copyWith(
        refundStatus: FormzStatus.submissionFailure,
        error: e.response?.data['message'],
      ));
    }
    emit(state.copyWith(
      refundStatus: FormzStatus.pure,
      comment: Required.pure(),
      uploadedImage: Required.pure(),
    ));
  }

  _FileToBase64() async {
    var documentDirectory = await getApplicationDocumentsDirectory();
    Image? image = decodeImage(state.uploadedImage.value!.readAsBytesSync());

    // Resize the image to a 120x? thumbnail (maintaining the aspect ratio).
    Image thumbnail = copyResize(image!, width: 100);

    // Save the thumbnail as a PNG.
    final _file = File('${documentDirectory.path}/thumbnail-test.png')
      ..writeAsBytesSync(encodePng(thumbnail));

    List<int> imageBytes = _file.readAsBytesSync();
    String base64Data = base64Encode(imageBytes);

    return base64Data;
  }

  changeComment(String comment) {
    Required<String> _comment = Required.dirty(comment);
    emit(state.copyWith(
      comment: _comment,
    ));
  }

  handleImageSelected(File image, int? p2) async {
    emit(state.copyWith(
      uploadedImage: Required.dirty(image),
    ));
  }
}
