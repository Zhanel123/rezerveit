import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/data/repositories/favorite_repository/favorite_repository.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/favourite_dish/favourite_dish.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class FavouriteDishesCubit extends Cubit<FavouriteDishesState> {
  final FavoriteRepository _favoriteRepository;

  FavouriteDishesCubit(
    this._favoriteRepository,
  ) : super(FavouriteDishesState());

  addDishToFavorite(Dish value) async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));

    try {
      final response = await serviceLocator<RestClient>()
          .addDishToFavorite(dishCardId: value.id);
      fetchFavouriteDishes();
      emit(state.copyWith(status: FormzStatus.submissionSuccess));
    } catch (e) {
      emit(state.copyWith(status: FormzStatus.submissionFailure));
      logger.e(e);
    }
  }

  fetchFavouriteDishes({
    bool append = false,
  }) async {
    if ((append && !state.hasNext) || state.isLoading) {
      return;
    }
    if (append) {
      emit(state.copyWith(page: state.page + 1));
    } else {
      emit(state.copyWith(
        page: 0,
      ));
    }
    emit(state.copyWith(isLoading: true));

    try {
      final response = await _favoriteRepository.getFavouriteDishes(
        page: state.page,
        size: 10,
      );

      emit(state.copyWith(
        favDishes: response.content,
        hasNext: response.hasNext ?? false,
      ));

      if (response.hasNext ?? false) {
        fetchFavouriteDishes(append: true);
      }
    } catch (e) {}

    emit(state.copyWith(isLoading: false));
  }

  deleteAllFavDishes() async {
    emit(state.copyWith(
      deletionAllDishesStatus: FormzStatus.submissionInProgress,
    ));

    try {
      final response = await _favoriteRepository.deleteAllFavouriteDishes();

      emit(state.copyWith(
        deletionAllDishesStatus: FormzStatus.submissionSuccess,
      ));

      fetchFavouriteDishes();
    } catch (e) {
      logger.e(e);

      emit(state.copyWith(
        deletionAllDishesStatus: FormzStatus.submissionFailure,
      ));
    }

    emit(state.copyWith(
      deletionAllDishesStatus: FormzStatus.pure,
    ));
  }

  deleteFavDish({required Dish value}) async {
    emit(state.copyWith(deletionStatus: FormzStatus.submissionInProgress));

    try {
      for (int i = 0; i < state.favDishes.length; i++) {
        if (state.favDishes[i].dishCardItem?.id == value.id) {
          final response = await _favoriteRepository.deleteFavouriteDishById(
              favDishId: state.favDishes[i].id);
          fetchFavouriteDishes();

          emit(state.copyWith(deletionStatus: FormzStatus.submissionSuccess));
        }
      }
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(deletionStatus: FormzStatus.submissionFailure));
    }

    emit(state.copyWith(deletionStatus: FormzStatus.pure));
  }
}
