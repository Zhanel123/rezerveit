part of 'cubit.dart';

class FavouriteDishesState extends Equatable {
  final FormzStatus status;
  final List<FavouriteDish> favDishes;
  final int page;
  final bool hasNext;
  final bool isLoading;
  final FormzStatus deletionAllDishesStatus;
  final FormzStatus deletionStatus;

  FavouriteDishesState({
    this.status = FormzStatus.pure,
    this.favDishes = const [],
    this.hasNext = true,
    this.page = 0,
    this.isLoading = false,
    this.deletionAllDishesStatus = FormzStatus.pure,
    this.deletionStatus = FormzStatus.pure,
  });

  @override
  List<Object?> get props => [
        status,
        favDishes,
        page,
        hasNext,
        isLoading,
        deletionAllDishesStatus,
        deletionStatus,
      ];

  FavouriteDishesState copyWith({
    FormzStatus? status,
    List<FavouriteDish>? favDishes,
    int? page,
    bool? hasNext,
    bool? isLoading,
    FormzStatus? deletionAllDishesStatus,
    FormzStatus? deletionStatus,
  }) =>
      FavouriteDishesState(
        status: status ?? this.status,
        favDishes: favDishes ?? this.favDishes,
        page: page ?? this.page,
        hasNext: hasNext ?? this.hasNext,
        isLoading: isLoading ?? this.isLoading,
        deletionAllDishesStatus:
            deletionAllDishesStatus ?? this.deletionAllDishesStatus,
        deletionStatus: deletionStatus ?? this.deletionStatus,
      );
}
