import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../data/dto/dish_dto/mapper/dish_dto_mapper.dart';
import 'state.dart';

@LazySingleton()
class ViewedHistoryCubit extends Cubit<ViewedHistoryState> {
  ViewedHistoryCubit() : super(ViewedHistoryState()) {
    viewedHistoryLoadHistory();
  }

  Future<void> setViewedHistorySaveStorage() async {
    final storage = await SharedPreferences.getInstance();

    List<Map<String, dynamic>> data =
        dishListToDTO(state.viewedHistory).map((e) => e.toJson()).toList();

    storage.setString('viewdHistoryTemp', json.encode(data));
  }

  Future<void> viewedHistoryLoadHistory() async {
    final storage = await SharedPreferences.getInstance();

    List<DishDTO> _data = [];

    if (storage.getString('viewedHistoryTemp') != null) {
      _data = (json.decode(storage.getString('viewedHistoryTemp')!) as List)
          .map((e) => DishDTO.fromJson(e))
          .toList();

      emit(state.copyWith(
        viewedHistory: dishDtoListToModel(_data),
      ));
    }
  }

  addDish(Dish dish) {
    emit(
      state.copyWith(viewedHistory: [
        ...state.viewedHistory,
        dish,
      ]),
    );

    setViewedHistorySaveStorage();
  }
}
