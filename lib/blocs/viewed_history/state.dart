import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/data/dto/dish_dto/dish_dto.dart';
import 'package:home_food/models/dish/dish.dart';

class ViewedHistoryState extends Equatable {
  final FormzStatus viewdHistoryStatus;
  final List<Dish> viewedHistory;

  ViewedHistoryState({
    this.viewdHistoryStatus = FormzStatus.pure,
    this.viewedHistory = const [],
  });

  ViewedHistoryState copyWith({
    List<Dish>? viewedHistory,
  }) =>
      ViewedHistoryState(
        viewedHistory: viewedHistory ?? this.viewedHistory,
      );

  @override
  List<Object?> get props => [
        viewdHistoryStatus,
        viewedHistory,
      ];
}
