part of 'cubit.dart';

class MerchantCancelOrderState extends Equatable{
  final Required<String> comment;
  final Required<int> grade;
  final FormzStatus cancelStatus;
  final String error;

  MerchantCancelOrderState({
    this.comment = const Required.pure(),
    this.grade = const Required.pure(),
    this.cancelStatus = FormzStatus.pure,
    this.error = '',
  });

  @override
  List<Object?> get props => [
    comment,
    grade,
    cancelStatus,
    error,
  ];

  MerchantCancelOrderState copyWith({
    Required<String>? comment,
    Required<int>? grade,
    FormzStatus? cancelStatus,
    String? error,
  }) =>
      MerchantCancelOrderState(
        comment: comment ?? this.comment,
        grade: grade ?? this.grade,
        error: error ?? this.error,
        cancelStatus: cancelStatus ?? this.cancelStatus,
      );
}
