import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/network/order_api/order_api.dart';
import 'package:home_food/service_locator.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class MerchantCancelOrderCubit extends Cubit<MerchantCancelOrderState> {
  MerchantCancelOrderCubit() : super(MerchantCancelOrderState());

  Future<void> cancelOrder({required OrderModel order}) async {
    emit(state.copyWith(
      comment: Required.dirty(state.comment.value),
    ));

    emit(
      state.copyWith(
          cancelStatus: Formz.validate(
        [
          state.comment,
        ],
      )),
    );

    if (!state.cancelStatus.isValid) {
      return;
    }

    emit(state.copyWith(cancelStatus: FormzStatus.submissionInProgress));

    try {
      await serviceLocator<OrderApi>().cancelOrder(
        orderId: order.id,
        comment: state.comment.value ?? '',
        grade: state.grade.value ?? 0,
      );

      emit(state.copyWith(
        cancelStatus: FormzStatus.submissionSuccess,
      ));
    } on DioError catch (e) {
      emit(state.copyWith(
        cancelStatus: FormzStatus.submissionFailure,
        error: e.response?.data['message'],
      ));
    }
    emit(state.copyWith(
      cancelStatus: FormzStatus.pure,
      grade: Required.pure(),
      comment: Required.pure(),
    ));
  }

  changeComment(String comment) {
    Required<String> _comment = Required.dirty(comment);
    emit(state.copyWith(
      comment: _comment,
    ));
  }

  changeGrade(int grade) {
    Required<int> _grade = Required.dirty(grade);
    emit(state.copyWith(
      grade: _grade,
    ));
  }
}
