import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:home_food/data/repositories/dish_repository/dish_repository.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class PopularDishesCubit extends Cubit<PopularDishesState> {
  final DishRepository _dishRepository;

  PopularDishesCubit(
    this._dishRepository,
  ) : super(PopularDishesState());

  getPopularDishes({
    bool append = false,
  }) async {
    if ((append && !state.hasNext) || state.isLoading) {
      return;
    }
    if (append) {
      emit(state.copyWith(page: state.page + 1));
    } else {
      emit(state.copyWith(
        page: 0,
      ));
    }
    emit(state.copyWith(isLoading: true));

    try {
      final response = await _dishRepository.getPopularDishList(
        page: state.page,
        size: 10,
      );

      emit(state.copyWith(
        popularDishes: append
            ? [
                ...state.popularDishes,
                ...response.content,
              ]
            : response.content,
        hasNext: response.hasNext ?? false,
      ));

      if (response.hasNext ?? false) {
        getPopularDishes(append: true);
      }
    } catch (e) {
      logger.e(e);
    }

    emit(state.copyWith(isLoading: false));
  }
}
