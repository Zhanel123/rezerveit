part of 'cubit.dart';

class PopularDishesState extends Equatable{

  final List<Dish> popularDishes;
  final int page;
  final bool hasNext;
  final bool isLoading;

  PopularDishesState({
    this.popularDishes = const [],
    this.hasNext = true,
    this.page = 0,
    this.isLoading = false,
  });

  @override
  List<Object?> get props => [
    page,
    popularDishes,
    hasNext,
    isLoading,
  ];


  PopularDishesState copyWith({
    List<Dish>? popularDishes,
    int? page,
    bool? hasNext,
    bool? isLoading,
  }) =>
      PopularDishesState(
        page: page ?? this.page,
        popularDishes: popularDishes ?? this.popularDishes,
        hasNext: hasNext ?? this.hasNext,
        isLoading: isLoading ?? this.isLoading,
      );
}
