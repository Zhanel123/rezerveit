part of 'cubit.dart';

class OrderDetailState extends Equatable {
  final OrderModel? order;
  final FormzStatus status;

  const OrderDetailState({
    this.order,
    this.status = FormzStatus.pure,
  });

  @override
  List<Object?> get props => [
        order,
        status,
      ];

  OrderDetailState copyWith({
    OrderModel? order,
    FormzStatus? status,
  }) =>
      OrderDetailState(
        order: order ?? this.order,
        status: status ?? this.status,
      );
}
