import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/data/repositories/order_repository/order_repository.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class OrderDetailCubit extends Cubit<OrderDetailState> {
  OrderDetailCubit(
    this.orderRepository,
    this.merchantOrdersCubit,
  ) : super(OrderDetailState());

  final OrderRepository orderRepository;
  final MerchantOrdersCubit merchantOrdersCubit;
  Timer? timer;

  Future<void> handleOrderChanged(OrderModel order) async {
    await getOrderById(orderId: order.id);

    if (timer?.isActive ?? false) {
      timer?.cancel();
    }

    timer = Timer.periodic(Duration(seconds: 15), (timer) {
      getOrderById(orderId: order.id);
    });
  }

  /// Получить заказ по айди
  /// [orderId] айди заказа
  Future<void> getOrderById({
    String? orderId,
  }) async {
    emit(state.copyWith(
      status: FormzStatus.submissionInProgress,
    ));
    try {
      final response = await orderRepository.getOrderById(
        orderId: orderId ?? state.order!.id,
      );
      emit(state.copyWith(
        order: response,
        status: FormzStatus.submissionSuccess,
      ));
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(
        status: FormzStatus.submissionFailure,
      ));
    }
    emit(state.copyWith(
      status: FormzStatus.pure,
    ));
  }

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDERED]
  /// [orderId] айди заказа
  Future<void> handleApproveOrder(
    String orderId,
  ) async {
    try {
      await orderRepository.changeOrderStatusToAccepted(orderId: orderId);
      await merchantOrdersCubit.getMerchantOrders();
      await getOrderById(orderId: orderId);
    } catch (e) {
      logger.e(e);
    }
  }

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.ORDER_ACCEPTED]
  /// [orderId] айди заказа
  Future<void> handlePrepareOrder(
    String orderId,
  ) async {
    try {
      await orderRepository.changeOrderStatusToPreparation(orderId: orderId);
      await merchantOrdersCubit.getMerchantOrders();
      await getOrderById(orderId: orderId);
    } catch (e) {
      logger.e(e);
    }
  }

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.PREPARATION]
  /// [orderId] айди заказа
  Future<void> handleToDeliveryOrder(
    String orderId,
  ) async {
    try {
      await orderRepository.changeOrderStatusToDelivery(
        orderId: orderId,
      );
      await merchantOrdersCubit.getMerchantOrders();
      await getOrderById(orderId: orderId);
    } catch (e) {
      logger.e(e);
    }
  }

  /// Принять заказ со стороны мерчанта [UserType.MANUFACTURER]
  /// Статус заказа должен быть [OrderStatusEnum.DELIVERY]
  /// [orderId] айди заказа
  Future<void> handleDeliveredOrder(
    String orderId,
  ) async {
    try {
      await orderRepository.changeOrderStatusToDelivered(
        orderId: orderId,
      );
      await merchantOrdersCubit.getMerchantOrders();
      await getOrderById(orderId: orderId);
    } catch (e) {
      logger.e(e);
    }
  }
}
