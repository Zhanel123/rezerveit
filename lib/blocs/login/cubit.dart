import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/data/repositories/auth_repository/auth_repository.dart';
import 'package:home_food/models/register_activate/register_activate.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:home_food/utils/shared_pref_util.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class LoginCubit extends Cubit<LoginState> {
  final AuthRepository _authRepository;

  LoginCubit(
    this._authRepository,
  ) : super(LoginState());

  changePhone(String value) {
    PhoneValidator _phone = PhoneValidator.dirty(value);
    emit(state.copyWith(phoneNumber: _phone));
    validate();
  }

  login() async {
    emit(state.copyWith(
      phoneNumber: PhoneValidator.dirty(state.phoneNumber.value),
    ));

    validate();
    if (!state.loginStatus.isValid) {
      return;
    }
    emit(state.copyWith(loginStatus: FormzStatus.submissionInProgress));

    try {
      final response = await _authRepository.login(state.phoneNumber.value);

      emit(state.copyWith(
        smsCode:
            '${response.split(":").length > 1 ? response.split(":")[1] : response.split(":")[0]}',
        loginStatus: FormzStatus.submissionSuccess,
      ));
    } on DioError catch (error) {
      emit(state.copyWith(loginStatus: FormzStatus.submissionFailure));
      FlushbarService().showErrorMessage(
          message: jsonDecode(error.response?.data)['message']);
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(loginStatus: FormzStatus.submissionFailure));
    }

    emit(state.copyWith(
      loginFormStatus: FormzStatus.pure,
    ));
  }

  Future<void> handlePinCodeCompleted(String value) async {
    emit(state.copyWith(pinCodeStatus: FormzStatus.submissionInProgress));

    try {
      final response = await _authRepository.registerActivate(RegisterActivate(
        phoneNumber: state.phoneNumber.value,
        activationCode: value,
        isSubscribedToSms: true,
      ));

      //
      String credentials =
          "${state.phoneNumber.value}|${response.generatedPass}";
      Codec<String, String> stringToBase64 = utf8.fuse(base64);
      String encode = stringToBase64.encode(credentials);
      // сохраняем authPair
      sharedPreference.setAuthPair(encode);
      sharedPreference.setAccessToken('');

      emit(state.copyWith(pinCodeStatus: FormzStatus.submissionSuccess));
      emit(LoginState());
    } on DioError catch (e) {
      emit(state.copyWith(pinCodeStatus: FormzStatus.submissionFailure));
      FlushbarService()
          .showErrorMessage(message: jsonDecode(e.response?.data)['message']);
    } catch (e) {
      emit(state.copyWith(pinCodeStatus: FormzStatus.submissionFailure));
    }

    emit(state.copyWith(pinCodeStatus: FormzStatus.pure));
  }

  void validate() async {
    emit(
      state.copyWith(
        loginStatus: Formz.validate(
          [
            state.phoneNumber,
          ],
        ),
      ),
    );
  }
}
