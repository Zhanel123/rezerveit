part of 'cubit.dart';

class LoginState extends Equatable {
  final int? userId;
  final PhoneValidator phoneNumber;
  final String smsCode;
  final FormzStatus loginFormStatus;
  final FormzStatus loginStatus;
  final FormzStatus pinCodeStatus;

  LoginState({
    this.userId,
    this.phoneNumber = const PhoneValidator.pure(),
    this.smsCode = '',
    this.loginFormStatus = FormzStatus.pure,
    this.loginStatus = FormzStatus.pure,
    this.pinCodeStatus = FormzStatus.pure,
  });

  LoginState copyWith({
    int? userId,
    bool? isSalesman,
    PhoneValidator? phoneNumber,
    String? smsCode,
    FormzStatus? loginFormStatus,
    FormzStatus? loginStatus,
    FormzStatus? pinCodeStatus,
  }) =>
      LoginState(
        userId: userId ?? this.userId,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        smsCode: smsCode ?? this.smsCode,
        loginFormStatus: loginFormStatus ?? this.loginFormStatus,
        loginStatus: loginStatus ?? this.loginStatus,
        pinCodeStatus: pinCodeStatus ?? this.pinCodeStatus,
      );

  @override
  List<Object?> get props => [
        userId,
        phoneNumber,
        smsCode,
        loginFormStatus,
        loginStatus,
        pinCodeStatus,
      ];
}
