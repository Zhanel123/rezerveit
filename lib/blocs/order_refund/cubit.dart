import 'package:bloc/bloc.dart';

import 'state.dart';

class Order_refundCubit extends Cubit<Order_refundState> {
  Order_refundCubit() : super(Order_refundState().init());
}
