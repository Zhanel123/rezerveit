import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/data/repositories/chat_repository/chat_repository.dart';
import 'package:home_food/data/repositories/message_upload_image_repository/message_upload_image_repository.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/network/chat_api/chat_api.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/chat_screen/chat_screen.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

import 'state.dart';

@LazySingleton()
class ChatCubit extends Cubit<ChatState> {
  final ChatRepository _chatRepository;
  final ProfileCubit _profileCubit;
  final MessageUploadPhotosRepository _messageUploadPhotosRepository;
  late StompClient _stompClient;
  int retries = 0;

  ChatCubit(
    this._chatRepository,
    this._profileCubit,
    this._messageUploadPhotosRepository,
  ) : super(ChatState()) {
    _profileCubit.stream.listen((event) {
      if (event.profile != null) {
        fetchChatList();
        emit(state.copyWith(
          profile: event.profile,
        ));
      }
    });
    emit(state.copyWith(
      profile: _profileCubit.state.profile,
    ));
  }

  Timer? _searchTimer;

  Future<void> fetchChatList() async {
    final response = await _chatRepository.getChatList();

    emit(state.copyWith(
      chatList: response,
    ));
  }

  Future<void> fetchChatMessages(String? chatId) async {
    if (chatId == null) {
      return;
    }
    final response = await _chatRepository.getChatMessages(chatId: chatId);

    emit(state.copyWith(
      chatMessages: response,
    ));
  }

  void handleChatChanged(String chatId) {
    emit(state.copyWith(chatId: chatId));
    initWebSocketConnection();

    fetchChatMessages(chatId);
  }

  Future<void> createChat(OrderModel order) async {
    final response = await _chatRepository.getChatByOrderId(
        orderId: order.id, manufacturerId: order.manufacturerId);

    Routes.router.navigate(Routes.chatScreen, args: ChatScreenArgs(response));
  }

  void handleNewMessageTextChanged(String newValue) {
    emit(state.copyWith(newMessageText: newValue));
  }

  Future<void> sendMessage(
    String chatId,
  ) async {
    try {
      final response = await _chatRepository.sendMessage(
        chatId: chatId,
        text: state.newMessageText,
        photoItems: state.messageUploadedImages.value,
      );
      fetchChatMessages(chatId);

      emit(state.copyWith(
        newMessageText: '',
        messageUploadedImages: const ListNotEmpty.pure(),
      ));
    } catch (e) {
      logger.e(e);
    }
  }

  Future<void> initWebSocketConnection() async {
    try {
      _stompClient = StompClient(
        config: StompConfig(
          url: 'ws://5.188.67.135:5558/homefood/ws',
          onConnect: _onWebSocketCallback,
          onStompError: (error) {
            logger.e(error);
          },
          onWebSocketDone: () {
            logger.e('DONE');
          },
          onWebSocketError: (error) {
            logger.e(error);
          },
        ),
      );

      // _webSocketChannel..then((dynamic _) {
      //   logger.e('ABORTED');
      // });
      _stompClient.activate();
    } on Exception catch (e) {
      logger.e(e);
    }
  }

  void messageUploadImages(List<File> images) async {
    try {
      final response =
          await _messageUploadPhotosRepository.messageUploadPhotos(images);

      ListNotEmpty<UploadedImage> messageUploadedImages = ListNotEmpty.dirty([
        ...state.messageUploadedImages.value,
        ...response,
      ]);

      emit(state.copyWith(messageUploadedImages: messageUploadedImages));
    } catch (e) {
      logger.e(e);
    }
  }

  Future<void> messageUploadImage(
    File image,
    int? index,
  ) async {
    try {
      final response =
          await _messageUploadPhotosRepository.messageUploadPhotos([image]);

      List<UploadedImage> messageUploadImages = [];

      for (int i = 0; i < state.messageUploadedImages.value.length; i++) {
        if (index != null && i == index) {
          messageUploadImages.add(response.first);
        } else {
          messageUploadImages.add(state.messageUploadedImages.value[i]);
        }
      }
      if (index == null || state.messageUploadedImages.value.isEmpty) {
        messageUploadImages.add(response.first);
      }
      emit(state.copyWith(
        messageUploadedImages: ListNotEmpty.dirty(response),
      ));
    } catch (e) {
      logger.e(e);
    }
  }

  removeImage(int index) {
    List<UploadedImage> _images = [...state.messageUploadedImages.value];
    _images.removeAt(index);

    emit(state.copyWith(
      messageUploadedImages: ListNotEmpty.dirty(_images),
    ));
  }

  Future<void> sendImage(File file, String chatId) async {
    await messageUploadImage(file, null);
    sendMessage(chatId);
  }

  void _onWebSocketCallback(StompFrame p1) {
    _stompClient.subscribe(
      destination: '/home-food-ws-user/{chatId}/chat'.replaceFirst(
        '{chatId}',
        state.chatId,
      ),
      headers: {},
      callback: _onChatMessageCallback,
    );
    _stompClient.subscribe(
      destination: '/home-food-ws-user/{visitorId}/visitor'.replaceFirst(
        '{visitorId}',
        _profileCubit.state.profile!.id!,
      ),
      headers: {},
      callback: _onProfileMessagesCallback,
    );
  }

  void _onChatMessageCallback(StompFrame p1) {
    fetchChatMessages(state.chatId);
    logger.i(p1);
  }

  void _onProfileMessagesCallback(StompFrame p1) {
    fetchChatList();
    logger.i(p1);
  }

  handleSearchText(String text) async {
    emit(state.copyWith(
      searchText: text,
    ));

    if (state.searchText == '') {
      emit(state.resetSearch());
      return;
    }
    final response = await _chatRepository.searchByAllChat(
      text: state.searchText ?? '',
      isManufacturer: false,
    );

    emit(state.copyWith(
      searchResult: response,
    ));
  }

  Future<void> chatSupport() async {
    try {
      final response =
          (await serviceLocator<ChatApi>().chatSupport()).replaceAll('\"', '');
      Routes.router.navigate(Routes.chatScreen, args: ChatScreenArgs(response));
    } catch (e) {
      logger.e(e);
    }
  }
}
