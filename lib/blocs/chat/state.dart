import 'package:equatable/equatable.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/models/chat/chat.dart';
import 'package:home_food/models/chat/chat_message.dart';
import 'package:home_food/models/chat/chat_search_result_model.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/profile/profile.dart';

class ChatState extends Equatable {
  final List<ChatModel> chatList;
  final ChatSearchResultModel? searchResult;
  final String chatId;
  final Profile? profile;
  final String newMessageText;
  final List<ChatMessageModel> chatMessages;
  final ListNotEmpty<UploadedImage> messageUploadedImages;
  final String? searchText;

  ChatState({
    this.newMessageText = '',
    this.chatId = '',
    this.profile,
    this.searchText = '',
    this.searchResult,
    this.chatList = const [],
    this.chatMessages = const [],
    this.messageUploadedImages = const ListNotEmpty.pure(),
  });

  @override
  List<Object?> get props => [
        chatId,
        searchText,
        newMessageText,
        profile,
        chatList,
        chatMessages,
        searchResult,
        messageUploadedImages,
      ];

  ChatState copyWith({
    String? chatId,
    String? searchText,
    String? newMessageText,
    Profile? profile,
    ChatSearchResultModel? searchResult,
    List<ChatModel>? chatList,
    List<ChatMessageModel>? chatMessages,
    ListNotEmpty<UploadedImage>? messageUploadedImages,
  }) =>
      ChatState(
        chatId: chatId ?? this.chatId,
        searchText: searchText ?? this.searchText,
        profile: profile ?? this.profile,
        searchResult: searchResult ?? this.searchResult,
        newMessageText: newMessageText ?? this.newMessageText,
        chatList: chatList ?? this.chatList,
        chatMessages: chatMessages ?? this.chatMessages,
        messageUploadedImages:
            messageUploadedImages ?? this.messageUploadedImages,
      );

  ChatState resetSearch() =>
      ChatState(
        chatId: chatId,
        searchText: searchText,
        profile: profile,
        searchResult: null,
        newMessageText: newMessageText,
        chatList: chatList,
        chatMessages: chatMessages,
        messageUploadedImages:
            messageUploadedImages,
      );
}
