import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/dish_filter_request_dto/dish_filter_request_dto.dart';
import 'package:home_food/data/dto/dish_type_dto/mapper/dish_type_dto_mapper.dart';
import 'package:home_food/data/dto/preference_dto/mapper/preference_dto_mapper.dart';
import 'package:home_food/data/repositories/dish_repository/dish_repository.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

import '../../models/dish/dish.dart';
import '../../models/dish_list_filter_request/dish_list_filter_request.dart';

part 'state.dart';

@LazySingleton()
class DishCatalogCubit extends Cubit<DishCatalogState> {
  final DishRepository _dishRepository;
  final MyDishesCubit _myDishesCubit;
  final DictionaryCubit _dictionaryCubit;

  DishCatalogCubit(
    this._dishRepository,
    this._dictionaryCubit,
    this._myDishesCubit,
  ) : super(DishCatalogState()) {
    _streamSubscription = _dictionaryCubit.stream.listen((event) {
      emit(state.copyWith(
        dishTypes: event.dishTypes,
      ));
    });
    getDishes();
  }

  late StreamSubscription _streamSubscription;
  Timer? _searchTimer;

  Future<void> getCountOfFilteredDishes() async {
    emit(state.copyWith(isLoading: true));

    try {
      final response = await _dishRepository.getDishesCount(DishFilterRequest(
        searchText: state.searchText,
        dishTypeId: state.selectedDishType,
        notDishTypeIds: state.excludedDishTypes,
        foodPreferenceIds: state.selectedPreferences,
        priceFrom: state.priceFrom,
        priceTo: state.priceTo,
        showNew: state.showNew,
        hasSale: state.hasSale,
        sortBy: state.priceSortDirection,
        showPopular: state.showPopular,
      ));

      emit(state.copyWith(
        dishCount: response,
      ));
    } catch (e) {
      logger.e(e);

      emit(state.copyWith(isLoading: false));
    }
  }

  getDishes({
    bool append = false,
  }) async {
    if ((append && !state.hasNext) || state.isLoading) {
      return;
    }
    if (append) {
      emit(state.copyWith(page: state.page + 1));
    } else {
      emit(state.copyWith(
        page: 0,
      ));
    }
    emit(state.copyWith(isLoading: true));

    try {
      final response = await _dishRepository.getDishes(
        page: state.page,
        size: 10,
        filter: DishFilterRequestDTO(
          radius: state.radius,
          searchText: state.searchText,
          dishTypeId: state.selectedDishType,
          notDishTypeIds: dishTypeListToDTO(state.excludedDishTypes),
          foodPreferenceIds: preferenceDTOListMapper(state.selectedPreferences),
          priceFrom: state.priceFrom == 0 ? null : state.priceFrom,
          priceTo: state.priceTo == 0 ? null : state.priceTo,
          showNew: state.showNew,
          hasSale: state.hasSale,
          sortBy: state.priceSortDirection,
          showPopular: state.showPopular,
        ),
      );

      emit(state.copyWith(
        dishes: append
            ? [
                ...state.dishes,
                ...response.content,
              ]
            : response.content,
        hasNext: response.hasNext ?? false,
      ));

      emit(state.copyWith(isLoading: false));
      if (response.hasNext ?? false) {
        getDishes(append: true);
      }
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(isLoading: false));
    }
  }

  void handlePreferenceSelected(Preference value) {
    if (state.selectedPreferences.any((element) => element.id == value.id)) {
      return;
    }

    List<Preference> preferences = [
      ...state.selectedPreferences,
      value,
    ];
    emit(state.copyWith(selectedPreferences: preferences));
    getDishes();
  }

  void saveFilters(DishFilterRequest dishListFilterRequest) {
    emit(state.copyWith(
      searchText: dishListFilterRequest.searchText,
      selectedDishType: dishListFilterRequest.dishTypeId,
      excludedDishTypes: dishListFilterRequest.notDishTypeIds,
      selectedPreferences: dishListFilterRequest.foodPreferenceIds,
      priceFrom: dishListFilterRequest.priceFrom,
      priceTo: dishListFilterRequest.priceTo,
      showNew: dishListFilterRequest.showNew,
      hasSale: dishListFilterRequest.hasSale,
      priceSortDirection: dishListFilterRequest.sortBy,
      showPopular: dishListFilterRequest.showPopular,
    ));

    getDishes();
  }

  handleSearchChanged(String value) {
    emit(state.copyWith(
      searchText: value,
    ));
    _searchTimer = Timer(Duration(seconds: 2), getDishes);
  }

  handleDishTypeChanged(int index) {
    emit(state.copyWith(
      selectedDishType: index == 0 ? '' : state.dishTypes[index - 1].id,
    ));

    getDishes();
  }

  handleRadiusSelected(int index) {
    emit(state.copyWith(
      radius: index == 0 ? 1 : index,
    ));

    getDishes();
  }

  Future<void> handleDishModerate(Dish dish) async {
    emit(state.copyWith(
      moderationStatus: FormzStatus.submissionInProgress,
    ));
    try {
      await _dishRepository.changeStatusModerate(dishCardId: dish.id);
      emit(state.copyWith(
        moderationStatus: FormzStatus.submissionSuccess,
      ));
      _myDishesCubit.fetchMyDishes();
    } on DioError catch (e) {
      emit(state.copyWith(
        moderationStatus: FormzStatus.submissionFailure,
        error: jsonDecode(e.response?.data)['message'],
      ));
    }
    emit(state.copyWith(
      moderationStatus: FormzStatus.pure,
      error: '',
    ));
  }

  Future<void> handleDishActivate(Dish dish) async {
    emit(state.copyWith(
      activationStatus: FormzStatus.submissionInProgress,
    ));
    try {
      await _dishRepository.changeStatusActivate(dishCardId: dish.id);
      emit(state.copyWith(
        activationStatus: FormzStatus.submissionSuccess,
      ));
      _myDishesCubit.fetchMyDishes();
    } on DioError catch (e) {
      emit(state.copyWith(
        activationStatus: FormzStatus.submissionFailure,
        error: jsonDecode(e.response?.data)['message'],
      ));
    }
    emit(state.copyWith(
      activationStatus: FormzStatus.pure,
      error: '',
    ));
  }

  Future<void> handleDishDeactivate(Dish dish) async {
    emit(state.copyWith(
      deactivationStatus: FormzStatus.submissionInProgress,
    ));
    try {
      await _dishRepository.changeStatusDeactive(dishCardId: dish.id);
      emit(state.copyWith(
        deactivationStatus: FormzStatus.submissionSuccess,
      ));
      _myDishesCubit.fetchMyDishes();
    } on DioError catch (e) {
      emit(state.copyWith(
        deactivationStatus: FormzStatus.submissionFailure,
        error: jsonDecode(e.response?.data)['message'],
      ));
    }
    emit(state.copyWith(
      deactivationStatus: FormzStatus.pure,
      error: '',
    ));
  }
}
