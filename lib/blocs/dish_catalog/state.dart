part of 'cubit.dart';

class DishCatalogState extends Equatable {
  final List<Dish> dishes;
  final int page;
  final bool hasNext;
  final bool isLoading;
  final String? searchText;
  final String? selectedDishType;
  final List<DishType> dishTypes;
  final List<DishType> excludedDishTypes;
  final List<Preference> selectedPreferences;
  final double? priceFrom;
  final double? priceTo;
  final bool? showNew;
  final bool? hasSale;
  final SortFieldEnum priceSortDirection;
  final bool? showPopular;
  final int? dishCount;
  final int? radius;
  final List<int> radiusList;
  final double? latitude;
  final double? longitude;
  final FormzStatus activationStatus;
  final FormzStatus moderationStatus;
  final FormzStatus deactivationStatus;
  final String error;

  DishCatalogState({
    this.dishes = const [],
    this.dishTypes = const [],
    this.hasNext = true,
    this.page = 0,
    this.isLoading = false,
    this.searchText = '',
    this.selectedDishType = '',
    this.error = '',
    this.excludedDishTypes = const [],
    this.selectedPreferences = const [],
    this.priceFrom = 5,
    this.priceTo = 30000,
    this.showNew = false,
    this.hasSale = false,
    this.priceSortDirection = SortFieldEnum.createdDate,
    this.showPopular = true,
    this.dishCount = 0,
    this.radius = 0,
    this.radiusList = const [],
    this.latitude = 43.235835,
    this.longitude = 76.881514,
    this.activationStatus = FormzStatus.pure,
    this.moderationStatus = FormzStatus.pure,
    this.deactivationStatus = FormzStatus.pure,
  });

  @override
  List<Object?> get props => [
        page,
        dishes,
        dishTypes,
        hasNext,
        isLoading,
        searchText,
        selectedDishType,
        excludedDishTypes,
        selectedPreferences,
        priceFrom,
        priceTo,
        showNew,
        hasSale,
        priceSortDirection,
        showPopular,
        dishCount,
        radius,
        radiusList,
        latitude,
        longitude,
        activationStatus,
        moderationStatus,
        deactivationStatus,
        error,
      ];

  DishCatalogState copyWith({
    List<Dish>? dishes,
    int? page,
    bool? hasNext,
    FormzStatus? activationStatus,
    FormzStatus? moderationStatus,
    FormzStatus? deactivationStatus,
    bool? isLoading,
    String? searchText,
    String? selectedDishType,
    List<DishType>? dishTypes,
    List<DishType>? excludedDishTypes,
    List<Preference>? selectedPreferences,
    double? priceFrom,
    double? priceTo,
    bool? showNew,
    bool? hasSale,
    SortFieldEnum? priceSortDirection,
    bool? showPopular,
    int? dishCount,
    int? radius,
    List<int>? radiusList,
    double? latitude,
    double? longitude,
    String? error,
  }) =>
      DishCatalogState(
        page: page ?? this.page,
        dishes: dishes ?? this.dishes,
        activationStatus: activationStatus ?? this.activationStatus,
        moderationStatus: moderationStatus ?? this.moderationStatus,
        deactivationStatus: deactivationStatus ?? this.deactivationStatus,
        dishTypes: dishTypes ?? this.dishTypes,
        hasNext: hasNext ?? this.hasNext,
        isLoading: isLoading ?? this.isLoading,
        searchText: searchText ?? this.searchText,
        selectedDishType: selectedDishType ?? this.selectedDishType,
        excludedDishTypes: excludedDishTypes ?? this.excludedDishTypes,
        selectedPreferences: selectedPreferences ?? this.selectedPreferences,
        priceFrom: priceFrom ?? this.priceFrom,
        priceTo: priceTo ?? this.priceTo,
        showNew: showNew ?? this.showNew,
        hasSale: hasSale ?? this.hasSale,
        priceSortDirection: priceSortDirection ?? this.priceSortDirection,
        showPopular: showPopular ?? this.showPopular,
        dishCount: dishCount ?? this.dishCount,
        radius: radius ?? this.radius,
        radiusList: radiusList ?? this.radiusList,
        latitude: latitude ?? this.latitude,
        longitude: longitude ?? this.longitude,
        error: error ?? this.error,
      );
}
