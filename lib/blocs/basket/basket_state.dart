part of 'basket_cubit.dart';

class BasketState extends Equatable {
  final OrderModel order;
  final Required<String> paymentMethod;
  final Required<String> deliveryMethod;
  final Required<Address> selectedAddress;
  final List<Address> myAddressList;

  final FormzStatus submitOrderStatus;
  final FormzStatus clearBasketStatus;
  final FormzStatus merchantConflictStatus;
  final FormzStatus status;
  final bool silverAndNapkin;

  BasketState({
    this.submitOrderStatus = FormzStatus.pure,
    this.status = FormzStatus.pure,
    this.merchantConflictStatus = FormzStatus.pure,
    this.clearBasketStatus = FormzStatus.pure,
    this.paymentMethod = const Required.pure(),
    this.deliveryMethod = const Required.pure(),
    OrderModel? order,
    this.selectedAddress = const Required.pure(),
    this.myAddressList = const [],
    this.silverAndNapkin = false,
  }) : order = order ?? OrderModel();

  @override
  List<Object?> get props => [
        submitOrderStatus,
        status,
        merchantConflictStatus,
        silverAndNapkin,
        order,
        selectedAddress,
        myAddressList,
        paymentMethod,
        clearBasketStatus,
        deliveryMethod,
      ];

  BasketState copyWith({
    FormzStatus? status,
    FormzStatus? submitOrderStatus,
    FormzStatus? merchantConflictStatus,
    FormzStatus? clearBasketStatus,
    OrderModel? order,
    Required<Address>? selectedAddress,
    Required<String>? paymentMethod,
    Required<String>? deliveryMethod,
    List<Address>? myAddressList,
    bool? silverAndNapkin,
  }) =>
      BasketState(
        status: status ?? this.status,
        submitOrderStatus: submitOrderStatus ?? this.submitOrderStatus,
        silverAndNapkin: silverAndNapkin ?? this.silverAndNapkin,
        paymentMethod: paymentMethod ?? this.paymentMethod,
        merchantConflictStatus:
            merchantConflictStatus ?? this.merchantConflictStatus,
        clearBasketStatus: clearBasketStatus ?? this.clearBasketStatus,
        order: order ?? this.order,
        selectedAddress: selectedAddress ?? this.selectedAddress,
        myAddressList: myAddressList ?? this.myAddressList,
        deliveryMethod: deliveryMethod ?? this.deliveryMethod,
      );
}
