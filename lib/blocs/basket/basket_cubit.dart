import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/data/repositories/order_repository/order_repository.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/models/order_position/order_position.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'basket_state.dart';

@LazySingleton()
class BasketCubit extends Cubit<BasketState> {
  final ProfileCubit _profileCubit;
  final DictionaryCubit _dictionaryCubit;
  final OrderRepository _orderRepository;
  late StreamSubscription<ProfileState> _profileSubscription;

  BasketCubit(
    this._dictionaryCubit,
    this._profileCubit,
    this._orderRepository,
  ) : super(BasketState()) {
    _profileCubit.getMyAddresses();
    _profileSubscription = _profileCubit.stream.listen((event) {
      emit(
        state.copyWith(
          myAddressList: event.addressList,
        ),
      );
    });
    fetchPreOrder();
  }

  putDishToPreorder(Dish dish, int amount) async {
    emit(state.copyWith(
      status: FormzStatus.submissionInProgress,
    ));

    try {
      final response = await _orderRepository.putDishToPreorder(
        dishCardId: dish.id ?? '',
        amount: amount,
      );

      emit(state.copyWith(status: FormzStatus.submissionSuccess));
    } on DioError catch (e) {
      // if (e.message ==
      //     'Недопустимо добавление блюда от другого производителя') {
      emit(state.copyWith(
        merchantConflictStatus: FormzStatus.submissionSuccess,
      ));
      // }
    } catch (e) {
      emit(state.copyWith(
        status: FormzStatus.submissionFailure,
      ));

      logger.e(e, 'fff');
    }
    emit(state.copyWith(
      merchantConflictStatus: FormzStatus.pure,
      status: FormzStatus.pure,
    ));
    fetchPreOrder();
  }

  fetchPreOrder() async {
    if (_profileCubit.state.profile?.userType == UserType.MANUFACTURER) {
      return;
    }
    // emit(state.copyWith(status: FormzStatus.submissionInProgress));

    try {
      final response = await _orderRepository.getPreOrder();

      emit(state.copyWith(
        // status: FormzStatus.submissionSuccess,
        order: response.copyWith(deliveryMethod: "Доставкой"),
      ));
    } catch (e) {
      emit(state.copyWith(
        status: FormzStatus.submissionFailure,
        order: OrderModel(),
      ));

      logger.e(e);
    }
  }

  updateDishCardOrder({
    required OrderPosition orderPosition,
    required int amount,
  }) async {
    emit(state.copyWith(
        // status: FormzStatus.submissionInProgress,
        ));

    try {
      final response = await _orderRepository.updateDishCardOrder(
        positionId: orderPosition.id,
        amount: amount,
      );

      emit(state.copyWith(
          // status: FormzStatus.submissionSuccess,
          ));
    } catch (e) {
      emit(state.copyWith(
          // status: FormzStatus.submissionFailure,
          ));
      logger.e(e);
    }
  }

  increaseOrderAmount({
    required OrderPosition orderPosition,
    required int amount,
  }) async {
    int new_amount = amount + 1;

    updateDishCardOrder(
      orderPosition: orderPosition,
      amount: new_amount,
    );

    fetchPreOrder();
  }

  decreaseOrderAmount({
    required OrderPosition orderPosition,
    required int amount,
  }) async {
    int new_amount = amount - 1;

    if (new_amount > 0) {
      updateDishCardOrder(
        orderPosition: orderPosition,
        amount: new_amount,
      );
    } else {
      deleteDishFromBasket(
        orderPosition.id,
      );
    }

    fetchPreOrder();
  }

  updatePaymentMethodOrder({required String paymentMethod}) async {
    emit(
      state.copyWith(
        paymentMethod: Required.dirty(paymentMethod),
        order: state.order.copyWith(
          paymentMethod: paymentMethod,
        ),
      ),
    );

    await _orderRepository.updatePreOrder(
      body: state.order.copyWith(
        paymentMethod: paymentMethod,
      ),
    );
  }

  changeDeliveryMethod({required String deliveryMethod}) async {
    emit(state.copyWith(
        order: state.order.copyWith(deliveryMethod: deliveryMethod),
        deliveryMethod: Required.dirty(deliveryMethod)));

    await _orderRepository.updatePreOrder(
      body: state.order.copyWith(
        deliveryMethod: deliveryMethod,
      ),
    );
  }

  /// Удалить одно блюдо из корзины
  /// [positionId] позиция блюда в корзине
  Future<void> deleteDishFromBasket(
    String positionId,
  ) async {
    try {
      await _orderRepository.deleteDishFromBasket(
        positionId: positionId,
      );
    } catch (e) {
      logger.e(e);
    }
  }

  /// Удалить все блюдо из корзины
  deleteAllDishCard() async {
    emit(state.copyWith(clearBasketStatus: FormzStatus.submissionInProgress));
    try {
      await _orderRepository.removeAllDishCardOrder();
      emit(state.copyWith(clearBasketStatus: FormzStatus.submissionSuccess));
    } catch (e) {
      emit(state.copyWith(clearBasketStatus: FormzStatus.submissionFailure));
      logger.e(e);
    }

    emit(state.copyWith(clearBasketStatus: FormzStatus.pure));
    fetchPreOrder();
  }

  void onAddressChanged(Address value) async {
    emit(
      state.copyWith(
        selectedAddress: Required.dirty(value),
      ),
    );
    await _orderRepository.updatePreOrder(
        body: state.order.copyWith(
      address: value,
    ));

    fetchPreOrder();
  }

  Future<void> submitOrder() async {
    emit(state.copyWith(
      paymentMethod: Required.dirty(state.paymentMethod.value),
      selectedAddress: Required.dirty(state.selectedAddress.value),
      submitOrderStatus: Formz.validate([
        state.paymentMethod,
        if (state.deliveryMethod.value != 'Самовывоз') state.selectedAddress,
      ]),
    ));

    if (!state.submitOrderStatus.isValid) {
      return;
    }
    emit(state.copyWith(submitOrderStatus: FormzStatus.submissionInProgress));
    try {
      final response = await _orderRepository.createOrder();
      logger.w('');
      emit(state.copyWith(
        submitOrderStatus: FormzStatus.submissionSuccess,
        paymentMethod: Required.pure(),
        selectedAddress: Required.pure(),
        deliveryMethod: Required.pure(),
      ));
      // FlushbarService().showSuccessMessage();
    } on DioError catch (e) {
      logger.e(e);
      emit(state.copyWith(submitOrderStatus: FormzStatus.submissionFailure));
      FlushbarService().showErrorMessage(message: e.response?.data['message']);
    } catch (e) {}

    emit(state.copyWith(submitOrderStatus: FormzStatus.pure));

    fetchPreOrder();
  }

  handleSilverAndNapkinChanged(bool? value) async {
    emit(state.copyWith(
      silverAndNapkin: value,
    ));
    await _orderRepository.updatePreOrder(
        body: state.order.copyWith(
      silverAndNapkin: value,
    ));
  }
}
