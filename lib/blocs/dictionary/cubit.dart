import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:home_food/data/repositories/dictionary_repository/dictionary_repository.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish/ingredient.dart';
import 'package:home_food/models/dish/preference.dart';
import 'package:home_food/models/district/district.dart';
import 'package:home_food/models/tag/tag.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class DictionaryCubit extends Cubit<DictionaryState> {
  final DictionaryRepository _dictionaryRepository;

  DictionaryCubit(this._dictionaryRepository) : super(DictionaryState()) {
    getDishTypes();
    getTagsList();
    getPreferences();
    getCityList();
    fetchDishIngredients();
    getDistricts();
  }

  Future<void> getDishTypes() async {
    try {
      final response = await _dictionaryRepository.getDishTypes();
      emit(state.copyWith(
        dishTypes: response,
      ));
    } catch (e) {
      logger.e(e);
    }
  }

  Future<void> getDistricts() async {
    try {
      final response = await _dictionaryRepository.getDistrictList();
      emit(state.copyWith(
        districts: response,
      ));
    } catch (e) {
      logger.e(e);
    }
  }

  Future<void> getPreferences() async {
    try {
      final response = await _dictionaryRepository.getPreferences();
      emit(state.copyWith(preferences: response));
    } catch (e) {
      logger.e(e);
    }
  }

  Future<void> fetchDishIngredients() async {
    try {
      final response = await _dictionaryRepository.getIngredientList();
      emit(state.copyWith(ingredients: response));
    } catch (e) {
      logger.e(e);
    }
  }

  void getTagsList() async {
    final response = await serviceLocator<RestClient>().getTagsList();
    emit(state.copyWith(tagsList: response));
  }

  void getCityList() async {
    final response = await _dictionaryRepository.getCities();
    emit(state.copyWith(cityList: response));
  }
}
