part of 'cubit.dart';

class DictionaryState extends Equatable {
  final List<City> cityList;
  final List<Tag> tagsList;
  final List<DishType> dishTypes;
  final List<Preference> preferences;
  final List<Ingredient> ingredients;
  final List<District> districts;

  DictionaryState({
    this.cityList = const [],
    this.preferences = const [],
    this.tagsList = const [],
    this.dishTypes = const [],
    this.ingredients = const [],
    this.districts = const [],
  });

  @override
  List<Object?> get props => [
        cityList,
        tagsList,
        dishTypes,
        preferences,
        ingredients,
        districts,
      ];

  DictionaryState copyWith({
    List<City>? cityList,
    List<Tag>? tagsList,
    List<DishType>? dishTypes,
    List<Preference>? preferences,
    List<Ingredient>? ingredients,
    List<District>? districts,
  }) =>
      DictionaryState(
        cityList: cityList ?? this.cityList,
        tagsList: tagsList ?? this.tagsList,
        dishTypes: dishTypes ?? this.dishTypes,
        preferences: preferences ?? this.preferences,
        ingredients: ingredients ?? this.ingredients,
        districts: districts ?? this.districts,
      );
}
