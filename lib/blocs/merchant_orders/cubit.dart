import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/repositories/order_repository/order_repository.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/merchant_screens/merchant_orders/widgets/merchant_order_screen.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';
import 'package:seafarer/seafarer.dart';

part 'state.dart';

@LazySingleton()
class MerchantOrdersCubit extends Cubit<MerchantOrdersState> {
  final OrderRepository _orderRepository;
  final ProfileCubit _profileCubit;

  MerchantOrdersCubit(
    this._orderRepository,
    this._profileCubit,
  ) : super(MerchantOrdersState()) {
    _profileCubit.stream.listen((event) {
      if (event.profile != null) {
        getMerchantOrders();
        getOrderedStatusOrders();
      }
    });
    if (_profileCubit.state.profile != null) {
      getMerchantOrders();
      getOrderedStatusOrders();
    }
  }

  /// Получить список заказов мерчанта [UserType.MANUFACTURER]
  Future<void> getMerchantOrders({
    bool append = false,
  }) async {
    if ((append && !state.hasNext) || state.status.isSubmissionInProgress) {
      return;
    }
    if (append) {
      emit(state.copyWith(page: state.page + 1));
    } else {
      emit(state.copyWith(
        page: 0,
      ));
    }
    emit(state.copyWith(status: FormzStatus.submissionInProgress));

    try {
      final response = await _orderRepository.getMerchantOrders(
        page: state.page,
        status: state.statusFilter.key,
        searchText: state.searchQuery,
      );

      emit(state.copyWith(
        status: FormzStatus.submissionSuccess,
        orders: append
            ? [
                ...state.orders,
                ...response.content,
              ]
            : response.content,
        hasNext: response.hasNext ?? false,
      ));

      if (response.hasNext ?? false) {
        getMerchantOrders(append: true);
      }
    } catch (e) {
      emit(state.copyWith(status: FormzStatus.submissionFailure));

      logger.e(e);
    }
  }

  Future<void> getOrderedStatusOrders({
    bool append = false,
  }) async {
    if ((append && !state.hasNext) ||
        state.orderedStatus.isSubmissionInProgress) {
      return;
    }
    if (append) {
      emit(state.copyWith(orderedStatusPage: state.orderedStatusPage + 1));
    } else {
      emit(state.copyWith(
        orderedStatusPage: 0,
      ));
    }
    emit(state.copyWith(orderedStatus: FormzStatus.submissionInProgress));

    try {
      final response = await _orderRepository.getMerchantOrders(
        page: state.orderedStatusPage,
        status: OrderStatusEnum.ORDERED.key,
        searchText: state.searchQuery,
      );

      emit(state.copyWith(
        orderedStatus: FormzStatus.submissionSuccess,
        ordered_status_orders_list: append
            ? [
                ...state.orderedStatusOrdersList,
                ...response.content,
              ]
            : response.content,
        hasNext: response.hasNext ?? false,
      ));

      if (response.hasNext ?? false) {
        getOrderedStatusOrders(append: true);
      }
    } catch (e) {
      emit(state.copyWith(orderedStatus: FormzStatus.submissionFailure));

      logger.e(e);
    }
  }

  void handleStatusFilterChanged(int value) {
    emit(state.copyWith(
      statusFilter: OrderStatusEnum.values[value],
    ));

    getMerchantOrders();
  }

  handleSearchQueryChanged(String query) {
    emit(state.copyWith(
      searchQuery: query,
    ));
    getMerchantOrders();
  }
}
