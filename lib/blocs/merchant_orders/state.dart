part of 'cubit.dart';

class MerchantOrdersState extends Equatable {
  final FormzStatus status;
  final FormzStatus orderedStatus;
  final String searchQuery;
  final List<OrderModel> orders;
  final List<OrderModel> orderedStatusOrdersList;
  final OrderStatusEnum statusFilter;
  final int page;
  final int orderedStatusPage;
  final bool hasNext;

  MerchantOrdersState({
    this.status = FormzStatus.pure,
    this.orderedStatus = FormzStatus.pure,
    this.orders = const [],
    this.orderedStatusOrdersList = const [],
    this.hasNext = true,
    this.searchQuery = '',
    this.statusFilter = OrderStatusEnum.ALL,
    this.page = 0,
    this.orderedStatusPage = 0,
  });

  @override
  List<Object?> get props => [
        status,
        orderedStatus,
        orders,
        orderedStatusOrdersList,
        searchQuery,
        page,
        orderedStatusPage,
        hasNext,
        statusFilter,
      ];

  MerchantOrdersState copyWith({
    String? searchQuery,
    FormzStatus? status,
    FormzStatus? orderedStatus,
    List<OrderModel>? orders,
    List<OrderModel>? ordered_status_orders_list,
    int? page,
    int? orderedStatusPage,
    bool? hasNext,
    OrderStatusEnum? statusFilter,
  }) =>
      MerchantOrdersState(
        searchQuery: searchQuery ?? this.searchQuery,
        status: status ?? this.status,
        orderedStatus: orderedStatus ?? this.orderedStatus,
        statusFilter: statusFilter ?? this.statusFilter,
        orders: orders ?? this.orders,
        orderedStatusOrdersList:
            ordered_status_orders_list ?? this.orderedStatusOrdersList,
        page: page ?? this.page,
        orderedStatusPage: orderedStatusPage ?? this.orderedStatusPage,
        hasNext: hasNext ?? this.hasNext,
      );
}
