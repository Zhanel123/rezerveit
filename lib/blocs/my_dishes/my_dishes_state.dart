part of 'my_dishes_cubit.dart';

class MyDishesState extends Equatable {
  final List<Dish> myDishes;
  final DishFilterRequest filter;
  final List<DishType> dishTypes;
  final bool hasNext;
  final int page;
  final FormzStatus status;

  MyDishesState({
    DishFilterRequest? filter,
    this.myDishes = const [],
    this.dishTypes = const [],
    this.status = FormzStatus.pure,
    this.hasNext = false,
    this.page = 0,
  }) : filter = filter ?? DishFilterRequest();

  @override
  List<Object?> get props => [
        myDishes,
        dishTypes,
        filter,
        status,
        page,
        hasNext,
      ];

  MyDishesState copyWith({
    List<Dish>? myDishes,
    List<DishType>? dishTypes,
    DishFilterRequest? filter,
    bool? hasNext,
    int? page,
    FormzStatus? status,
  }) =>
      MyDishesState(
        myDishes: myDishes ?? this.myDishes,
        dishTypes: dishTypes ?? this.dishTypes,
        filter: filter ?? this.filter,
        hasNext: hasNext ?? this.hasNext,
        page: page ?? this.page,
        status: status ?? this.status,
      );
}
