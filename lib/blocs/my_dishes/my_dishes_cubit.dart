import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/dish_filter_request_dto/mapper/mapper.dart';
import 'package:home_food/data/repositories/dish_repository/dish_repository.dart';
import 'package:home_food/models/dish/dish.dart';
import 'package:home_food/models/dish/dish_type.dart';
import 'package:home_food/models/dish_list_filter_request/dish_list_filter_request.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'my_dishes_state.dart';

@LazySingleton()
class MyDishesCubit extends Cubit<MyDishesState> {
  final DishRepository _dishRepository;
  final DictionaryCubit _dictionaryCubit;
  final ProfileCubit _profileCubit;

  Timer? searchTimer;

  MyDishesCubit(
    this._dishRepository,
    this._dictionaryCubit,
    this._profileCubit,
  ) : super(MyDishesState()) {
    _dictionaryCubit.getDishTypes();
    _dictionaryCubit.stream.listen((event) {
      emit(
        state.copyWith(
          dishTypes: event.dishTypes,
        ),
      );
    });
    fetchMyDishes();
    _profileCubit.stream.listen((event) {
      if (event.profile != null) {
        fetchMyDishes();
      }
    });
  }

  Future<void> fetchMyDishes({
    bool append = false,
  }) async {
    if ((append && !state.hasNext) || state.status.isSubmissionInProgress) {
      return;
    }
    if (append) {
      emit(state.copyWith(page: state.page + 1));
    } else {
      emit(state.copyWith(
        page: 0,
      ));
    }
    emit(
      state.copyWith(
        status: FormzStatus.submissionInProgress,
      ),
    );
    try {
      final response = await _dishRepository.getMyDishes(
        filter: dishFilterRequestToDTO(state.filter),
        page: state.page,
      );

      emit(state.copyWith(
        myDishes: append
            ? [
                ...state.myDishes,
                ...response.content,
              ]
            : response.content,
        hasNext: response.hasNext ?? false,
      ));
    } catch (e) {
      logger.e(e);
      emit(
        state.copyWith(
          status: FormzStatus.submissionFailure,
        ),
      );
    }
    emit(
      state.copyWith(
        status: FormzStatus.pure,
      ),
    );
  }

  void handleSortByChanged(SortFieldEnum value) {
    emit(state.copyWith(
      filter: state.filter.copyWith(
        sortBy: value,
      ),
    ));
    fetchMyDishes();
  }

  void handleHalfPortionFilterChanged(bool value) {
    emit(state.copyWith(
      filter: state.filter.copyWith(
        hasHalfPortion: value,
      ),
    ));
    fetchMyDishes();
  }

  void handleDishTypeChanged(int value) {
    emit(state.copyWith(
      filter: state.filter.copyWithDishType(
        dishTypeId: value == 0 ? null : state.dishTypes[value - 1].id,
      ),
    ));
    fetchMyDishes();
  }

  void onSearchChanged(String query) {
    emit(state.copyWith(
      filter: state.filter.copyWith(
        searchText: query,
      ),
    ));

    if (searchTimer?.isActive ?? false) {
      searchTimer?.cancel();
    }

    searchTimer = Timer(
      Duration(seconds: 1),
      fetchMyDishes,
    );
  }
}
