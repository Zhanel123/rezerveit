part of 'cubit.dart';

class ProfileState extends Equatable {
  final Profile? profile;
  final StringRequired name;
  final PhoneValidator phone;
  final CityRequired city;
  final List<City> cities;
  final AddressRequired address;
  final List<Address> addressList;
  final List<Notification> notificationList;
  final BoolRequired isReceiveNotification;
  final StringRequired companyName;
  final StringRequired companyDescription;
  final BoolRequired multiplePoints;
  final BoolRequired preOrderIsAccess;
  final IntRequired preOrderMinDays;
  final IntRequired deliveryRadius;
  final List<String> areasList;
  final bool showActiveOrders;
  final bool showStatisticsPanel;
  final bool duplicateOrdersByEmail;
  final FormzStatus isLogging;
  final FormzStatus addressStatus;
  final FormzStatus updateProfileFormzStatus;
  final FormzStatus updateProfileStatus;

  ProfileState({
    this.name = const StringRequired.pure(),
    this.phone = const PhoneValidator.pure(),
    this.city = const CityRequired.pure(),
    this.address = const AddressRequired.pure(),
    this.addressList = const [],
    this.isReceiveNotification = const BoolRequired.pure(),
    this.companyName = const StringRequired.pure(),
    this.companyDescription = const StringRequired.pure(),
    this.multiplePoints = const BoolRequired.pure(),
    this.preOrderIsAccess = const BoolRequired.pure(),
    this.preOrderMinDays = const IntRequired.pure(),
    this.deliveryRadius = const IntRequired.pure(),
    this.areasList = const [],
    this.notificationList = const [],
    this.profile,
    this.isLogging = FormzStatus.pure,
    this.addressStatus = FormzStatus.pure,
    this.cities = const [],
    this.updateProfileFormzStatus = FormzStatus.pure,
    this.updateProfileStatus = FormzStatus.pure,
    this.showActiveOrders = false,
    this.showStatisticsPanel = false,
    this.duplicateOrdersByEmail = false,
  });

  @override
  List<Object?> get props => [
        profile,
        isLogging,
        name,
        phone,
        city,
        address,
        addressList,
        notificationList,
        addressStatus,
        isReceiveNotification,
        companyName,
        companyDescription,
        multiplePoints,
        preOrderIsAccess,
        preOrderMinDays,
        deliveryRadius,
        areasList,
        updateProfileFormzStatus,
        updateProfileStatus,
        showActiveOrders,
        showStatisticsPanel,
        duplicateOrdersByEmail,
      ];

  ProfileState copyWith({
    Profile? profile,
    FormzStatus? isLogging,
    StringRequired? name,
    PhoneValidator? phone,
    CityRequired? city,
    List<City>? cities,
    List<Notification>? notificationList,
    AddressRequired? address,
    List<Address>? addressList,
    BoolRequired? isReceiveNotification,
    StringRequired? companyName,
    StringRequired? companyDescription,
    BoolRequired? multiplePoints,
    BoolRequired? preOrderIsAccess,
    IntRequired? preOrderMinDays,
    IntRequired? deliveryRadius,
    List<String>? areasList,
    FormzStatus? updateProfileFormzStatus,
    FormzStatus? updateProfileStatus,
    FormzStatus? addressStatus,
    bool? showActiveOrders,
    bool? showStatisticsPanel,
    bool? duplicateOrdersByEmail,
  }) =>
      ProfileState(
        profile: profile ?? this.profile,
        isLogging: isLogging ?? this.isLogging,
        name: name ?? this.name,
        phone: phone ?? this.phone,
        city: city ?? this.city,
        cities: cities ?? this.cities,
        notificationList: notificationList ?? this.notificationList,
        address: address ?? this.address,
        addressList: addressList ?? this.addressList,
        isReceiveNotification:
            isReceiveNotification ?? this.isReceiveNotification,
        companyName: companyName ?? this.companyName,
        companyDescription: companyDescription ?? this.companyDescription,
        multiplePoints: multiplePoints ?? this.multiplePoints,
        preOrderIsAccess: preOrderIsAccess ?? this.preOrderIsAccess,
        preOrderMinDays: preOrderMinDays ?? this.preOrderMinDays,
        deliveryRadius: deliveryRadius ?? this.deliveryRadius,
        addressStatus: addressStatus ?? this.addressStatus,
        areasList: areasList ?? this.areasList,
        updateProfileFormzStatus:
            updateProfileFormzStatus ?? this.updateProfileFormzStatus,
        updateProfileStatus: updateProfileStatus ?? this.updateProfileStatus,
        showActiveOrders: showActiveOrders ?? this.showActiveOrders,
        showStatisticsPanel: showStatisticsPanel ?? this.showStatisticsPanel,
        duplicateOrdersByEmail:
            duplicateOrdersByEmail ?? this.duplicateOrdersByEmail,
      );
}
