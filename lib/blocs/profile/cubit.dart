import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/login/cubit.dart';
import 'package:home_food/blocs/registration/registration_cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/data/dto/city_dto/mapper/city_dto_mapper.dart';
import 'package:home_food/data/dto/profile_edit_dto/profile_edit_dto.dart';
import 'package:home_food/data/repositories/address_repository/address_repository.dart';
import 'package:home_food/data/repositories/profile_repository/profile_repository.dart';
import 'package:home_food/data/repositories/visitor_repository/visitor_repository.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/models/notification/notification.dart';
import 'package:home_food/models/profile/profile.dart';
import 'package:home_food/network/refresh_token_client.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/shared_pref_util.dart';
import 'package:injectable/injectable.dart';
import 'package:stomp_dart_client/stomp.dart';

import '../../utils/logger.dart';

part 'state.dart';

@LazySingleton()
class ProfileCubit extends Cubit<ProfileState> {
  final DictionaryCubit _dictionaryCubit;
  final ProfileRepository _profileRepository;
  final AddressRepository _addressRepository;
  final RegistrationCubit _registrationCubit;
  final VisitorRepository _visitorRepository;
  final LoginCubit _loginCubit;
  int retries = 0;

  ProfileCubit(
    this._dictionaryCubit,
    this._profileRepository,
    this._addressRepository,
    this._registrationCubit,
    this._loginCubit,
    this._visitorRepository,
  ) : super(ProfileState()) {
    _loginCubit.stream.listen((event) {
      if (event.pinCodeStatus.isSubmissionSuccess) {
        isAuthenticated();
      }
    });
    _registrationCubit.stream.listen((event) {
      if (event.pinCodeStatus.isSubmissionSuccess) {
        isAuthenticated();
      }
    });
    _dictionaryCubit.stream.listen((event) {
      emit(state.copyWith(
        cities: event.cityList,
      ));
    });
    _dictionaryCubit.getCityList();
    isAuthenticated();
  }

  void isAuthenticated() async {
    if (sharedPreference.authPair.isEmpty) {
      sharedPreference.logout();
      emit(state.copyWith(isLogging: FormzStatus.submissionFailure));
      emit(state.copyWith(isLogging: FormzStatus.pure));
      return;
    }
    if (sharedPreference.accessToken.isEmpty &&
        sharedPreference.authPair.isEmpty) {
      emit(state.copyWith(isLogging: FormzStatus.submissionFailure));
      emit(state.copyWith(isLogging: FormzStatus.pure));
      return;
    }

    if (sharedPreference.accessToken.isNotEmpty &&
        sharedPreference.authPair.isNotEmpty) {
      try {
        final response = await serviceLocator<RestClient>().getUserInfo();
        getMyAddresses();

        // final firebaseToken = await FirebaseMessaging.instance.getToken();

        // if (firebaseToken != null) {
        //   uploadFirebaseToken(firebaseToken);
        // }
        fetchNotifications();
        emit(state.copyWith(
          profile: response,
          showActiveOrders: response.showActiveOrders,
          showStatisticsPanel: response.showStatisticsPanel,
          duplicateOrdersByEmail: response.duplicateOrdersByEmail,
          name: StringRequired.dirty(response.firstName ?? ''),
          phone: PhoneValidator.dirty(response.phoneNumber ?? ''),
          isLogging: FormzStatus.submissionSuccess,
        ));

        if (sharedPreference.role.isNotEmpty) {
          changeLocalRole(
            userType: UserType.CUSTOMER.name == sharedPreference.role
                ? UserType.CUSTOMER
                : UserType.MANUFACTURER,
          );
        }
      } catch (e) {
        logger.e(e);
        emit(state.copyWith(
          isLogging: FormzStatus.submissionFailure,
        ));
        // getAccessToken();
      }
    }
    if (sharedPreference.accessToken.isEmpty) {
      getAccessToken();
    }
  }

  Future<void> fetchNotifications() async {
    try {
      final response = await _profileRepository.getRemoteNotifications();

      try {
        _profileRepository.setWasReaden(response);
      } catch (e) {
        logger.e(e);
      }

      emit(state.copyWith(
        notificationList: response,
      ));
    } catch (e) {
      logger.e(e);
    }
  }

  Future<void> getAccessToken() async {
    if (sharedPreference.authPair.isEmpty) {
      emit(state.copyWith(isLogging: FormzStatus.submissionFailure));
      return;
    }
    try {
      final response = await serviceLocator<RefreshTokenClient>().authorize(
        username: 'rest',
        password: '12345',
        token: 'Basic cmVzdDoxMjM0NQ==',
      );

      sharedPreference.setAccessToken(response.accessToken ?? '');
      isAuthenticated();
    } catch (e) {}
  }

  Future<void> logOut() async {
    emit(ProfileState());
    await sharedPreference.logout();
  }

  handleNameChanged(String value) {
    emit(state.copyWith(name: StringRequired.dirty(value)));
    validate();
  }

  handlePhoneChanged(String value) {
    PhoneValidator _phone = PhoneValidator.dirty(value);
    emit(state.copyWith(phone: _phone));
    validate();
  }

  handleIsReceiveNotification(bool value) {
    BoolRequired _isReceive = BoolRequired.dirty(value);
    emit(state.copyWith(isReceiveNotification: _isReceive));
  }

  changeCompany(String value) {
    StringRequired _companyName = StringRequired.dirty(value);
    emit(state.copyWith(companyName: _companyName));
    validate();
    print(_companyName);
  }

  handleCompanyDescriptionChanged(String value) {
    StringRequired _companyDescription = StringRequired.dirty(value);
    emit(state.copyWith(companyDescription: _companyDescription));
  }

  handleMultiplePoints(bool value) {
    BoolRequired _multiplePoints = BoolRequired.dirty(value);
    emit(state.copyWith(multiplePoints: _multiplePoints));
  }

  handleCityChanged(City? value) {
    CityRequired _city = CityRequired.dirty(value);
    emit(state.copyWith(city: _city));
    // validate();
  }

  handleChangeAddress(String? value) {
    Address a = Address(address: value ?? '', cityId: state.city.value?.id);
    AddressRequired _address = AddressRequired.dirty(a);
    emit(state.copyWith(address: _address));
  }

  getMyAddresses() async {
    emit(state.copyWith(
      addressStatus: FormzStatus.submissionInProgress,
    ));
    try {
      final response = await _addressRepository.getMyAddresses();

      logger.wtf(response.where((element) => element.title.isNotEmpty));
      emit(state.copyWith(
        addressList: [...response],
        addressStatus: FormzStatus.submissionSuccess,
      ));
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(
        addressStatus: FormzStatus.submissionFailure,
      ));
    }
    emit(state.copyWith(
      addressStatus: FormzStatus.pure,
      // address: AddressRequired.pure,
    ));
  }

  Future<void> uploadFirebaseToken(String firebaseToken) async {
    try {
      final response = await _visitorRepository.uploadFirebaseToken(
        firebaseToken: firebaseToken,
      );
    } catch (e) {
      logger.e(e);
    }
  }

  changeAddressList(String value, int index) {
    // StringRequired _address = StringRequired.dirty(value);
    // List<String> _temp = state.addressList;
    // _temp[index] = value;
    // emit(state.copyWith(addressList: _temp));
  }

  changePreOrderIsAccess(bool value) {
    BoolRequired _preOrderIsAccess = BoolRequired.dirty(value);
    emit(state.copyWith(preOrderIsAccess: _preOrderIsAccess));
  }

  changePreOrderMinDays(String value) {
    IntRequired _minDays = IntRequired.dirty(int.parse(value));
    emit(state.copyWith(preOrderMinDays: _minDays));
  }

  changeAreasList(String value) {
    // StringRequired _area = StringRequired.dirty(value);
    emit(state.copyWith(areasList: [value]));
  }

  changeDeliveryRadius(String value) {
    IntRequired _radius = IntRequired.dirty(int.parse(value));
    emit(state.copyWith(deliveryRadius: _radius));
  }

  void validate() {
    emit(state.copyWith(
      updateProfileFormzStatus: Formz.validate([
        state.name,
        state.phone,
        // state.city,
        // state.companyName,
        // state.companyDescription,
      ]),
    ));
  }

  void submissionValidate() {
    emit(state.copyWith(
      name: StringRequired.dirty(state.name.value),
      phone: PhoneValidator.dirty(state.phone.value),
      // city: CityRequired.dirty(state.city.value),
      // companyName: StringRequired.dirty(state.companyName.value),
      // companyDescription: StringRequired.dirty(state.companyDescription.value),
    ));

    validate();
  }

  void update() async {
    if (!state.updateProfileFormzStatus.isValid) {
      return;
    }
    emit(state.copyWith(updateProfileStatus: FormzStatus.submissionInProgress));
    try {
      final response = await _profileRepository.updateProfile(
        body: ProfileEditDTO(
          firstName: state.name.value,
          phoneNumber: state.phone.value,
          showActiveOrders: state.showActiveOrders,
          showStatisticsPanel: state.showStatisticsPanel,
          duplicateOrdersByEmail: state.duplicateOrdersByEmail,
        ),
      );

      emit(state.copyWith(
        profile: response,
        updateProfileStatus: FormzStatus.submissionSuccess,
      ));
    } catch (e) {
      emit(state.copyWith(updateProfileStatus: FormzStatus.submissionFailure));
    }
    emit(state.copyWith(updateProfileStatus: FormzStatus.pure));
  }

  changeCompanyDesc(String value) {
    StringRequired companyDescription = StringRequired.dirty(value);
    emit(state.copyWith(companyDescription: companyDescription));
    logger.d(companyDescription.value);
  }

  changeLocalRole({
    UserType userType = UserType.CUSTOMER,
  }) {
    emit(state.copyWith(
      profile: Profile.fromJson({
        ...?state.profile?.toJson(),
        'userType': userType.name,
      }),
    ));
  }

  void showActiveOrdersChanged(bool showActiveOrders) {
    emit(state.copyWith(
      showActiveOrders: showActiveOrders,
    ));
  }

  void showStatisticsPanelChanged(bool showStatisticsPanel) {
    emit(state.copyWith(
      showStatisticsPanel: showStatisticsPanel,
    ));
  }

  void duplicateOrdersByEmailChanged(bool duplicateOrdersByEmailChanged) {
    emit(state.copyWith(
      duplicateOrdersByEmail: duplicateOrdersByEmailChanged,
    ));
  }

  Future<void> updateProfileSettings() async {
    emit(state.copyWith(updateProfileStatus: FormzStatus.submissionInProgress));
    try {
      final response = await _profileRepository.updateProfile(
        body: ProfileEditDTO(
          showActiveOrders: state.showActiveOrders,
          showStatisticsPanel: state.showStatisticsPanel,
          duplicateOrdersByEmail: state.duplicateOrdersByEmail,
        ),
      );

      emit(state.copyWith(
        profile: response,
        updateProfileStatus: FormzStatus.submissionSuccess,
      ));
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(updateProfileStatus: FormzStatus.submissionFailure));
    }
    emit(state.copyWith(updateProfileStatus: FormzStatus.pure));
  }
}
