import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/data/dto/order_dto/mapper/order_mapper.dart';
import 'package:home_food/data/repositories/order_repository/order_repository.dart';
import 'package:home_food/models/order/order.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:injectable/injectable.dart';

part 'state.dart';

@LazySingleton()
class CustomerOrderCubit extends Cubit<CustomerOrderState> {
  final OrderRepository _orderRepository;
  final ProfileCubit _profileCubit;

  CustomerOrderCubit(
    this._orderRepository,
    this._profileCubit,
  ) : super(CustomerOrderState()) {
    _profileCubit.stream.listen((event) {
      if (event.profile != null) {
        getCustomerOrders();
      }
    });
    if (_profileCubit.state.profile != null) {
      getCustomerOrders();
    }
  }

  Future<void> getCustomerOrders({
    bool append = false,
  }) async {
    if ((append && !state.hasNext) || state.status.isSubmissionInProgress) {
      return;
    }
    if (append) {
      emit(state.copyWith(page: state.page + 1));
    } else {
      emit(state.copyWith(
        page: 0,
      ));
    }
    emit(state.copyWith(status: FormzStatus.submissionInProgress));

    try {
      final response = await _orderRepository.getCustomerOrders(
        page: state.page,
        status: state.statusFilter.key,
        searchText: state.searchQuery,
      );

      emit(state.copyWith(
        orders: append
            ? [
                ...state.orders,
                ...response.content,
              ]
            : response.content,
        status: FormzStatus.submissionSuccess,
        hasNext: response.hasNext ?? false,
      ));
    } catch (e) {
      emit(state.copyWith(status: FormzStatus.submissionFailure));

      logger.e(e);
    }
  }

  void handleStatusFilterChanged(int value) {
    emit(state.copyWith(
      statusFilter: CustomerOrderStatusEnum.values[value],
    ));

    getCustomerOrders();
  }

  handleSearchQueryChanged(String query) {
    emit(state.copyWith(
      searchQuery: query,
    ));
    getCustomerOrders();
  }
}
