part of 'cubit.dart';

class CustomerOrderState extends Equatable {
  final FormzStatus status;
  final List<OrderModel> orders;
  final String searchQuery;
  final CustomerOrderStatusEnum statusFilter;
  final int page;
  final bool hasNext;

  CustomerOrderState({
    this.status = FormzStatus.pure,
    this.orders = const [],
    this.searchQuery = '',
    this.hasNext = true,
    this.page = 0,
    this.statusFilter = CustomerOrderStatusEnum.ALL,
  });

  @override
  List<Object?> get props => [
        status,
        orders,
        page,
        hasNext,
        statusFilter,
        searchQuery,
      ];

  CustomerOrderState copyWith({
    String? searchQuery,
    FormzStatus? status,
    List<OrderModel>? orders,
    int? page,
    bool? hasNext,
    CustomerOrderStatusEnum? statusFilter,
  }) =>
      CustomerOrderState(
        status: status ?? this.status,
        orders: orders ?? this.orders,
        searchQuery: searchQuery ?? this.searchQuery,
        page: page ?? this.page,
        hasNext: hasNext ?? this.hasNext,
        statusFilter: statusFilter ?? this.statusFilter,
      );
}
