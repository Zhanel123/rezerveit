import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/core/enums.dart';
import 'package:home_food/core/input_validators.dart';
import 'package:home_food/data/repositories/auth_repository/auth_repository.dart';
import 'package:home_food/models/address/address.dart';
import 'package:home_food/models/city/city.dart';
import 'package:home_food/models/dish/uploaded_image.dart';
import 'package:home_food/models/district/district.dart';
import 'package:home_food/models/register_activate/register_activate.dart';
import 'package:home_food/models/register_user/register_user.dart';
import 'package:home_food/network/rest_client.dart';
import 'package:home_food/service/notify_service.dart';
import 'package:home_food/service_locator.dart';
import 'package:home_food/utils/logger.dart';
import 'package:home_food/utils/shared_pref_util.dart';
import 'package:injectable/injectable.dart';

part 'registration_state.dart';

@LazySingleton()
class RegistrationCubit extends Cubit<RegistrationState> {
  final DictionaryCubit _dictionaryCubit;
  final AuthRepository _authRepository;

  RegistrationCubit(
    this._dictionaryCubit,
    this._authRepository,
  ) : super(RegistrationState()) {
    _initializeDictionaryListener();
  }

  void _initializeDictionaryListener() {
    emit(state.copyWith(
      cities: _dictionaryCubit.state.cityList,
      districts: _dictionaryCubit.state.districts,
    ));
    _dictionaryCubit.stream.listen((event) {
      emit(state.copyWith(
        cities: event.cityList,
        districts: event.districts,
      ));
    });
  }

  void changeRole(bool value) {
    emit(state.copyWith(isSalesman: value));
  }

  changeName(String value) {
    StringRequired _name = StringRequired.dirty(value);
    emit(state.copyWith(name: _name));
  }

  changePhone(String value) {
    PhoneValidator _phone = PhoneValidator.dirty(value);
    emit(state.copyWith(phoneNumber: _phone));
    validate();
  }

  handlePrivacyPolicyAndTermsOfService(bool privacyValue) {
    BoolRequired _privacyValue = BoolRequired.dirty(privacyValue);
    emit(state.copyWith(privacyPolicy: _privacyValue));

    emit(state.copyWith(termsOfService: _privacyValue));
    validate();
  }

  changeCompany(String value) {
    StringRequired _companyName = StringRequired.dirty(value);
    emit(state.copyWith(companyName: _companyName));
    print(_companyName);
  }

  Future<void> imageChanged(File file) async {
    try {} catch (e) {
      logger.e(e);
    }
  }

  Future<void> uploadImage(File image) async {
    try {
      final response =
          await serviceLocator<RestClient>().visitorUploadPhotos(photo: image);

      Required<UploadedImage> uploadedImage = Required.dirty(response);

      emit(state.copyWith(uploadedImage: uploadedImage));
    } catch (e) {
      logger.e(e);
    }
  }

  register() async {
    emit(state.copyWith(
      name: StringRequired.dirty(state.name.value),
      phoneNumber: PhoneValidator.dirty(state.phoneNumber.value),
      // city: CityRequired.dirty(state.city.value),
      privacyPolicy: BoolRequired.dirty(state.privacyPolicy.value),
    ));
    if (state.isSalesman) {
      emit(state.copyWith(
        companyName: StringRequired.dirty(state.companyName.value),
        uploadedImage: Required.dirty(state.uploadedImage.value),
      ));
    }

    validate();
    if (!state.registrationStatus.isValid) {
      return;
    }
    emit(state.copyWith(registrationStatus: FormzStatus.submissionInProgress));
    try {
      final response = await _authRepository.register(
        UserRegisterRequest(
          userType:
              state.isSalesman ? UserType.MANUFACTURER : UserType.CUSTOMER,
          firstName: state.name.value,
          phoneNumber: state.phoneNumber.value,
          companyName: state.companyName.value,
          visitorPhoto: state.uploadedImage.value ?? UploadedImage(),
          address: state.city.value != null
              ? Address(
                  type: state.isSalesman
                      ? AddressType.MANUFACTURER_ADDRESS
                      : AddressType.CUSTOMER_ADDRESS,
                  districtId:
                      state.isSalesman ? state.district.value?.id : null,
                  cityId: state.city.value?.id,
                  city: state.city.value,
                )
              : null,
          termsOfService: state.termsOfService.value,
          privacyPolicy: state.privacyPolicy.value,
          // privacyPolicy:
        ),
      );
      emit(state.copyWith(
        smsCode:
            '${response.split(":").length > 1 ? response.split(":")[1] : response.split(":")[0]}',
        registrationStatus: FormzStatus.submissionSuccess,
      ));
    } on DioError catch (error) {
      emit(state.copyWith(registrationStatus: FormzStatus.submissionFailure));
      FlushbarService().showErrorMessage(
          message: jsonDecode(error.response?.data)['message']);
    } catch (e) {
      logger.e(e);
      emit(state.copyWith(registrationStatus: FormzStatus.submissionFailure));
    }

    emit(state.copyWith(
      registrationFormStatus: FormzStatus.pure,
    ));
  }

  Future<void> handlePinCodeCompleted(String value) async {
    emit(state.copyWith(pinCodeStatus: FormzStatus.submissionInProgress));
    try {
      final response = await _authRepository.registerActivate(RegisterActivate(
        phoneNumber: state.phoneNumber.value,
        activationCode: value,
        isSubscribedToSms: true,
      ));

      //
      String credentials =
          "${state.phoneNumber.value}|${response.generatedPass}";
      Codec<String, String> stringToBase64 = utf8.fuse(base64);
      String encode = stringToBase64.encode(credentials);
      // сохраняем authPair
      sharedPreference.setAuthPair(encode);
      sharedPreference.setRole(
        state.isSalesman ? UserType.MANUFACTURER.name : UserType.CUSTOMER.name,
      );

      emit(state.copyWith(pinCodeStatus: FormzStatus.submissionSuccess));
      emit(RegistrationState());
    } on DioError catch (e) {
      FlushbarService()
          .showErrorMessage(message: jsonDecode(e.response?.data)['message']);
      emit(state.copyWith(pinCodeStatus: FormzStatus.submissionFailure));
    }

    emit(state.copyWith(pinCodeStatus: FormzStatus.pure));
  }

  void handleCityChanged(City? value) {
    CityRequired _city = CityRequired.dirty(value);
    emit(state.copyWith(city: _city));
    validate();
  }

  void handleDistrictChanged(District? value) {
    Required<District> _district = Required.dirty(value);
    emit(state.copyWith(district: _district));
    validate();
  }

  void validate() async {
    emit(
      state.copyWith(
        registrationStatus: Formz.validate(
          [
            state.name,
            state.phoneNumber,
            // state.city,
            state.privacyPolicy,
            state.termsOfService,
            if (state.isSalesman) state.companyName,
            // if (state.isSalesman) state.district,
            if (state.isSalesman) state.uploadedImage,
          ],
        ),
      ),
    );
  }
}
