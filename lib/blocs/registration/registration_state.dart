part of 'registration_cubit.dart';

class RegistrationState extends Equatable {
  final int? userId;
  final bool isSalesman;
  final StringRequired name;
  final StringRequired companyName;
  final Required<UploadedImage> uploadedImage;
  final PhoneValidator phoneNumber;
  final String smsCode;
  final BoolRequired privacyPolicy;
  final BoolRequired termsOfService;
  final CityRequired city;
  final Required<District> district;
  final List<City> cities;
  final List<District> districts;
  final FormzStatus registrationFormStatus;
  final FormzStatus registrationStatus;
  final FormzStatus pinCodeStatus;

  RegistrationState({
    this.userId,
    this.isSalesman = false,
    this.cities = const [],
    this.districts = const [],
    this.name = const StringRequired.pure(),
    this.companyName = const StringRequired.pure(),
    this.uploadedImage = const Required.pure(),
    this.district = const Required.pure(),
    this.phoneNumber = const PhoneValidator.pure(),
    this.smsCode = '',
    this.privacyPolicy = const BoolRequired.pure(),
    this.termsOfService = const BoolRequired.pure(),
    this.city = const CityRequired.pure(),
    this.registrationFormStatus = FormzStatus.pure,
    this.registrationStatus = FormzStatus.pure,
    this.pinCodeStatus = FormzStatus.pure,
  });

  // в props - пишем по каким параметрам будем сравнивать
  @override
  List<Object?> get props => [
        userId,
        isSalesman,
        cities,
        districts,
        name,
        phoneNumber,
        companyName,
        uploadedImage,
        smsCode,
        privacyPolicy,
        termsOfService,
        registrationStatus,
        pinCodeStatus,
        city,
    district,
        registrationFormStatus,
      ];

  RegistrationState copyWith({
    int? userId,
    bool? isSalesman,
    List<City>? cities,
    List<District>? districts,
    StringRequired? name,
    StringRequired? companyName,
    Required<UploadedImage>? uploadedImage,
    Required<District>? district,
    PhoneValidator? phoneNumber,
    String? smsCode,
    BoolRequired? privacyPolicy,
    BoolRequired? termsOfService,
    CityRequired? city,
    FormzStatus? registrationStatus,
    FormzStatus? pinCodeStatus,
    FormzStatus? registrationFormStatus,
  }) =>
      RegistrationState(
        userId: userId ?? this.userId,
        isSalesman: isSalesman ?? this.isSalesman,
        name: name ?? this.name,
        cities: cities ?? this.cities,
        districts: districts ?? this.districts,
        companyName: companyName ?? this.companyName,
        uploadedImage: uploadedImage ?? this.uploadedImage,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        smsCode: smsCode ?? this.smsCode,
        privacyPolicy: privacyPolicy ?? this.privacyPolicy,
        termsOfService: termsOfService ?? this.termsOfService,
        registrationStatus: registrationStatus ?? this.registrationStatus,
        pinCodeStatus: pinCodeStatus ?? this.pinCodeStatus,
        registrationFormStatus:
            registrationFormStatus ?? this.registrationFormStatus,
        city: city ?? this.city,
        district: district ?? this.district,
      );
}
