import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:home_food/blocs/chat/cubit.dart';
import 'package:home_food/blocs/merchant/cubit.dart';
import 'package:home_food/core/env.dart';
import 'package:home_food/network/intercepters/log_interceptor.dart';
import 'package:home_food/network/intercepters/refresh_token_interceptor.dart';
import 'package:home_food/network/refresh_token_client.dart';
import 'package:home_food/service_locator.config.dart';
import 'package:injectable/injectable.dart';

GetIt serviceLocator = GetIt.instance;

@InjectableInit(
  asExtension: false,
)
Future<void> serviceLocatorSetup() async {
  init(serviceLocator);
  serviceLocator
    // ..registerFactory<RestClient>(() {
    //   final dio = Dio();
    //
    //   // dio.options.connectTimeout = 5000;
    //   // dio.options.receiveTimeout = 5000;
    //
    //   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
    //       (HttpClient client) {
    //     client.badCertificateCallback =
    //         (X509Certificate cert, String host, int port) => true;
    //     return client;
    //   };
    //
    //   dio.interceptors
    //     ..add(authInterceptors(dio))
    //     ..add(LogInterceptor(
    //         requestBody: true, responseBody: true, requestHeader: true));
    //
    //   return RestClient(dio, baseUrl: restURL);
    // })
    ..registerFactory<RefreshTokenClient>(() {
      final dio = Dio();

      dio.options.baseUrl = baseURL;
      dio.interceptors
        ..add(refreshTokenInterceptor(dio))
        ..add(LoggerInterceptor(
            requestBody: true, responseBody: true, requestHeader: true));

      return RefreshTokenClient(dio);
    });
}
