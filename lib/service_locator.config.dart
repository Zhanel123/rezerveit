// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i10;
import 'package:get_it/get_it.dart' as _i1;
import 'package:home_food/blocs/basket/basket_cubit.dart' as _i50;
import 'package:home_food/blocs/chat/cubit.dart' as _i51;
import 'package:home_food/blocs/create_food/cubit.dart' as _i48;
import 'package:home_food/blocs/customer_cancel_order/cubit.dart' as _i3;
import 'package:home_food/blocs/customer_orders/cubit.dart' as _i52;
import 'package:home_food/blocs/customer_refund_order/cubit.dart' as _i8;
import 'package:home_food/blocs/dictionary/cubit.dart' as _i43;
import 'package:home_food/blocs/dish_catalog/cubit.dart' as _i58;
import 'package:home_food/blocs/dish_edit/cubit.dart' as _i44;
import 'package:home_food/blocs/dish_filter/cubit.dart' as _i45;
import 'package:home_food/blocs/favorite_dishes/cubit.dart' as _i38;
import 'package:home_food/blocs/login/cubit.dart' as _i39;
import 'package:home_food/blocs/maps_cubit/maps_cubit_cubit.dart' as _i13;
import 'package:home_food/blocs/merchant/cubit.dart' as _i6;
import 'package:home_food/blocs/merchant_cancel_order/cubit.dart' as _i5;
import 'package:home_food/blocs/merchant_detail/cubit.dart' as _i46;
import 'package:home_food/blocs/merchant_orders/cubit.dart' as _i53;
import 'package:home_food/blocs/merchant_statistics/cubit.dart' as _i54;
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart' as _i55;
import 'package:home_food/blocs/new_address/cubit.dart' as _i56;
import 'package:home_food/blocs/order_detail/cubit.dart' as _i57;
import 'package:home_food/blocs/popular_dishes/cubit.dart' as _i41;
import 'package:home_food/blocs/profile/cubit.dart' as _i49;
import 'package:home_food/blocs/rate_cubit/cubit.dart' as _i7;
import 'package:home_food/blocs/registration/registration_cubit.dart' as _i47;
import 'package:home_food/blocs/viewed_history/cubit.dart' as _i9;
import 'package:home_food/data/repositories/address_repository/address_repository.dart'
    as _i32;
import 'package:home_food/data/repositories/auth_repository/auth_repository.dart'
    as _i33;
import 'package:home_food/data/repositories/chat_repository/chat_repository.dart'
    as _i34;
import 'package:home_food/data/repositories/dictionary_repository/dictionary_repository.dart'
    as _i35;
import 'package:home_food/data/repositories/dish_repository/dish_repository.dart'
    as _i36;
import 'package:home_food/data/repositories/favorite_repository/favorite_repository.dart'
    as _i37;
import 'package:home_food/data/repositories/message_upload_image_repository/message_upload_image_repository.dart'
    as _i40;
import 'package:home_food/data/repositories/order_repository/order_repository.dart'
    as _i28;
import 'package:home_food/data/repositories/profile_repository/profile_repository.dart'
    as _i42;
import 'package:home_food/data/repositories/upload_image_repository/upload_image_repository.dart'
    as _i30;
import 'package:home_food/data/repositories/visitor_repository/visitor_repository.dart'
    as _i31;
import 'package:home_food/data/services/address_service/address_service.dart'
    as _i20;
import 'package:home_food/data/services/auth_service/auth_service.dart' as _i21;
import 'package:home_food/data/services/chat_service/chat_service.dart' as _i23;
import 'package:home_food/data/services/dictionary_service/dictionary_service.dart'
    as _i24;
import 'package:home_food/data/services/dish_service/dish_service.dart' as _i25;
import 'package:home_food/data/services/favorite_service/favourite_service.dart'
    as _i26;
import 'package:home_food/data/services/message_upload_photos/message_upload_photos.dart'
    as _i27;
import 'package:home_food/data/services/order_service/order_service.dart'
    as _i15;
import 'package:home_food/data/services/profile_service/profile_service.dart'
    as _i29;
import 'package:home_food/data/services/upload_image_service/upload_image_service.dart'
    as _i17;
import 'package:home_food/data/services/visitor_service/visitor_service.dart'
    as _i19;
import 'package:home_food/network/chat_api/chat_api.dart' as _i22;
import 'package:home_food/network/dio/dio_module.dart' as _i11;
import 'package:home_food/network/dio/flavor_module.dart' as _i4;
import 'package:home_food/network/mapbox_api/mapbox_api.dart' as _i12;
import 'package:home_food/network/order_api/order_api.dart' as _i14;
import 'package:home_food/network/rest_client.dart' as _i16;
import 'package:home_food/network/visitor_api/visitor_api.dart' as _i18;
import 'package:injectable/injectable.dart' as _i2;

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt init(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final dioModule = _$DioModule();
  gh.lazySingleton<_i3.CustomerCancelOrderCubit>(
      () => _i3.CustomerCancelOrderCubit());
  gh.factory<_i4.Flavor>(() => _i4.ProdFlavor());
  gh.lazySingleton<_i5.MerchantCancelOrderCubit>(
      () => _i5.MerchantCancelOrderCubit());
  gh.lazySingleton<_i6.MerchantCubit>(() => _i6.MerchantCubit());
  gh.lazySingleton<_i7.RateOrderCubit>(() => _i7.RateOrderCubit());
  gh.lazySingleton<_i8.RefundOrderCubit>(() => _i8.RefundOrderCubit());
  gh.lazySingleton<_i9.ViewedHistoryCubit>(() => _i9.ViewedHistoryCubit());
  gh.lazySingleton<_i10.Dio>(() => dioModule.getDio(flavor: gh<_i4.Flavor>()));
  gh.singleton<_i11.DioService>(_i11.DioService(gh<_i10.Dio>()));
  gh.singleton<_i12.MapboxApi>(_i12.MapboxApi(gh<_i10.Dio>()));
  gh.lazySingleton<_i13.MapsCubitCubit>(
      () => _i13.MapsCubitCubit(gh<_i6.MerchantCubit>()));
  gh.singleton<_i14.OrderApi>(_i14.OrderApi(gh<_i10.Dio>()));
  gh.factory<_i15.OrderService>(
      () => _i15.OrderServiceImpl(gh<_i14.OrderApi>()));
  gh.singleton<_i16.RestClient>(_i16.RestClient(gh<_i10.Dio>()));
  gh.factory<_i17.UploadImageService>(
      () => _i17.UploadImageServiceImpl(gh<_i16.RestClient>()));
  gh.singleton<_i18.VisitorApi>(_i18.VisitorApi(gh<_i10.Dio>()));
  gh.factory<_i19.VisitorService>(
      () => _i19.VisitorServiceImpl(gh<_i18.VisitorApi>()));
  gh.factory<_i20.AddressService>(
      () => _i20.AddressServiceImpl(gh<_i16.RestClient>()));
  gh.factory<_i21.AuthService>(
      () => _i21.AuthServiceImpl(gh<_i16.RestClient>()));
  gh.singleton<_i22.ChatApi>(_i22.ChatApi(gh<_i10.Dio>()));
  gh.factory<_i23.ChatService>(() => _i23.ChatServiceImpl(gh<_i22.ChatApi>()));
  gh.factory<_i24.DictionaryService>(
      () => _i24.DictionaryServiceImpl(gh<_i16.RestClient>()));
  gh.factory<_i25.DishService>(
      () => _i25.DishServiceImpl(gh<_i16.RestClient>()));
  gh.factory<_i26.FavoriteService>(
      () => _i26.FavoriteServiceImpl(gh<_i16.RestClient>()));
  gh.factory<_i27.MessageUploadPhotosService>(
      () => _i27.MessageUploadPhotosServiceImpl(gh<_i22.ChatApi>()));
  gh.factory<_i28.OrderRepository>(
      () => _i28.OrderRepositoryImpl(gh<_i15.OrderService>()));
  gh.factory<_i29.ProfileService>(
      () => _i29.ProfileServiceImpl(gh<_i16.RestClient>()));
  gh.factory<_i30.UploadImageRepository>(
      () => _i30.UploadImageRepositoryImpl(gh<_i17.UploadImageService>()));
  gh.factory<_i31.VisitorRepository>(
      () => _i31.VisitorRepositoryImpl(gh<_i19.VisitorService>()));
  gh.factory<_i32.AddressRepository>(
      () => _i32.AddressRepositoryImpl(gh<_i20.AddressService>()));
  gh.factory<_i33.AuthRepository>(
      () => _i33.AuthRepositoryImpl(gh<_i21.AuthService>()));
  gh.factory<_i34.ChatRepository>(
      () => _i34.ChatRepositoryImpl(gh<_i23.ChatService>()));
  gh.factory<_i35.DictionaryRepository>(
      () => _i35.DictionaryRepositoryImpl(gh<_i24.DictionaryService>()));
  gh.factory<_i36.DishRepository>(
      () => _i36.DishRepositoryImpl(gh<_i25.DishService>()));
  gh.factory<_i37.FavoriteRepository>(
      () => _i37.FavoriteRepositoryImpl(gh<_i26.FavoriteService>()));
  gh.lazySingleton<_i38.FavouriteDishesCubit>(
      () => _i38.FavouriteDishesCubit(gh<_i37.FavoriteRepository>()));
  gh.lazySingleton<_i39.LoginCubit>(
      () => _i39.LoginCubit(gh<_i33.AuthRepository>()));
  gh.factory<_i40.MessageUploadPhotosRepository>(() =>
      _i40.MessageUploadPhotosRepositoryImpl(
          gh<_i27.MessageUploadPhotosService>()));
  gh.lazySingleton<_i41.PopularDishesCubit>(
      () => _i41.PopularDishesCubit(gh<_i36.DishRepository>()));
  gh.factory<_i42.ProfileRepository>(
      () => _i42.ProfileRepositoryImpl(gh<_i29.ProfileService>()));
  gh.lazySingleton<_i43.DictionaryCubit>(
      () => _i43.DictionaryCubit(gh<_i35.DictionaryRepository>()));
  gh.lazySingleton<_i44.DishEditCubit>(() => _i44.DishEditCubit(
        gh<_i43.DictionaryCubit>(),
        gh<_i35.DictionaryRepository>(),
        gh<_i36.DishRepository>(),
        gh<_i30.UploadImageRepository>(),
      ));
  gh.lazySingleton<_i45.DishFilterCubit>(() => _i45.DishFilterCubit(
        gh<_i43.DictionaryCubit>(),
        gh<_i31.VisitorRepository>(),
      ));
  gh.lazySingleton<_i46.MerchantDetailCubit>(() => _i46.MerchantDetailCubit(
        gh<_i36.DishRepository>(),
        gh<_i43.DictionaryCubit>(),
      ));
  gh.lazySingleton<_i47.RegistrationCubit>(() => _i47.RegistrationCubit(
        gh<_i43.DictionaryCubit>(),
        gh<_i33.AuthRepository>(),
      ));
  gh.lazySingleton<_i48.CreateFoodCubit>(() => _i48.CreateFoodCubit(
        gh<_i43.DictionaryCubit>(),
        gh<_i36.DishRepository>(),
        gh<_i30.UploadImageRepository>(),
      ));
  gh.lazySingleton<_i49.ProfileCubit>(() => _i49.ProfileCubit(
        gh<_i43.DictionaryCubit>(),
        gh<_i42.ProfileRepository>(),
        gh<_i32.AddressRepository>(),
        gh<_i47.RegistrationCubit>(),
        gh<_i39.LoginCubit>(),
        gh<_i31.VisitorRepository>(),
      ));
  gh.lazySingleton<_i50.BasketCubit>(() => _i50.BasketCubit(
        gh<_i43.DictionaryCubit>(),
        gh<_i49.ProfileCubit>(),
        gh<_i28.OrderRepository>(),
      ));
  gh.lazySingleton<_i51.ChatCubit>(() => _i51.ChatCubit(
        gh<_i34.ChatRepository>(),
        gh<_i49.ProfileCubit>(),
        gh<_i40.MessageUploadPhotosRepository>(),
      ));
  gh.lazySingleton<_i52.CustomerOrderCubit>(() => _i52.CustomerOrderCubit(
        gh<_i28.OrderRepository>(),
        gh<_i49.ProfileCubit>(),
      ));
  gh.lazySingleton<_i53.MerchantOrdersCubit>(() => _i53.MerchantOrdersCubit(
        gh<_i28.OrderRepository>(),
        gh<_i49.ProfileCubit>(),
      ));
  gh.lazySingleton<_i54.MerchantStatisticsCubit>(
      () => _i54.MerchantStatisticsCubit(
            gh<_i28.OrderRepository>(),
            gh<_i49.ProfileCubit>(),
          ));
  gh.lazySingleton<_i55.MyDishesCubit>(() => _i55.MyDishesCubit(
        gh<_i36.DishRepository>(),
        gh<_i43.DictionaryCubit>(),
        gh<_i49.ProfileCubit>(),
      ));
  gh.lazySingleton<_i56.NewAddressCubit>(() => _i56.NewAddressCubit(
        gh<_i35.DictionaryRepository>(),
        gh<_i32.AddressRepository>(),
        gh<_i49.ProfileCubit>(),
      ));
  gh.lazySingleton<_i57.OrderDetailCubit>(() => _i57.OrderDetailCubit(
        gh<_i28.OrderRepository>(),
        gh<_i53.MerchantOrdersCubit>(),
      ));
  gh.lazySingleton<_i58.DishCatalogCubit>(() => _i58.DishCatalogCubit(
        gh<_i36.DishRepository>(),
        gh<_i43.DictionaryCubit>(),
        gh<_i55.MyDishesCubit>(),
      ));
  return getIt;
}

class _$DioModule extends _i11.DioModule {}
