import 'package:carousel_slider/carousel_controller.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_food/blocs/basket/basket_cubit.dart';
import 'package:home_food/blocs/chat/cubit.dart';
import 'package:home_food/blocs/create_food/cubit.dart';
import 'package:home_food/blocs/customer_cancel_order/cubit.dart';
import 'package:home_food/blocs/customer_orders/cubit.dart';
import 'package:home_food/blocs/customer_refund_order/cubit.dart';
import 'package:home_food/blocs/dictionary/cubit.dart';
import 'package:home_food/blocs/dish_edit/cubit.dart';
import 'package:home_food/blocs/dish_filter/cubit.dart';
import 'package:home_food/blocs/favorite_dishes/cubit.dart';
import 'package:home_food/blocs/login/cubit.dart';
import 'package:home_food/blocs/maps_cubit/maps_cubit_cubit.dart';
import 'package:home_food/blocs/merchant_cancel_order/cubit.dart';
import 'package:home_food/blocs/merchant_detail/cubit.dart';
import 'package:home_food/blocs/merchant_orders/cubit.dart';
import 'package:home_food/blocs/merchant_statistics/cubit.dart';
import 'package:home_food/blocs/my_dishes/my_dishes_cubit.dart';
import 'package:home_food/blocs/new_address/cubit.dart';
import 'package:home_food/blocs/order_detail/cubit.dart';
import 'package:home_food/blocs/popular_dishes/cubit.dart';
import 'package:home_food/blocs/profile/cubit.dart';
import 'package:home_food/blocs/rate_cubit/cubit.dart';
import 'package:home_food/blocs/viewed_history/cubit.dart';
import 'package:home_food/routes.dart';
import 'package:home_food/screens/initial_screen.dart';
import 'package:home_food/service_locator.dart';

import 'blocs/dish_catalog/cubit.dart';
import 'blocs/merchant/cubit.dart';
import 'blocs/registration/registration_cubit.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => serviceLocator<RegistrationCubit>(),
        ),
        BlocProvider(
          create: (context) =>
              serviceLocator<ProfileCubit>()..isAuthenticated(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<CreateFoodCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<BasketCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<DishEditCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<ChatCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<DishCatalogCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<ProfileCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<NewAddressCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<MerchantCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<PopularDishesCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<DictionaryCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<DishFilterCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<MapsCubitCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<FavouriteDishesCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<CustomerOrderCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<ViewedHistoryCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<MerchantOrdersCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<MyDishesCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<RateOrderCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<MerchantDetailCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<OrderDetailCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<CustomerCancelOrderCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<MerchantCancelOrderCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<MerchantStatisticsCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<RefundOrderCubit>(),
        ),
        BlocProvider(
          create: (context) => serviceLocator<LoginCubit>(),
        ),
      ],
      child: MaterialApp(
        navigatorKey: Routes.router.navigatorKey,
        // important
        onGenerateRoute: Routes.router.generator(),
        // important
        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        home: InitialScreen(),
      ),
    );
  }
}
