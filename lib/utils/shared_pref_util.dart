import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String USER_ID = 'USER_ID';
const String ACCESS_TOKEN = 'ACCESS_TOKEN';
const String AUTH_PAIR = 'AUTH_PAIR';
const String ROLE = 'ROLE';
const String LOGIN = 'LOGIN';
const String PINCODE = 'PINCODE';
const String PSWD = 'PSWD';
const String SHOW_ONBOARDING = 'SHOW_ONBOARDING';

final sharedPreference = SharedPrefUtil();

class SharedPrefUtil {
  late SharedPreferences storage;

  init() async {
    storage = await SharedPreferences.getInstance();
    // setAccessToken('value');
  }

  LatLng getLatLngFromSharedPrefs() => LatLng(
      storage.getDouble('latitude') ?? 43,
      storage.getDouble('longitude') ?? 77);

  Future<bool> setAccessToken(String value) {
    return storage.setString(ACCESS_TOKEN, value);
  }

  String get accessToken {
    return storage.getString(ACCESS_TOKEN) ?? '';
  }

  Future<bool> setAuthPair(String value) {
    return storage.setString(AUTH_PAIR, value);
  }

  Future<void> reload() async {
    await storage.reload();
  }

  String get authPair {
    return storage.getString(AUTH_PAIR) ?? '';
  }

  Future<bool> setRole(String value) {
    return storage.setString(ROLE, value);
  }

  String get role {
    return storage.getString(ROLE) ?? '';
  }

  Future<bool> setLogin(String value) {
    return storage.setString(LOGIN, value);
  }

  String? get login {
    return storage.getString(LOGIN);
  }

  Future<bool> setPinCode(String value) {
    return storage.setString(PINCODE, value);
  }

  String? get pinCode {
    return storage.getString(PINCODE);
  }

  Future<bool> setPassword(String value) {
    return storage.setString(PSWD, value);
  }

  String? get pswd {
    return storage.getString(PSWD);
  }

  Future<bool> setUserId(int value) {
    return storage.setInt(USER_ID, value);
  }

  int? get userId {
    return storage.getInt(USER_ID);
  }

  Future<bool> removeAccessToken() {
    return storage.remove(ACCESS_TOKEN);
  }

  Future<bool> setShowOnBoarding(bool value) {
    return storage.setBool(SHOW_ONBOARDING, value);
  }

  bool get showOnBoarding {
    return storage.getBool(SHOW_ONBOARDING) ?? true;
  }

  Future<void> logout() async {
    await storage.remove(accessToken);
    setAccessToken('');
    setAuthPair('');
    await storage.remove(authPair);
  }
}
